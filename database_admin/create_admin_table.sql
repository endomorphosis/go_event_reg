CREATE TABLE IF NOT EXISTS `db_admin_information` (
  `id` int(11) NOT NULL auto_increment,
  `table_name` varchar(80) NOT NULL default '',
  `table_title` varchar(80) NOT NULL default '',
  `sort_variables` varchar(80) NOT NULL default '',
  `flash_variable` varchar(80) NOT NULL default '',
  `field_titles` text NOT NULL default '',
  `form_data` text NOT NULL default '',
  `field_labels` text NOT NULL default '',
  `default_fields` text NOT NULL default '',
  `limit_default` int(11) NOT NULL default '0',
  `unique_fields` text NOT NULL default '',
  `access_level` SMALLINT(2) NOT NULL default '0',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
        
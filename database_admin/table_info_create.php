<?php
include 'database_admin_config.php';

SetPost('TABLE NEWENTRY');

$ADMIN_INFO_TABLE_NAME = 'db_admin_information';

$SQL = "CREATE TABLE IF NOT EXISTS `$ADMIN_INFO_TABLE_NAME` (
  `id` int(11) NOT NULL auto_increment,
  `table_name` varchar(80) NOT NULL default '',
  `table_title` varchar(80) NOT NULL default '',
  `sort_variables` varchar(80) NOT NULL default '',
  `flash_variable` varchar(80) NOT NULL default '',
  `field_titles` text NOT NULL default '',
  `form_data` text NOT NULL default '',
  `field_labels` text NOT NULL default '',
  `default_fields` text NOT NULL default '',
  `limit_default` int(11) NOT NULL default '0',
  `unique_fields` text NOT NULL default '',
  `access_level` SMALLINT(2) NOT NULL default '0',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";


$FormDataArray = array(
    "hidden|table_name|$TABLE",
    "text|Table Title|table_title|Y|40|80",
    "text|Sort Variable(s)|sort_variables|Y|40|80",
    "text|Flash Variable|flash_variable|Y|40|80",
    "textarea|Field Titles|field_titles|Y|80|10",
    "textarea|Form Data|form_data|Y|80|10",
    "textarea|Field Labels|field_labels||80|10",
    "textarea|Default Search Fields|default_fields||80|3",
    "text|Display Limit Default(0=all)|limit_default||10|10",
    "text|Unique Fields|unique_fields||80|255",
    "selectcount|Access Level|access_level||0|9",
    "submit|Submit|TABLE_SETUP_SUBMIT"
);


$tables = db_GetTables();
$table_select = '<select name="TABLE" onchange="tableSelect(this.value);"><option value="">'.$StartSelect.'</option>';
foreach ($tables as $table) {
    $select = ($TABLE==$table)? ' selected="selected"' : '';
    if ($table != $ADMIN_INFO_TABLE_NAME) {
        $table_select .= qqn("<option value=`$table`$select>$table</option>");
    }
}
$table_select .= '</select>';
//$table_select .= '<input type="submit" value="Go" name="SUBMIT_TABLE" />';



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Database Table Information Setup</title>
  <style type="text/css">
    body{background-color:#eee; font-family:Arial,Sans Serif; margin:0px;}
    #header {padding:10px 1em; background-color:#888; border-bottom:1px solid #fff;}
    #content{padding:1em;}
    h1{color:#fff; font-size:1.1em; padding:0px; margin:0px}
    h1 span, h2 span {color:#f00;}
    h2{font-size: 1.1em;}
    td{padding:2px 10px; background-color: #fff;}
    th{padding:2px 10px; background-color:#888; color:#fff; font-weight:bold;}
    tr.odd td{background-color:#eef;}
    a.stdbutton {font-size:9pt; text-align:center; text-decoration:none; padding:0.2em 0.4em;
    background-color:#eee; color:#000; border:1px solid; border-color: #ddd #888 #777 #ccc;}
    a.stdbutton:active{border-color:#777 #ccc #ddd #888; }
    a.stdbutton:hover, a.orderbutton:active {background-color: #888; color:#fff; cursor:pointer;}
    #definition_table {margin-left:210px; margin-bottom: 2em; background-color:#999; font-size:0.9em; }
    .formitem {background-color:#fff; border:1px solid #000; }
    .formitemerror {background-color:#ff7; border:1px solid #000; }
    span.formrequired {color:#f00; font-weight:bold; padding-right:2px; }
    div.formtitle {clear:both; float:left; text-align:right; font-weight:bold; width:200px; padding:5px 0px; font-size:0.8em; }
    div.forminfo {margin-left:210px; padding:5px 0px;}
    input.formsubmit {color:#000; cursor:pointer; font-size:1em; }
    div.error { margin:10px auto; border:2px solid #f00; background-color:#f88; padding:0.5em; width:300px; text-align:center; }
    #tableselection {color:#fff; font-weight:bold; float:right; }
    #flash{position:absolute; top:100px; left:50%; margin-left:-210px; width:400px;  border:2px solid;  border-color:#ccc #666 #555 #bbb; background-color:#000; color:#fff; padding:10px; text-align:center; z-index:10000; }    
  </style>
  <script type="text/javascript" src="/lib/effects.js"></script>
  <script type="text/javascript">
        function tableSelect(table) {
            document.infotableform.submit();
        }
        window.onload = function() {
            setTimeout("closeCenter('flash')",4000);
        }
  </script>
  </head>
<body>
<form name="infotableform" method="post" action="table_info_create.php">
<div id="header">
<div id="tableselection">
Select Table: <?php echo $table_select; ?>
</div>
<h1>Database Table Information Setup: <span><?php echo $DB_INFO['NAME']; ?></span></h1>
</div>
<div id="content">
<?php
if($TABLE) {

    if (!db_TableExists($ADMIN_INFO_TABLE_NAME)) {
        printqn( "
        <div id=`sql`>
            <h3>Admin Information Table does not yet exist.  Use the following SQL.</h3>
            <pre>$SQL</pre>
        </div>");
    
    } else {
    
        $TableInfo = db_TableFieldInfo($TABLE);

    
        $error = '';
        $FLASH = '';
        $NEW = '';

        if (HaveSubmit('TABLE_SETUP_SUBMIT')) {
        
            $FormArray = ProcessForm($FormDataArray,$form_table,'','','',$error);
            if (!$error) {            
                if ($NEWENTRY) {
                        $fields = db_Keys($FormArray).',created';
                        $values = db_Values($FormArray).',NOW()';
                        if(db_AddRecord($ADMIN_INFO_TABLE_NAME,$fields,$values)){
                            $FLASH = "$TABLE Added";
                        } else {
                            $error .= 'DB Write Error';
                        }
                     
                } else {

                    $key_values = db_KeyValues($FormArray);
                    if (db_UpdateRecord($ADMIN_INFO_TABLE_NAME,$key_values,"`table_name`='$TABLE'")) {
                        $FLASH = "$TABLE Updated";
                    } else {
                        $error .= 'DB Write Error';
                    }
                }
            } 
            if ($FLASH) {
                echo '<div id="flash">'.$FLASH.'</div>';
            }
            WriteError($error);
        
        } else {           
            
            $TableRec = db_GetRecord($ADMIN_INFO_TABLE_NAME,'*',"table_name='$TABLE'");
            
            if($TableRec) {
                foreach($TableRec as $field=>$value) {
                    $_POST[$FormPrefix.$field] = $value;
                }
            } else {  
                // if not defined setup defaults
                echo '<input type="hidden" name="NEWENTRY" value="1" />';
                $NEW = ' (New)';
                
                
                $_POST[$FormPrefix.'table_title'] = NameToTitle($TABLE);
                $_POST[$FormPrefix.'sort_variables'] = $TableInfo[0]['Field'];
                $_POST[$FormPrefix.'flash_variable'] = $TableInfo[0]['Field'];
                
                $FieldTitlesArray = db_TableFieldTitleNames($TABLE);    
                $FieldTitles = '';
                foreach($FieldTitlesArray as $var=>$title) {
                    $FieldTitles .= "$var|$title\n";
                }
                $_POST[$FormPrefix.'field_titles'] = $FieldTitles;
                
                
                $TableFormData = '';
                $Default_Fields = '';
                foreach($TableInfo as $ROW) {
                    $kind   = $ROW['Kind'];
                    $size   = $ROW['Size'];
                    $field  = $ROW['Field'];
                    $extra  = $ROW['Extra'];
                    $title  = NameToTitle($field);
                    $default= $ROW['Default'];
                    
                    $Default_Fields .= "$field,";
                    
                    if (($extra != 'auto_increment') and ($default != 'CURRENT_TIMESTAMP') 
                       and ($field != 'created')) {
                        if ($kind=='text')  {
                            $TableFormData .= "textarea|$title|$field|Y|80|10\n";
                        } else {
                            $colsize = ($size<60)? $size : 60;
                            $TableFormData .= "text|$title|$field|Y|$colsize|$size\n";
                        }
                    }
                }
                $_POST[$FormPrefix.'form_data'] = $TableFormData;
                $_POST[$FormPrefix.'default_fields'] = substr($Default_Fields,0,-1);
                
                
                $_POST[$FormPrefix.'limit_default'] = 0;
                $_POST[$FormPrefix.'access_level'] = 0;
            }
        
        }
 
        echo "<div class=\"forminfo\"><h2>Table: <span>$TABLE$NEW</span></h2></div>";
        echo db_output_table($TableInfo,'','id="definition_table"');

        echo OutputForm($FormDataArray,'');
    }
}
writedbquery();
?>
</div>
</form>
</body>
</html>
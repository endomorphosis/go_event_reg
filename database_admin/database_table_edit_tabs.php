<?php

$TABLE_INFO = db_GetRecord($ADMIN_INFO_TABLE_NAME,'*',"table_name='$TABLE'");
/*
  `table_name` varchar(80) NOT NULL default '',
  `table_title` varchar(80) NOT NULL default '',
  `sort_variables` varchar(80) NOT NULL default '',
  `flash_variable` varchar(80) NOT NULL default '',
  `field_titles` text NOT NULL default '',
  `form_data` text NOT NULL default '',
  `field_labels` text NOT NULL default '',
  `default_fields` text NOT NULL default '',
  `limit_default` int(11) NOT NULL default '0',
  `unique_fields` text NOT NULL default '',
  `access_level` SMALLINT(2) NOT NULL default '0',
*/

//------------- Config Variables ---------------


$sort_default = $TABLE_INFO['sort_variables'];
//$admin_var   = 'adminvar';
$flash_field = $TABLE_INFO['flash_variable'];

$FieldTitlesList = explode("\n", $TABLE_INFO['field_titles']);
$FieldTitles = array();
foreach ($FieldTitlesList as $line) {
    list($key, $value) = explode('|',$line);
    $FieldTitles[$key] = $value;
}

$FormDataArray = explode("\n", $TABLE_INFO['form_data']);
//  "h2|Edit Record",
$FormDataArray[] = "submit|Save Changes|UPDATE_RECORD";

$FieldValues = array();
if ($TABLE_INFO['field_labels'] != '') {
    $Field_Labels = explode("\n", trim($TABLE_INFO['field_labels']));
    foreach ($Field_Labels as $line) {
        list($key,$values) = explode(':', $line);
        $valuepairs = explode('|',$values);
        $valuearray = array();
        foreach ($valuepairs as $var=>$label) $valuearray[$var] = $label;
        $FieldValues[$key] = $valuearray;
    }
}

$tab1content = '';
$tab2content = '';
$tab3content = '';

//---------keys for table-----------
$keys = array();
foreach ($FieldTitles  as $key => $value){
  if(!empty($_POST["field_$key"])) $keys[] = $_POST["field_$key"];
}
$havekeys = count($keys)>0;


if($CanEdit) {
$EditLinks = qq("
  <div style=`width:8.5em; text-align:center;`>
  <a class=`dbeditbutton` href=`#`
    onclick=`document.$form_name.action='$THIS_PAGE?view_record=@@ID@@'; document.$form_name.submit(); return false;`>View</a>&nbsp;<a
  class=`dbeditbutton` href=`#`
    onclick=`document.$form_name.action='$THIS_PAGE?edit_record=@@ID@@'; document.$form_name.submit(); return false;`>Edit</a>&nbsp;<a
  class=`dbeditbutton` href=`#`
    onclick=`if (confirm('Are you sure you want to delete this record (id=@@ID@@)?')){
     document.$form_name.action='$THIS_PAGE?delete_record=@@ID@@';
     document.$form_name.submit();
     }
     return false;`>Delete</a>
  </div>
");
} else {
$EditLinks = qq("
  <div style=`width:8.5em; text-align:center;`>
  <a class=`dbeditbutton` href=`#`
    onclick=`document.$form_name.action='$THIS_PAGE?view_record=@@ID@@'; document.$form_name.submit(); return false;`>View</a>
  </div>
");
}


$error ='';
//printqn("<input type=`hidden` name=`$admin_var` value=`1` />");

if(empty($add_record) and empty($edit_record)) $view_list=1;
//----------------------------add record--------------------------------
if(!empty($add_record)){
    $DISPLAYDBE=1;
    //$FormDataArray[0] = "h2|Add Database Record";
    if ($UPDATE_RECORD){
      $FormArray = ProcessForm($FormDataArray,$table,'','','',$error);
      $token = md5(uniqid(rand(), true));
      while (!db_IsUnique($TABLE,'unique_id',$token,'')) $token = md5(uniqid(rand(), true));
      if(!db_IsUnique($TABLE,'email',$FormArray['email'],'')) $error .= 'Email Already Exists<br />';
      if(!$error){
         $fields = db_Keys($FormArray).',unique_id,created';
         $values = db_Values($FormArray).",'$token',NOW()";
         if(db_AddRecord($TABLE,$fields,$values)){
           $FLASH = "Record: [{$FormArray[$flash_field]}] Added";
           $view_list=1;
         } else {$error .= 'DB Write Error';}
       }
    }
    $tab3content .= WriteErrorText($error);
    if ((!$UPDATE_RECORD) or ($error)) $tab3content .= OutputForm($FormDataArray,$UPDATE_RECORD);
}

//----------------------------edit record--------------------------------
if(!empty($edit_record)){
    $DISPLAYDBE=1;
    if ($UPDATE_RECORD){
      $FormArray = ProcessForm($FormDataArray,$table,'','','',$error);
      //if(!db_IsUnique($TABLE,'email',$FormArray['email'],"id!=$edit_record")) $error .= 'Email Already Exists<br />';
      if(!$error){
         $key_values = db_KeyValues($FormArray);
         if(db_UpdateRecord($TABLE,$key_values,"id=$edit_record")){
           $FLASH = "Admin record: [{$FormArray[$flash_field]}] Updated";
           $view_list=1;
         } else {$error .= 'DB Write Error';}
       }
    }
    $tab3content .= WriteErrorText($error);
    if ((!$UPDATE_RECORD) or ($error)){
      if (!$UPDATE_RECORD) PostVars($FieldTitles, db_GetRecord($TABLE,'*',"id=$edit_record"));
      $tab3content .=  OutputForm($FormDataArray,$UPDATE_RECORD);
    }
}

//----------------------------delete record--------------------------------
if(!empty($delete_record)){
   $DISPLAYDBE=1;
   if(!db_DeleteRecord($TABLE,"id=$delete_record")) WriteError("Delete of id=$delete_record Failed");
   else {
   $FLASH = "Record: [ID = $delete_record] Deleted";
   $view_list=1;
   }
}

//----------------------------view record--------------------------------
if(!empty($view_record)){
  $DISPLAYDBE=1;
  $tab3content .= qq("<p style=`text-align:center`><a class=`dbeditbutton` href=`#` onclick=`document.$form_name.action='$THIS_PAGE?edit_record=$view_record'; document.$form_name.submit(); return false;`>Edit</a></p>");
  $tab3content .= db_ViewRecord($TABLE,$FieldTitles,'align="center"','*',"id=$view_record");
  $view_list=0;
}



//========================================================TAB 1 TABLE SELECTION=======================================================
//----------------------------output list--------------------------------


//-----------------javascript selection buttons----------------

$checkcount = count($FieldTitles);

$default = '2,3,4';

$tab1content = <<<LBL1

<script type="text/javascript">
function ClearAll(){
    for(var i=1; i<= $checkcount; i++){
       var elem = getId('check'+i);
       if(elem) elem.checked = '';
       var elem = getId('td'+i);
       if(elem) elem.style.backgroundColor = '#fff';
     }
}
</script>

<p style="text-align:center;">
<a class="stdbutton" style="display:inline;" href="#"
  onclick="
     ClearAll();
     var df = Array($default);
     for(i in df){
       var elem = getId('check'+df[i]);
       if(elem) elem.checked = 'checked';
       var elem = getId('td'+df[i]);
       if(elem) elem.style.backgroundColor = '#ff0';
     }
     return false;
  ">Default</a>


<a class="stdbutton" style="display:inline;" href="#"
  onclick="
     for(var i=1; i<= $checkcount; i++){
       var elem = getId('check'+i);
       if(elem) elem.checked = 'checked';
       var elem = getId('td'+i);
       if(elem) elem.style.backgroundColor = '#ff0';
     }
     return false;
  ">Select All</a>
<a class="stdbutton" style="display:inline;" href="#"
  onclick="ClearAll(); return false;">Deselect All</a>
</p>
LBL1;

//-------------------------selection items----------------------------
  $DefaultArray = array('first_name','last_name','email');
  $count = 0;
  $item = 0;
  $keynames = '';
  $tab1content .= '<table align="center">';
  foreach ($FieldTitles as $key => $value){
    $count++; $item++;
    if($count == 6) $count = 1;
    if($count == 1) $tab1content .= '<tr>';
    if(!$havekeys){
      $postkey = (in_array($key,$DefaultArray))? $key : '';
    } else $postkey = empty($_POST["field_$key"])? '' : $_POST["field_$key"];
    $checked = ($postkey)? 'checked="checked"': '';
    $bgcolor = ($checked)? 'style="background-color:#ff0;"': '';
    if ($checked) {$keynames .= "$key, ";}
    $tab1content .= qq("<td id=`td$item` $bgcolor><input id=`check$item` type=`checkbox` value=`$key` name=`field_$key` $checked onclick=`getId('td$item').style.backgroundColor = (this.checked)? '#ff0':'#fff'` /> $value</td>");
    if($count == 5) $tab1content .= '</tr>';
  }
  $tab1content .= '</table>';

//-------------------------sort options----------------------------
$sortkey = (empty($_POST['SORTKEY']))? 'email' : $_POST['SORTKEY'];
$tab1content .= '<p style="text-align:center;"><b>Sort by:</b> <select name="SORTKEY">';
foreach($FieldTitles as $key => $value){
  $selected = ($sortkey == $key)? ' selected="selected"' : '';
  $tab1content .= qq("<option value=`$key`$selected>$value</optio
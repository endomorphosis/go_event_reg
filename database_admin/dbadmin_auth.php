<?PHP
// DB Admin Authentication

//===========================LOGOUT=======================
if($LOGOUT){
unset($_SESSION['DBAdminUsername'],$_SESSION['DBAdminPassword'],$_SESSION['DBOK'],$_SESSION['DBNAME']);

if (isset($_COOKIE[session_name()])) {
   setcookie(session_name(), '', time()-42000, '/');}

print <<<LOUT
$DOCTYPE_XHTML
<head>
  <title>$DB_ADMIN_TITLE</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  <link rel="stylesheet" type="text/css" href="dbadmin_style.css" />
</head>
<body class="admin" style="text-align:center;">
<div id="login">
<p id="loginheading">$DB_ADMIN_TITLE</p>
<h1>Good Bye!</h1>
<p>
  <input type="button" value="Log-in" onclick="window.location='$THIS_PAGE'" />
</p>
</div>
</body>
</html>
LOUT;
exit;
}

//=======================AUTHENTICATION=========================

$LOGIN_OK = '';
if ($LOGIN and $USER and $PASS) {
    $USER = strtolower($USER);
    if ((ereg($ADMIN_USER_PATTERN,$USER)) and (ereg($ADMIN_PASSWORD_PATTERN,$PASS))) {
        $ThisUserArray = db_GetRecord($ADMIN_USER_TABLE,'*',"username='$USER'");
        if($ThisUserArray['password'] === $PASS){
            $_SESSION['DBAdminUsername'] = strtolower($USER);
            $TableAccessStr = $ThisUserArray['table_access'];
            
            $SUPERUSER = $ThisUserArray['superuser'];
            $_SESSION['DBAdminSuperUser'] = $SUPERUSER;
            
            $lines = explode("\n", $TableAccessStr);
            $TABLE_ACCESS = array();
            foreach ($lines as $line) {
                if ($line == 'all') break;
                list($table,$access) = explode('|',$line);
                if ($SUPERUSER) $access = 9;
                $TABLE_ACCESS[$table] = $access;                
            }
            $_SESSION['DBAdminTableAccess'] = $TABLE_ACCESS;
            $_SESSION['DBAdminName'] = $ThisUserArray['name'];
            $_SESSION['DBOK'] = 'OK';

            //-------------------- update logfile---------------------

            $line=date('Y-m-d H:i:s').'|'.$ThisUserArray['name']."\n";
            append_file($ADMIN_LOGFILE,$line);
        }
    }
}

$DBUSER       = Session('DBAdminUsername');
$DBLEVEL      = Session('DBAdminLevel');
$DBNAME       = Session('DBAdminName');
$LOGIN_OK     = Session('DBOK');
$SUPERUSER    = Session('DBAdminSuperUser');
$TABLE_ACCESS = Session('DBAdminTableAccess');

if(empty($LOGIN_OK)){
print <<<AUTH
$DOCTYPE_XHTML
  <head>
    <title>$DB_ADMIN_TITLE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" type="text/css" href="dbadmin_style.css" />
    <script type="text/javascript">
/* <![CDATA[ */
      function getId(id){return document.getElementById(id);}
      var MyPassword = '<input type
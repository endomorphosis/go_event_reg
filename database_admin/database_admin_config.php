<?php
ini_set('display_errors','1');

$DB_INFO = array('HOST'=>'localhost','USER'=>'adm1n1strator','PASS'=>'','NAME'=>'intelserver'); 

$ADMIN_USER_TABLE       = 'db_admin_users';
$DB_ADMIN_TITLE         = 'Database Admin';
$ADMIN_USER_PATTERN     = '^[a-zA-Z0-9]+$';
$ADMIN_PASSWORD_PATTERN = '^[a-zA-Z0-9:\-\!\@\#\$\%\^\&\*]+$';
$ADMIN_LOGFILE          = 'admin_logfile.dat';
$ADMIN_INFO_TABLE_NAME  = 'db_admin_information';

include $_SERVER['DOCUMENT_ROOT'].'/lib/tools.php';
include "$LIB/db_helper.php";
include "$LIB/form_helper.php";

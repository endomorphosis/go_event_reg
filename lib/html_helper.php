<?php
// HTML creation routines

// ===================== FORM ROUTINES =====================
$HTML_noSelect = '-- select --';
$HTML_SubmitClickText = 'Processing. . .';

function HTML_Form($method, $action, $options='')
{
    $method = strtolower($method);
    return "<form method=\"$method\" action=\"$action\" $options>\n";
}

function HTML_EndForm()
{
    return "</form>\n";
}

function HTML_Submit($name,$value,$class)
{
    global $HTML_SubmitClickText;
    return "<input type=\"submit\" class=\"$class\" name=\"$name\" value=\"$value\" onclick=\"this.value='$HTML_SubmitClickText';\" />\n";
}

function HTML_Button($name,$value,$class,$onclick)
{
    return "<input type=\"button\" class=\"$class\" id=\"$name\" name=\"$name\" value=\"$value\" onclick=\"$onclick\" />\n";
}

function HTML_input_hidden($name,$value)
{
    return "<input type=\"hidden\" id=\"$name\" name=\"$name\" value=\"$value\" />\n";
}

function HTML_input_text($name, $value, $class, $options='')
{
    return "<input type=\"text\" name=\"$name\" value=\"$value\" class=\"$value\" id=\"$name\" $options />\n";
}


function HTML_AssocArrayToDropDown($ARRAY, $SELECTNAME, $CLASS, $VALUE, $USE_NO_SELECT=true, $OPTIONS='')
{
    global $HTML_noSelect;

    if($OPTIONS) $OPTIONS = " $OPTIONS";
    $RESULT = "<select id=\"$SELECTNAME\" class=\"$CLASS\" name=\"$SELECTNAME\"$OPTIONS>\n";
    if ($USE_NO_SELECT) $RESULT .= '<option value="">'.$HTML_noSelect."</option>\n";

	foreach ($ARRAY as $optionValue => $optionName)
	{
        $select = ($optionValue == $VALUE)? ' selected="selected"' : '';
		$RESULT .= "    <option value=\"$optionValue\"$select>$optionName</option>\n";
	}

    $RESULT .= "</select>\n";
    RETURN $RESULT;
}

function HTML_ArrayToDropDown($ARRAY, $SELECTNAME, $CLASS, $VALUE, $USE_NO_SELECT=true, $OPTIONS='')
{
    global $HTML_noSelect;

    if($OPTIONS) $OPTIONS = " $OPTIONS";
    $RESULT = "<select id=\"$SELECTNAME\" class=\"$CLASS\" name=\"$SELECTNAME\"$OPTIONS>\n";
    if ($USE_NO_SELECT) $RESULT .= '<option value="">'.$HTML_noSelect."</option>\n";

	foreach ($ARRAY as $optionValue)
	{
        $select = ($optionValue == $VALUE)? ' selected="selected"' : '';
		$RESULT .= "    <option value=\"$optionValue\"$select>$optionValue</option>\n";
	}

    $RESULT .= "</select>\n";
    RETURN $RESULT;
}


// ===================== TABLES =====================
function HTML_TableRow($Array, $cell='td')
{
    if (count($Array) > 0 ) {
        $RESULT = '<tr>';
        foreach ($Array as $field) {
            $RESULT .= "<$cell>$field</$cell>";
        }
        return $RESULT . "</tr>\n";
    } else {
        return '';
    }
}

function HTML_TableRowId($Array, $row, $name, $cell='td')
{
    if (count($Array) > 0 ) {
        $RESULT = "<tr id=\"$name
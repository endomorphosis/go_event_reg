<?php
// ============PHP Routines=================
// ============REVISIONS============
/*
   20080615: added mcrypt and hexencode functions
   20080421: added stripos (for version 4), GetDirCount, directory now has both include and exclude options parameters
   20070616: added SendHTMLmail
   20070601: added Get, Post
   20070325: added GetFolders
   20070307 : added PostGetStr
   20070225 : omit svn from directory search
   20070218 : directory sorted in natural case-insensitive order
   20071010 : added NameToTitle
   20061227 : added write_file
   20061222 : added strFrom, strTo
   20061122 : added qq, printq, printqn, printn
   20061121 : added GetDirectory
   20061024 : added server variables
   20071004 : updated FromThisDomain
   20080701 : added astr_replace
   20080918 : added readln and filelinecount
*/

$DOCUMENT_ROOT   = Server('DOCUMENT_ROOT');
$ROOT            = $DOCUMENT_ROOT;
$SCRIPT_FILENAME = Server('SCRIPT_FILENAME');
$QUERY_STRING    = Server('QUERY_STRING');
$HTTP_HOST       = Server('HTTP_HOST');
$REQUEST_URI     = Server('REQUEST_URI');
$THIS_PAGE_QUERY = $REQUEST_URI;
$THIS_PAGE       = strTo($THIS_PAGE_QUERY,'?');
$HTTPS           = Server('HTTPS');
$SCRIPT_URI      = empty($HTTPS)? 'http://'.$HTTP_HOST.$REQUEST_URI : 'https://'.$HTTP_HOST.$REQUEST_URI;
$HTTPS_URI       = 'https://'.$HTTP_HOST.$REQUEST_URI;
$HTTP_USER_AGENT = Server('HTTP_USER_AGENT');
$REFERER         = Server('HTTP_REFERER');
$PHP_SELF        = Server('PHP_SELF');

if (strpos($HTTP_USER_AGENT, 'MSIE') !== FALSE) $BROWSER = 'IE';
elseif (strpos($HTTP_USER_AGENT, 'Firefox') !== FALSE) $BROWSER = 'FF';
elseif (strpos($HTTP_USER_AGENT, 'Safari') !== FALSE) $BROWSER = 'SF';
else $BROWSER = 'NotIE';

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('America/Los_Angeles');
}

//$LIB = $ROOT.'/'.basename(dirname(__FILE__));  // for consistent results in CGI mode
$LIB = dirname(__FILE__);
$lib = $LIB;


$MailCR = "\n";

$SV = (strpos(get_cfg_var('arg_separator.input'),';')!==false)? ';' : '&amp;';

function PathFromRoot($path)
{
    global $ROOT;
    return substr($path, strlen($ROOT));
}

function RootPath($path)
{
    global $ROOT;
    if (strpos($path,$ROOT)===false) return "$ROOT$path";
    else return $path;
}


$DOCTYPE_XHTML = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">';

// ============CLEAR ANY MAGIC QUOTES============

if (get_magic_quotes_gpc()) {
    $_GET = array_map('stripslashes', $_GET);
    $_POST = array_map('stripslashes', $_POST);
    //$_COOKIE = array_map('stripslashes', $_COOKIE);
    //$_REQUEST = array_map('stripslashes', $_REQUEST);
}

//-----PRINT DOUBLE QUOTE------

function qq($text) {return str_replace('`','"',$text);}
function qqn($text) {return str_replace('`','"',$text)."\n";}
function printq($text) { print qq($text);}
function printqn($text) { print qq($text)."\n";}
function printn($text) { print $text."\n";}

//-----POST-GET------

function TransformContent($var,$list)
{
    //List: S=strip tags, T=trim, Q=remove quotes, H=htmlentities, A=addslashes
    if (empty($list)) return $var;
    $list = strtoupper($list);
    if (strpos($list,'S')!==false) $var = strip_tags($var);
    if (strpos($list,'Q')!==false) $var = str_replace(array('"',"'"),'',$var);
    if (strpos($list,'H')!==false) $var = htmlentities($var);
    if (strpos($list,'T')!==false) $var = trim($var);
    if (strpos($list,'A')!==false) $var = addslashes($var);
    return $var;
}

function SetPost($str,$ModStr='ST')
{
    $VARS = explode(' ',$str);
    foreach ($VARS as $PV) $GLOBALS[$PV] = isset($_POST[$PV])? TransformContent($_POST[$PV],$ModStr) : '';
}

function SetGet($str,$ModStr='ST')
{
    $VARS = explode(' ',$str);
    foreach ($VARS as $PV) $GLOBALS[$PV] = isset($_GET[$PV])? TransformContent($_GET[$PV],$ModStr) : '';
}

function SetBoth($str,$ModStr='ST')
{
    $VARS = explode(' ',$str);
    foreach ($VARS as $PV) $GLOBALS[$PV] = isset($_REQUEST[$PV])? TransformContent($_REQUEST[$PV],$ModStr) : '';
}

function SetPostArray($ArrayName,$str,$ModStr='ST')
{
    $VARS = explode(' ',$str);
    foreach ($VARS as $PV) {
        $GLOBALS[$ArrayName][$PV] = isset($_POST[$ArrayName][$PV])? TransformContent($_POST[$ArrayName][$PV],$ModStr) : '';
    }
}


// example:  parse_str(PostGetStr('NAME PASSWORD','POST'));
function PostGetStr($str,$type) {

// ------------------- DEPRECATED ----------------------

  $VARS = explode(' ',$str);
  $RESULT = '';
  if ($type == 'POST') {
     foreach ($VARS as $PV) if (isset($_POST[$PV])) {
       $RESULT .= "$PV=".trim(strip_tags($_POST[$PV])).'&';} else {$RESULT .= "$PV=&";}
  } elseif ($type == 'GET') {
     foreach ($VARS as $PV) if (isset($_GET[$PV])) {
       $RESULT .= "$PV=".trim(strip_tags($_GET[$PV])).'&';} else {$RESULT .= "$PV=&";}
  } elseif ($type == 'BOTH') {
     foreach ($VARS as $PV) if (isset($_REQUEST[$PV])) {
       $RESULT .= "$PV=".trim(strip_tags($_REQUEST[$PV])).'&';} else {$RESULT .= "$PV=&";}
  }
  $RESULT = substr($RESULT,0,-1);
  return $RESULT;
}


function Post($name) {return (isset($_POST[$name]))? $_POST[$name] : '';}
function Get($name) {return (isset($_GET[$name]))? $_GET[$name] : '';}
function GetPost($name) {return (isset($_REQUEST[$name]))? $_REQUEST[$name] : '';}
function Session($name) {return (isset($_SESSION[$name]))? $_SESSION[$name] : '';}
function Server($name) {return (isset($_SERVER[$name]))? $_SERVER[$name] : '';}



//-----GET VALID BASE TO CHECK REFERRER------
function GetValidBase($mydomain)
{
    if (substr($_SERVER['HTTP_REFERER'],0,11)=='http://www.') {return "http://www.$mydomain";}
    else {return "http://$mydomain";}
}

function FromThisDomain()
{
    $ValidBase = Server('HTTP_HOST');
    $Base = strFrom(Server('HTTP_REFERER'),'://');
    $Base = strTo($Base,'/');
    return ($Base == $ValidBase);
}

//------------ SEND EMAIL --------------
function SendHTMLmail($FromName,$FromEmail,$Recipientlist,$Subject,$Message)
{
    global $MailCR;
    $headers  = "From: $FromName <$FromEmail>$MailCR";
    $headers .= 'MIME-Version: 1.0' .$MailCR;
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . $MailCR;
    
    $count = 0;
    do {
        $RESULT = mail($Recipientlist, "$Subject", $Message, $headers);
        $count++;
        if (!$RESULT) sleep(1);
    } while (($count < 10) and (!$RESULT));

    return $RESULT;
}

//-----------WRITE FILE-----------
function write_file($filename, $filetext)
{
    $filepointer = fopen($filename,"w");
    fwrite($filepointer,$filetext);
    fclose($filepointer);
}

function append_file($filename, $filetext)
{
    if (!file_exists($filename)) {
        write_file($filename,$filetext);
    } else {
        $filepointer = fopen($filename,"a");
        fwrite($filepointer,$filetext);
        fclose($filepointer);
    }
}


//-----------SUBSTRINGS-----------

function strTo($string, $to) {
  $i = strpos($string,$to);
  if ( $i !== false ) return substr($string,0,$i);
    else return $string;
}

function strFrom($string, $from) {
  if (empty($from)) return $string;
  $i = strpos($string,$from);
  if ( $i !== false ) return substr($string,$i+strlen($from));
    else return '';
}

function TruncStr($str,$count) {
  $str = trim($str);
  if (strlen($str) < $count) return $str;
  $str = substr($str,0,$count + 1);
  if (substr($str,-1) != ' ') while((substr($str,-1) != ' ') and (strlen($str)>0)) $str = substr($str,0,-1);
  $str = rtrim($str);
  $pos = strrpos($str, '<');
  if ($pos !== false) $str = substr($str,0,$pos-1);
  return $str.'&hellip;';
}


//-----GET TEXT BETWEEN STRINGS------
function TextBetween($s1,$s2,$s) 
{
    $s1=strtolower($s1);
    $s2=strtolower($s2);
    $L1=strlen($s1);
    $scheck=strtolower($s);
    if ($L1>0) {$pos1 = strpos($scheck,$s1);} else {$pos1=0;}
    if ($pos1!==false) {
        if ($s2=='') return substr($s,$pos1+$L1);
        $pos2 = strpos(substr($scheck,$pos1+$L1),$s2);
        if ($pos2!==false) return substr($s,$pos1+$L1,$pos2);
    }
    return '';
}

//-----Convert File Names/Variables to Title Words

function NameToTitle($string) {
  //separate camelcase and underscores words in title
  $SText = str_replace('_',' ',$string);
  $text = '';
  for ($i=0; $i < strlen($SText); $i++) {
     $ch = $SText[$i];
     if ((($i > 0) and ($SText[$i-1] > 'Z') and ($ch < 'a'))  or
        (($i > 0) and ($ch < 'a') and ($i<strlen($SText)-1) and ($SText[$i+1] > 'Z'))) $text .= ' ';
     $text .= $ch;
  }
  $text = preg_replace('/\s\s+/', ' ', $text);  //remove white space
  return ucwords(trim($text));
}


//-----GET ARRAY TEXT BETWEEN STRINGS------
function TextBetweenArray($s1,$s2,$s) {
  $myarray=array();
  $s1=strtolower($s1);
  $s2=strtolower($s2);
  $L1=strlen($s1);
  $L2=strlen($s2);
  $scheck=strtolower($s);

  do{
  $pos1 = strpos($scheck,$s1);
  if ($pos1!==false) {
    $pos2 = strpos(substr($scheck,$pos1+$L1),$s2);
    if ($pos2!==false) {
      $myarray[]=substr($s,$pos1+$L1,$pos2);
      $s=substr($s,$pos1+$L1+$pos2+$L2);
      $scheck=strtolower($s);
      }
		}
  } while (($pos1!==false)and($pos2!==false));

  return $myarray;
}
//-----GET SUBTEXT IN ARRAY ITEMS------
function SubTextBetweenArray($s1,$s2,$myarray) {
  for ($i=0; $i< count($myarray); $i++)
   {$myarray[$i]=TextBetween($s1,$s2,$myarray[$i]);}
  return $myarray;
}

//-----CONVERT E-MAIL STRING TO CHARACTER CODES------
function ConvertString($s) {
  $result='';
  for($i=0;$i<strlen($s);$i++) { $result.='&#'.ord(substr($s,$i,1)).';'; }
  return $result;
}

//-----ARRAY TO STRING------

function AssocArrayToStr($array, $template = '<b>KEY</b> = VALUE<br />')
{
    if (empty($array)) {
        return '';
    }

    $RESULT='';
    ksort($array);

    foreach ($array as $key=>$value) {
        if (is_array($value)) {
            $value = 'ARRAY('.count($value).')';
        } else {
            $value = htmlentities($value);
        }

        $RESULT .= str_replace(array('KEY','VALUE'),array($key,$value),$template);
    }
    return $RESULT;
}

function ArrayToStr($array)
{
    if (empty($array)) {
        return '';
    }
    $numargs  = func_num_args();
    $template = ($numargs>1)? func_get_arg(1) : 'VALUE<br />';
    $RESULT='';
    foreach ($array as $value) {
        $RESULT .= str_replace('VALUE',$value,$template);
    }
    return $RESULT;
}



//-----TRIM ARRAY------
function TrimValue(&$value) {$value = trim($value);}
function TrimArray(&$myarray) {array_walk($myarray, 'TrimValue');}

//-----WRITE MESSAGE------
function MText($title,$message) {
    global $DOCTYPE_XHTML;
    print <<<MTL
$DOCTYPE_XHTML
<head><title>$title</title></head><body style="background-color:#006;">
<table align="center" style="background-color:#fff; color:#000; border:2px solid #f00; padding:1em; margin-top:20px;">
<tr><td align="center">
  $message
</td></tr></table></body></html>
MTL;
    exit;
}

//-----DISPLAY ALERT------
function alert($message)
{
    echo JavaScriptString("alert('$message');");
}

//----HTML OUTPUT----
function StyleString($mystring)
{
    if ($mystring) $result="<style type=\"text/css\">\n$mystring\n</style>";
    else $result='';
    return $result;
}

function JavaScriptString($mystring)
{
    if ($mystring) $result="<script type=\"text/javascript\">\n<!--\n$mystring\n-->\n</script>";
    else $result='';
    return $result;
}

//--------DATE FUNCTIONS----------

function DateToDashes($d)
{
    //for 4 or 6 char date YYYYMMDD or YYYYMM
    $d=str_replace('-','',$d);
    if (strlen($d)>6) return substr($d,0,4).'-'.substr($d,4,2).'-'.substr($d,6,2);
    else return substr($d,0,4).'-'.substr($d,4,2);
}

function DateWODashes($d)
{
    //for 4 or 6 char date YYYY-MM-DD or YYYY-MM
    return str_replace('-','',$d);
}


function DateToStdHM($d)
{
    $hv = intval(substr($d,8,2));
    if ($hv > 11) {$ampm='pm'; $hv=$hv-12;} else {$ampm='am';}
    if ($hv==0) {$hv=12;}
    if (strlen($hv)==1) {$hv='0'.$hv;}
    $R=substr($d,4,2).'/'.substr($d,6,2).'/'.substr($d,0,4).' - '.$hv.':'.substr($d,10,2).$ampm;
    return $R;
}

//--------SEND XML----------
function SendReceiveXML($url,$page,$post_xml)
{
    $header  = "POST ".$page." HTTP/1.0 \r\n";
    $header .= "MIME-Version: 1.0 \r\n";
    $header .= "Content-type: application/PTI26 \r\n";
    $header .= "Content-length: ".strlen($post_xml)." \r\n";
    $header .= "Content-transfer-encoding: text \r\n";
    $header .= "Request-number: 1 \r\n";
    $header .= "Document-type: Request \r\n";
    $header .= "Interface-Version: Test 1.4 \r\n";
    $header .= "Connection: close \r\n\r\n";
    $header .= $post_xml;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 25);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header);

    $data  = curl_exec($ch);
    $error = curl_errno($ch);
    curl_close($ch);
    if ($error) {
        return "$data\nCURL ERROR($error)";
    }
    return $data;
}


//---------CHECK IF ARRAY ITEMS IN STRING----------
function ArrayItemsWithinStr($myarray,$str)
{
    $arraycount = count($myarray);
    if (empty($str) or ($arraycount==0)) return false;
    for ($i=0; $i<$arraycount; $i++) {
        if (stristr($str,$myarray[$i]) != '') return true;
    }
    return false;
}

function GetDirectory() {
    //gets a directory and subdirectory filelist with optional include and exclude string
    //$url,$includestr,$excludestr
    $numargs  = func_num_args();
    $url = func_get_arg(0);
    $includestr = ($numargs>1)? func_get_arg(1) : '';
    $excludestr = ($numargs>2)? func_get_arg(2) : '';
    $files  = array();
    if (!file_exists($url)) return $files;
    if (!empty($includestr)) $include_strings = explode(',',$includestr);
    if (!empty($excludestr)) $exclude_strings = explode(',',$excludestr);
    $dir = opendir("$url/");
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..') {
            if (is_dir("$url/$file")) {
                $echeck = (!empty($excludestr))? !ArrayItemsWithinStr($exclude_strings,"$url/$file") : true;
                if ($echeck and !eregi('.svn',$file)) {
                    $sfiles = GetDirectory("$url/$file",$includestr,$excludestr);
                    foreach ($sfiles as $f) $files[] = "$file/$f";
                }
            } else {
                $icheck = (!empty($includestr))? ArrayItemsWithinStr($include_strings,$file) : true;
                $echeck = (!empty($excludestr))? !ArrayItemsWithinStr($exclude_strings,$file) : true;
                if ($icheck and $echeck) $files[] = $file;
            }
        }
    }
    closedir($dir);
    natcasesort($files);
    $files = array_values($files);
    return $files;
}

function GetDirCount() {
    //gets Count of directory and subdirectory Count
    //$url,$includestr,$excludestr
    $numargs  = func_num_args();
    $url = func_get_arg(0);
    $includestr = ($numargs>1)? func_get_arg(1) : '';
    $excludestr = ($numargs>2)? func_get_arg(2) : '';

    $count  = 0;
    if (!file_exists($url)) return 0;
    if (!empty($includestr)) $include_strings = explode(',',$includestr);
    if (!empty($excludestr)) $exclude_strings = explode(',',$excludestr);
    $dir = opendir("$url/");
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..') {
            if (is_dir("$url/$file")) {
                if (!eregi('.svn',$file)) {
                    $scount = GetDirCount("$url/$file",$includestr,$excludestr);
                    $count += $scount;
                }
            } else {
                $icheck = (!empty($includestr))? ArrayItemsWithinStr($include_strings,$file) : true;
                $echeck = (!empty($excludestr))? !ArrayItemsWithinStr($exclude_strings,$file) : true;
                if ($icheck and $echeck) $count++;
            }
        }
    }
    closedir($dir);
    return $count;
}

function GetFolders($url) {
    //gets a folders only
    $folders = array();
    if (!file_exists($url)) return $folders;
    $dir = opendir("$url/");
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..') {
            if (is_dir("$url/$file")) {
                if (!eregi('.svn',$file)) {
                    $folders[] = $file;
                    $sfolders = GetFolders("$url/$file");
                    foreach ($sfolders as $f) $folders[] = "$file/$f";
                }
            }
        }
    }
    closedir($dir);
    natcasesort($folders);
    $files = array_values($folders);
    return $folders;
}

if (!function_exists('str_ireplace')) {
  function str_ireplace($search,$replace,$subject) {
    $token = chr(1);
    $haystack = strtolower($subject);
    $needle = strtolower($search);
    while (($pos=strpos($haystack,$needle))!==FALSE) {
        $subject = substr_replace($subject,$token,$pos,strlen($search));
        $haystack = substr_replace($haystack,$token,$pos,strlen($search));
    }
    $subject = str_replace($token,$replace,$subject);
    return $subject;
  }
}

if (!function_exists('stripos')) {
    function stripos($haystack, $needle) {
        return strpos($haystack, stristr( $haystack
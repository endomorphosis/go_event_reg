<ol>
<?php
//FAQ requires toggleDisplay and hideGroup Javascript functions
// needs $faqfile

if(empty($faqfile)) $faqfile = 'faq.dat';

//-----------read the directory----------

$FAQdata = file_get_contents("$ROOT{$SITECONFIG['listdir']}/$faqfile");

$FAQs=TextBetweenArray('<faq>','</faq>',$FAQdata);

//---------------Output the FAQs-------

$count=0;

foreach ($FAQs as $FAQ){
  $question=TextBetween('<ques>','</ques>',$FAQ);
  $answer=TextBetween('<ans>','</ans>',$FAQ);

if($question){
    $count++;

  printqn("
  <li style=`margin-top:0.5em;`>
    <a class=`faqquestion` href=`#` onclick=`javascript:toggleDisplay('faq$count');
    return false;` style=`text-decoration:none;`><b>$question</b></a>
    <div class=`faqanswer` id=`faq$count` style=`margin:5px 2em 1em 2em; display:none`>$answer</div>
  </li>
");    
    
 }
}

?>
</ol>
<script type="text/javascript">hideGroup('faq');</script>
<?php
//------- page helper file ---------
ini_set("url_rewriter.tags",'');
ini_set("session.use_trans_sid", false);
session_start();

$ConfigPath = empty($ConfigPath)? '.' : $ConfigPath;

require_once $_SERVER['DOCUMENT_ROOT'].'/lib/tools.php';
require_once "$ConfigPath/config/siteconfig.php";
$SITE_ROOT = $ROOT.$SITECONFIG['sitedir'];

$BlockListTerms = array('awstats','xmlrpc','chat/','phpmyadmin','phpMyAdmin-','/main','azenv','thisdoesnotexistahaha');
$PAGE           = array('ERROR' => '', 'MESSAGE' => '', 'FLASH' => '');
$QUERYVAR       = array();
$MENUID         = 'id="menu_@"';
$MENUSELECT     = 'id="pageselected"';
$PAGEDEFAULT    = 'index';

function addError($ERRORMSG)
{
    global $PAGE;
    $PAGE['ERROR'] .= "<p>$ERRORMSG</p>";
}

function addMessage($MSG)
{
    global $PAGE;
    $PAGE['MESSAGE'] .= "<p>$MSG</p>";
}

function addFlash($MSG)
{
    global $PAGE;
    $PAGE['FLASH'] .= "<p>$MSG</p>";
}


function GetQuery($name)
{
    global $QUERYVAR;
    return (isset($QUERYVAR[$name]))? $QUERYVAR[$name] : '';
}

function GetPageName()
{
    global $PAGE,$QUERY_STRING,$HTTP_HOST,$SITECONFIG,$MENUID,$PAGEDEFAULT,$QUERYVAR,$PAGE_ALIAS;

    //---- process psudo query string ----
    $querystr = strFrom($QUERY_STRING,':');
    if (!empty($querystr)) {
        $querystr = str_replace(';','&',$querystr);
        $qlist = explode('&',$querystr);
        foreach ($qlist as $var) {
            $varinfo = explode('=',$var);
            $key = $varinfo[0];
            $value = (count($varinfo)>1)? $varinfo[1] : 1;
            $QUERYVAR[$key] = $value;
        }
    }

    $QS = strTo($QUERY_STRING,':');
    $pagename = strTo($QS,';');

    if (substr($pagename,0,2)=='P-') {
        $Print=1;
        $pagename=substr($pagename,2);
    } else {
        $Print=0;
    }

    $pagename = strTo($pagename,'.');

    if ($pagename == '') {
        $pagename=$PAGEDEFAULT;
    }

    $PAGE['dirpage'] = strTo($pagename,'/');

    if (substr($pagename,0,1)=='/')  {
        $pagename=substr($pagename,1);
    }

    if (substr($pagename,-1,1)=='/') {
        $pagename.= $PAGEDEFAULT;
    }

    if (!empty($PAGE_ALIAS)) {
        if (array_key_exists($pagename,$PAGE_ALIAS)) $pagename = $PAGE_ALIAS[$pagename];
    }

    $PAGE['pagename']         = $pagename;
    $PAGE['print']            = $Print;
    $PAGE['printversionlink'] = "{$SITECONFIG['pagedir']}/P-$pagename";
    $PAGE['pagelink']         = "{$SITECONFIG['pagedir']}/$pagename";
    $PAGE['url']              = urlencode($HTTP_HOST)."{$SITECONFIG['pagedir']}/$pagename";
    $PAGE['id']               = str_replace('/','_',$pagename);
    $PAGE['menuid']           = str_replace('@',$PAGE['id'],$MENUID);
}


function GetPageFileNames($redirect='/')
{
    global $PAGE, $SITECONFIG, $ROOT, $QUERY_STRING;

    $PAGE['titlefilename']   = $ROOT.$SITECONFIG['contentdir']."/{$PAGE['pagename']}{$SITECONFIG['titlestr']}";
    $PAGE['contentfilename'] = $ROOT.$SITECONFIG['contentdir']."/{$PAGE['pagename']}{$SITECONFIG['contentstr']}";

    if (!file_exists($PAGE['titlefilename'])) {
        // ----- add to missing log -----
        $missingfile = $ROOT.$SITECONFIG['logdir'].'/missingpage-'.date('Y-m').'.dat';
        $date = date('Y-m-d:H:i:s');
        $ADDR = Server('REMOTE_ADDR');
        $HTTP_REFERER = Server('HTTP_REFERER');
        $USER_AGENT = Server('HTTP_USER_AGENT');
        $line="$date|$QUERY_STRING|$HTTP_REFERER|$ADDR|$USER_AGENT\n";
        append_file($missingfile,$line);
        // ----- redirect -----
        header("Location: $redirect");
        exit;
    }

    $t=filemtime($PAGE['titlefilename']);
    $c=filemtime($PAGE['contentfilename']);
    $PAGE['updated'] = date("m\/d\/Y", max($t,$c));
}


function GetTitleVariables() {
    global $PAGE, $PAGE_TITLE_CONTENT;

    $PAGE_TITLE_CONTENT    = file_get_contents($PAGE['titlefilename']);
    $PAGE['name']          = TextBetween('<name>','</name>',$PAGE_TITLE_CONTENT);
    $PAGE['title']         = TextBetween('<title>','</title>',$PAGE_TITLE_CONTENT);
    $PAGE['description']   = TextBetween('<description>','</description>',$PAGE_TITLE_CONTENT);
    $PAGE['keywords']      = TextBetween('<keywords>','</keywords>',$PAGE_TITLE_CONTENT);
    $PAGE['style']         = StyleString(TextBetween('<style>','</style>',$PAGE_TITLE_CONTENT));
    $PAGE['script']        = JavaScriptString(TextBetween('<script>','</script>',$PAGE_TITLE_CONTENT));

    $mBody                 = TextBetween('<body>','</body>',$PAGE_TITLE_CONTENT);
    if ($mBody) {
        $mBody     =" $mBody";
    }

    $PAGE['body']          = $mBody;

    $PAGE['banner']        = TextBetween('<banner>','</banner>',$PAGE_TITLE_CONTENT);
    $PAGE['template']      = TextBetween('<template>','</template>',$PAGE_TITLE_CONTENT);

    $mRobots               = TextBetween('<robots>','</robots>',$PAGE_TITLE_CONTENT);

    if (!empty($mRobots)) {
        $mRobots  = "<meta name=\"robots\" content=\"$mRobots\" />";
    }

    $PAGE['robots']        = $mRobots;

    $mIncScript            = TextBetween('<scriptinclude>','</scriptinclude>',$PAGE_TITLE_CONTENT);
    $IncScript             = '';

    if (!empty($mIncScript)) {
        $scripts   = explode(',',str_replace(array("\n","\r"),'',$mIncScript));
        foreach ($scripts as $script) {
            $IncScript .= qq("<script type=`text/javascript` src=`$script`></script>\n");
        }
    }

    $PAGE['scriptinc']     = $IncScript;
}


function BlockedIPCheck()
{
    global $BlockListTerms,$PAGE,$SITECONFIG,$ROOT;
    $blockfile = $ROOT.$SITECONFIG['logdir'].'/block-'.date("Y-m-d").'.dat';
    $ADDR  = Server('REMOTE_ADDR');
    //---------- check if blocked --------
    $blocklist = file_exists($blockfile)? file_get_contents($blockfile) : '';
    $pageok = !ArrayItemsWithinStr($BlockListTerms,$PAGE['pagename']);
    if ((strpos($blocklist,$ADDR)!==false) or ($pageok==false)) {
        $time = date('H:i:s');
        $line = "$ADDR|{$_SERVER['QUERY_STRING']}|$time\n";
        append_file($blockfile,$line);
        sleep(rand(5,10));
        exit;
        //MText('Error','<h1>Blocked</h1>'); // will exit;
    }
}


function WriteTrackingLog()
{
    global $PAGE,$SITECONFIG,$ROOT;

    if (empty($_SESSION[$PAGE['pagename']])) {
        $_SESSION[$PAGE['pagename']] = 1;
        if (empty($_SESSION['starttime'])) {
            $_SESSION['starttime'] = time();
        } else {
            $elapsedtime = time() - $_SESSION['starttime'];
        }

        $tid = $_SESSION['starttime'].substr(session_id(),-4);
        $logfile = $ROOT.$SITECONFIG['logdir'].'/log-'.date('Y-m').'.dat';

        if (empty($_SESSION['REF'])) {
            $_SESSION['REF'] = 1;
            $ADDR = Server('REMOTE_ADDR');
            $HTTP_REFERER = Server('HTTP_REFERER');
            $USER_AGENT = Server('HTTP_USER_AGENT');
            $line="$tid|REF|{$HTTP_REFERER}[$ADDR][$USER_AGENT]\n$tid|0|{$PAGE['pagename']}\n";
        } else {
            $line="$tid|$elapsedtime|{$PAGE['pagename']}\n";
        }
        append_file($logfile,$line);
    }
}


function SwapStdMarkUp()
{
    global $PAGE_STREAM,$PAGE,$SITECONFIG,$PAGE_CONTENT,$MENUSELECT,$TESTVAR;
    $NEWENDBODY = isset($TESTVAR)? "$TESTVAR\n</body>" : '</body>';
    $StdOLDtext =array(
       '@@TITLE@@','@@DESCRIPTION@@','@@KEYWORDS@@',
       '<!-- @@STYLE@@ -->','<!-- @@SCRIPT@@ -->','<!-- @@SCRIPTINCLUDE@@ -->',
       ' title="@@BODY@@"','<!-- @@ROBOTS@@ -->',
       '@@CONTENT@@','@@UPDATED@@',
       '@@PRINTVERSIONLINK@@','@@PAGELINK@@',
       '@@PAGENAME@@','@@DIR@@','@@PAGEURL@@','@@PAGEID@@',
       '@@COMPANYNAME@@','@@BANNER@@',$PAGE['menuid'],
       '@@ERROR@@', '@@MESSAGE@@', '@@FLASH@@',
       '</body>'
    );
    
    $ERROR   = (empty($PAGE['ERROR']))?   '' : "<div id=\"error\">{$PAGE['ERROR']}</div>";
    $MESSAGE = (empty($PAGE['MESSAGE']))? '' : "<div id=\"message\">{$PAGE['MESSAGE']}</div>";
    $FLASH   = (empty($PAGE['FLASH']))?   '' : "<div id=\"flash\" style=\"position:absolute\">{$PAGE['FLASH']}</div>";

    $StdNEWtext =array(
        $PAGE['title'],$PAGE['description'],$PAGE['keywords'],
        $PAGE['style'],$PAGE['script'],$PAGE['scriptinc'],
        $PAGE['body'],$PAGE['robots'],
        $PAGE_CONTENT,$PAGE['updated'],
        $PAGE['printversionlink'],$PAGE['pagelink'],
        $PAGE['pagename'],$PAGE['dirpage'],$PAGE['url'],$PAGE['id'],
        $SITECONFIG['companyname'],$PAGE['banner'],$MENUSELECT,
        $ERROR, $MESSAGE, $FLASH,
        $NEWENDBODY
    );

    $PAGE_STREAM = str_replace($StdOLDtext, $StdNEWtext, $PAGE_STREAM);
}
?>
<?php
//------MySQL Helper Routines

/*  ============================== defined functions =============================
RowSelect($row_count,$rows,$start_row,$STARTROW,$ROWS,$HOME,$PREVIOUSPAGE,$NEXTPAGE,$END,$SHOW)
SetDbQuery($function,$query)
WriteDbQuery()
WriteDbQueryText()
db_QuoteValueC($value)
db_KeyValues($array)
db_Keys($array)
db_Values($array)
ArrayToPost($arraymap,$array)
db_output_table($SearchArray,$UserInfo,$TableSetup,$EditTitle,$EditLinks,$id)
db_IsUnique($db_table,$key,$value,$exclude)
db_GetUniqueID($db_table,$field)
db_GetNextValue($db_table,$key)
db_GetNextDate($db_table,$key,$format,$inc)
db_GetFieldValues($db_table, $key, $where='', $case='')
db_GetFieldValuesLC($db_table,$key, $where='')
db_ViewRecord($db_table,$keymap,$TableSetup,$keys,$where)
db_ViewRecordArray($Record,$Titles,$TableSetup)
db_GetRecord($db_table,$keys,$where)
db_UpdateRecord($db_table,$key_values,$where)
db_DeleteRecord($db_table,$where)
db_AddRecord($db_table,$keys,$values)
db_GetSearchArray($db_table, $keys, $conditions, $order, $startlist, $listsize, &$num_rows, &$query)
db_GetArray($db_table, $keys, $conditions, $order, $startlist, $listsize, &$num_rows)
db_GetArrayAll($db_table, $keys='*', $conditions='', $order='')
db_GetArrayAssoc($db_table, $referencekey, $keys='*', $conditions='', $order='')
db_Count($TABLE, $WHERE)
db_GetFreq($db_table, $key, $order, $orderdir, $conditions)
db_GetMax($db_table, $maxkey, $indexkey, $conditions)
db_FieldArray($db_table, $key, $conditions, $order)
db_GetValue($db_table, $key, $conditions)
db_IncValue($db_table, $key, $conditions)
db_GetCount($db_table,$conditions)
db_GetTables()
db_TableExists($db_table)
db_TableFieldInfo($db_table)
db_TableFieldNames($db_table)
db_TableFieldTitleNames($db_table)
*/

$DBQUERY               = '';
$DbLastQuery           = '';
$DbQueryTableOptions   = 'align="center" style="background-color:#888; border:1px solid #000; margin-top:10px;"';
$DbQueryTableOptionsTH = 'align="center" style="background-color:#aaa; color:#fff;"';
$DbQueryTableOptionsTD = 'align="left" style="background-color:#ff7; color:#000; font-size:0.8em; padding:1em 1em 0px 0px;"';
$FieldValues           = array();

if (!empty($DB_INFO)) {
    SetUpDbConnection($DB_INFO);
}

//------ call this function at start-up
function SetUpDbConnection($DB_INFO)
{
    @mysql_connect($DB_INFO['HOST'],$DB_INFO['USER'],$DB_INFO['PASS']) or Mtext('Error','Could not connect to Database!');
    mysql_select_db($DB_INFO['NAME']);
}

function RowSelect($row_count,$rows,$start_row,$STARTROW,$ROWS,$HOME,$PREVIOUSPAGE,$NEXTPAGE,$END,$SHOW)
{
    if ($row_count > $rows) {
        $RESULT = '
        <div class=rowselect"><p>Found: "' . number_format($row_count) . '" records</p>
        <p>Start Row: <input type="text" size="4" maxlength="10" name="' . $STARTROW . '" value="' .$start_row . '" />&nbsp;&nbsp;
        Rows per Page: <input type="text" size="4" maxlength="4" name="' . $ROWS . '" value="' . $rows . '" />&nbsp;&nbsp;
        <input type="submit" value="Show" name="'.$SHOW.'" />&nbsp;';
        if ($start_row > 1) {
            $RESULT .= '<input type="submit" value="&lt;&lt;" name="' . $HOME . '" />&nbsp;';
        }
        if ($start_row > 1) {
            $RESULT .= '<input type="submit" value="<&mdash;" name="' . $PREVIOUSPAGE. '" />&nbsp;';
        }
        if ($row_count >= $start_row+$rows) {
            $RESULT .= '<input type="submit" value="&mdash;&gt;" name="' . $NEXTPAGE . '" />';
        }
        if ($row_count >= $start_row+$rows) {
            $RESULT .= '<input type="submit" value="&gt;&gt;" name="' . $END . '" />';
        }
        $RESULT .= '</p></div>';
    } else {
        $RESULT = '';
    }
    return $RESULT;
}

// ------------------ Database Query Tracking --------------
function SetDbQuery($function,$query)
{
    global $DBQUERY, $DbLastQuery;
    $query = htmlentities(trim($query));
    $DbLastQuery = $query;
    $span1 = '<span style="color:#f00; font-weight:bold;">';
    $span2 = '</span>';
    $keywords = array('UPDATE ','INSERT ',' INTO ','SET ','LEFT ','JOIN ','COUNT(',')',' AS ','CONCAT(',
        'WHERE ','LIMIT ','SELECT ',' AND ',' OR ', ' FROM ',' MAX ','DELETE ','VALUES(','GROUP',' BY ',' ORDER');
    foreach ($keywords as $word) {
        $query = str_ireplace($word,"$span1$word$span2",$query);
    }

    if (!empty($query)) $query = "<li style=\"padding:5px;\"><b>$function:</b> $query</li>";

    $error = mysql_error();
    if (!empty($error)) {
        $error = "<li style=\"background-color:#f00;color:#fff;padding:3px;\">$error</li>";
    }
    if("$error$query" != '') $DBQUERY .= "$error$query\n";
}

function WriteDbQuery()
{
    echo WriteDbQueryText();
}

function WriteDbQueryText()
{
    global $DBQUERY;
    SetDbQuery('','');  // update in case last query had an error
    $DbQueryTableOptions   = 'id="db_queries" align="center" style="background-color:#888; border:1px solid #000; margin:10px auto;"';
    $DbQueryTableOptionsTH = 'align="center" style="background-color:#aaa; color:#fff;"';
    $DbQueryTableOptionsTD = 'align="left" style="background-color:#ff7; color:#000; font-size:0.8em; padding:1em 1em 0px 0px;"';

    $RESULT = '';
    if (!empty($DBQUERY)) {
        $RESULT .= "\n\n<table $DbQueryTableOptions>\n";
        $RESULT .= '<tr><th '.$DbQueryTableOptionsTH.'>DB Queries (<a href="#" onclick="getElementById(\'db_queries\').style.display=\'none\'; return false;">Hide</a>)</th></tr>';
        $RESULT .= "\n<tr><td $DbQueryTableOptionsTD>\n<ul style=\"margin:0px 2em;\">$DBQUERY</ul>\n</td></tr>\n";
        $RESULT .= "</table>\n";
    }
    return $RESULT;
}

function db_QuoteValue($value)
{
    $value = trim($value);
    if (($value == 'NOW()') or (is_numeric($value)))  return $value;
    elseif ((strpos($value, "'")!==false) and (strpos($value, '"')===false)) return "\"$value\"";
    elseif (strpos($value, "'")===false)  return "'$value'";
    else return "'" . mysql_real_escape_string($value) . "'";
}

function db_QuoteValueC($value)
{
    return db_QuoteValue($value) . ',';
}


function db_KeyValues($array)
{
    $RESULT = '';
    foreach ($array as $key => $value) {
        $RESULT .= "`$key`=". db_QuoteValueC($value);
    }
    return substr($RESULT,0,-1);
}


function db_Keys($array)
{
    $RESULT = '';
    foreach ($array as $key => $value) {
        $RESULT .= "`$key`,";
    }
    return substr($RESULT,0,-1);
}

function db_Values($array)
{
    $RESULT = '';
    foreach ($array as $key => $value) {
        $RESULT .= db_QuoteValueC($value);
    }
    return substr($RESULT,0,-1);
}

function ArrayToPost($arraymap,$array)
{
    foreach ($arraymap as $varname => $postname) {
        $_POST[$postname] = empty($array[$varname])? '' : $array[$varname];
    }
}

function db_output_table($SearchArray,$UserInfo='',$TableSetup='',$EditTitle='',$EditLinks='',$id='')
{
    global $FieldValues;
    if (count($SearchArray) == 0) return '';
    $RESULT = "<table $TableSetup>\n<tr><th>No.</th>";
    $wantedit = (!empty($id) and !empty($EditLinks));
    if ($wantedit) $RESULT .= "<th>$EditTitle</th>";
    if (empty($UserInfo)) {
        $UserInfo = array();
        foreach ($SearchArray[0] as $key=>$value) {
            $UserInfo[$key] = NameToTitle($key);
        }
    }
    foreach ($UserInfo as $key => $value) { $RESULT .= "<th>$value</th>"; }
    $RESULT .= "</tr>\n";
    $evenodd = 2;
    $count = 0;
    foreach ($SearchArray as $row) {
        $count++;
        $evenodd = 3 - $evenodd;
        $class = ($evenodd == 1) ? 'odd' : 'even';
        $RESULT .= "<tr class=\"$class\"><td align=\"right\">$count.</td>";
        if ($wantedit) $RESULT .= '<td>'. str_replace('@@ID@@',$row[$id],$EditLinks) .'</td>';
        foreach ($UserInfo as $key => $value) {
            $field = (empty($row[$key]) and ($row[$key] != '0'))? '' : $row[$key];
            $outvalue = (empty($FieldValues[$key][$field]))? $field : $FieldValues[$key][$field];
            $RESULT .= "<td>$outvalue</td>";
        }
        $RESULT .= "</tr>\n";
    }
    $RESULT .= "</table>\n";
    return $RESULT;
}

function db_IsUnique($db_table,$key,$value,$exclude)
{
    if (empty($key) or empty($value)) {
        return false;
    }
    $excludestr = (!empty($exclude))? " AND $exclude" :'';
    $query = "SELECT $key FROM `$db_table` WHERE $key='$value'$excludestr LIMIT 1";
    $db_query = mysql_query($query);
    SetDbQuery('db_IsUnique',$query);
    if ($db_query) {
        $row = mysql_fetch_assoc($db_query);
    }
    return empty($row[$key]);
}

function db_GetUniqueID($db_table,$field) {
    $UID     = md5(uniqid(rand(), true));
    while (!db_IsUnique($db_table,$field,$UID,'')) {
        $UID = md5(uniqid(rand(),true));
    }
    return $UID;
}


function db_GetNextValue($db_table,$key)
{
    if (empty($key)){
        return '';
    }
    $query    = "SELECT MAX($key) AS maxkey FROM `$db_table`";
    $db_query = mysql_query($query);
    SetDbQuery('db_GetNextValue',$query);
    $row      = mysql_fetch_assoc($db_query);
    $RESULT   = $row['maxkey'] + 1;
    return $RESULT;
}

function db_GetNextDate($db_table,$key,$format,$inc)
{
    if (empty($key)) {
        return '';
    }
    $query    = "SELECT MAX($key) AS maxkey FROM `$db_table`";
    $db_query = mysql_query($query);
    SetDbQuery('db_GetNextDate',$query);
    $row      = mysql_fetch_assoc($db_query);
    $maxdate  = DateToDashes($row['maxkey']);
    if (strlen($maxdate)<8) {
        $maxdate .= '-01';
    }
    return date($format,strtotime("$maxdate +1 $inc"));
}


function db_GetFieldValues($db_table, $key, $where='', $case='')
{
    $RESULT = array();
    $case = strtoupper($case);

    if (!empty($where)) $where = " WHERE $where";

    if (empty($key)) {
        return $RESULT;
    }

    $query    = "SELECT $key FROM `$db_table`$where GROUP BY $key ORDER BY $key";
    $db_query = mysql_query($query);

    SetDbQuery('db_GetFieldValues',$query);

    if (!$db_query) {
        return $RESULT;
    }

    while ($row = mysql_fetch_array($db_query, MYSQL_NUM)) {
        $value = $row[0];
        if (!empty($value)) {
            if ($case == 'U') $value = strtoupper($value);
            elseif ($case == 'L') $value = strtolower($value);
            $RESULT[] = $value;
        }
    }

    return $RESULT;
}

function db_GetFieldValuesLC($db_table,$key, $where='')
{
    return db_GetFieldValues($db_table, $key, $where, 'L');
}

function db_ViewRecord($db_table,$keymap,$TableSetup,$keys,$where)
{
    global $FieldValues;
    if (empty($keys) or empty($where)) return '';
    $query = "SELECT $keys FROM `$db_table` WHERE $where LIMIT 1";
    $db_query = mysql_query($query);
    SetDbQuery('db_ViewRecord',$query);
    if (!$db_query) return '';
    $row =mysql_fetch_assoc($db_query);
    if (count($row)>0) {
        $RESULT = "<table $TableSetup>\n";
        foreach ($row as $key => $value) {
            $outkey = (empty($keymap[$key]))? $key : $keymap[$key];
            $outvalue = (empty($FieldValues[$key][$value]))? $value : $FieldValues[$key][$value];
            $RESULT .= "<tr><th align=\"right\">$outkey</th><td>$outvalue</td></tr>\n";
        }
        $RESULT .= "</table>\n";
    } else {
        $RESULT = '';
    }
    return $RESULT;
}

function db_ViewRecordArray($Record,$Titles,$TableSetup)
{
    global $FieldValues;
    if (count($Record)>0) {
        $RESULT = "<table $TableSetup>\n";
        foreach ($Record as $key => $value) {
            $outkey = (empty($Titles[$key]))? $key : $Titles[$key];
            $outvalue = (empty($FieldValues[$key][$value]))? $value : $FieldValues[$key][$value];
            $RESULT .= "<tr><th align=\"right\">$outkey</th><td>$outvalue</td></tr>\n";
        }
        $RESULT .= "</table>\n";
    } else {
        $RESULT = '';
    }
    return $RESULT;
}


function db_GetRecord($db_table,$keys,$where)
{
    if (empty($keys) or empty($where)) {
        return '';
    }

    $query = "SELECT $keys FROM `$db_table` WHERE $where LIMIT 1";
    $db_query = mysql_query($query);
    if ($db_query) {
        SetDbQuery('db_GetRecord',$query);
        return mysql_fetch_assoc($db_query);
    } else {
        return 0;
    }
}

function db_UpdateRecord($db_table,$key_values,$where)
{
    if (empty($key_values) or empty($where)) {
        return false;
    }

    $query = "UPDATE `$db_table` SET $key_values WHERE $where";
    $RESULT = mysql_query($query);
    SetDbQuery('db_UpdateRecord',$query);
    return $RESULT;
}

function db_DeleteRecord($db_table,$where)
{
  if (empty($where)) {
    return false;
  }

  $query = "DELETE FROM `$db_table` WHERE $where";
  $RESULT = mysql_query($query);
  SetDbQuery('db_DeleteRecord',$query);
  return $RESULT;
}

function db_AddRecord($db_table,$keys,$values)
{
    if (empty($keys) or empty($values)) {
        return false;
    }
    $query = "INSERT INTO `$db_table` ($keys) VALUES ($values)";
    $RESULT = mysql_query($query);
    SetDbQuery('db_AddRecord',$query);
    return $RESULT;
}


function db_GetSearchArray($db_table, $keys, $conditions, $order, $startlist, $listsize, &$num_rows, &$query)
{
  global $DbLastQuery;
  //deprecated - do not need &$query can get from $DbLastQuery  -- use db_GetArray()

  $RESULT = db_GetArray($db_table, $keys, $conditions, $order, $startlist, $listsize, $num_rows);
  $query  = $DbLastQuery;
  return $RESULT;
}

function db_GetArray($db_table, $keys, $conditions, $order, $startlist, $listsize, &$num_rows)
{
    if ($keys == '') {
        return '';
    }

    if (empty($order) or ($order == 'none')) {
        $ORDER = '';
    } else {
        $order = "ORDER BY $order";
    }

    if (!empty($conditions)) {
        $conditions = "WHERE $conditions";
    }

    $query_num = "SELECT 1 FROM `$db_table` $conditions";
    $db_query = mysql_query($query_num);
    $num_rows = mysql_num_rows($db_query);

    if ($startlist < 0) $startlist = $num_rows + $startlist;  // gets last rows (-100, would get last 100 rows)

    if ((empty($startlist)) and (empty($listsize))) {
        $LIMIT = '';
    } else {
        $LIMIT = "LIMIT $startlist, $listsize";
    }

    $query     = "SELECT $keys FROM `$db_table` $conditions $order $LIMIT";
    $db_query = mysql_query($query);

    SetDbQuery('db_GetSearchArray',$query);

    $RESULT = array();
    if ($db_query) {
        while ($row = mysql_fetch_assoc($db_query)) {
            $RESULT[] = $row;
        }
    }

    return $RESULT;
}


function db_GetArrayAll($db_table, $keys='*', $conditions='', $order='')
{
    if ($keys == '') {
        return '';
    }

    $order = (empty($order) or ($order == 'none'))? '' : "ORDER BY $order";

    if (!empty($conditions)) {
        $conditions = "WHERE $conditions";
    }

    $query = "SELECT $keys FROM `$db_table` $conditions $order";
    $db_query = mysql_query($query);
    SetDbQuery('db_GetArrayAll',$query);
    $RESULT = array();
    if ($db_query) {
        while ($row = mysql_fetch_assoc($db_query)) {
            $RESULT[] = $row;
        }
    }
    return $RESULT;
}

function db_GetArrayAssoc($db_table, $referencekey, $keys='*', $conditions='', $order='')
{
    if(!empty($order)) $order = "ORDER BY $order";

    if (!empty($conditions)) {
        $conditions = "WHERE $conditions";
    }

    $query = "SELECT $keys FROM `$db_table` $conditions $order";
    $db_query = mysql_query($query);
    SetDbQuery('db_GetArrayRef',$query);
    $RESULT = array();
    if ($db_query) {
        while ($row = mysql_fetch_assoc($db_query)) {
            $RESULT[$row[$referencekey]] = $row;
        }
    }
    return $RESULT;
}

// -------------------- function to return an Associative Array from two fields in a Table ---------------------------
function db_GetAssocArray($db_table, $key, $value , $conditions = '', $keycase = '')
{
    $RESULT = array();
    $keycase = strtoupper($keycase);

    if (($key == '') or ($value == '')) {
        return $RESULT;
    }

    if (!empty($conditions)) {
        $conditions = "WHERE $conditions";
    }

    $query = "SELECT $key,$value FROM `$db_table` $conditions ORDER BY $key";
    $db_query = mysql_query($query);
    SetDbQuery('db_GetAssocArray',$query);
    if ($db_query) {
        while ($row = mysql_fetch_array($db_query, MYSQL_NUM)) {
            $idx = $row[0];
            if (!empty($idx)) {
                if ($keycase == 'U') $idx = strtoupper($idx);
                elseif ($keycase == 'L') $idx = strtolower($idx);
                $RESULT[$idx] = $row[1];
            }
        }
    }
    return $RESULT;
}


function db_Count($TABLE, $WHERE)
{
    $query = "SELECT count(1) FROM `$TABLE` WHERE $WHERE";
    $db_query = mysql_query($query);
    SetDbQuery('db_Count',$query);
    $row = mysql_fetch_row($db_query);
    return $row[0];
}

function db_GetFreq($db_table, $key, $order, $orderdir, $conditions='')
{
    if ($key == '') {
        return '';
    }

    $order = ($order == 1)? 'ORDER BY 1 ' : 'ORDER BY 2 ';
    $order .= $orderdir;
    if (!empty($conditions)) {
        $conditions = "WHERE $conditions";
    }

    $query = "SELECT $key, COUNT(*) FROM `$db_table` $conditions GROUP BY $key $order";
    $db_query = mysql_query($query);
    SetDbQuery('db_GetFreq',$query);
    $RESULT = array();
 
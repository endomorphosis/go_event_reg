<?PHP
//  WEBSITE ADMINISTRATION PROGRAM
//  Admin Process File

//======================NEW FILE==========================
$DuplicateFile=0;

if ($NEWFILE and ($NEWFILETYPE=='helper')) {
    $NEWFILENAME = str_replace(array("'",'\\',' '),array('','','_'),$NEWFILENAME);
    write_file("$SITE_ROOT/helper/$NEWFILENAME","<?php\n\n?>");
    $F = "../helper/$NEWFILENAME";
    $SP = 1;
} elseif ($NEWFILE and ($NEWFILETYPE=='common')) {
    $NEWFILENAME = str_replace(array("'",'\\',' '),array('','','_'),$NEWFILENAME);
    write_file("$SITE_ROOT/common/$NEWFILENAME",'');
    $F = "../common/$NEWFILENAME";
    $SP = 1;
} elseif ($NEWFILE and ($NEWFILETYPE=='lists')) {
    $NEWFILENAME = str_replace(array("'",'\\',' '),array('','','_'),$NEWFILENAME);
    write_file("$SITE_ROOT/lists/$NEWFILENAME",'');
    $F = "../lists/$NEWFILENAME";
    $SP = 1;

} elseif ($NEWFILE and ($NEWFILETYPE=='page')) {
    //remove bad characters
    $NEWFILENAME = str_replace(array("'",'\\',' '),array('','','_'),$NEWFILENAME);

    if (!(file_exists(ADMIN_CONTENT_DIR."/$NEWFILENAME".ADMIN_CONTENT_STR))) {
        $F=$NEWFILENAME;
        if (substr($F,0,1) == '/') {
            $F = substr($F,1);
        }

        if (!file_exists(dirname(ADMIN_CONTENT_DIR."/$F"))) {
            mkdir(dirname(ADMIN_CONTENT_DIR."/$F"));
        }

        $QUERY_STRING = "F=$NEWFILENAME";
        $ADMIN_PAGE_QUERY_LINK = "$ADMIN_FILE?$QUERY_STRING";
        $TTEXT =  file_get_contents(ADMIN_FILES_DIR.'/blanktitle.dat');

        //separate camelcase and underscores words in title
        $text = NameToTitle(basename($NEWFILENAME));
        $OLDtext = array('<name></name>','<summary></summary>','<title></title>','<description></description>');
        $NEWtext = array("<name>$text</name>","<summary>$text</summary>","<title>$text</title>","<description>$text</description>");
        $TTEXT = str_replace($OLDtext,$NEWtext,$TTEXT);
        $CTEXT="<h1>$text</h1>";
        $PUBLISH=1;
        LogUpdate(ADMIN_USERNAME,'New File',$NEWFILENAME);
        AddFlash("New File Created: <b>$NEWFILENAME</b>");

    } else {
        $NEW=1;
        $DuplicateFile=1;
    }
}


if ($RENAMEFILE) {
    if (!(file_exists(ADMIN_CONTENT_DIR."/$NEWNAME".ADMIN_CONTENT_STR))) {
        $filename1  = ADMIN_CONTENT_DIR."/$OLDNAME".ADMIN_CONTENT_STR;
        $titlename1 = ADMIN_CONTENT_DIR."/$OLDNAME".ADMIN_TITLE_STR;
        $filename2  = ADMIN_CONTENT_DIR."/$NEWNAME".ADMIN_CONTENT_STR;
        $titlename2 = ADMIN_CONTENT_DIR."/$NEWNAME".ADMIN_TITLE_STR;

        if (!file_exists(dirname(ADMIN_CONTENT_DIR."/$NEWNAME"))) {
            mkdir(dirname(ADMIN_CONTENT_DIR."/$NEWNAME"));
        }

        rename($filename1,$filename2);
        rename($titlename1,$titlename2);
        AddFlash("Files Renamed: <b>$OLDNAME</b> to <b>$NEWNAME</b>");
        LogUpdate(ADMIN_USERNAME,'Rename File',"$OLDNAME - $NEWNAME");
        $FM=1;
    } else {
        $RENAME=$OLDNAME;
        $DuplicateFile=1;
    }
}


if ($COPYFILE) {
    if (!(file_exists(ADMIN_CONTENT_DIR."/$NEWNAME".ADMIN_CONTENT_STR))) {
        $filename1  = ADMIN_CONTENT_DIR."/$OLDNAME".ADMIN_CONTENT_STR;
        $titlename1 = ADMIN_CONTENT_DIR."/$OLDNAME".ADMIN_TITLE_STR;
        $filename2  = ADMIN_CONTENT_DIR."/$NEWNAME".ADMIN_CONTENT_STR;
        $titlename2 = ADMIN_CONTENT_DIR."/$NEWNAME".ADMIN_TITLE_STR;

        if (!file_exists(dirname(ADMIN_CONTENT_DIR."//$NEWNAME"))) {
            mkdir(dirname(ADMIN_CONTENT_DIR."//$NEWNAME"));
        }

        copy($filename1,$filename2);
        copy($titlename1,$titlename2);
        AddFlash("File Copied: <b>$OLDNAME</b> to <b>$NEWNAME</b>");
        LogUpdate(ADMIN_USERNAME,'Copy File',"$OLDNAME - $NEWNAME");
        $FM=1;
    } else {
        $COPY=$OLDNAME; $DuplicateFile=1;
    }
}


if ($DELETE) {
    $filename=ADMIN_CONTENT_DIR."/$DELETE".ADMIN_CONTENT_STR;
    $titlename=ADMIN_CONTENT_DIR."/$DELETE".ADMIN_TITLE_STR;
    if (file_exists($filename)) {
        unlink($filename);
        AddFlash("File Deleted: <b>$DELETE</b>");
    } else {
        $ERROR_MSG = 'File Not Found for Delete!';
    }

    if (file_exists($titlename)) {
        unlink($titlename);
    } else {
        $ERROR_MSG = 'File Not Found for Delete!';
    }
}




function DataToStd($d) {
    $hv = intval(substr($d,8,2));

    if ($hv > 11) {$ampm='pm'; $hv=$hv-12;} else {$ampm='am';}

    if ($hv==0) {
        $hv=12;
    }

    if (strlen($hv)==1) {
        $hv='0'.$hv;
    }

    $R=substr($d,4,2).'/'.substr($d,6,2).'/'.substr($d,0,4).' - '.$hv.':'.substr($d,10,2).$ampm;
    return $R;
}

//--------------------- file processing------------------
if ($F) {
    $ADMIN_PAGE_QUERY_LINK = str_replace('F=_','F=',$ADMIN_PAGE_QUERY_LINK);
    if (substr($F,0,1)=='_') {$draft=1; $FZ=substr($F,1);} else {$draft=0; $FZ=$F;}
    $DraftNotice = (($draft) or ($SAVEDRAFT))? '<span class="draft">DRAFT:</span> ' : '';

    if ($SP=='1') {
        $Tfilename = $F;
        $Cfilename = $F;
        $FS = basename($F);
    } else {
        $Tfilename = ADMIN_CONTENT_DIR."/$F".ADMIN_TITLE_STR;
        $Cfilename = ADMIN_CONTENT_DIR."/$F".ADMIN_CONTENT_STR;
        $SP = 0;
        $FS = $F;
    }

    if (file_exists($Tfilename)) {
        $TitleDate   = date("YmdHis",filemtime($Tfilename));
    } else {
        $TitleDate = 0;
    }

    if (file_exists($Cfilename)) {
        $ContentDate = date("YmdHis",filemtime($Cfilename));
    } else {
        $ContentDate=0;
    }

//-------------SAVE FILES--------------
    if ($PUBLISH or $SAVEDRAFT) {
        if ($SP != 1) {
            //---update title
            if ($TITLEDATE < $TitleDate) {
               AddError('File Date of Title File is Newer on Server. File Not Saved!');
               $TitleDate = $TITLEDATE;
            } else {
                $Tdraftfilename = ADMIN_CONTENT_DIR."/_$FZ".ADMIN_TITLE_STR;

                if ($SAVEDRAFT) {
                    $Tfilename = $Tdraftfilename;
                } else {
                    $Tfilename = ADMIN_CONTENT_DIR."/$FZ".ADMIN_TITLE_STR;
                }

                $filepointer = fopen($Tfilename,"w");
                fwrite($filepointer,$TTEXT);

                if ($NEWFILE) {
                    chmod($Tfilename, 0766);
                }

                fclose($filepointer);
                clearstatcache();
                $TitleDate = date("YmdHis",filemtime($Tfilename));

                if (file_exists($Tdraftfilename) and ($PUBLISH)) {
                    unlink($Tdraftfilename);
                }
            }
        }
        //---update content
        if ($CONTENTDATE < $ContentDate) {
            AddError("File Date of Content File is Newer on Server. File Not Saved! ($CONTENTDATE, $ContentDate)");
            $ContentDate = $CONTENTDATE;
        } else {
            if ($SP!= 1) {
                $Cdraftfilename = ADMIN_CONTENT_DIR."/_$FZ".ADMIN_CONTENT_STR;
                if ($SAVEDRAFT) {
                    $Cfilename=$Cdraftfilename;
                    $F='_'.$FZ;
                } else {
                    $Cfilename=ADMIN_CONTENT_DIR."/$FZ".ADMIN_CONTENT_STR;
                    $F=$FZ;
                }
            }

            if (is_writable($Cfilename) or !file_exists($Cfilename)) {
                $filepointer=fopen($Cfilename,"w");
                fwrite($filepointer,$CTEXT);

                if ($NEWFILE) {
                    chmod($Cfilename, 0766);
                }

                fclose($filepointer);

                if (($SP!= 1) and ($PUBLISH) and file_exists($Cdraftfilename)) {
                    unlink($Cdraftfilename);
                }

                if ($PUBLISH) {
                    LogUpdate(ADMIN_USERNAME,'Publish Page',$F);
                } else {
                    LogUpdate(ADMIN_USERNAME,'Save Draft',$F);
                }

                clearstatcache();
                $ContentDate = date("YmdHis",filemtime($Cfilename));

            } else {
                $ERROR_MSG = 'File is Not Writeable - Check Permissions!';
            }
        }
    }

    if ($TitleDate) {
        $Tfd = date("m\/d\/Y - h:ia",filemtime($Tfilename));
    }
    if ($ContentDate) {
        $Cfd = date("m\/d\/Y - h:ia",filemtime($Cfilename));
    }

    if (empty($TitleDate) and empty($ContentDate)) {
        $ERROR_MSG = 'File Not Found!
<!-- =========================== CONTENT EDITING MENU ======================= -->
<?php

$alinks = '<select id="filelinks">';
$count = 0;
foreach ($files as $file) {
  $count++;
  $mfile = str_replace('_','~',$file);
  $alinks .= qq("\n<option value=`$mfile{$SITECONFIG['extension']}`>$count. $file{$SITECONFIG['extension']}</option>");
}
$alinks .= '</select>';


$ifiles = GetDirectory(ADMIN_IMAGE_DIR,ADMIN_IMAGE_TYPES);
$ilinks = '<select id="ifilelinks">';
$count = 0;
foreach ($ifiles as $file) {
    $count++;

    $filename = ADMIN_IMAGE_DIR."/$file";
    list($width, $height, $type, $attr) = getimagesize($filename);

    $mfile = str_replace('_','~',$file);
    $title = str_replace(' ','_',NameToTitle(strTo($file,'.')));
    $mfile = "[img_src=@".ADMIN_IMAGE_LINK_DIR."/$mfile@_alt=@$title@_width=@$width@_height=@$height@_border=@0@_/]";
    $ilinks .= qq("\n<option value=`$mfile`>$count. $file</option>");
}

$ilinks .= '</select>';
$ADMIN_IMAGE_LINK_DIR = ADMIN_IMAGE_LINK_DIR;

print <<<CONTENTMENULABEL
<div id="editmenu" style="background-color:#ccc; height:2em;">
<ul class="menu">

<li><a style="width:4em;" href="#" onclick="return false;">Text</a>
  <ul>
<li><a href="#" onclick="tagSurround('[b]','[/b]','CTEXT'); return false;">&lt;b&gt;</a></li>
<li><a href="#" onclick="tagSurround('[i]','[/i]','CTEXT'); return false;">&lt;i&gt;</a></li>
<li><a href="#" onclick="tagSurround('[strong]','[/strong]','CTEXT'); return false;">&lt;strong&gt;</a></li>
<li><a href="#" onclick="tagSurround('[em]','[/em]','CTEXT'); return false;">&lt;em&gt;</a></li>
<li><a href="#" onclick="tagSurround('[u]','[/u]','CTEXT'); return false;">&lt;u&gt;</a></li>
<li><a href="#" onclick="tagSurround('[sub]','[/sub]','CTEXT'); return false;">&lt;sub&gt;</a></li>
<li><a href="#" onclick="tagSurround('[sup]','[/sup]','CTEXT'); return false;">&lt;sup&gt;</a></li>
<li><a href="#" onclick="tagSurround('[br_/]','','CTEXT'); return false;">&lt;br /&gt;</a></li>
<li><a href="#" onclick="tagSurround('&amp;amp;','','CTEXT'); return false;">&amp;amp;</a></li>
<li><a href="#" onclick="tagSurround('&amp;nbsp;','','CTEXT'); return false;">nbsp</a></li>
<li><a href="#" onclick="tagSurround('&amp;ldquo;','&amp;rdquo;','CTEXT'); return false;">&ldquo;&nbsp;&rdquo;</a></li>
<li><a href="#" onclick="tagSurround('&amp;lsquo;','&amp;rsquo;','CTEXT'); return false;">&lsquo;&nbsp;&rsquo;</a></li>
  </ul>
</li>

<li><a style="width:4em;" href="#" onclick="return false;">Attribute</a>
  <ul>
<li><a href="#" onclick="tagSurround('_id=@@','','CTEXT'); return false;">[id]</a></li>
<li><a href="#" onclick="tagSurround('_class=@@','','CTEXT'); return false;">[class]</a></li>
<li><a href="#" onclick="tagSurround('_style=@@','','CTEXT'); return false;">[style]</a></li>
<li><a href="#" onclick="tagSurround('_name=@@','','CTEXT'); return false;">[name]</a></li>
<li><a href="#" onclick="tagSurround('_onclick=@@','','CTEXT'); return false;">[onclick]</a></li>
<li><a href="#" onclick="tagSurround('_alt=@@','','CTEXT'); return false;">[alt]</a></li>
  </ul>
</li>

<li><a style="width:4em;" href="#" onclick="return false;">Heading</a>
  <ul>
  <li><a href="#" 
     onclick="
       clearBlock();
       tagSurround('[h1]','[/h1]','CTEXT'); return false;"
     >&lt;h1&gt;</a></li>
  <li><a href="#" onclick="
       clearBlock();
       tagSurround('[h2]','[/h2]','CTEXT'); return false;">&lt;h2&gt;</a></li>
  <li><a href="#" onclick="
       clearBlock();
       tagSurround('[h3]','[/h3]','CTEXT'); return false;">&lt;h3&gt;</a></li>
  </ul>
</li>

<li><a style="width:4em;" href="#" onclick="return false;">Block</a>
  <ul>
  <li><a href="#" onclick="tagSurround('[div]','[/div]','CTEXT'); return false;">&lt;div&gt;</a></li>
  <li><a href="#" onclick="clearBlock(); tagSurround('[p]','[/p]','CTEXT'); return false;">&lt;p&gt;</a></li>
  <li><a href="#" onclick="tagSurround('[span_style=@@]','[/span]','CTEXT'); return false;">&lt;span&gt;</a></li>
  <li><a href="#" onclick="clearBlock(); tagSurround('[pre]','[/pre]','CTEXT'); return false;">&lt;pre&gt;</a></li>
  </ul>
</li>

<li><a href="#" onclick="tagSurround('[a_href=@@]','[/a]','CTEXT'); return false;">&lt;a&gt;</a></li>
<li><a href="#" onclick="tagSurround('[img_src=@$ADMIN_IMAGE_LINK_DIR/@_alt=@@_border@0@_/]','','CTEXT'); return false;">&lt;img&gt;</a></li>

<li><a style="width:2em;" href="#">List</a>
  <ul>
    <li><a href="#" onclick="tagSurround('[li]','[/li]','CTEXT'); return false;">&lt;li&gt;</a></li>
    <li><a href="#" onclick="tagSurround('[ul]^CR','^CR[/ul]','CTEXT'); return false;">&lt;ul&gt;</a></li>
    <li><a href="#" onclick="tagSurround('[ol]^CR','^CR[/ol]','CTEXT'); return false;">&lt;ol&gt;</a></li>
    <li><a href="#" onclick="
           compressSpaces();
           replaceWithinSelection('^CR^CR','^CR');
           replaceWithinSelection('^CR','[/li]^CR[li]');
           tagSurround('[li]','[/li]','CTEXT');
           return false;">Create List</a></li>
  </ul>
</li>

<li><a href="#" onclick="tagSurround('[?php^CR^CR','?]','CTEXT'); return false;">php</a></li>

<li><a style="width:5em;" href="#">Comment</a>
  <ul>
   <li><a href="#" onclick="tagSurround('[!--_','_-->','CTEXT'); return false;">&lt;!-- --&gt;</a></li>
   <li><a href="#" onclick="tagSurround('[!--_====================_','_====================_-->','CTEXT'); return false;">&lt;!-- ==== ==== --&gt;</a></li>
   <li><a href="#" onclick="tagSurround('/*_','_*/','CTEXT'); return false;">/* */</a></li>
  </ul>
</li>

<li><a style="width:3em;" href="#">Table</a>
  <ul>
   <li><a href="#" onclick="tagSurround('[table_width=@100%@_cellspacing=@0@_cellpadding=@0@_align=@center@]^CR[tbody]','^CR[/tbody]^CR[/table]','CTEXT'); return false;">&lt;table&gt;</a></li>
   <li><a href="#" onclick="tagSurround('[tr]','[/tr]','CTEXT'); return false;">&lt;tr&gt;</a></li>
   <li><a href="#" onclick="tagSurround('[th]','[/th]','CTEXT'); return false;">&lt;th&gt;</a></li>
   <li><a href="#" onclick="tagSurround('[td]','[/td]','CTEXT'); return false;">&lt;td&gt;</a></li>
   <li><a href="#" onclick="tagSurround('_align=@center@','','CTEXT'); return false;">[align="center"]</a></li>
   <li><a href="#" onclick="tagSurround('_align=@left@','','CTEXT'); return false;">[align="left"]</a></li>
   <li><a href="#" onclick="tagSurround('_align=@right@','','CTEXT'); return false;">[align="right"]</a></li>
   <li><a href="#" onclick="tagSurround('_colspan=@@','','CTEXT'); return false;">[colspan=""]</a></li>
   <li><a href="#" onclick="
           compressSpaces();
           replaceWithinSelection('[td','[th');
           replaceWithinSelection('[/td','[/th');
           return false;">&lt;td&gt; to &lt;th&gt;</a></li>

   <li><a href="#" onclick="
           compressSpaces();
           replaceWithinSelection('^T','[/td][td]');
           replaceWithinSelection('^CR','[/td][/tr]^CR[tr][td]');
           tagSurround('[table_width=@100%@_cellspacing=@0@_cellpadding=@0@_align=@center@]^CR[tbody]^CR[tr][td]','[/td][/tr]^CR[/tbody]^CR[/table]','CTEXT');
           return false;">Create Table</a></li>
  </ul>
</li>

<li><a style="width:6em;" href="#" onclick="return false;">Clean Up</a>
  <ul>
  <li><a href="#" onclick="compressSpaces(); return false;">Compress Space</a></li>
  <li><a href="#" onclick="clearBlock(); return false;">Remove Blocks</a>
  </li>
  <li><a href="#" onclick="
       replaceWithinSelection('^CR','^CR__');
       tagSurround('__','_','CTEXT');
       return false;">Indent 2</a>
  </li>
  </ul>
</li>

<li><a href="#" onclick="showId('FandR'); return false;">Replace</a></li>
<li><a href="#" onclick="showId('LINKS'); return false;">Links</a></li>

<li id="maximize"><a href="#" onclick="
          var editmenu = getId('editmenu');
          editmenu.style.position = 'fixed'; 
          editmenu.style.top = '0';
          editmenu.style.width = '100%';
          getId('pagewrapper').style.borderWidth = '0px';
          hideId('maximize');
          hideId('tabdiv');
          showId('minimize');
          showId('publish_button2');
          showId('publish_button3');
          hideId('mainheader');
          return false;">Maximize</a></li>

<li id="minimize" style="display:none;"><a href="#" onclick="
          var editmenu = getId('editmenu');
          editmenu.style.position = 'relative'; 
          getId('pagewrapper').style.borderWidth = '5px';
          editmenu.style.width = '95%';
         
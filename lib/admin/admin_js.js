// -------------- ADMIN JAVASCRIPT ROUTINES -------------

function setTab(num){
  hideGroupExcept('mainpage',num);
  setClassGroup('tablink',num,'tablink','tabselect');
}

function getkey(e){
  if (window.event) return window.event.keyCode;
  else if (e) return e.which;
  else return null;
}

function changeText(myItem,newText){
  document.getElementById(myItem).innerHTML=newText;
}


function updatePreview(){
  if(needEditor){ var outText=document.getElementById('CTEXT').value; }
  else {var outText=getContentModified('CTEXT');}
  document.getElementById('PREVIEW').innerHTML=outText;
}


function showInfo(){
  toggleDisplay('infobox');
}


//--------EDIT MENU ITEMS-------
function removeExtraSpace(mytext){
  mytext = mytext.replace( /( ){2,}/g," ");
  return mytext;
}

function Trim(sString){
  while (sString.substring(0,1) == ' ') sString = sString.substring(1, sString.length);
  while (sString.substring(sString.length-1, sString.length) == ' ') sString = sString.substring(0,sString.length-1);
  return sString;
}


function compressSpaces(){
  var input = document.getElementById('CTEXT');
  if (input.setSelectionRange) {
    var selectionStart = input.selectionStart;
    var selectionEnd = input.selectionEnd;
    var myText = input.value.substring(selectionStart,selectionEnd);
    var replaceString = Trim(myText.replace( /( ){2,}/g," "));
    replaceString = replaceString.replace( /\n /g,"\n");
    input.value = input.value.substring(0, selectionStart)+ replaceString+ input.value.substring(selectionEnd);
    input.setSelectionRange(selectionStart,selectionStart + replaceString.length);
    showId('contentmodifed');
  }
}


function clearBlock(){
  var input = document.getElementById('CTEXT');
 if (input.setSelectionRange) {
    var selectionStart = input.selectionStart;
    var selectionEnd   = input.selectionEnd;
    var replaceString  = input.value.substring(selectionStart,selectionEnd);
    var blockStrings = ['h1','/h1','h2','/h2','h3','/h3','h4',
        '/h4','h5','/h5','h6','/h6','div','/div','p','/p'];
    for (var n = 0; n < blockStrings.length; n++){
       replaceString = replaceString.replace( new RegExp( '<'+ blockStrings[n] + '>', "g" ), '' );
    }
    input.value = input.value.substring(0, selectionStart)+ replaceString+ input.value.substring(selectionEnd);
    input.setSelectionRange(selectionStart,selectionStart + replaceString.length);
    showId('contentmodifed');
  }
}


function replaceText(){
 var input = document.getElementById('CTEXT');
 var findText = document.getElementById('find').value;
 var replaceText = document.getElementById('replace').value;

  findText = findText.replace(/@/g,'"');
  findText = findText.replace(/\[/g,'<');
  findText = findText.replace(/\]/g,'>');
  findText = findText.replace(/\^CR/g,"\n");
  findText = findText.replace(/_/g,' ');

  replaceText = replaceText.replace(/@/g,'"');
  replaceText = replaceText.replace(/\[/g,'<');
  replaceText = replaceText.replace(/\]/g,'>');
  replaceText = replaceText.replace(/\^CR/g,"\n");
  replaceText = replaceText.replace(/_/g,' ');

 input.value = input.value.replace( new RegExp( findText, "g" ), replaceText );
 showId('contentmodifed');
}

function replaceWithinSelection(findText,replaceText){
  var input = document.getElementById('CTEXT');
  findText = findText.replace(/@/g,'"');
  findText = findText.replace(/\[/g,'<');
  findText = findText.replace(/\]/g,'>');
  findText = findText.replace(/\^CR/g,"\n");
  findText = findText.replace(/\^T/g,"\t");
  findText = findText.replace(/_/g,' ');

  replaceText = replaceText.replace(/@/g,'"');
  replaceText = replaceText.replace(/\[/g,'<');
  replaceText = replaceText.replace(/\]/g,'>');
  replaceText = replaceText.replace(/\^CR/g,"\n");
  replaceText = replaceText.replace(/\^T/g,"\t");
  replaceText = replaceText.replace(/_/g,' ');

  if (input.setSelectionRange) {
    var selectionStart = input.selectionStart;
    var selectionEnd = input.selectionEnd;
    var myText = input.value.substring(selectionStart,selectionEnd);
    var replaceString = myText.replace( new RegExp( findText, "g" ), replaceText );
    input.value = input.value.substring(0, selectionStart)+ replaceString+ input.value.substring(selectionEnd);
    input.setSelectionRange(selectionStart,selectionStart + replaceString.length);
  }
}



function tagSurround(tag1,tag2,inputID){
var input = document.getElementById(inputID);

  tag1 = tag1.replace(/@/g,'"');
  tag1 = tag1.replace(/\[/g,'<');
  tag1 = tag1.replace(/\]/g,'>');
  tag1 = tag1.replace(/\^CR/g,"\n");
  tag1 = tag1.replace(/_/g,' ');
  tag1 = tag1.replace(/~/g,'_');


  tag2 = tag2.replace(/@/g,'"');
  tag2 = tag2.replace(/\[/g,'<');
  tag2 = tag2.replace(/\]/g,'>');
  tag2 = tag2.replace(/\^CR/g,"\n");
  tag2 = tag2.replace(/_/g,' ');
  tag2 = tag2.replace(/~/g,'_');


  if (input.setSelectionRange) {
    var selectionStart = input.selectionStart;
    var selectionEnd = input.selectionEnd;

    var myText = input.value.substring(selectionStart,selectionEnd);
    if (tag2=='') myText ='';
    var replaceString = tag1 + myText + tag2;
    input.value = input.value.substring(0, selectionStart)+ replaceString+ input.value.substring(selectionEnd);
    input.setSelectionRange(selectionStart + replaceString.length,selectionStart + replaceString.length);
    showId('contentmodifed');
  }
  else if (document.selection) {
    var range = document.selection.createRange();
    if (range.parentElement() == input) {
      var isCollapsed = range.text == '';

      var myText = range.text;
      if (tag2=='') myText ='';
      var replaceString = tag1 + myText + tag2;
      range.text = replaceString;
      if (!isCollapsed)  {
        range.moveStart('character', -replaceString.length);
        range.select();
      }
    }
  }
}

var needEditor = true;
var HTMLedit   = false;

function getContentModified(editor_id) {
    if (typeof(editor_id) != "undefined")
        tinyMCE.selectedInstance = tinyMCE.getInstanceById(editor_id);
    if (tinyMCE.selectedInstance) {
        var html = tinyMCE._cleanupHTML(tinyMCE.selectedInstance, tinyMCE.selectedInstance.getDoc(), tinyMCE.settings, tinyMCE.selectedInstance.getBody(), false, true);
        return html;
    }
    return null;
}


function SetEditor(){
  if(needEditor){
    StartEditor();
    needEditor=false;
    hideId('editmenu');
    HTMLedit = true;
    changeText('HTMLcontentButton','Edit&nbsp;Text');
  }
  else{
    HTMLedit = !HTMLedit;
    tinyMCE.execCommand('mceToggleEditor',false,'CTEXT');
    if(HTMLedit){
      changeText('HTMLcontentButton','Edit&nbsp;Text');
      hideId('editmenu');
    }
    else {
      changeText('HTMLcontentButton','Edit&nbsp;Content&nbsp;(HTML)');
      showId('editmenu');
    }
  }
  //window.location='#LCONTENT';
}

function setAutoTextAreaHeight(id){
  var myelem = getId(id);
  if(myelem){
    if (myelem.scrollHeight > myelem.offsetHeight) myelem.style.height = myelem.scrollHeight + 50 + 'px';
  }
}

//------------------------drag item--------------------------
//  http://www.webtoolkit.info/

var doDrag = false;

var DragHandler = {
	// private property.
	_oElem : null,


	// public method. Attach drag handler to an element.
	attach : function(oElem) {
		oElem.onmousedown = DragHandler._dragBegin;

		// callbacks
		oElem.dragBegin = new Function();
		oElem.drag = new Function();
		oElem.dragEnd = new Function();

		return oElem;
	},


	// private method. Begin drag process.
	_dragBegin : function(e) {
        if (doDrag == false) return;
		var oElem = DragHandler._oElem = this;

		if (isNaN(parseInt(oElem.style.left))) { oElem.style.left = '0px'; }
		if (isNaN(parseInt(oElem.style.top))) { oElem.style.top = '0px'; }

		var x = parseInt(oElem.style.left);
		var y = parseInt(oElem.style.top);

		e = e ? e : window.event;
		oElem.mouseX = e.clientX;
		oElem.mouseY = e.clientY;

		oElem.dragBegin(oElem, x, y);

		document.onmousemove = DragHandler._drag;
		document.onmouseup = DragHandler._dragEnd;
		return false;
	},


	// private method. Drag (move) element.
	_drag : function(e) {
        if (doDrag == false) return;
		var oElem = DragHandler._oElem;

		var x = parseInt(oElem.style.left);
		var y = parseInt(oElem.style.top);

		e = e ? e : window.event;
		oElem.style.left = x + (e.clientX - oElem.mouseX) + 'px';
		oElem.style.top = y + (e.clientY - oElem.mouseY) + 'px';

		oElem.mouseX = e.clientX;
		oElem.mouseY = e.clientY;

		oElem.drag(oElem, x, y);

		return false;
	},


	// private method. Stop drag process.
	_dragEnd : function() {
		var oElem = DragHandler._oElem;

		var x = parseInt(oElem.style.left);
		var y = parseInt(oElem.style.top);

		oElem.dragEnd(oElem, x, y);

		document.onmousemove = null;
		document.onmouseup = null;
		DragHandler._oElem = null;
	}

};

function adminOnload(){
    if(getId('TTEXT')) setAutoTextAreaHeight('TTEXT');
    if(getId('CTEXT')) {
        setAutoTextAreaHeight('CTEXT');
        var drag1 = DragHandler.attach(getId('LINKS'));
        var drag2 = DragHandler.attach(getId('FandR'));
    }
    setTab(1);
}

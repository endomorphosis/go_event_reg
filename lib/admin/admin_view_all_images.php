<?php
//==========================================================
//                    VIEW ALL IMAGES
//==========================================================
if ($DELETE_IMAGE) {
    if(file_exists(ADMIN_IMAGE_DIR."/$DELETE_IMAGE")) {
       unlink(ADMIN_IMAGE_DIR."/$DELETE_IMAGE");
    } else {
        AddError('IMAGE: '.ADMIN_IMAGE_LINK_DIR."/$DELETE_IMAGE -- NOT FOUND");
    }
    $VG=1;
}

if ($RENAMEFILE_IMAGE) {
    if (!(file_exists(ADMIN_IMAGE_DIR."/$NEWNAME_IMAGE"))) {
        $filename1  = ADMIN_IMAGE_DIR."/$OLDNAME_IMAGE";
        $filename2  = ADMIN_IMAGE_DIR."/$NEWNAME_IMAGE";
        rename($filename1,$filename2);
        LogUpdate(ADMIN_USERNAME,'Rename Image File',"$OLDNAME_IMAGE - $NEWNAME_IMAGE");
        $VG=1;
    } else {
        $RENAME_IMAGE=$OLDNAME_IMAGE; $DuplicateFile=1;
    }
}

if ($RENAME_IMAGE) {
    print <<<RENAMEIMAGELABEL
  <form method=post action="$ADMIN_FILE">
  <input type="hidden" name="OLDNAME_IMAGE" value="$RENAME_IMAGE">
  <table align="center" border="0" cellpadding="3" id="rename_image">
    <tr>
      <td align="right">Old Filename:</td><td>$RENAME_IMAGE</td>
    </tr>
    <tr>
      <td align="right">New Filename:</td>
      <td>
        <input type="text" name="NEWNAME_IMAGE" size="30" value="$RENAME_IMAGE" />
      </td>
    </tr>
      <td>
      </td>
      <td>
        <input type="Submit" name="RENAMEFILE_IMAGE" value="Rename File" />
      </td>
    </tr>
  </table>
RENAMEIMAGELABEL;
}

if ($DuplicateFile) {
    print '<p class="duplicatefile">File: <b>'.$NEWNAME_IMAGE.'</b> already exists.</p>';
    print '</form>';
}

if ($VG=='1') {
    print '<table id="viewallimages" cellpadding="5" align="center" width="95%">';
    print '    <tr>
        <th class="header" align="center" colspan="2">
           <span class="subheader">Image Files</span>
        </th>
    </tr>';

    $files = GetDirectory(ADMIN_IMAGE_DIR,ADMIN_IMAGE_TYPES);

    $count=0;
    foreach ($files as $fi) {
        if (!(eregi('.LCK',$fi))) {
            $Lfilename = ADMIN_IMAGE_LINK_DIR."/$fi";
            $filename = ADMIN_IMAGE_DIR."/$fi";
            $t=date("m\/d\/Y",filemtime($filename));
            list($width, $height, $type, $attr) = getimagesize($filename);
            $fsize = number_format (filesize($filename)/1024,1).'KB';
            $deletelink = "$ADMIN_FILE?DELETE_IMAGE=$fi";
            $count++;

            if($width > $height){
                $thumbwidth  = min(200,$width);
                $thumbheight = round($thumbwidth * $height/$width);
            } else {
                $thumbheight  = min(200,$height);
                $thumbwidth = round($thumbheight * $width/$height);
            }

            $margintop = $thumbheight+6;

            if (($width > 200) or ($height
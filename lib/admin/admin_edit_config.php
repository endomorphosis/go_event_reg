<script type="text/javascript">

function setNewPath(sitestr,id) {
  var dir = getId(id);
  var dirstr = dir.value;
  var x = dirstr.lastIndexOf('/');
  if (x >= 0) dirstr = dirstr.substr(x+1);
  if (sitestr != '/') dir.value = sitestr + '/' + dirstr;
}

function configDirUpdate() {
  var sitestr = getId('FORM_sitedir').value;
  setNewPath(sitestr,'FORM_contentdir');
  setNewPath(sitestr,'FORM_imagedir');
  setNewPath(sitestr,'FORM_archivedir');
  setNewPath(sitestr,'FORM_listdir');
  setNewPath(sitestr,'FORM_logdir');
  setNewPath(sitestr,'FORM_csspath');
}
</script>

<?php
if (ADMIN_LEVEL<9) return;

$configfile = '../config/siteconfig.php';
require_once "$LIB/form_helper.php";
include $configfile;


$SUBMIT = Post('SUBMIT');
$ErrorMsg = '';

$TitleTemplate = '<br class="formbreak" /><div class="formtitle">@@VAR@@:</div>'."\n";

echo '
<div id="configform">
<h1>Site Configuration</h1>';

// Config Information to Form Info

if (empty($SUBMIT)) {
foreach ($SITECONFIG as $key=>$value) {
  if ($key == 'companyaddress') $value = str_replace("<br />","",$value);
  elseif ($key == 'docdirs') {
    $new = '';
    foreach ($value as $row) $new .= "$row\n";
    $value = $new;
  } elseif ($key == 'emaillist') {
      $new = '';
      foreach ($value as $newkey=>$newvalue) if (!empty($newkey))$new .= "$newkey|".$newvalue."\n";
      $value = $new;
  } elseif ($key == 'emailtopics') {
    $new = '';
    foreach ($value as $row) $new .= "$row\n";
    $value = $new;
  }
  $_POST[$FormPrefix.$key] = $value;
}

if (empty($SITECONFIG['libdir'])) $_POST[$FormPrefix.'libdir'] = '/'.basename(dirname(dirname(__FILE__)));

if (!empty($DB_INFO)) {
  foreach ($DB_INFO as $key=>$value) $_POST[$FormPrefix.'DB_'.$key] = $value;
}

if (!empty($USER_ARRAY)) {
  $new = '';
  foreach ($USER_ARRAY as $key=>$value) {
    $new .= "$key|{$value['password']}|{$value['level']}\n";
  }
  $_POST[$FormPrefix.'userarray'] = $new;
}

if (!empty($SITE_CUSTOM)) {
  $new = '';
  foreach ($SITE_CUSTOM as $key=>$value)  $new .= "$key|$value\n";
  $_POST[$FormPrefix.'custom'] = $new;
}

}

$Mask_Dir   = '^[a-zA-Z0-9\/_\.\-]+$';
$Mask_Ext   = '^[a-zA-Z0-9_\-\.]+$';

$FormDataArray = array(
"form|$THIS_PAGE_QUERY|post",
"h3|Company Information",
"text|Site Name|sitename|Y|40|80||$Mask_General_Line",
"text|Company Name|companyname|Y|40|80||$Mask_General_Line",
"text|Short Company Name|shortcompanyname|Y|40|80||$Mask_General_Line",
"html|Company Address|companyaddress|N|50|6|wrap=\"off\"",

"h3|Mail Settings",
"html|Email List<br />(<i>site name</i>&#124;<i>email</i>)|emaillist|N|70|5|wrap=\"off\"|$Mask_General",
"textarea|Email Topics|emailtopics|N|30|5|wrap=\"off\"|$Mask_General",
"text|Email Subject Prefix|emailsubjectprefix|N|40|80||$Mask_General_Line",
"checkbox|Send Plain Text Messages|emailplaintext||1|0",
"checkbox|Use Swift Mailer (experimental)|emailsendswift||1|0",
"checkbox|Add Contact Address|contactaddress||1|0",

"h3|Site Directories",
"text|Page Directory<br />(Used in URL)|pagedir|N|30|80||$Mask_Dir",
"text|Site Directory|sitedir|N|30|80|onkeyup=\"configDirUpdate();\"|$Mask_Dir",
"text|Library Directory|libdir|Y|30|80||$Mask_Dir",
"text|Content Directory|contentdir|Y|30|80||$Mask_Dir",
"text|Image Directory|imagedir|Y|30|80||$Mask_Dir",
"text|Archive Directory|archivedir|Y|30|80||$Mask_Dir",
"text|List Directory|listdir|Y|30|80||$Mask_Dir",
"text|Log Directory|logdir|Y|30|80||$Mask_Dir",
"text|Site CSS Path|csspath|Y|40|80||$Mask_Dir",
"text|TinyMCE Path (HTML Editor)|tinymcepath|Y|40|80||$Mask_Dir",
"textarea|Doc Directories|docdirs|N|30|5|wrap=\"off\"|$Mask_General",

"h3|Site File Configuration",
"text|Title File Extension<br />(should not change)|titlestr|Y|10|30||$Mask_Ext",
"text|Content File Extension<br />(should not change)|contentstr|Y|10|30||$Mask_Ext",
"text|View Page Extension<br />(example '.html')|extension|N|10|30||$Mask_Ext",

"h3|Admin Configuration",
"checkbox|Use SSL Admin|usehttps||1|0",
"checkbox|Want Drafts|wantdraft||1|0",
"checkbox|Want HTML Editor|wanthtml||1|0",
"checkbox|Want Edit-Area<br />(experimental)|wanteditarea||1|0",
"text|Content Width<br />(for Preview)|contentwidth|N|4|4||$Mask_Integer",

"h3|Users",
"textarea|Users<br /><i>Username</i>&#124;<i>Password</i>&#124;<i>Level(1-9)</i>|userarray|Y|40|5|wrap=\"off\"|$Mask_General",

"h3|Database",
"text|Database Name|DB_NAME|N|20|40||$Mask_Password",
"text|Database Host|DB_HOST|N|20|40||$Mask_Password",
"text|Database User|DB_USER|N|20|40||$Mask_Password",
"text|Database Password|DB_PASS|N|20|40||$Mask_Password",

"h3|Custom",
"html|Custom Variables<br /><i>Variable</i>&#124;<i>Value</i>|custom|N|70|8|wrap=\"off\"|$Mask_General",

"submit|Update|SUBMIT",
'endform'
);


if ($SUBMIT) {
  $FormArray = ProcessForm($FormDataArray,$table,'align="center" style="background-color:#888;"','align="right"','style="background-color:#fff;"',$ErrorMsg);
  if (empty($ErrorMsg)) {  
  $out = '$'."SITECONFIG = array();\n";
  foreach ($FormArray as $key=>$value) {
    $haveDB = false;
    
    if ($key=='companyaddress') {
      $value = nl2br($value);
    
    } elseif ($key=='emaillist') {
       $array = explode("\n",$value);
       $new = "array(\n";
       foreach ($array as $row) {
          $row = trim($row);
          list($newkey,$newvalue) = explode('|',$row);
          $new .= "'$newkey' => '$newvalue',\n";
       }
       $value = substr($new,0,-2).')';
    
    } elseif ($key=='emailtopics') {
        $new = "array(\n";
        $count = 0; 
        $array = explode("\n",$value);
        if (count($array>0)) {
          foreach ($array as $row) {
            $row = trim($row);
            if (!empty($row)) { $new .= qqn("`$row`,"); $count++; }
          }
        }
        $value = ($count>0)? substr($new,0,-2).')' : substr($new,0,-1).')';

    } elseif ($key=='docdirs') {
        $new = "array(\n";
        $count = 0; 
        $array = explode("\n",$value);
        if (count($array>0)) {
          foreach ($array as $row) {
            $row = trim($row);
            if (!empty($row)) { $new .= qqn("`$row`,"); $count++; }
          }
        }
        $value = ($count>0)? substr($new,0,-2).')' : substr($new,0,-1).')';


        
    } elseif ($key=='userarray') {
       $array = explode("\n",$value);
       $new = "array(\n";
       foreach ($array as $row) {
          $row = trim($row);
          list($newuser,$newpass,$newlevel) = explode('|',$row);
          $new .= "'$newuser' => array('password'=>'$newpass','level'=>'$newlevel'),\n";
       }
       $value = substr($new,0,-2).')';
       
    } elseif (($key=='DB_NAME') or ($key=='DB_HOST') or ($key=='DB_USER') or ($key=='DB_PASS')) {
        $haveDB = true;
        
    } elseif ($key=='custom') {
       $array = explode("\n",$value);
       $SiteCustom = '$SITE_CUSTOM = array();'."\n";
       foreach ($array as $row) {
          $row = trim($row);
          if (!empty($row)) {
            list($newkey,$newvalue) = explode('|',$row);
            $SiteCustom .= '$'."SITE_CUSTOM['$newkey'] = \"$newvalue\";\n";
          }
       }
    }
    if (!$haveDB and ($key != 'custom')) {
        if ($key=='userarray') $out .= '$'.qqn("USER_ARRAY = $value;");
        elseif ((substr($value,0,5)=='array') or ($value=='0') or ($value=='1')) {
             $out .= '$'.qqn("SITECONFIG['$key'] = $value;");
        } else $out .= '$'.qqn("SITECONFIG['$key'] = `$value`;");
    }
  } 
  
  if (!empty($FormArray['DB_NAME'])) {
    $DBstr = '$'."DB_INFO = array('NAME'=>'@NAME@','HOST'=>'@HOST@','USER'=>'@USER@','PASS'=>'@PASS@');\n";
    $DBstr = str_replace(array('@NAME@','@HOST@','@USER@','@PASS@'),
             array($FormArray['DB_NAME'],$FormArray['DB_HOST'],$FormArray['DB_USER'],$FormArray['DB_PASS']),$DBstr);
  } else $DBstr = '$DB_INFO = array();'."\n";
            
  $out .= $DBstr;
  $out .= $SiteCustom;

  //--------------------- output file -----------------------
  $config = file_get_contents($configfile);
  $end  = TextBetween("//---STARTCUSTOM---","\n?>",$config);
  $config = "<?php\n".$out."\n\n//---STARTCUSTOM---".$end."\n?>\n";
  write_file($configfile,$config);
  echo "<h2>Configuration file has been updated!</h2>";
}
}

if ((!$SUBMIT) or ($ErrorMsg)) {
  WriteError($ErrorMsg);
  echo OutputForm($FormDataArray,$SUBMIT);
}  

echo "</div>";

?>
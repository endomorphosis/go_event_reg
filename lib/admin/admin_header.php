<?php


if ($F) {
  $dateinfo = qqn("<form method=`post` action=`$ADMIN_PAGE_QUERY_LINK`>
  <input type=`hidden` name=`TITLEDATE` value=`$TitleDate` />
  <input type=`hidden` name=`CONTENTDATE` value=`$ContentDate` />");
} else $dateinfo = '';


$errorout = ($ERROR_MSG)? qqn("<div id=`errormsg`>$ERROR_MSG</div>") : '';


$dropdown ='';
if ($F=='') { $dropdown .= qqn("<option value=`$ADMIN_FILE`>----Choose File----</option>");}
$count=0;

$space = '            ';
//-----------Process Standard Files----------
  foreach ($files as $fi) {
  $count++;
  $QS="F=$fi";
  $selected = ($F==$fi)? ' selected="selected"' : '';
  $m=explode('/',$fi);
  if (count($m)>1) {
    $fi1='';
    for($i=0;$i<count($m)-1;$i++) {$fi1.=$m[$i].':';}
    $fi=$fi1.'&nbsp;&nbsp;&nbsp;'.$m[count($m)-1];
    }
  if (substr($fi,0,1)=='_') $dropdown .= qqn("$space<option class=`draft` value=`$ADMIN_FILE?$QS`$selected>$count. Draft: $fi</option>");
      else $dropdown .= qqn("$space<option class=`filedropdown` value=`$ADMIN_FILE?$QS`$selected>$count. $fi</option>");
  }

//-----------Process Special Files----------

if (ADMIN_LEVEL==9) {
    $SpecialFiles = GetDirectory("$SITE_ROOT/common");
    foreach ($SpecialFiles as $fi) {
        $count++;
        $OPTtype = (strpos($fi,'.htm')!==false)? 'CT' : 'C';
        $QS="F=../common/$fi$SV"."OPT=$OPTtype$SV"."SP=1";
        $selected = ($F=="../common/$fi")? ' selected="selected"' : '';
        $dropdown .= qqn("$space<option class=`commonfile` value=`$ADMIN_FILE?$QS`$selected>$count. COMMON: $fi</option>");
    }    
    $SpecialFiles = GetDirectory("$SITE_ROOT/helper");
    foreach ($SpecialFiles as $fi) {
        $count++;
        $QS="F=../helper/$fi$SV"."OPT=C$SV"."SP=1";
        $selected = ($F=="../helper/$fi")? ' selected="selected"' : '';
        $dropdown .= qqn("$space<option class=`helperfile` value=`$ADMIN_FILE?$QS`$selected>$count. HELPER: $fi</option>");
    }    

    $SpecialFiles = GetDirectory("$SITE_ROOT/lists");
    foreach ($SpecialFiles as $fi) {
        $count++;
        $QS="F=../lists/$fi$SV"."OPT=C$SV"."SP=1";
        $selected = ($F=="../lists/$fi")? ' selected="selected"' : '';
        $dropdown .= qqn("$space<option class=`listfile` value=`$ADMIN_FILE?$QS`$selected>$count. LISTS: $fi</option>");
    }       
  
    
    $special = file(ADMIN_FILES_DIR.'/special.dat');
    foreach ($special as $i) {
        $i=trim($i);
        $m=explode('|',$i);
        $content=$m[1];
        $count++;
        $QS="F=$content$SV"."OPT=$m[2]$SV"."SP=1";
        $selected = ($F==$m[1])? ' selected="selected"' : '';
        
        if (strPos($m[1],'./common/')!==false) $class = 'commonfile';
        elseif (strPos($m[1],'./page.php')!==false) $class = 'controllerfile';
        elseif (strPos($m[1],'/siteconfig.
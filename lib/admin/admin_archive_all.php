
<div style="margin:1em; background-color:#fff; padding:2em; text-align:left;">
<h1>Archiving All Files &mdash; Folders = (content, common, lists, helper)</h1>
<ol style="text-align:left; font-size:1.2em;">
<?php

//----------ARCHIVE ALL FILES (/content and /common)-------------

$ContentFiles = GetDirectory(ADMIN_CONTENT_DIR,'');
$CommonFiles  = GetDirectory("$SITE_ROOT/common",'');
$ListFiles    = GetDirectory("$SITE_ROOT/lists",'');
$HelperFiles  = GetDirectory("$SITE_ROOT/helper",'');
natcasesort($ContentFiles);
natcasesort($CommonFiles);
natcasesort($ListFiles);
natcasesort($HelperFiles);
$count=0;

//------------ARCHIVE CONTENT FILES--------------
foreach ($ContentFiles as $AF) {
    $filename = ADMIN_CONTENT_DIR."/$AF";
    $filedate = date("Ymdhi",filemtime($filename));
    $AF = str_replace('/','@',$AF);
    $filename2 = "$ROOT{$SITECONFIG['archivedir']}/$AF"."_$filedate.php";
    if (!file_exists($filename2)) {
        copy($filename,$filename2);
        chmod($filename2, 0766);
        $out ="<li>$filename --> /archive/$AF"."_$filedate.php</li>\n";
        echo str_replace($ROOT,'',$out);
        $count++;
    }
}
foreach ($CommonFiles as $AF) {
    $filename = "$SITE_ROOT/common/$AF";
    $filedate=date("Ymdhi",filemtime($filename));
    $AF = str_replace('/','@',$AF);
    $filename2="$ROOT{$SITECONFIG['archivedir']}/common@$AF"."_$filedate.php";
    if (!file_exists($filename2)) {
        copy($filename,$filename2);
        chmod($filename2, 0766);
        $out ="<li>$filename --> $filename2</li>\n";
        echo str_replace($ROOT,'',$out);
        $count++;
    }
}

foreach ($ListFiles as $AF) {
    $filename = "$SITE_ROOT/lists/$AF";
    $filedate=date("Ymdhi",filemtime($filename));
    $AF = str_replace('/','@',$AF);
    $filename2="$ROOT{$SITECONFIG['archivedir']}/lists@$AF"."_$filedate.php";
    if (!file_exists($filename2)) {
        copy($filename,$filename2);
        chmod($filename2, 0766);
        $out ="<li>$filename --> $filename2</li>\n";
        echo str_replace($ROOT,'',$out);
        $count++;
    }
}

foreach ($HelperFiles as $AF) {
    $filename = "$SITE_ROOT/helper/$AF";
    $filedate=date("Ymdhi",filemtime($filename));
    $AF = str_replace('/','@',$AF);
    $filename2="$ROOT{$SITECONFIG['archivedir']}/helper@$AF"."_$filedate.php";
    if (!file_exists($filename2)) {
        copy($filename,$filename2);
        chmod($filename2, 0766);
        $out ="<li>$filename --> $filename2</li>\n";
        echo str_replace($ROOT,'',$out);
        $count++;
    }
}

//archive controller
    $filename = "$SITE_ROOT/page.php";
    $filedate=date("Ymdhi",filemtime($filename));
    $filename2="$ROOT{$SITECONFIG['archivedir']}/page.php"."_$filedate.php";
    if (file_exists($filename) and !file_exists($filename2)) {

<?PHP
//  WEBSITE ADMINISTRATION PROGRAM
//  Admin Authentication

//===========================LOGOUT=======================
if ($LOGOUT) {
    if (isset($_SESSION['AdminUsername'])) {
        LogUpdate($_SESSION['AdminUsername'],'Log-out','');        
    }
    
    unset($_SESSION['AdminUsername'],$_SESSION['AdminLoginOK'],$_SESSION['AdminLevel']);

    if (isset($_COOKIE[session_name()])) {
       setcookie(session_name(), '', time()-42000, '/');
    }

    print <<<LOUT
$DOCTYPE_XHTML
<head>
  <title>{$SITECONFIG['sitename']} - Website Administration</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  <link rel="stylesheet" type="text/css" href="/lib/admin/admin_style.css" />
</head>
<body class="admin">
<div id="wrapper">
<div id="login">
<p id="loginheading">{$SITECONFIG['sitename']}<br />Website Administration</p>
<h1>Good Bye!</h1>
<p>
  <input type="button" value="Log-in" onclick="window.location='$THIS_PAGE';" />
</p>
</div>
</div>
</body>
</html>
LOUT;

    exit;
}

//=======================AUTHENTICATION=========================

if ($LOGIN and $USER and $PASS) {
    $USER = strtolower($USER);
    $userpass  = (array_key_exists($USER, $USER_ARRAY))? $USER_ARRAY[$USER]['password'] : '';
    $userlevel = (array_key_exists($USER, $USER_ARRAY))? $USER_ARRAY[$USER]['level'] : '';
    
    if ($PASS === $userpass) {
        $NeedAuth = false;
        $_SESSION['AdminLevel']    = $userlevel;
        $_SESSION['AdminUsername'] = $USER;
        $_SESSION['AdminLoginOK'] = 'ok';
    } 
}


define('ADMIN_LEVEL',Session('AdminLevel'));
define('ADMIN_USERNAME',Session('AdminUsername'));

if (empty($_SESSION['AdminLoginOK']) or ((!empty($_SESSION['AdminLoginOK'])) and  ($_SESSION['AdminLoginOK'] != 'ok'))) {
    print <<<AUTH
$DOCTYPE_XHTML
  <head>
    <title>{$SITECONFIG['sitename']} - Website Administration</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" type="text/css" href="/lib/admin/admin_style.css" />
    <script type="text/javascript">
/* <![CDATA[ */
      function getId(id) {return document.getElementById(id);}
      var MyPassword = '<input type="@@TYPE@@" id="PASS" name="PASS" size="12" value="@@VALUE@@" />';
      function changePassText() {
    
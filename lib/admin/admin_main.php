<?PHP
//  WEBSITE ADMINISTRATION PROGRAM

function ReadConfigFile() {
  global $SITECONFIG,$USER_ARRAY;
  require_once '../config/siteconfig.php';
}

$LIB = dirname(dirname(__FILE__));

//============INITIALIZE VARIABLES=========

require_once $LIB.'/tools.php';

$VIEWPAGE = Get('VIEWPAGE');
if ($VIEWPAGE) {
    include "admin_view_page.php";
    exit;
}    

ReadConfigFile();

ini_set('display_errors','1');
session_start();


if (!empty($SITECONFIG['usehttps']) and empty($HTTPS)) {
   header("Location:$HTTPS_URI");
}

$admin_inc = "$LIB/admin";
include "$admin_inc/admin_updates.php";
include "$admin_inc/admin_vars.php";

include "$admin_inc/admin_log.php";
include "$admin_inc/admin_auth.php";

if ($TINYMCE_IMAGE_LINKS or $TINYMCE_FILE_LINKS) {
    include "$admin_inc/admin_tinymce_links.php";
    exit;
}

if ($PHP=='1') {phpinfo(); exit;}
if ($SITELOG=='1') {include "$admin_inc/admin_viewlogs.php"; exit;}

if (ADMIN_LEVEL == 9) {
  // --------- diagnostics routines --------
    if ($ADMININFO == '1') {
        $_SESSION['ADMININFO'] = 1; 
        AddFlash('Admin Info Set');
    }
    if ($ADMININFO == '0') {
        $_SESSION['ADMININFO'] = 0;
        AddFlash('Admin Info Removed');
    }  
}


include "$admin_inc/admin_process_file.php";

//=====================START PAGE BODY=====================
if (!empty($PRINT)) {include "$admin_inc/admin_print_page.php"; exit;}

$need_title   = (($F) and (($SP==0) or (($SP==1) and !(strpos($OPT,'L')===false))));
$need_content = ((($F) and ($SP=='')) or
                 (($SP==1) and (strpos($OPT,'P')!==false)) or
                 (($SP==1) and (strpos($OPT,'C')!==false)));
$need_preview = ( (($F) and ($SP=='')) or
                 (($SP==1) and (strpos($OPT,'T')!==false)) or
                 (($SP==1) and (strpos($OPT,'P')!==false))  );

require_once "$admin_inc/admin_head.php";
require_once "$admin_inc/admin_header.php";

if ($F) {
  $QS1="F=$F";
  if ($OPT) {$QS1.=$SV."OPT=$OPT";}
  if ($SP) {$QS1.=$SV."SP=1";}
}

if ($need_preview and $F) include "$admin_inc/admin_preview.php";
if ($need_title and $F)   include "$admin_inc/admin_title_edit.php";
if ($need_content and !empty($F)) include "$admin_inc/admin_content_edit.php";

if ($IU or $IMAGEUPLOAD or $DU or $DOCUMENTUPLOAD) include "$admin_inc/admin_upload.php";
if ($NEW) include "$admin_inc/admin_new_page.php";
if ($FIND or $SEARCHSTR) include "$admin_inc/admin_search.php";
if ($FINDSTR or $REPLACE) include "$admin_inc/admin_find_and_replace.php";
if ($GOOGLE or $SITEMAP or $WRITESITEMAP or $GENERATESITEMAP) include "$admin_inc/admin_google_sitemap.php";

if ($DELETE_IMAGE or $RENAMEFILE_IMAGE or $RENAME_IMAGE or $VG) include "$admin_inc/admin_view_all_images.php";
if ($VD or $
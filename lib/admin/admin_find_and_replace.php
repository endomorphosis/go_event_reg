<?php
//======================REPLACE IN FILES==========================

printqn("<form method=`post` action=`$ADMIN_FILE`>");
?>

<table align="center" style="background-color:#eee;">
<tr><td>
<div style="float:left; width:150px; text-align:right; font-weight:bold;">Find:</div>
<div style="margin-left:160px;">
<input class="formitem" type="text" name="FINDSTR" value="<?php echo htmlentities($FINDSTR) ?>" size="60" /></div>

<div style="float:left; width:150px; text-align:right; font-weight:bold;">Replace&nbsp;With:</div>
<div style="margin-left:160px;">
<input class="formitem" type="text" name="REPLACESTR" value="<?php echo htmlentities($REPLACESTR) ?>" size="60" /></div>

<?php
print '<div style="margin-left:160px;">
  <input class="messagesubmit" name="DISPLAYREPLACE" type="Submit" value="Display" />'; 

  if ($DISPLAYREPLACE) {
    print '&nbsp;<input class="messagesubmit" name="REPLACEALL" type="Submit" value="Replace All" />'; 
}
  
print '</div>';


//======================Search Files==========================


if ($FINDSTR) {
    $files = GetDirectory(ADMIN_CONTENT_DIR);

    printqn("<div class=`search` style=`padding:0.5em;`>
          <h2>[$FINDSTR] Found in Files . . .</h2>
          <ol>");

    $OLDtext = array('^T','^CR');
    $NEWtext = array("\t","\n");
  
    $FINDSTR    = str_replace($OLDtext,$NEWtext,$FINDSTR);
    $REPLACESTR = str_replace($OLDtext,$NEWtext,$REPLACESTR);
  
    $count=0;
    $OrTerms = explode('|',$FINDSTR);
    $ReplaceTerms = explode('|',$REPLACESTR);
  
    foreach ($files as $fi) {
        $filename=ADMIN_CONTENT_DIR."/$fi";
        $text=(file_get_contents($filename)); 

        $newtext  = str_replace($OrTerms,$ReplaceTerms,$text);
        $viewtext = htmlentities($text);
        for ($i=0; $i<count($OrTerms); $i++) {
            $f = htmlentities($OrTerms[$i]);
            $r = "<span style=\"background-color:#f00;\">$ReplaceTerms[$i]</span>";
            $viewtext = str_replace($f,$r,$viewtext);
        }
    
        if ($newtext != $text) {      
            $count++;
            $link=strtok("$fi",'.');
            printqn("<li>	     
		 <a style=`font-size:1.1em; font-weight:bold; border-bottom:2px solid #036;` href=`#` onclick=`toggleDisplay('replace_page_view$count'); return false;`>$fi</a>
		 <a target=`_blank` class=`editbutton` href=`$ADMIN_FILE?F=$link`>Edit</a> 
			<div id=`replace_page_view$count` style=`font-size:0.8em; margin:1em; display:none; padding:1em; border:1px dashed #888;`>
			<pre>$viewtext</pre>
			</div>
			</li>");
       
            if ($REPLACEALL) {
                write_file("$filename",$newtext);
            } 
        }
    }
    print '</ol>';
    if ($count==0) {
        printqn("<h3 style=`margin-left:4em;`>[$SEARCHSTR] Not Found!</h3>");
    }
} 
<?php

if (!empty($SITECONFIG['wanthtml']) and $need_content) {

    $ADMIN_CSS_PATH     = ADMIN_CSS_PATH;
    $ADMIN_TINYMCE      = ADMIN_TINYMCE;
    $ADMIN_TINYMCE_PATH = dirname(ADMIN_TINYMCE);

    $ADMIN_DIR = '/' . basename(dirname(dirname(__FILE__))) . '/admin';

    print <<<LABELTINYMCE
    <script type="text/javascript" src="$ADMIN_TINYMCE"></script>
    <script type="text/javascript" src="$ADMIN_DIR/admin_tinymce_js.php?TP=$THIS_PAGE{$SV}CSS=$ADMIN_CSS_PATH{$SV}PATH=$ADMIN_TINYMCE_PATH"></script>

LABELTINYMCE;
}


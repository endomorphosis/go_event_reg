<?php
  $SEARCHSTR = Post('SEARCHSTR');
?>

<form method="post" action="<?php echo $THIS_PAGE ?>">
<div class="formtitle">Find:</div>
<div class="forminfo"><input class="formitem" type="text" name="SEARCHSTR" value="<? echo htmlentities($SEARCHSTR) ?>" size="40" />
<a class="stdbutton" style="padding-left:20px; display:inline;
  background-image:url(/images/help.gif); background-repeat:no-repeat; background-position: 2px 2px;" 
  href="#" onclick="toggleDisplay('helptext'); return false;">Help</a>
</div>
<div class="forminfo" id="helptext" style="display:none; background-color:#FFFF7F; border:1px dashed #888; padding:0px 1em;">
<p style="font-size:1.2em; font-weight:bold;">Search Help</p>
<p>Enter search string in box above. The entire string will be matched, unless:</p>
<ul>
  <li>&ldquo; <b>AND</b> &rdquo; (in uppercase) is used to separate search groups, which finds items containing all groups delimited by the &ldquo; AND &rdquo;, or</li>
  <li>&ldquo; <b>OR</b> &rdquo; (in uppercase) is used to separate search groups, which finds items containing either of the groups delimited by the &ldquo; OR &rdquo;. </li>
  <li>If both AND <i>and</i> OR are used, AND groups will be found within OR groups</li>
  </ul>

</div>
<div class="forminfo"><input class="messagesubmit" type="submit" value="Search" /></div>
</form>


<?php

$thisfile = $PAGE['pagename'].$SITECONFIG['contentstr'];

//======================Search Files==========================

if($SEARCHSTR){

  $files = GetDirectory("$SITE_ROOT/{$SITECONFIG['contentdir']}",$SITECONFIG['contentstr']);
  printqn("<div class=`search`>
          <h2>[".htmlentities($SEARCHSTR)."] Found in Files . . .</h2>
  <ol>");

  $count=0;
  foreach ($files as $fi){

  $titlefile = str_replace($SITECONFIG['contentstr'],$SITECONFIG['titlestr'],$fi);
  $title = TextBetween('<name>','</name>',file_get_contents("$SITE_ROOT/{$SITECONFIG['contentdir']}/$titlefile"));

  if (($fi != $thisfile ) and !empty($title)) {
      $filename="$SITE_ROOT/{$SITECONFIG['contentdir']}/$fi";
      ob_start();
      include $filename;
      $text = ob_get_contents();
      ob_end_clean();
      $text = strtolower(strip_tags($text));
      

      $OrTerms  = explode(' OR ',$SEARCHSTR);
      $FOUND = 0;
      foreach ($OrTerms as $terms){
        if($FOUND==0){
          $AndTerms = explode(' AND ',$terms);
          foreach ($AndTerms as $aterms){
            $searchstr = trim(strtolower($aterms));
            if (strpos($text,$searchstr)!==false) $FOUND++;      
            if($FOUND==count($AndTerms)){
              $count++;
              $link=strTo("{$SITECONFIG['pagedir']}/$fi",'.').$SITECONFIG['extension'];
              if($title) printqn("<li><a href=`$link`>$title</a></li>");
             }
          }
        }
      }
 
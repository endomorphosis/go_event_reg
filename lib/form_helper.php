<?php
// ------------- FORM HELPER UNIT ----------------

require_once dirname(__FILE__).'/tools.php';

$SubmitClickText = 'Processing. . .';
$StartSelect = '-- select --';
$NewSelectText     = '-- new --';
$FormPrefix = 'FORM_';
$FormPhoneDelimiter = '-';
$DefaultCountry = 'US';
//recommended style:  span.required { color:#f00; font-weight:bold; padding-right:2px; }
$RequiredText  = '<span class="formrequired">&bull;</span>';
$TitleTemplate = qqn("<div class=`formtitle`>@@VAR@@:</div>");
$InfoTemplate  = '<div class="forminfo">@@VAR@@</div>'."\n\n";
$ErrorTemplate = '<div class="error">@@ERROR@@</div>'."\n";
$form_show_posted  = true;
$posted_cell_style = 'white-space:nowrap; text-align:center; font-size:0.76em;';
$block_referrer    = true;
$form_show_missing = false;  //show missing in output table
$Strip_Quotes      = false;
$form_date_code = 'l, M j, Y, g:ia';  //for posted note in table
$timeshift      =  0; // set for server time differences hours

//masks for edit
$Mask_Integer    = '^[0-9]+$';
$Mask_Name       = '^[a-zA-Z0-9 \'\-]+$';
$Mask_UserName   = '^[a-zA-Z0-9]+$';
$Mask_Password   = '^[a-zA-Z0-9:\-\!\@\#\$\%\^\&\*_]+$';
$Mask_Email      = '^([0-9a-z]+)([0-9a-z\.-_]+)@([0-9a-z\.-_+)\.([0-9a-z]+)';
$Mask_Real       = '^[0-9.\-]+$';
$Mask_RealC      = '^[0-9\,.\-]+$';
$Mask_ZIP        = '^[0-9\-]+$';
$Mask_4int       = '^[0-9]{4}$';
$Mask_2int       = '^[0-9]{2}$';
$Mask_Char       = '^[a-zA-Z]+$';
$Mask_2chr       = '^[a-zA-Z]{2}$';
$Mask_General_Line = '^[a-zA-Z0-9_ \!-\?[:punct:]]+$';
$Mask_General    = '^[a-zA-Z0-9_ '."\r\n\t".'\!-\?[:punct:]]+$';
$Mask_Words      = '^[[:alnum:][:space:][:punct:]]+$';
$Mask_Dir        = '^[a-zA-Z0-9\/_\.\-]+$';


$months = array('January','February','March','April','May','June','July','August','September','October','November','December');

$CountryCodes=array('US'=>'United States','AL'=>'Albania','DZ'=>'Algeria','AS'=>'American Samoa','AD'=>'Andorra','AI'=>'Anguilla','AG'=>'Antigua',
'AR'=>'Argentina','AW'=>'Aruba','AU'=>'Australia','AT'=>'Austria','AP'=>'Azores','BS'=>'Bahamas','BH'=>'Bahrain','BD'=>'Bangladesh','BB'=>'Barbados',
'BC'=>'Barbuda','BY'=>'Belarus','BE'=>'Belgium','BZ'=>'Belize','BJ'=>'Benin','BM'=>'Bermuda','BO'=>'Bolivia','BL'=>'Bonaire','BW'=>'Botswana',
'BR'=>'Brazil','VG'=>'British Virgin Islands','BN'=>'Brunei','BG'=>'Bulgaria','BF'=>'Burkina Faso','BI'=>'Burundi','KH'=>'Cambodia',
'CM'=>'Cameroon','CA'=>'Canada','CE'=>'Canary Islands','CV'=>'Cape Verde Islands','KY'=>'Cayman Islands',
'CF'=>'Central African Republic','TD'=>'Chad','NN'=>'Channel Islands','CL'=>'Chile','CN'=>'China, Peoples Republic of',
'CX'=>'Christmas Island','CC'=>'Cocos Islands',
'CO'=>'Colombia','CG'=>'Congo','CK'=>'Cook Islands','CR'=>'Costa Rica','HR'=>'Croatia','CB'=>'Curacao','CY'=>'Cyprus',
'CZ'=>'Czech Republic','DK'=>'Denmark','DJ'=>'Djibouti','DM'=>'Dominica','DO'=>'Dominican Republic','EC'=>'Ecuador',
'EG'=>'Egypt','SV'=>'El Salvador','EN'=>'England','GQ'=>'Equitorial Guinea','ER'=>'Eritrea','EE'=>'Estonia',
'ET'=>'Ethiopia','FO'=>'Faeroe Islands','FJ'=>'Fiji','FI'=>'Finland','FR'=>'France','GF'=>'French Guiana',
'PF'=>'French Polynesia','GA'=>'Gabon','GM'=>'Gambia','DE'=>'Germany','GH'=>'Ghana','GI'=>'Gibraltar','GR'=>'Greece','GL'=>'Greenland','GD'=>'Grenada',
'GP'=>'Guadeloupe','GU'=>'Guam','GT'=>'Guatemala','GN'=>'Guinea','GW'=>'Guinea-Bissau','GY'=>'Guyana','HT'=>'Haiti',
'HO'=>'Holland','HN'=>'Honduras','HK'=>'Hong Kong','HU'=>'Hungary','IS'=>'Iceland','IN'=>'India',
'ID'=>'Indonesia','IR'=>'Iran','IE'=>'Ireland','IL'=>'Israel','IT'=>'Italy','CI'=>'Ivory Coast','JM'=>'Jamaica','JP'=>'Japan',
'JO'=>'Jordan','KZ'=>'Kazakhstan','KE'=>'Kenya','KI'=>'Kiribati','KR'=>'Korea, South','KO'=>'Kosrae','KW'=>'Kuwait',
'KG'=>'Kyrgyzstan','LA'=>'Laos','LV'=>'Latvia','LB'=>'Lebanon','LS'=>'Lesotho','LR'=>'Liberia','LI'=>'Liechtenstein','LT'=>'Lithuania',
'LU'=>'Luxembourg','MO'=>'Macau','MK'=>'Macedonia','MG'=>'Madagascar','ME'=>'Madeira','MW'=>'Malawi','MY'=>'Malaysia','MV'=>'Maldives',
'ML'=>'Mali','MT'=>'Malta','MH'=>'Marshall Islands','MQ'=>'Martinique','MR'=>'Mauritania','MU'=>'Mauritius','MX'=>'Mexico','FM'=>'Micronesia',
'MD'=>'Moldova','MC'=>'Monaco','MS'=>'Montserrat','MA'=>'Morocco','MZ'=>'Mozambique','MM'=>'Myanmar','NA'=>'Namibia','NP'=>'Nepal',
'NL'=>'Netherlands','AN'=>'Netherlands Antilles','NV'=>'Nevis','NC'=>'New Caledonia','NZ'=>'New Zealand','NI'=>'Nicaragua',
'NE'=>'Niger','NG'=>'Nigeria','NU'=>'Niue','NF'=>'NorfolkIsland','NB'=>'Northern Ireland','MP'=>'Northern Mariana Islands','NO'=>'Norway',
'OM'=>'Oman','PK'=>'Pakistan','PW'=>'Palau','PA'=>'Panama','PG'=>'Papua New Guinea','PY'=>'Paraguay','PE'=>'Peru','PH'=>'Philippines',
'PL'=>'Poland','PO'=>'Ponape','PT'=>'Portugal','PR'=>'Puerto Rico','QA'=>'Qatar','RE'=>'Reunion','RO'=>'Romania',
'RT'=>'Rota','RU'=>'Russia','RW'=>'Rwanda','SS'=>'Saba','SP'=>'Saipan','SA'=>'Saudi Arabia','SF'=>'Scotland',
'SN'=>'Senegal','SC'=>'Seychelles','SL'=>'Sierra Leone','SG'=>'Singapore','SK'=>'Slovak Republic','SI'=>'Slovenia','SB'=>'Solomon Islands',
'ZA'=>'South Africa','ES'=>'Spain','LK'=>'Sri Lanka','NT'=>'St. Barthelemy','SW'=>'St. Christopher','SX'=>'St. Croix','EU'=>'St. Eustatius',
'UV'=>'St. John','KN'=>'St. Kitts','LC'=>'St. Lucia','MB'=>'St. Maarten','TB'=>'St. Martin','VL'=>'St. Thomas',
'VC'=>'St. Vincent and the Grenadines','SD'=>'Sudan','SR'=>'Suriname','SZ'=>'Swaziland','SE'=>'Sweden','CH'=>'Switzerland','SY'=>'Syria',
'TA'=>'Tahiti','TW'=>'Taiwan','TJ'=>'Tajikistan','TZ'=>'Tanzania','TH'=>'Thailand','TI'=>'Tinian','TG'=>'Togo','TO'=>'Tonga',
'TL'=>'Tortola','TT'=>'Trinidad Tobago','TU'=>'Truk','TN'=>'Tunisia','TR'=>'Turkey',
'TC'=>'Turks and Caicos Islands','TV'=>'Tuvalu',
'VI'=>'U.S.Virgin Islands','UG'=>'Uganda','UA'=>'Ukraine','UI'=>'Union Island','AE'=>'United Arab Emirates',
'GB'=>'United Kingdom','US'=>'United States','UY'=>'Uruguay','UZ'=>'Uzbekistan','VU'=>'Vanuatu','VE'=>'Venezuela','VN'=>'Vietnam',
'VR'=>'Virgin Gorda','WK'=>'Wake Island','WL'=>'Wales','WF'=>'Wallis and Futuna Islands','WS'=>'Western Samoa','YA'=>'Yap','YE'=>'Yemen',
'ZR'=>'Zaire','ZM'=>'Zambia','ZW'=>'Zimbabwe');

$StateCharCodes=array('INT','AL','AK','AZ','AR','CA','CO','CT','DE','DC','FL','GA','HI','IA','ID','IL','IN','KS','KY','LA','MA','MD','ME','MN','MI','MO','MS','MT','NC','ND','NE','NH','NJ','NM','NV','NY','OK','OR','OH','PA','RI','SC','SD','TN','TX','UT','VA','VT','WA','WI','WV','WY');
$StateCodes = array(
  'International','AL - Alabama','AK - Alaska','AZ - Arizona','AR - Arkansas',
  'CA - California','CO - Colorado','CT - Connecticut','DE - Delaware','DC - Washington D.C.',
  'FL - Florida','GA - Georgia','HI - Hawaii','IA - Iowa','ID - Idaho','IL - Illinois','IN - Indiana',
  'KS - Kansas','KY - Kentucky','LA - Louisiana',
  'MA - Massachusetts','MD - Maryland','ME - Maine','MN - Minnesota','MI - Michigan','MO - Missouri',
  'MS - Mississippi','MT - Montana','NC - North Carolina','ND - North Dakota','NE - Nebraska','NH - New Hampshire',
  'NJ - New Jersey','NM - New Mexico','NV - Nevada','NY - New York','OK - Oklahoma','OR - Oregon','OH - Ohio',
  'PA - Pennsylvania','RI - Rhode Island','SC - South Carolina','SD - South Dakota','TN - Tennessee','TX - Texas',
  'UT - Utah','VA - Virgina','VT - Vermont','WA - Washington','WI - Wisconsin','WV - West Virginia','WY - Wyoming'
  );

function HaveSubmit($submitname)
{
    GLOBAL $SubmitClickText;
    return Post($submitname) == $SubmitClickText;
}


function ValidCreditCardNumber($cardNumber)
{
    $cardNumber = ereg_replace('[^0-9]', '', $cardNumber);

    if ( empty($cardNumber) ) {
        return false;
    }

    $validFormat = ereg("^5[1-5][0-9]{14}|"  // mastercard
        . "^4[0-9]{12}([0-9]{3})?|" // visa
        . "^3[47][0-9]{13}|" // american express
        . "^3(0[0-5]|[68][0-9])[0-9]{11}|" //discover
        . "^6011[0-9]{12}|" //diners
        . "^(3[0-9]{4}|2131|1800)[0-9]{11}$", $cardNumber); //JC

    if (!$validFormat) {
        return false;
    }

    // Is the number valid?
    $revNumber = strrev($cardNumber);
    $numSum = 0;

    for ($i = 0; $i < strlen($revNumber); $i++) {

        $currentNum = substr($revNumber, $i, 1);

        // Double every second digit
        if ($i % 2 == 1) {
            $currentNum *= 2;
        }

        // Add digits of 2-digit numbers together
        if ($currentNum > 9) {
            $firstNum = $currentNum % 10;
            $secondNum = ($currentNum - $firstNum) / 10;
            $currentNum = $firstNum + $secondNum;
        }

        $numSum += $currentNum;
    }

    // If the total has no remainder it's OK
    $passCheck = ($numSum % 10 == 0);
    return $passCheck;
}

function WriteErrorText($error)
{
    global $ErrorTemplate;
    $RESULT = (!empty($error))? str_replace('@@ERROR@@',$error,$ErrorTemplate) : '';
    return $RESULT;
}

function WriteError($error)
{
    echo WriteErrorText($error);
}

function PhoneConvert($phone)
{
    global $FormPhoneDelimiter;
    $RESULT = ereg_replace('[ -/]|[:-~]+','',$phone);
    if (strlen($RESULT)>4) $RESULT = substr($RESULT,0,-4).$FormPhoneDelimiter.substr($RESULT,-4);
    if (strlen($RESULT)>8) $RESULT = substr($RESULT,0,-8).$FormPhoneDelimiter.substr($RESULT,-8);
    if (strlen($RESULT)>12) $RESULT = substr($RESULT,0,-12).$FormPhoneDelimiter.substr($RESULT,-12);
    return $RESULT;
}

function CheckEmail($email)
{
    $lastdot=strrpos($email,'.');
    $at_sign=strrpos($email,'@');
    $AtGroups = explode('@',$email);
    $length=strlen($email);
    $BadEmail=((count($AtGroups) != 2)||($lastdot===false)||($at_sign===false)||($length===false)||
        ($lastdot-$at_sign<2)||($length-$lastdot<3)||($at_sign==0));
    return !$BadEmail;
}


function GetCountryNameOrCode($cc)
{
    global $CountryCodes;
    $name=$CountryCodes[$cc];
    if ($name) {return $name;}
    $code=array_search($cc,$CountryCodes);
    return $code;
}

function KeyValuesToVar($FormArray)
{
    // OLD USE: parse_str(KeyValuesToVar($array));
    // NOW just call this function
    $str = '';
    //foreach($FormArray as $key => $value) $str .= "$key=$value&";
    foreach($FormArray as $key => $value) $GLOBALS[$key] = $value;
    return '';
}

function PostVars($titlearray,$valuearray)
{
    global $FormPrefix;
    foreach ($titlearray as $key => $value) $_POST[$FormPrefix.$key] = empty($valuearray[$key])? '' : $valuearray[$key];
}

function StripQuotes($str)
{
    global $Strip_Quotes;
    if ($Strip_Quotes) return str_replace(array("'",'"'),'',$str);
    else return $str;
}

function GetPostItem($str)
{
    global $FormPrefix;
    $value = Post($FormPrefix.$str);
    if ($value == '0') return '0';
    elseif (!empty($value)) return trim(htmlentities(strip_tags(StripQuotes($value))));
    else return '';
}

function GetPostItemQuotes($str)
{
    global $FormPrefix;
    $value = Post($FormPrefix.$str);
    return (empty($value))? '' : addslashes(trim(htmlentities(strip_tags($value))));
}


function GetPostHTML($str)
{
    global $FormPrefix;
    //if (!empty($_POST[$FormPrefix.$str])) return addslashes(trim($_POST[$FormPrefix.$str]));
    if (!empty($_POST[$FormPrefix.$str])) return trim($_POST[$FormPrefix.$str]);
    else return '';
}

function WriteFormStart($action,$method)
{
    echo qqn("<form action=`$action` method=`$method`>");
}

function WriteFormEnd()
{
    echo "</form>\n";
}

//===============================================================================
//                                                          PROCESS FORM
//===============================================================================

function OKrow($value)
{
    global $form_show_missing;
    return ($form_show_missing or ($value!=''));
}

function ProcessForm($formdata,&$table,$tableoptions,$thoptions,$tdoptions,&$error) {
    global $TitleTemplate,$InfoTemplate,$months,$StartSelect,$RequiredText,$CountryCodes,
    $StateCodes,$form_date_code,$timeshift, $DefaultCountry,$FormPrefix,
    $Mask_Integer, $Mask_Name, $Mask_UserName, $Mask_Password, $Mask_Real, $Mask_4int, $Mask_2int, $Mask_Char, $Mask_2chr, $Mask_General,$form_show_posted,$posted_cell_style,$form_show_missing,$NewSelectText;

    $n = "\n";
    $brk = "<br />\n";
    //example options: class="formresult" align="center"
    $RESULT = array();

    if (!FromThisDomain() and $block_referrer) {
        $error = 'Invalid Referrer - Blocked for Security!';
        $table = '';
        return $RESULT;
    }

    $table = "<table $tableoptions>\n<tbody>\n";
    $error = '';
    $CountryDivOption = '';

    foreach($formdata as $item) {

        $field = explode('|',$item);
        $kind = $field[0];
        $field1 = (count($field) > 1) ? trim($field[1]) : '';
        $field2 = (count($field) > 2) ? trim($field[2]) : '';
        $field3 = (count($field) > 3) ? trim($field[3]) : '';
        $field4 = (count($field) > 4) ? trim($field[4]) : '';
        $field5 = (count($field) > 5) ? trim($field[5]) : '';
        $field6 = (count($field) > 6) ? trim($field[6]) : '';
        $field7 = (count($field) > 7) ? trim($field[7]) : '';
        $field8 = (count($field) > 8) ? trim($field[8]) : '';

        switch ($kind) {

        case 'h1':
        case 'h2':
        case 'h3':
        case 'hh1':
        case 'hh2':
        case 'hh3':
            //"hx|text|options"
            if (substr($kind,0,2)=='hh') $kind = substr($kind,1);
            $option = (empty($field2))? 'style="margin:0px 3px;"' : $field2;
            $table .= qq("<tr><td colspan=`2` $tdoptions><$kind $option>$field1</$kind>$n</td></tr>$n");
            break;

        case 'cell':
            //"cell|content
            $table .= qqn("<tr><td colspan=`2` $tdoptions>$field1$n</td></tr>");
            break;

        case 'info':
            //"info|title|info"
            $table .= qqn("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$field2</td></tr>");
            break;


        case 'hidden':
          //"hidden|VARNAME|value|mask|title"
            $value = GetPostItem($field1);
            if (!empty($field3) and $value) if (!ereg($field3,$value)) {
                if (!empty($field4)) $error .= "$field4 has illegal characters$brk";
                else $error .= "$field1 has illegal characters$brk";
            }
            if (!empty($field4)) {
                $tvalue = nl2br($value);
                if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field4</th>$n<td $tdoptions>$tvalue</td></tr>$n");
            }
            $RESULT[$field1] = $value;
            break;

        case 'text':
        case 'lctext':
        case 'password':
        case 'textquote':
        case 'qtext':
            //"text|title|VARNAME|required|size|maxlength|options|mask|hide in table(H)",
            $value = (($kind == 'textquote') or ($kind == 'qtext')) ? GetPostItemQuotes($field2) : GetPostItem($field2);
            if ($kind == 'lctext') $value=strtolower($value);
            if (($field3 == 'Y') and ($value=='')) $error .= "$field1 is missing$brk";
            if (!empty($field7) and $value) if (!ereg($field7,$value)) $error .= "$field1 has illegal characters$brk";
            if (OKrow($value) and ($field8 != 'H')) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'creditcard':
            //"text|title|VARNAME|required|options"  //no mask needed
            $value = GetPostItem($field2);
            if (($field3 == 'Y') and (!$value)) $error .= "$field1 is missing$brk";
            if ($value) {
                if (!ereg($Mask_Integer,$value)) $error .= "$field1 has illegal characters$brk";
                if (!ValidCreditCardNumber($value)) $error .= "$field1 is not a valid number $brk";
                if (strlen($value)<15) $error .= "$field1 has too few numbers$brk"; // for VISA need 16, american express 15
                $outputvalue = "xxxx-xxxx-xxxx-".substr($value,12,4);
            } else $outputvalue = '';
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$outputvalue</td></tr>$n");
            $RESULT[$field2] = $value;
            break;


        case 'phone':
            //"phone|title|VARNAME|required|options",  (mask not needed)
            $value = PhoneConvert(GetPostItem($field2));
            if($value and strlen($value)< 12) $error .= "$field1 is missing numbers$brk";
            if (($field3 == 'Y') and ($value=='')) $error .= "$field1 is missing$brk";
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $_POST[$FormPrefix.$field2] = $value;  // update post with phone number transform
            $RESULT[$field2] = $value;
            break;

        case 'email':
            //"email|title|VARNAME|required|size|maxlength|options"
            $value = GetPostItem($field2);
            if (($field3 == 'Y') and (!$value)) $error .= "$field1 is missing$brk";
            if ($value) {
                if (!CheckEmail($value)) {$error .= "$field1 is Not Valid$brk";}
            }
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'textarea':
        case 'qtextarea':
            //"textarea|title|VARNAME|required|cols|rows|options|mask"
            $value = ($kind == 'qtextarea') ? GetPostItemQuotes($field2) : GetPostItem($field2);
            $value = GetPostItem($field2);
            if (($field3 == 'Y') and (!$value)) $error .= "$field1 is missing$brk";
            if (!empty($field7) and $value) if (!ereg($field7,$value)) $error .= "$field1 has illegal characters$brk";
            $tvalue = nl2br($value);
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$tvalue</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'html':
            //"html|title|VARNAME|required|cols|rows|mask"
            $value = GetPostHTML($field2);
            if (($field3 == 'Y') and (!$value)) $error .= "$field1 is missing$brk";
            if (!empty($field7) and $value) if (!ereg($field7,$value)) $error .= "$field1 has illegal characters$brk";
            $tvalue = nl2br($value);
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$tvalue</td></tr>$n");
            $RESULT[$field2] = $value;
            break;


        case 'select':
            //"select|title|VAR|required|value1=text|value2=text"
            $value = GetPostItem($field2);
            if (($field3 == 'Y') and ($value==$StartSelect)) $error .= "$field1 is missing$brk";
            if ($value==$StartSelect) $value = '';
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;


        case 'selecttext':
            //"select|title|VAR|required|size|maxlength|mask|value1|value2 . . ."
            $value = GetPostItem($field2);
            $value2 = GetPostItem("new_$field2");
            if (($field3 == 'Y') and ($value==$StartSelect)) $error .= "$field1 is missing$brk";
            if (($field3 == 'Y') and ($value==$NewSelectText) and ($value2 == '')) $error .= "$field1 is missing$brk";
            if ($value==$StartSelect) $value = '';
            elseif ($value==$NewSelectText) $value = $value2;
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'selectcount':
            //"selectcount|title|VAR|required|start|end"
            $value = GetPostItem($field2);
            if (($field3 == 'Y') and ($value==$StartSelect)) $error .= "$field1 is missing$brk";
            elseif (!empty($value) and (!ereg($Mask_Integer,$value))) $error .= "$field1 has illegal characters$brk";
            if ($value==$StartSelect) $value = '';
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'dateCC':
            //"dateCC|title|varname"
            $field3 = strtolower($field3);
            $yearvar  = "{$field2}_YEAR";
            $monthvar = "{$field2}_MONTH";

            $value = GetPostItem($yearvar);
            if ($value=='00') $error .= "$field1 Year is missing$brk";
            if (!empty($value) and ($value!='00') and (!ereg($Mask_2int,$value))) $error .= "($value) $field1 has illegal characters$brk";
            $yearvar_result = $value;

            $value = GetPostItem($monthvar);
            if ($value=='00') $error .= "$field1 Month is missing$brk";
            if (!empty($value) and ($value!='00') and (!ereg($Mask_2int,$value))) $error .= "($value) $field1 has illegal characters$brk";

            $monthvar_result = $value;
            if (($yearvar_result==date('y')) and (intval($monthvar_result)< date('n'))) $error .= "$field1 has passed$brk";

            if (!empty($yearvar_result) and !empty($monthvar_result)) {
                $value = "$monthvar_result$yearvar_result";
                $_POST[$FormPrefix.$field2] = $value;
            } else $value ='';
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;


        case 'dateYMD':
        case 'dateYM':
            //"dateYMD|title|varname|format|required|startyear|NOW"

            $field3 = strtolower($field3);
            $yearvar  = "{$field2}_YEAR";
            $monthvar = "{$field2}_MONTH";
            $dayvar   = "{$field2}_DAY";

            $value = GetPostItem($yearvar);
            if (($field4 == 'Y') and ($value==$StartSelect)) $error .= "$field1 Year is missing$brk";
            if (!empty($value) and ($value!=$StartSelect) and (!ereg($Mask_4int,$value))) $error .= "($value) $field1 has illegal characters$brk";
            if ($value==$StartSelect) $value = '';
            $yearvar_result = $value;

            $value = GetPostItem($monthvar);
            if (($field4 == 'Y') and ($value==$StartSelect)) $error .= "$field1 Month is missing$brk";
            if (!empty($value) and ($value!=$StartSelect) and (!ereg($Mask_2int,$value))) $error .= "($value) $field1 has illegal characters$brk";
            if ($value==$StartSelect) $value = '';
            $monthvar_result = $value;

            if ($kind == 'dateYMD') {
                $value = GetPostItem($dayvar);
                if (($field4 == 'Y') and ($value==$StartSelect)) $error .= "$field1 Day is missing$brk";
                if (!empty($value) and ($value!=$StartSelect) and (!ereg($Mask_2int,$value))) $error .= "$field1 has illegal characters$brk";
                if ($value==$StartSelect) $value = '';
                $dayvar_result = $value;
            }
            if ((($kind == 'dateYMD') and !empty($yearvar_result) and !empty($monthvar_result) and !empty($dayvar_result))
              or  (($kind == 'dateYM') and !empty($yearvar_result) and !empty($monthvar_result))) {
                $value = str_replace('y',$yearvar_result,$field3);
                $value = str_replace('m',$monthvar_result,$value);
                if ($kind == 'dateYMD') $value = str_replace('d',$dayvar_result,$value);
            } else $value ='';
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'time':
            //"time|title|varname|format|required|options"
            // NOTE : format not used

            $field3 = strtolower($field3);
            $hourvar  = "{$field2}_HOUR";
            $minutevar = "{$field2}_MINUTE";

            $value = GetPostItem($hourvar);
            if (($field4 == 'Y') and ($value==$StartSelect)) $error .= "$field1 Year is missing$brk";
            if (!empty($value) and ($value!=$StartSelect) and (!ereg($Mask_2int,$value))) $error .= "($value) $field1 has illegal characters$brk";
            if ($value==$StartSelect) $value = '';
            $hour_result = $value;

            $value = GetPostItem($minutevar);
            if (($field4 == 'Y') and ($value==$StartSelect)) $error .= "$field1 Month is missing$brk";
            if (!empty($value) and ($value!=$StartSelect) and (!ereg($Mask_2int,$value))) $error .= "($value) $field1 has illegal characters$brk";
            if ($value==$StartSelect) $value = '';
            $minute_result = $value;
            
            if (!empty($hour_result) and !empty($minute_result)) {
                $value = "$hour_result:$minute_result";
            } else $value ='';

            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;
            
            
        case 'country':
            //"country|title|VAR|required"
            $value = GetPostItem($field2);
            $cname = GetCountryNameOrCode($value);
            if (($field3 == 'Y') and ($value==$StartSelect)) $error .= "$field1 is missing$brk";
            if ($value==$StartSelect) $value = '';
            if (!empty($value) and (!ereg($Mask_2chr,$value))) $error .= "$field1 has illegal characters$brk";
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$cname</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'state':
            //"state|title|VAR|required|US"
            $value = GetPostItem($field2);
            if (($field3 == 'Y') and ($value==$StartSelect)) $error .= "$field1 is missing$brk";
            if ($value==$StartSelect) $value = '';
            if (!empty($value) and (!ereg($Mask_Char,$value))) $error .= "$field1 has illegal characters$brk";
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;

        case 'intstate':
            //"intstate|title|VAR|required|options|countryid"
            $value = GetPostItem($field2);
            $intvalue = GetPostItem("INT_$field2");
            if (($field3 == 'Y') and ($value=='INT') and ($intvalue == '')) $error .= "$field1 International requires Non-US entry$brk";
            if (!empty($value) and (!ereg($Mask_Char,$value))) $error .= "$field1 has illegal characters$brk";
            if ($value == 'INT') $value = $intvalue;
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$value</td></tr>$n");
            $RESULT[$field2] = $value;
            break;


        case 'radio':
        case 'radioh':
            //"radio|title|VAR|required|options|value1=text|value2=text"
            $value = GetPostItem($field2);
            for($i=5; $i<count($field); $i++) {
                $Items     = explode('=',$field[$i]);
                $itemvalue = trim($Items[0]);
                $itemtext  = (count($Items)>1)? trim($Items[1]) : $itemvalue;
                if ($itemvalue == $value) $avalue = $itemtext;
            }
            if (($field3 == 'Y') and (!$value)) $error .= "$field1 is missing$brk";
            if (!empty($value) and (!ereg($Mask_General,$value))) $error .= "$field1 has illegal characters$brk";
            if (OKrow($value)) $table .= qq("<tr><th $thoptions>$field1</th>$n<td $tdoptions>$avalue</td></tr>$n");
            $RESULT[$field2] = $value;
            break;


        case 'checkboxlist':
            //"checkboxlist|title|options|value1=text|value2=text"
            for($i=3; $i<count($field); $i++) {
                $Items     = explode('=',$field[$i]);
                $itemvalue = trim($Items[0]);
                $itemtext  = (count($Items)>1)? trim($Items[1]) : $itemvalue;
                $itemname = str_replace(' ','_',$itemtext
<?php
/*
REQUIRED VARIABLES
$SITECONFIG['emaillist'] : determines the recipients
$SITECONFIG['emailtopics'] : is an array of drop-down topics.
$SITECONFIG['companyaddress'] : the company address to output
$SITECONFIG['emailsubjectprefix'] = Subject prefix in email sent
$SITECONFIG['emailplaintext']    = 0 for HTML, 1 for Plaintext;
$SITECONFIG['emailsendswift']   = 0;
$SITECONFIG['contactaddress']   = 0 for no address, 1 for address box;

OPTION VARIABLES
$QUERYVAR['mail'] is 'get' variable for $recipient  :  /contact:mail=My_Name  (underscores for spaces)
$QUERYVAR['subject'] is 'get' variable for the subject  :  /contact:subject=mysubject
$QUERYVAR['message'] is 'get' variable for a default message  :  /contact:message=mymessage
$SetTimeShift: is used to change posted time from server time.
$ContactUsText: is the introductory text before the form

TABLE STYLES: 
$SetPostedCell: is the style used for the posted table cell at the bottom of the message.  Changes form_helper default.
$TableOption: is the options for the HTML table head.
$ThOption: is the <th> heading options.
$TdOptions: is the <td> cell options.

// SESSION varible set upon each loading of a new page.  Stops double submitting and machine submission.

*/
require_once "$LIB/form_helper.php";

//-----VARIABLES SET-UP------
$FormAction=$PAGE['pagelink'];
if(!empty($SetTimeShift)) $timeshift = $SetTimeShift;   // used in form_helper
if(!empty($SetPostedCell)) $posted_cell_style = $SetPostedCell;  // used in form_helper
if(empty($TableOption)) $TableOption = 'align="center" width="90%" border="0" cellspacing="1" cellpadding="2" style="background-color:#888; font-family:Arial, Helvetica, sans-serif; color:#000;"';
if(empty($ThOption)) $ThOption = 'width="20%" style="padding:3px; text-align:right; background-color:#ccf; font-weight:bold;"';
if(empty($TdOption)) $TdOption = 'style="padding:3px; text-align:left; background-color:#fff;"';

SetPost('SEND');

if(!empty($QUERYVAR['mail'])) $_POST[$FormPrefix.'mail']  = str_replace('_',' ',$QUERYVAR['mail']);
if(!empty($QUERYVAR['subject'])) $_POST[$FormPrefix.'SUBJECT'] = $QUERYVAR['subject'];
if(!empty($QUERYVAR['message'])) $_POST[$FormPrefix.'MESSAGE'] = $QUERYVAR['message'];

$ADDR  = Server('REMOTE_ADDR');

//-----CREATE EMAIL DROP DOWN LIST------
if(count($SITECONFIG['emaillist'])>1){
  $EmailDropDown='select|Recipient|mail|Y||N';
  foreach ($SITECONFIG['emaillist'] as $key => $value) $EmailDropDown .= "|$key=".str_replace('_',' ',$key);
} else $EmailDropDown = '';

$Address = !empty($SITECONFIG['contactaddress'])? "textarea|Address|ADDRESS|N|30|3||$Mask_General" : '';

if(count($SITECONFIG['emailtopics'])>0){
  $TopicStr = 'select|Topic|TOPIC|N|';
  foreach ($SITECONFIG['emailtopics'] as $i) $TopicStr .= "|$i";
} else $TopicStr = ''; 


$_SES
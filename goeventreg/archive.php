<?php
//  copies files to archive directory.  Appends file date and replaces folders  delimiters with @ signs

//================ Needed Definitions ===================
// configure this section

$TITLE             = 'Website';
$FOLDER_TO_ARCHIVE = dirname(__FILE__);
$ARCHIVE_FOLDER    = $FOLDER_TO_ARCHIVE . '/archive';
$EXCLUDE_LIST      = 'Desktop.ini,Thumbs,logs';  // already will exclude the archive folder

$IMAGELIST = array('.gif','.jpg','.png');

//======================================================
$ROOT        = $_SERVER['DOCUMENT_ROOT'];
$REQUEST_URI = strTo($_SERVER['REQUEST_URI'],'?');

function Post($name) {return (isset($_POST[$name]))? $_POST[$name] : '';}
function Get($name) {return (isset($_GET[$name]))? $_GET[$name] : '';}

function strTo($string, $to) {
  $i = strpos($string,$to);
  if ( $i !== false ) return substr($string,0,$i);
    else return $string;
}

function StripRoot($str)
{
    global $ROOT;
    $str = str_replace('\\', '/', $str);  // needed for Windows
    $str = str_replace($ROOT,'',$str);
    return $str;
}

//---------CHECK IF ARRAY ITEMS IN STRING----------
function ArrayItemsWithinStr($myarray,$str)
{
    $arraycount = count($myarray);
    if (empty($str) or ($arraycount==0)) return false;
    for($i=0; $i<$arraycount; $i++) {
        if (stristr($str,$myarray[$i]) != '') return true;
    }
    return false;
}


function GetDirectory() {
    //gets a directory and subdirectory filelist with optional include and exclude string
    //$url,$includestr,$excludestr
    $numargs  = func_num_args();
    $url = func_get_arg(0);
    $includestr = ($numargs>1)? func_get_arg(1) : '';
    $excludestr = ($numargs>2)? func_get_arg(2) : '';
    $files  = array();
    if (!file_exists($url)) return $files;
    if (!empty($includestr)) $include_strings = explode(',',$includestr);
    if (!empty($excludestr)) $exclude_strings = explode(',',$excludestr);
    $dir = opendir("$url/");
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..') {
            if (is_dir("$url/$file")) {
                $echeck = (!empty($excludestr))? !ArrayItemsWithinStr($exclude_strings,"$url/$file") : true;
                if ($echeck and !eregi('.svn',$file)) {
                    $sfiles = GetDirectory("$url/$file",$includestr,$excludestr);
                    foreach ($sfiles as $f) $files[] = "$file/$f";
                }
            } else {
                $icheck = (!empty($includestr))? ArrayItemsWithinStr($include_strings,$file) : true;
                $echeck = (!empty($excludestr))? !ArrayItemsWithinStr($exclude_strings,$file) : true;
                if ($icheck and $echeck) $files[] = $file;
            }

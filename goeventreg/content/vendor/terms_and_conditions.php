<div style="font-size:10px; padding:20px;">
Intel&reg; Solutions Summit 2009<br>
Terms and Conditions of Participation in Event
<p>1. ACCEPTANCE BY ORGANIZER. Event participation by third-party companies who 
  wish to exhibit their products or services at the Intel&reg; Solutions Summit 
  2009 (&quot;Sponsors&quot;) is subject to approval by Intel Corporation (&quot;Organizer&quot;). 
  A contract is deemed to be created only when Organizer countersigns the Application. 
  Organizer may revoke its acceptance at any time by refunding Sponsor's paid 
  Total Space Fee if Organizer deems Sponsor or its product ineligible. No warranties 
  or representations are made by Organizer concerning the number of Intel Solutions 
  Summit (&quot;Event&quot;) attendees. Dates, hours, and venue for the Event 
  may be modified by written information provided to Sponsor.</p>
<p>2. ASSIGNMENT AND USE OF SPACE.</p>
<p>a. Benefits and License Grant. At the Event, Organizer will provide Sponsor 
  with a designated space (&quot;Space&quot;) in which to exhibit its qualified 
  products and services (the &quot;Exhibit&quot;). Included in the Total Space 
  Fee are use of the Space and any other benefits as set forth in the Application. 
  Organizer is granted by Sponsor permission to employ the name and logo of Sponsor 
  in connection with Event promotion and production.</p>
<p>b. Space Assignment, Installation, Use, Occupancy, and Dismantling. Space is 
  assigned by Organizer, which reserves the right to reassign Space or change 
  Event layout or venue at any time. Space is for Sponsor's sole use. No portion 
  of Space can be shared, sold, assigned or sublet by Sponsor (including to an 
  affiliated company) without prior written consent from Organizer. Sponsor must 
  occupy Space fully, and must provide equipment, displays, carpeting, etc., unless 
  otherwise specified by Organizer. Any Exhibits provided by Sponsor must be safely 
  built and assembled according to sound engineering practices. Exhibits must 
  be installed prior to, occupied during, and dismantled following the Event according 
  to Organizer's schedule. Activities of Sponsor must remain within the confines 
  of the Space, and must relate to products or services identified on the Application, 
  as well as Sponsor's normal business focus. Organizer may withhold permission 
  to display any products or services deemed by Organizer to be objectionable 
  or inappropriate for the Event. Sponsor may not exchange goods or money at the 
  Event, nor help any other party to solicit business at the Event, without prior 
  written consent from Organizer.</p>
<p>c. Own Risk. Sponsor is solely responsible for any loss of its equipment or 
  proprietary information, or for any other loss. This includes any subrogation 
  claims by its insurer. Persons who visit, view, or otherwise take part in Sponsor's 
  Space are deemed the invitees or licensees of Sponsor and not of Organizer.</p>
<p>d. Third-Party Contractors. Sponsor may be required by Organizer to select 
  from among designated third-party contractors (&quot;Required Contractors&quot;) 
  for certain services; Sponsor must then use only those Required Contractors 
  for such services. Notwithstanding such designation, Required Contractors and 
  third-party vendors listed in the Manual function solely as independent contractors; 
  Organizer will not be held responsible for their performance, actions, or omissions.</p>
<p>e. Other Events and Marketing. Sponsor agrees to not use any Organizer event 
  to leverage any other event in which Sponsor participates or sponsors. At the 
  Event, Sponsor may not promote its products or organization within 500 yards 
  of any Event locations, except (a) in advertising that appears in periodicals 
  or similar regularly published media, or (b) as allowed in the terms set forth 
  by this Agreement or by written approval from Organizer.</p>
<p>f. Other Event Payments. Organizer may allocate any Sponsor payments made under 
  this Agreement to any obligation that is past due under any other event-related 
  agreement between Organizer and Sponsor. In cases of such application, Organizer 
  will inform Sponsor.</p>
<p>3. COMPLIANCE WITH LAWS AND RULES.</p>
<p>a. Laws and Rules. Sponsor must comply with all applicable laws, ordinances, 
  and regulations relating to its Event participation, including but not limited 
  to requirements of the venue and any relevant labor union; Exhibit construction 
  to comply with the Americans With Disabilities Act or similar local regulations; 
  and Event-related terms, conditions, and rules issued on occasion by Organizer.</p>
<p>b. Third Party Proprietary Rights. Sponsor must respect proprietary rights 
  of third parties in connection with Event participation, including but not limited 
  to the performance, dissemination, or posting of copyrighted content without 
  a license, assignment, or other legally effective permission.</p>
<p>c. Taxes and Licenses. It is Sponsor's sole responsibility to acquire any licenses 
  and permits, and to pay all license fees, taxes (including sales and use taxes, 
  as well as taxes collected by Organizer), or other charges related to its participation 
  in the Event.</p>
<p>4. CANCELLATION OR TERMINATION.</p>
<p>a. Cancellation. Organizer reserves the right to cancel Event entirely or in 
  any part for any cause beyond its reasonable control, including but not limited 
  to natural or public disaster, acts of God, venue repairs or construction, inadequate 
  participation, market conditions, government regulation, or similar reasons. 
  In such cases Organizer will refund to Sponsor any space fees already collected 
  by Organizer; after that, Sponsor has no further recourse against Organizer. 
  A name change for the Event cannot be deemed a cancellation by Organizer.</p>
<p>b. Termination by Sponsor. All fees, when due, are considered fully earned 
  and non-refundable. Sponsor must deliver to Organizer, Attn: &quot;Event&quot; 
  General Manager, written notice of termination, which is effective upon receipt. 
  Sponsor acknowledges that a precise value for Organizer's services rendered 
  and expenses incurred for the Event are difficult to determine. If Sponsor terminates 
  this Agreement or participation in the Event, the amounts due from Sponsor under 
  this Agreement as of the effective date of any termination by Sponsor belong 
  to Organizer. This represents an agreed measure of compensation, and shall not 
  be construed as a penalty or forfeiture.</p>
<p>c. Termination by Organizer. Should Sponsor fail to meet any obligations under 
  the Agreement, Organizer may assume possession of the Space and terminate Sponsor's 
  participation in the Event. Such occurrence may include but not be limited to 
  Sponsor's failure to pay for the Space or related services; assemble its Exhibit; 
  maintain all exhibited products in safe, presentable working order, or staff 
  the Space adequately, in a timely manner; or adhere to Organizer's standards 
  of conduct. Any such termination will be deemed Sponsor-prompted under this 
  Agreement.</p>
<p>d. Payment Terms. The total cost must be received no later than February 15, 
  2009. The Organizer reserves the right to cancel a sponsor's participation for 
  non-compliance of these payment terms.</p>
<p>5. ORGANIZER MATERIALS. The Manual and any other planning materials or methodologies 
  provided to Sponsor concerning the planning or production of the Event (&quot;Organizer 
  Materials&quot;) are owned exclusively by Organizer and are Organizer's confidential 
  information. Sponsor is granted by Organizer a nonexclusive, nontransferable 
  license to employ such Organizer Materials, on an &quot;AS IS&quot; basis, solely 
  in relation to Sponsor's Event participation. It is Sponsor's responsibility 
  to acquire the Manual from Organizer. After the Event or earlier termination 
  of this Agreement, and upon Organizer's written request, Sponsor must promptly 
  return the Organizer Materials to Organizer. Sponsor may use lists of Event 
  exhibitors or attendees but may not sell such lists without Organizer's prior 
  written permission.</p>
<p>6. LIMITATION OF LIABILITY; INDEMNITY.</p>
<p>a. Organizer, its affiliates, or the venue (the &quot;Event Providers&quot;) 
  under no circumstances will be held liable for lost revenues or other indir
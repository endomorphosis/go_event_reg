<h1>Contact Us</h1>

<?php



define ('STEP1_STEP',			'Step 1');
define ('STEP2_STEP',			'Step 2');
define ('STEP3_STEP',			'Step 3');
define ('STEP4_STEP',			'Step 4');
define ('STEP5_STEP',			'Step 5');

define ('STEP1_IMG_ON',			'<img style="border:1px dashed #000;" alt="" src="images/step_start_on.jpg" width="43" height="43" border="0" />');
define ('STEP2_IMG_ON',			'<img style="border:1px dashed #000;" alt="" src="images/step_next_on.jpg" width="43" height="43" border="0" />');
define ('STEP3_IMG_ON',			'<img style="border:1px dashed #000;" alt="" src="images/step_next_on.jpg" width="43" height="43" border="0" />');
define ('STEP4_IMG_ON',			'<img style="border:1px dashed #000;" alt="" src="images/step_next_on.jpg" width="43" height="43" border="0" />');
define ('STEP5_IMG_ON',			'<img style="border:1px dashed #000;" alt="" src="images/step_stop_on.jpg" width="43" height="43" border="0" />');

define ('STEP1_IMG_OFF',		'<img alt="" src="images/step_start_off.jpg" width="43" height="43" border="0" />');
define ('STEP2_IMG_OFF',		'<img alt="" src="images/step_next_off.jpg" width="43" height="43" border="0" />');
define ('STEP3_IMG_OFF',		'<img alt="" src="images/step_next_off.jpg" width="43" height="43" border="0" />');
define ('STEP4_IMG_OFF',		'<img alt="" src="images/step_next_off.jpg" width="43" height="43" border="0" />');
define ('STEP5_IMG_OFF',		'<img alt="" src="images/step_stop_off.jpg" width="43" height="43" border="0" />');

define ('STEP_ARROW',			'<img alt="" src="images/step_arrow.jpg" width="43" height="43" border="0" />');


define ('STEP1_TEXT',			'Terms & Conditions');
define ('STEP2_TEXT',			'Sponsorship Matrix');
define ('STEP3_TEXT',			'Contact Information');
define ('STEP4_TEXT',			'Payment Details');
define ('STEP5_TEXT',			'Summary');




function displayStepTable ($CURSTEP, $EVENT_ID)
{
	$STEP_1_LINK = '<span onclick="hideGroupExcept(\'creator\',\'1\')">';
	$STEP_2_LINK = '<span onclick="hideGroupExcept(\'creator\',\'2\')">';
	$STEP_3_LINK = '<span onclick="hideGroupExcept(\'creator\',\'3\')">';
	$STEP_4_LINK = '<span onclick="hideGroupExcept(\'creator\',\'4\')">';
	$STEP_5_LINK = '<span onclick="'.$PREVIEW_LINK.'">';

	$STEP1 = ($CURSTEP == 1) ? STEP1_IMG_ON : STEP1_IMG_OFF;
	$STEP2 = ($CURSTEP == 2) ? STEP2_IMG_ON : STEP2_IMG_OFF;
	$STEP3 = ($CURSTEP == 3) ? STEP3_IMG_ON : STEP3_IMG_OFF;
	$STEP4 = ($CURSTEP == 4) ? STEP4_IMG_ON : STEP4_IMG_OFF;
	$STEP5 = ($CURSTEP == 5) ? STEP5_IMG_ON : STEP5_IMG_OFF;
	
	$TABLE = '
        <table class="tbl_registration_steps" align="center" border="0" style="border:0px solid red;">
          <tr align="center"> 
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_1_LINK . STEP1_STEP .'</span></font></td>
            <td>&nbsp;</td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_2_LINK . STEP2_STEP .'</span></font></td>
            <td>&nbsp;</td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_3_LINK . STEP3_STEP .'</span></font></td>
			<td>&nbsp;</td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_4_LINK . STEP4_STEP .'</span></font></td>
			<td>&nbsp;</td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_5_LINK . STEP5_STEP .'</span></font></td>
            </tr>
          <tr align="center"> 
            <td>'. $STEP_1_LINK . $STEP1 .'</span></td>
            <td>'. STEP_ARROW .'</td>
            <td>'. $STEP_2_LINK . $STEP2 .'</span></td>
            <td>'. STEP_ARROW .'</td>
            <td>'. $STEP_3_LINK . $STEP3 .'</span></td>
			<td>'. STEP_ARROW .'</td>
            <td>'. $STEP_4_LINK . $STEP4 .'</span></td>
			<td>'. STEP_ARROW .'</td>
            <td>'. $STEP_5_LINK . $STEP5 .'</span></td>
            </tr>
          <tr align="center"> 
            <td ><font face="Arial, Helvetica, sans-serif">'. $STEP_1_LINK . STEP1_TEXT .'</span></font></td>
            <td><font face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_2_LINK . STEP2_TEXT .'</span></font></td>
            <td><font face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_3_LINK . STEP3_TEXT .'</span></font></td>
			<td><font face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_4_LINK . STEP4_TEXT .'</span></font></td>
			<td><font face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
            <td><font face="Arial, Helvetica, sans-serif">'. $STEP_5_LINK . STEP5_TEXT .'</span></font></td>
            </tr>
          </table>
	';
	return $TABLE;
}

function writeButtons ($BACK_TAB = 0, $NEXT_TAB = 0, $EVENT_ID = 0, $FORM_SAVE = 0, $ACTION = '')
{
	echo '		
	<!-- START BUTTONS AREA -->
	<div class="buttons">
	<br />
	<div style="border:1px solid #174D6F; padding:2px; ">
	<div style="background-color:#174D6F; padding:5px; ">';
	echo '<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr>';
	if ($BACK_TAB != 0)
	{
		echo '<td width="33%" align="left"><input type="button" name="Button4" style="width:80px;" value="Back" onclick="hideGroupExcept(\'creator\','.$BACK_TAB.')" /></td>';
	} else {
		echo '<td width="33%">&nbsp;</td>';
	}
	
	
	if ($ACTION == 'edit')
	{
		echo '
		<td align="center" width="33%">
		<input type="button" name="Button" style="width:80px;" value="Cancel" onclick="document.location=\'event_list.php\';" />
		aa<input class="submit" type="submit" value="Save" style="width:80px;" onclick="document.getElementById(\'form_action\').value=\'UPDATE\';" />
		</td>
		';
	} elseif ($ACTION == 'edit_save')
	{
		echo '
		<td align="center" width="33%">
		<input type="button" name="Button" style="width:80px;" value="Cancel" onclick="document.location=\'event_list.php\';" />
		<input class="submit" type="submit" value="Save" style="width:80px;" onclick="document.getElementById(\'form_action\').value=\'UPDATE_SAVE\';" />
		</td>
		';
	} else {
		echo '
		<td align="center" width="33%">
		<input type="button" name="Button" style="width:80px;" value="Cancel" onclick="document.location=\'event_list.php\';" />
		<input class="submit" type="submit" value="Save" style="width:80px;" onclick="document.getElementById(\'form_action\').value=\'SAVE\';" />
		</td>';
	}
	
		
	
	if ($FORM_SAVE == 0)
	{
		if ($NEXT_TAB != 0)
		{
			echo '<td width="33%" align="right"><input type="button" name="Button4" style="width:80px;" value="Next" onclick="hideGroupExcept(\'creator\','.$NEXT_TAB.')" /></td>';
		} else {
			echo '<td width="33%">&nbsp;</td>';
		}
	} else {
		if ($EVENT_ID != 0)
		{
			echo '<td width="33%" align="right"><input type="button" name="Button" style="width:80px;" value="Next" onclick="document.getElementById(\'form_action\').value=\'UPDATE_PREVIEW\'; document.form1.submit();" /></td>';
		} else {
			echo '<td width="33%" align="right"><input type="button" name="Button" style="width:80px;" value="Next" onclick="document.getElementById(\'form_action\').value=\'SAVE_PREVIEW\'; document.form1.submit();" /></td>';
		}
	}
	echo '</tr></table>';
	echo '
	</div>
	</div>
	</div>
	<!-- END BUTTONS AREA -->';
}






?>







<br /><br />

	<script type="text/javascript">
		function loadContent1(id) {
			$("#contentArea1").load("content/vendor/terms_and_conditions.php");
		}
	</script>

<div id="creator1" > 
	<?PHP echo displayStepTable(1, Get('id')); ?>
	<br /><br />
	TERMS AND CONDITIONS
	<br />
	<p align="center">
	<div id="contentArea1" style="width: 90%; height: 150px; overflow-y: scroll; scrollbar-arrow-color: blue; 
	scrollbar-face-color: #e7e7e7; scrollbar-3dlight-color: #a0a0a0; scrollbar-darkshadow-color: #888888;
	border-top: 1px dashed #888888;
	">
	</div>
	</p>
	<script type="text/javascript">
	loadContent1();
	</script>
</div>

<div id="creator2" > 
	<?PHP echo displayStepTable(2, Get('id')); ?>
	<br /><br />
	SPONSORSHIP MATRIX
	<div id="contentArea2"></div>
	
	
	
			
	Choice of one (1) Event Marketing Opportunities: 
    Breakfasts � choice of one
    Breaks � choice of one     
    Conference Bag/Portfolio     
    Email Center including printers     
    Massage Chairs     
    Conference Guide     
    Pocket Agenda     
    Shipping Center     
    Showcase Session 1     
    Showcase Session 2     
    Wireless Network 

		
		
</div>

<div id="creator3" > 
	<?PHP echo displayStepTable(3, Get('id')); ?>
	<br /><br />

<script language="JavaScript">
function divSponsorDetailsShowHide(source)
{
	document.getElementById('sponsor_details_gold').style.display='none';
	document.getElementById('sponsor_details_platinum').style.display='none';

	var checkVal = document.getElementById(source).value;
	switch(checkVal)
	{
		case "platinum":
		  document.getElementById('sponsor_details_gold').style.display='none';
		  document.getElementById('sponsor_details_platinum').style.display='';
		  break;    
		case "gold":
		  document.getElementById('sponsor_details_platinum').style.display='none';
		  document.getElementById('sponsor_details_gold').style.display='';
		  break;
		default:
	}
}

function divPaymentMethodShowHide(source)
{
	document.getElementById('payment_method_po').style.display='none';
	document.getElementById('payment_method_cc').style.display='none';

	var checkVal = document.getElementById(source).value;
	switch(checkVal)
	{
		case "purchaseorder":
		  document.getElementById('payment_method_cc').style.display='none';
		  document.getElementById('payment_method_po').style.display='';
		  break;    
		case "creditcard":
		  document.getElementById('payment_method_po').style.display='none';
		  document.getElementById('payment_method_cc').style.display='';
		  break;
		default:
	}
}
</script>
	
	<?PHP
	include_once "../../lib/form_helper.php";
	global $THIS_PAGE_QUERY;

	$TABLE 				= 'deliverables_item';
	$flash_field 		= 'title';
	$THIS_PAGE_QUERY 	.= ((strpos($THIS_PAGE_QUERY, '?')===false))? '?method=insert' : '&method=insert';
	$FormDataArray = array (
		
		"form|$THIS_PAGE_QUERY|post|cmxform",

		"h1|COMPANY INFORMATION",		
		"code|<div style='border:1px dashed #333333; background:#c6c6c6;'>",
		"text|Company Name|company_name|Y|40|80",
		"text|Company Address 1|company_address1|Y|40|80",
		"text|Company Address 2|company_address2|Y|40|80",
		"text|City / Province|company_city|Y|40|80",
		"intstate|State|company_state|Y|40|80",
		"country|Country|company_country|Y|40|80",
		"text|Zip / Postal Code|company_zip|Y|40|80",
		"code|</div>",
		"code|<br /><br />",


		"h1|CONTACT INFORMATION",
		"code|<div style='border:1px dashed #333333; background:#c6c6c6;'>",
		"text|Primary Contact Name|primary_contact_name|Y|40|80",
		"email|Primary Contact Email|primary_contact_email|Y|40|80",
		"phone|Primary Contact Phone|primary_contact_phone|Y|40|80",
		"code|</div>",
		"code|<br /><br />",


		"h1|SPONSORSHIP LEVEL",
		"code|<div style='border:1px dashed #333333; background:#c6c6c6;'>",
		"select|Sponsorship Level|type|Y|onchange=\"divSponsorDetailsShowHide(this.id)\"|platinum=Platinum|gold=Gold|silver=Silver|disti=DISTI Pavillion Cornerstone|disti_sub=DISTI Pavillion Member",
		"code|<div id='sponsor_details_platinum' style='border-top:1px dashed #333333; padding-left:20px;'>",
			"h3|SPONSORSHIP DETAILS - PLATINUM",
			"info|Sponsorship Details|<h2>Cost: $75,000</h2><br />
			<li>8' x 6' Exhibit Booth</li>
			<li>6 Dedicated Showcase Hours</li>
			<li>Promotional item dropped at attendee's room</li>
			<li>Electronic lead tracking</li>
			<li>Tech Insight via Intel will highlight sponsors</li>
			<li>Complimentary Suite at the Red Rock Resort (Six (6) Suites for 2 nights)</li>
			",
		"code|</div>",
		"code|<div id='sponsor_details_gold' style='border-top:1px dashed #666666; padding-left:20px;'>",
			"h3|SPONSORSHIP DETAILS - GOLD",
			"info|Sponsorship Details|<h2>Cost: $45,000</h2><br />
			<li>8' x 6' Exhibit Booth</li>
			<li>6 Dedicated Showcase Hours</li>
			<li>Promotional item dropped at attendee's room</li>
			<li>Electronic lead tracking</li>
			<li>Tech Insight via Intel will highlight sponsors</li>
			<li>Complimentary Suite at the Red Rock Resort (Four (4) Suites for 2 nights)</li>
			",
			"select|Marketing Opportunity|type|N||1=(1) Breakfast|2=(1) Break|3=Conference Bag / Portfolio|4=Email Center|5=Massage Chairs|6=Conference Guide|7=Pocket Agenda|8=Shipping Center|9=Showcase Session 1|10=Showcase Session 2|11=Wireless Network",
		"code|</div>",
		"code|</div>",
		"endform"
	);
	$OUTPUT = $FormDataArray;
	//echo $OUTPUT;





	
	
	if (!Post('SUBMIT') or $ERROR) {
    WriteError($ERROR);
    echo OutputForm($OUTPUT,Post('SUBMIT'));
}
	?>
</div>

<div id="creator4" > 
	<?PHP echo displayStepTable(4, Get('id')); ?>
	<br /><br />
	
		<?PHP
	include_once "../../lib/form_helper.php";
	global $THIS_PAGE_QUERY;

	$TABLE 				= 'deliverables_item';
	$flash_field 		= 'title';
	$THIS_PAGE_QUERY 	.= ((strpos($THIS_PAGE_QUERY, '?')===false))? '?method=insert' : '&method=insert';
	$FormDataArray = array (
		"form|$THIS_PAGE_QUERY|post|cmxform",


		"h1|BILLING CONTACT INFORMATION",		
		"code|<div style='border:1px dashed #333333; background:#c6c6c6;'>",
		"text|Billing Contact Name|billing_contact_name|Y|40|80",
		"email|Billing Contact Email|billing_contact_email|Y|40|80",
		"phone|Billing Contact Phone|billing_contact_phone|Y|40|80",
		"fax|Billing Contact Fax|billing_contact_fax|N|40|80",
		"code|</div>",
		"code|<br /><br />",



		"h1|PAYMENT METHOD",		
		"code|<div style='border:1px dashed #333333; background:#c6c6c6;'>",
		"select|Type|payment_type|Y|onchange=\"divPaymentMethodShowHide(this.id)\"|purchaseorder=Purchase Order|creditcard=Credit Card",
		"code|<div id='payment_method_cc' style='border-top:1px dashed #333333; padding-left:20px;'>",
				"text|Name on Credit Card|payment_cc_name|Y|40|80",
				"creditcard|Credit Card #|payment_cc_number|Y",
				"dateCC|Expiration Date|payment_cc_expire|Y",
				"text|CCV (3 or 4 digits)|payment_cc_ccv|Y|4|4",
				"text|Address|payment_cc_address|Y|40|80",
				"phone|Billing Contact Phone|payment_contact_phone|Y|40|80",
		"code|</div>",
		"code|<div id='payment_method_po' style='border-top:1px dashed #333333; padding-left:20px;'>",
				"text|Purchase Order Number (if available)|payment_po_number|N|40|40",
				"checkbox|Require Estimate to Generate PO|payment_estimate_requested|0|0",
		"code|</div>",
		"code|</div>",
		"code|<br /><br />",


		"h1|OTHER",		
		"code|<div style='border:1px dashed #333333; background:#c6c6c6;'>",
		"text|Promo Code|promo_code|Y|40|80",
		"textarea|Comments|comments|Y|40|5",
		"code|</div>",
		"code|<br /><br />",

		"submit|Submit|submit",
		"endform"
	);
	$OUTPUT = $FormDataArray;
	//echo $OUTPUT;
	
	
	if (!Post('SUBMIT') or $ERROR) {
    WriteError($ERROR);
    echo OutputForm($OUTPUT,Post('SUBMIT'));
}
	?>
</div>


<script src="js/jquery-latest.js" type="text/javascript"></script>
<script type="text/javascript">
	function loadContent(file) {
		$("#contentArea2").load(file);
	}
	
	//javascript:loadContent("content/sponsorship_matrix.htm");
</script>

<script language="JavaScript" type="text/javascript">
// SCRIPTS FOR THE FIRST DIV SECTION

function processJavascript() {
	<?PHP
	if (is_numeric(Get('tab')))
	{
		$num = Get('tab');
		echo "hideGroupExcept('creator',$num);";
	} else {
		echo "hideGroupExcept('creator',1);";
	}
	?>
}



function getId(id)
{
	if(!id) 
		return false; 
	else 
		return document.getElementById(id);
}

function hideGroup(group)
{
	var i=1;
	while (getId(group+i))
	{
		getId(group+i).style.display='none';  
		i++;
	} 
}

function hideGroupSpecific(group,id)
{
	getId(group+id).style.display='none';  
}

function hideGroupExcept(group,except)
{
	hideGroup(group);
	if (getId(group+except))
	{
		getId(group+except).style.display='';
	}

}

</script>


<?PHP
#===============================================================================
#===============================================================================
#===============================================================================
#===============================================================================
#===============================================================================
?>


<script language="JavaScript">
divSponsorDetailsShowHide();
</script>

<script language="JavaScript">
divPaymentMethodShowHide();
</script>

<script language="JavaScript">
hideGroupExcept('creator',1);
</script>

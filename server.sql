-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 16, 2015 at 06:44 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `barberb_intelserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `first_name` varchar(32) NOT NULL DEFAULT '',
  `last_name` varchar(32) NOT NULL DEFAULT '',
  `super_user` tinyint(4) NOT NULL DEFAULT '0',
  `roles` varchar(128) NOT NULL DEFAULT '',
  `created_by` varchar(64) NOT NULL DEFAULT '',
  `company_name` varchar(32) NOT NULL DEFAULT '',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `email`, `password`, `first_name`, `last_name`, `super_user`, `roles`, `created_by`, `company_name`, `active`, `updated`, `created`) VALUES
(1, 'barberb@barberb.com', 'benjimama', 'Benjamin', 'Barber', 1, 'role1, role2, role3', 'Benjamin Barber', '', 1, '0000-00-00 00:00:00', '2015-12-16 03:08:13'),
(6, 'test@barberb.com', 'test', 'Test', 'User', 0, 'role1, role2, role3', 'Benjamin Barber', 'Test Company', 1, '0000-00-00 00:00:00', '2015-12-16 03:08:37');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(80) NOT NULL DEFAULT '',
  `username` varchar(80) NOT NULL DEFAULT '',
  `password` varchar(40) NOT NULL DEFAULT '',
  `address1` varchar(80) NOT NULL DEFAULT '',
  `address2` varchar(80) NOT NULL DEFAULT '',
  `city` varchar(40) NOT NULL DEFAULT '',
  `state` varchar(40) NOT NULL DEFAULT '',
  `postal_code` varchar(20) NOT NULL DEFAULT '',
  `country` varchar(40) NOT NULL DEFAULT '',
  `phone_number` varchar(20) NOT NULL DEFAULT '',
  `fax_number` varchar(20) NOT NULL DEFAULT '',
  `website` varchar(80) NOT NULL DEFAULT '',
  `business_id` varchar(40) NOT NULL DEFAULT '',
  `created_by` varchar(80) NOT NULL DEFAULT '',
  `updated_by` varchar(80) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(80) NOT NULL DEFAULT '',
  `last_name` varchar(80) NOT NULL DEFAULT '',
  `job_title` varchar(80) NOT NULL DEFAULT '',
  `address1` varchar(40) NOT NULL DEFAULT '',
  `address2` varchar(40) NOT NULL DEFAULT '',
  `city` varchar(20) NOT NULL DEFAULT '',
  `state` varchar(40) NOT NULL DEFAULT '',
  `country` varchar(20) NOT NULL DEFAULT '',
  `postal_code` varchar(20) NOT NULL DEFAULT '',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `phone_number` varchar(40) NOT NULL DEFAULT '',
  `fax_number` varchar(20) NOT NULL DEFAULT '',
  `cell_number` varchar(80) NOT NULL DEFAULT '',
  `password` varchar(80) NOT NULL DEFAULT '',
  `login_id` varchar(40) NOT NULL DEFAULT '',
  `email_address` varchar(80) NOT NULL DEFAULT '',
  `email_format` varchar(4) NOT NULL DEFAULT '',
  `created_by` varchar(80) NOT NULL DEFAULT '',
  `updated_by` varchar(80) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_admin_information`
--

CREATE TABLE IF NOT EXISTS `db_admin_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(80) NOT NULL DEFAULT '',
  `table_title` varchar(80) NOT NULL DEFAULT '',
  `sort_variables` varchar(80) NOT NULL DEFAULT '',
  `flash_variable` varchar(80) NOT NULL DEFAULT '',
  `field_titles` text NOT NULL,
  `form_data` text NOT NULL,
  `field_lables` text NOT NULL,
  `default_fields` text NOT NULL,
  `limit_default` int(11) NOT NULL DEFAULT '0',
  `unique_fields` text NOT NULL,
  `access_level` smallint(2) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverables_config`
--

CREATE TABLE IF NOT EXISTS `deliverables_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverables_groups`
--

CREATE TABLE IF NOT EXISTS `deliverables_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL DEFAULT '',
  `item_id` varchar(255) NOT NULL DEFAULT '',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverables_items`
--

CREATE TABLE IF NOT EXISTS `deliverables_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `due_date` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `upload_file_extensions` varchar(255) NOT NULL DEFAULT '',
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `file_url` varchar(255) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `alert_email` int(1) NOT NULL DEFAULT '0',
  `alert_email_day_1` int(5) NOT NULL DEFAULT '0',
  `alert_email_day_2` int(5) NOT NULL DEFAULT '0',
  `verification` int(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverables_notes`
--

CREATE TABLE IF NOT EXISTS `deliverables_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL DEFAULT '0',
  `note` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverables_users`
--

CREATE TABLE IF NOT EXISTS `deliverables_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `email_notify_reports` int(1) NOT NULL DEFAULT '0',
  `email_notify_changes` int(1) NOT NULL DEFAULT '0',
  `email_notify_uploads` int(1) NOT NULL DEFAULT '0',
  `email_notify_approvals` int(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `discount_code` varchar(16) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `discount_amount` varchar(8) NOT NULL DEFAULT '',
  `discount_type` varchar(8) NOT NULL DEFAULT '',
  `expiration_date` varchar(32) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_bounced`
--

CREATE TABLE IF NOT EXISTS `email_bounced` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT '',
  `message_id` varchar(250) NOT NULL DEFAULT '',
  `table` varchar(200) NOT NULL DEFAULT '',
  `row_id` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(250) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_list`
--

CREATE TABLE IF NOT EXISTS `email_list` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(40) NOT NULL DEFAULT '',
  `first_name` varchar(40) NOT NULL DEFAULT '',
  `last_name` varchar(40) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `company_name` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `country` varchar(40) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `contact_id` varchar(80) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_list2`
--

CREATE TABLE IF NOT EXISTS `email_list2` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(40) NOT NULL DEFAULT '',
  `first_name` varchar(40) NOT NULL DEFAULT '',
  `last_name` varchar(40) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `company_name` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `country` varchar(40) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_store`
--

CREATE TABLE IF NOT EXISTS `email_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `subject` varchar(80) NOT NULL DEFAULT '',
  `email_html` text NOT NULL,
  `email_text` text NOT NULL,
  `from_name` varchar(80) NOT NULL DEFAULT '',
  `from_email` varchar(80) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `creator` varchar(80) NOT NULL DEFAULT '',
  `from_name` varchar(80) NOT NULL DEFAULT '',
  `from_email` varchar(80) NOT NULL DEFAULT '',
  `subject` varchar(80) NOT NULL DEFAULT '',
  `attachment` varchar(255) NOT NULL DEFAULT '',
  `email_html` text NOT NULL,
  `email_text` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_unsubscribe`
--

CREATE TABLE IF NOT EXISTS `email_unsubscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT '',
  `message_id` varchar(250) NOT NULL DEFAULT '',
  `table` varchar(200) NOT NULL DEFAULT '',
  `row_id` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(250) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(80) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `start_time` time NOT NULL DEFAULT '00:00:00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `end_time` time NOT NULL DEFAULT '00:00:00',
  `timezone_gmt` int(8) NOT NULL DEFAULT '0',
  `location_city` varchar(32) NOT NULL DEFAULT '',
  `location_state` varchar(32) NOT NULL DEFAULT '',
  `location_country` varchar(32) NOT NULL DEFAULT '',
  `loc_address_1` varchar(64) NOT NULL DEFAULT '',
  `loc_address_2` varchar(64) NOT NULL DEFAULT '',
  `loc_postalcode` int(16) NOT NULL DEFAULT '0',
  `location_description` text NOT NULL,
  `languages` text NOT NULL,
  `currency` text NOT NULL,
  `showcase_vendors` text NOT NULL,
  `attendee_cost` int(16) NOT NULL DEFAULT '0',
  `event_owner_name` varchar(80) NOT NULL DEFAULT '',
  `event_owner_phone` varchar(32) NOT NULL DEFAULT '',
  `event_type` varchar(32) NOT NULL DEFAULT '',
  `event_requestor_name` varchar(32) NOT NULL DEFAULT '',
  `event_requestor_phone` varchar(32) NOT NULL DEFAULT '',
  `membership_status_registered` tinyint(1) NOT NULL DEFAULT '0',
  `membership_status_member` tinyint(1) NOT NULL DEFAULT '0',
  `membership_status_associate` tinyint(1) NOT NULL DEFAULT '0',
  `membership_status_premiere` tinyint(1) NOT NULL DEFAULT '0',
  `membership_status_all` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_method` varchar(80) NOT NULL DEFAULT '',
  `created_by` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_series`
--

CREATE TABLE IF NOT EXISTS `event_series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(20) NOT NULL DEFAULT '',
  `geo` varchar(20) NOT NULL DEFAULT '',
  `series_name` varchar(80) NOT NULL DEFAULT '',
  `notes` text NOT NULL,
  `created_by` varchar(80) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `financials`
--

CREATE TABLE IF NOT EXISTS `financials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL DEFAULT '0',
  `event_series_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(8) NOT NULL DEFAULT '0',
  `company_id` int(8) NOT NULL DEFAULT '0',
  `transaction_type` varchar(16) NOT NULL DEFAULT '',
  `currency` varchar(16) NOT NULL DEFAULT '',
  `transaction_date` varchar(16) NOT NULL DEFAULT '',
  `transaction_time` varchar(16) NOT NULL DEFAULT '',
  `transaction_amount` int(16) NOT NULL DEFAULT '0',
  `created_by` varchar(16) NOT NULL DEFAULT '',
  `invoice_date` varchar(16) NOT NULL DEFAULT '',
  `notes` text NOT NULL,
  `success` tinyint(1) NOT NULL DEFAULT '0',
  `po_id` int(8) NOT NULL DEFAULT '0',
  `invoice_id` int(8) NOT NULL DEFAULT '0',
  `estimate_id` int(8) NOT NULL DEFAULT '0',
  `payment_id` int(8) NOT NULL DEFAULT '0',
  `deposit_id` int(8) NOT NULL DEFAULT '0',
  `contact_method` varchar(16) NOT NULL DEFAULT '',
  `contact_reason` text NOT NULL,
  `contact_resolution` text NOT NULL,
  `original_invoice_date` varchar(16) NOT NULL DEFAULT '',
  `original_invoice_amount` int(16) NOT NULL DEFAULT '0',
  `payment_method` varchar(16) NOT NULL DEFAULT '',
  `tracking_number` varchar(32) NOT NULL DEFAULT '',
  `source_account` varchar(32) NOT NULL DEFAULT '',
  `destination_account` varchar(32) NOT NULL DEFAULT '',
  `cancel_type` varchar(16) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_method` varchar(16) NOT NULL DEFAULT '',
  `contact_id` int(8) NOT NULL DEFAULT '0',
  `estimate_number` varchar(255) NOT NULL DEFAULT '',
  `po_number` varchar(255) NOT NULL DEFAULT '',
  `invoice_number` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mail_queue`
--

CREATE TABLE IF NOT EXISTS `mail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `table` varchar(40) NOT NULL DEFAULT '',
  `table_id` int(11) NOT NULL DEFAULT '0',
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error` varchar(255) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mail_queue_store`
--

CREATE TABLE IF NOT EXISTS `mail_queue_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `table` varchar(40) NOT NULL DEFAULT '',
  `table_id` int(11) NOT NULL DEFAULT '0',
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `error` varchar(255) NOT NULL DEFAULT '',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `creator` varchar(80) NOT NULL DEFAULT '',
  `from_name` varchar(80) NOT NULL DEFAULT '',
  `from_email` varchar(80) NOT NULL DEFAULT '',
  `subject` varchar(80) NOT NULL DEFAULT '',
  `email_html` text NOT NULL,
  `email_text` text NOT NULL,
  `send_date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `table` varchar(80) NOT NULL DEFAULT '',
  `where` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `title`, `name`, `image`, `active`, `updated`, `created`) VALUES
(1, 'Add Template', 'add_template', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(3, 'Admin Users', 'admin_users', 'people.gif', 1, '2008-12-06 22:42:30', '2008-11-30 12:55:46'),
(7, 'Companies', 'companies', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(8, 'Company Report', 'company_report', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(10, 'Contacts', 'contacts', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(11, 'Create Message', 'create_message', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(12, 'Deliverables', 'deliverables', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(13, 'Deliverables Sample', 'deliverables_sample', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(14, 'Discounts', 'discounts', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(15, 'Edit Message', 'edit_message', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(17, 'Edit Template', 'edit_template', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(18, 'Email List Test', 'email_list_test', 'mail.gif', 1, '2008-12-07 11:56:11', '2008-11-30 12:55:46'),
(19, 'Events', 'events', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(21, 'Financials', 'financials', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(22, 'Financials1', 'financials1', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(23, 'Financials2', 'financials2', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(24, 'Import', 'import', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(26, 'Mail Queue', 'mail_queue', 'mail.gif', 1, '2008-12-07 11:56:11', '2008-11-30 12:55:46'),
(27, 'Mail Send', 'mail_send', 'mail.gif', 1, '2008-12-07 11:56:11', '2008-11-30 12:55:46'),
(28, 'Mail Status', 'mail_status', 'mail.gif', 1, '2008-12-07 11:56:11', '2008-11-30 12:55:46'),
(29, 'Maps', 'maps', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(30, 'Modules', 'modules', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(31, 'Random Generator', 'random_generator', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(32, 'Roles', 'roles', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(54, 'Update My Profile', 'update_profile', 'people.gif', 1, '2008-12-07 13:14:21', '0000-00-00 00:00:00'),
(34, 'Sponsorship', 'sponsorship', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(35, 'Summary', 'summary', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(36, 'Super Test', 'super_test', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(37, 'Test Page', 'test_page', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(38, 'To Do', 'to_do', 'checkmark.gif', 0, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(39, 'Vendor Approval Status', 'vendor_approval_status', 'checkmark.gif', 0, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(40, 'Vendor Event Registrations', 'vendor_event_registrations', 'checkmark.gif', 0, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(41, 'View List', 'view_list', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(42, 'View Record', 'view_record', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(43, 'Financials', '_financials', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-11-30 12:55:46'),
(44, 'Emailstorelist', 'emailstorelist', 'mail.gif', 1, '2008-12-07 11:56:11', '2008-12-03 15:51:12'),
(45, 'Email Template List', 'email_template_list', 'mail.gif', 1, '2008-12-07 11:56:11', '2008-12-03 15:51:12'),
(48, 'Sponsor Approval Status', 'sponsor_approval_status', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-12-03 15:51:12'),
(49, 'Sponsor Event Registrations', 'sponsor_event_registrations', 'checkmark.gif', 1, '2008-12-04 16:03:34', '2008-12-03 15:51:12'),
(50, 'Find Map', 'find_map', 'checkmark.gif', 0, '2008-12-05 16:16:46', '2008-12-04 20:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `perms` text NOT NULL,
  `created_by` varchar(80) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `perms`, `created_by`, `active`, `updated`, `created`) VALUES
(1, 'wh', 'a:36:{s:12:"add_template";s:3:"`9`";s:11:"admin_users";s:3:"`9`";s:9:"companies";s:3:"`9`";s:14:"company_report";s:3:"`9`";s:8:"contacts";s:3:"`9`";s:14:"create_message";s:3:"`9`";s:12:"deliverables";s:3:"`9`";s:19:"deliverables_sample";s:3:"`9`";s:9:"discounts";s:3:"`9`";s:12:"edit_message";s:3:"`9`";s:13:"edit_template";s:3:"`9`";s:15:"email_list_test";s:3:"`9`";s:6:"events";s:3:"`9`";s:10:"financials";s:3:"`9`";s:11:"financials1";s:3:"`9`";s:11:"financials2";s:3:"`9`";s:6:"import";s:3:"`9`";s:10:"mail_queue";s:3:"`9`";s:9:"mail_send";s:3:"`9`";s:11:"mail_status";s:3:"`9`";s:4:"maps";s:3:"`9`";s:7:"modules";s:3:"`9`";s:16:"random_generator";s:3:"`9`";s:5:"roles";s:3:"`9`";s:14:"update_profile";s:3:"`0`";s:11:"sponsorship";s:3:"`9`";s:7:"summary";s:3:"`9`";s:10:"super_test";s:3:"`9`";s:9:"test_page";s:3:"`9`";s:9:"view_list";s:3:"`9`";s:11:"view_record";s:3:"`9`";s:11:"_financials";s:3:"`9`";s:14:"emailstorelist";s:3:"`9`";s:19:"email_template_list";s:3:"`9`";s:23:"sponsor_approval_status";s:3:"`9`";s:27:"sponsor_event_registrations";s:3:"`9`";}', '', 1, '2008-12-09 19:01:28', '2008-11-29 14:31:20'),
(4, 'intel', 'a:38:{s:12:"add_template";s:1:"5";s:11:"admin_users";s:1:"0";s:9:"companies";s:1:"0";s:14:"company_report";s:1:"0";s:8:"contacts";s:1:"0";s:14:"create_message";s:1:"0";s:12:"deliverables";s:1:"0";s:19:"deliverables_sample";s:1:"0";s:9:"discounts";s:1:"0";s:12:"edit_message";s:1:"0";s:13:"edit_template";s:1:"0";s:15:"email_list_test";s:1:"0";s:6:"events";s:1:"0";s:10:"financials";s:1:"0";s:11:"financials1";s:1:"0";s:11:"financials2";s:1:"0";s:6:"import";s:1:"0";s:10:"mail_queue";s:1:"0";s:9:"mail_send";s:1:"0";s:11:"mail_status";s:1:"0";s:4:"maps";s:1:"0";s:7:"modules";s:1:"0";s:16:"random_generator";s:1:"0";s:5:"roles";s:1:"0";s:14:"update_profile";s:1:"0";s:11:"sponsorship";s:1:"0";s:7:"summary";s:1:"0";s:10:"super_test";s:1:"0";s:9:"test_page";s:1:"0";s:9:"view_list";s:1:"0";s:11:"view_record";s:1:"0";s:11:"_financials";s:1:"0";s:14:"emailstorelist";s:1:"0";s:19:"email_template_list";s:1:"0";s:23:"sponsor_approval_status";s:1:"0";s:27:"sponsor_event_registrations";s:1:"0";s:18:"deliverables_admin";s:1:"0";s:12:"translations";s:1:"0";}', 'Michael Petrovich', 1, '2008-12-08 23:52:31', '2008-12-04 20:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `sponsorships`
--

CREATE TABLE IF NOT EXISTS `sponsorships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `sponsorship_level` varchar(80) NOT NULL DEFAULT '',
  `opportunity_description` varchar(255) NOT NULL DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `company_id` varchar(11) NOT NULL DEFAULT '',
  `status` varchar(32) NOT NULL DEFAULT 'Available',
  `created_by` varchar(80) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_event_registrations`
--

CREATE TABLE IF NOT EXISTS `sponsor_event_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `primary_contact_id` int(11) NOT NULL DEFAULT '0',
  `secondary_contact_id` int(11) NOT NULL DEFAULT '0',
  `billing_contact_id` int(11) NOT NULL DEFAULT '0',
  `sponsorship_level` varchar(80) NOT NULL DEFAULT '',
  `number_of_badges` smallint(11) NOT NULL DEFAULT '0',
  `hotel_rooms` smallint(11) NOT NULL DEFAULT '0',
  `charge_for_hotel` tinyint(1) NOT NULL DEFAULT '0',
  `promotional_codes` varchar(255) NOT NULL DEFAULT '',
  `approval_status` varchar(40) NOT NULL DEFAULT '',
  `payment_type` varchar(40) NOT NULL DEFAULT '',
  `purchase_order` varchar(80) NOT NULL DEFAULT '',
  `payment_estimate_requested` varchar(4) NOT NULL DEFAULT '',
  `other_items` text NOT NULL,
  `marketing_opportunity` varchar(80) NOT NULL DEFAULT '',
  `marketing_opportunity_id` int(11) NOT NULL DEFAULT '0',
  `comments` text NOT NULL,
  `notes` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_update_log`
--

CREATE TABLE IF NOT EXISTS `table_update_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(80) NOT NULL DEFAULT '',
  `table_id` int(11) NOT NULL DEFAULT '0',
  `action` varchar(20) NOT NULL DEFAULT '',
  `old_record` text,
  `new_record` text,
  `changed_by` varchar(80) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `to_do`
--

CREATE TABLE IF NOT EXISTS `to_do` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(80) NOT NULL DEFAULT '',
  `priority` tinyint(1) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `assign_to` varchar(80) NOT NULL DEFAULT '',
  `completion_date` date NOT NULL DEFAULT '0000-00-00',
  `status` varchar(40) NOT NULL DEFAULT '',
  `created_by` varchar(80) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL DEFAULT '',
  `super_user` tinyint(4) NOT NULL DEFAULT '0',
  `roles` text NOT NULL,
  `created_by` varchar(64) NOT NULL DEFAULT '',
  `company_name` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `firstname`, `lastname`, `super_user`, `roles`, `created_by`, `company_name`) VALUES
(1, 'barberb@barberb.com', 'benjimama', 'Benjamin', 'Barber', 1, '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

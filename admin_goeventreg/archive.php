<?php
//  copies files to archive directory.  Appends file date and replaces folders  delimiters with @ signs

//================ Needed Definitions ===================
// configure this section

$TITLE             = 'Website';
$FOLDER_TO_ARCHIVE = dirname(__FILE__);
$ARCHIVE_FOLDER    = $FOLDER_TO_ARCHIVE . '/archive';
$EXCLUDE_LIST      = 'Desktop.ini,Thumbs,logs,tmp';  // already will exclude the archive folder

$IMAGELIST = array('.gif','.jpg','.png');

//======================================================
$ROOT        = $_SERVER['DOCUMENT_ROOT'];
$REQUEST_URI = strTo($_SERVER['REQUEST_URI'],'?');

function Post($name) {return (isset($_POST[$name]))? $_POST[$name] : '';}
function Get($name) {return (isset($_GET[$name]))? $_GET[$name] : '';}

function strTo($string, $to) {
  $i = strpos($string,$to);
  if ( $i !== false ) return substr($string,0,$i);
    else return $string;
}

function StripRoot($str)
{
    global $ROOT;
    $str = str_replace('\\', '/', $str);  // needed for Windows
    $str = str_replace($ROOT,'',$str);
    return $str;
}

//---------CHECK IF ARRAY ITEMS IN STRING----------
function ArrayItemsWithinStr($myarray,$str)
{
    $arraycount = count($myarray);
    if (empty($str) or ($arraycount==0)) return false;
    for($i=0; $i<$arraycount; $i++) {
        if (stristr($str,$myarray[$i]) != '') return true;
    }
    return false;
}


function GetDirectory() {
    //gets a directory and subdirectory filelist with optional include and exclude string
    //$url,$includestr,$excludestr
    $numargs  = func_num_args();
    $url = func_get_arg(0);
    $includestr = ($numargs>1)? func_get_arg(1) : '';
    $excludestr = ($numargs>2)? func_get_arg(2) : '';
    $files  = array();
    if (!file_exists($url)) return $files;
    if (!empty($includestr)) $include_strings = explode(',',$includestr);
    if (!empty($excludestr)) $exclude_strings = explode(',',$excludestr);
    $dir = opendir("$url/");
    while (false !== ($file = readdir($dir))) {
        if ($file != '.' && $file != '..') {
            if (is_dir("$url/$file")) {
                $echeck = (!empty($excludestr))? !ArrayItemsWithinStr($exclude_strings,"$url/$file") : true;
                if ($echeck and !eregi('.svn',$file)) {
                    $sfiles = GetDirectory("$url/$file",$includestr,$excludestr);
                    foreach ($sfiles as $f) $files[] = "$file/$f";
                }
            } else {
                $icheck = (!empty($includestr))? ArrayItemsWithinStr($include_strings,$file) : true;
                $echeck = (!empty($excludestr))? !ArrayItemsWithinStr($exclude_strings,$file) : true;
                if ($icheck and $echeck) $files[] = $file;
            }
        }
    }
    closedir($dir);
    natcasesort($files);
    $files = array_values($files);
    return $files;

}

$ARCHIVE     = Post('ARCHIVE');
$VIEW        = Post('VIEW');
$LISTARCHIVE = Post('LISTARCHIVE');
$viewfile    = Get('viewfile');
$INCLUDE     = Post('INCLUDE');

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Archive &mdash; <?php echo $TITLE; ?></title>
  <style type="text/css">
     body{background-color:#eee;}
     #content{margin:1em auto; background-color:#fff; padding:1em; border:1px dashed #888;}
     h1{color:#036; border-bottom:2px solid #036; margin-top:0px;}
     h2{background-color:#eee; border-bottom:1px solid #888;}
     #submitbuttons {float:right; text-align:right;}
     #info {float:right; white-space:nowrap; font-size:0.7em; padding:0px 0px 2em 4em;}
     img {border: 1px solid #000;}
  </style>
  <script type="text/javascript" src="../std_gtmm/lib/core.js"></script>
  </head>
<body>
<div id="content">

<form method="post" action="<?php echo $REQUEST_URI; ?>">
<div id="info">
<?php
    $info = "<b>FROM:</b> $FOLDER_TO_ARCHIVE<br /><b>TO:</b> $ARCHIVE_FOLDER";
    echo StripRoot($info);
?>
</div>
<div id="submitbuttons">
<input type="submit" value="View Files to Archive" name="VIEW" />&nbsp;&nbsp;
<input type="submit" value="Archive" name="ARCHIVE" />&nbsp;&nbsp;
<input type="submit" value="List Archive" name="LISTARCHIVE" />
<input type="text" value="<?php echo $INCLUDE; ?>" name="INCLUDE" />
</div>

<h1>Archive &mdash; <?php echo $TITLE; ?></h1>

<?php



if ($viewfile) {
    echo "<h2>FILE: $viewfile</h2>";
    if(ArrayItemsWithinStr($IMAGELIST,$viewfile)) {
        $link = StripRoot("$ARCHIVE_FOLDER/$viewfile");
        echo "<img src=\"$link\" alt=\"IMAGE: $link\" />";
    } else {
        $content = htmlentities(file_get_contents("$ARCHIVE_FOLDER/$viewfile"));
        echo "<textarea rows=\"25\" style=\"width:100%\">$content</textarea>";
    }
}

if ($LISTARCHIVE) {
    echo '<h2>Archive List. . .</h2>';
    $ArchiveFiles = GetDirectory($ARCHIVE_FOLDER,$INCLUDE);
    echo '<ol>';
    foreach ($ArchiveFiles as $file) {
        $link = urlencode($file);
        echo "<li><a href=\"$REQUEST_URI?viewfile=$link\">$file</a></li>\n";
    }
    echo '</ol>';
}


if ($ARCHIVE or $VIEW) {
    define('CONTENT_DIR',$FOLDER_TO_ARCHIVE);
    $exclude_archive = basename($ARCHIVE_FOLDER);

    if($ARCHIVE) echo '<h2>Archiving Files. . .</h2>';
    else  echo '<h2>Viewing Files to Archive. . .</h2>';

    //----------ARCHIVE ALL FILES (/content and /common)-------------

    $ContentFiles = GetDirectory(CONTENT_DIR,'',"/$exclude_archive,$EXCLUDE_LIST");

    $count = 0;

    echo '<ol style="text-align:left; font-size:1.2em;">';

    //------------ARCHIVE CONTENT FILES--------------
    foreach ($ContentFiles as $AF) {
        $filename = CONTENT_DIR."/$AF";
        $filedate = date("Ymdhi",filemtime($filename));
        $AF = str_replace('/','@',$AF);
        if (ArrayItemsWithinStr($IMAGELIST,$filename)) {
            $ext = substr($filename,-4);
        } else $ext = '.php';
        $filename2 = "$ARCHIVE_FOLDER/{$AF}_$filedate$ext";
        if (!file_exists($filename2)) {
            if($ARCHIVE) {
                copy($filename,$filename2);
                $out ="<li>$filename --> $ARCHIVE_FOLDER/{$AF}_$filedate$ext</li>\n";
            } else {
                $out ="<li>$filename</li>\n";
            }
            echo StripRoot($out);
            $count++;
        }
    }
    if ($count==0) {
        echo '<li>All files Archived!</li>';
    }
    echo '</ol>';

}
?>
</form>
</div>
</body>
</html>
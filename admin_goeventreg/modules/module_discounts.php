<?php
#==================================================================================================
#REQUIRED MODULE INFORMATION
#==================================================================================================
$MODULE_INFO['discounts'] = array(
    'Created By'  => 'Benjamin Barber',
    'Description' => 'Create and manage showcase discounts',
    'Created'     => '2008-11-12',
    'Updated'     => '2008-11-12',
);

#==================================================================================================
#FUNCTION DESCRIPTIONS
#==================================================================================================

/*
function discounts_db_CreateTable ()   #creates discount table if none exists

*/


function discounts_db_CreateTable () 
{
    $QUERY = "
CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(11) NOT NULL auto_increment,
  `event` varchar(64) NOT NULL,
  `discount_code` varchar(16) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `discount_amount` varchar(8) NOT NULL,
  `discount_type` varchar(8) NOT NULL,
  `expiration_date` varchar(32) NOT NULL,
  `active` tinyint(1) NOT NULL default '1',
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";

    $RESULT = mysql_query($QUERY);
    return $RESULT;
}


$discounts_FormDataArrayCreate = array (
  "form|$THIS_PAGE|post|name",
  "select|Event|event|required||IST|TST|ICC",
  "text|Code|discount_code|Y|12|12|||",
  "textarea|Description|description|Y|30|6||",
  "text|Discount|discount_amount|Y|12|12||$Mask_Integer",
  "radio|Type of Discount|discount_type|required||dollars|percent",
  "dateYMD|Expiration|expiration_date|Y-M-D|Y|NOW|12",
  "submit|Add Discount|SUBMIT",
  "endform"
);

$discounts_FormDataArrayEdit = array (
  "form|$THIS_PAGE|post|name",
  "select|Event|event|required||IST|TST|ICC",
  "text|Code|discount_code|Y|12|12|||",
  "textarea|Description|description|Y|30|6||",
  "text|Discount|discount_amount|Y|12|12||$Mask_Integer",
  "radio|Type of Discount|discount_type|required||dollars|percent",
  "dateYMD|Expiration|expiration_date|Y-M-D|Y|NOW|12",
  "checkbox|Active||1|0",
  "submit|Save Discount|SUBMIT",
  "endform"
);



$SUBMIT = Post('SUBMIT');
$discounts_ERROR  = '';
$discounts_TableName = 'discounts';


//======================Search Within Files==========================


if ($SUBMIT) {

    $FormArray = ProcessForm($FormDataArray,$Result_Table,'','','',$ERROR); 
	
    if(!$ERROR){
         $fields = db_Keys($FormArray).',created';
         $values = db_Values($FormArray).",NOW()";
         if (db_AddRecord($TABLE,$fields,$values)) {
           AddMessage("Record: [{$FormArray['description']}] Added");
           echo $Result_Table;
         } else {$ERROR .= 'DB Write Error';}
	}
}

function discounts_InputAdd()
{

}

if (!Post('SUBMIT') or $ERROR) {
    WriteError($ERROR);
    echo OutputForm($FormDataArray,Post('SUBMIT'));
}

function discounts_GetPrice($price , $code, $event)
{
    $new_price = '';
	//GET ALL SHORT URLS
	$FIELDS 		= 'discount, discount_type, created, expiration_date';
	$ORDER			= 'created DESC';
	$WHERE			= 'discount_code="'.$code.'" AND event="'.$event.'"';
	$result 		= db_GetArray($TABLE, $FIELDS, $WHERE, $ORDER, 0, 1, $num_rows);
	//$result 		= db_GetArrayAll($TABLE, $FIELDS, $WHERE, $ORDER);

	$row = mysql_fetch_assoc($result);
	if($row['expiration_date'] > date('Y-m-d')) {
		if ($row['discount_type'] == 'dollars')
		{
		$new_price = ($price-$row['discount']);
		}
	
		if ($row['discount_type'] == 'percent') {
			$new_price = ((1-($row['discount']/100))*$price);
		}
	} else {
		$new_price = $price;
	}
return $new_price;
}

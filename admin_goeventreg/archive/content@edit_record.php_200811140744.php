<?php
echo "<h1>$QUERY_STRING</h1>";

include 'classes/class.BaseClass.php';
$TABLE = Get('table');    
$id = Get('id');

if (empty($TABLE)) {
    echo "<h1>Cannot find Table</h1>";
}
$CLASS_NAME = ucfirst($TABLE);
include 'classes/class.' . $CLASS_NAME . '.php';
    
echo "<h1>Edit Record - $CLASS_NAME ($id)</h1>";

$Obj = new $CLASS_NAME;

$Obj->EditRecord($id);

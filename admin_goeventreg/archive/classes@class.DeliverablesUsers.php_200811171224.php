<?PHP

// FILE: class.DeliverablesUsers.php

class DeliverablesUsers extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';

	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';


	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
	#USERS#
    $this->Default_Fields = 'id,type'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table  = 'deliverables_users';
    $this->Field_Titles = array(
        'id' => 'Id',
        'type' => 'Type',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Add = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD USER",
		"code|<hr style='border:0px; height:1px;'>",
		"select|User Type|type|Y||vendor=Vendor|admin=Administrator",
		"text|Contact ID|contact_id|Y|60|80",
		"text|Events List|events|Y|60|80",
		"checkbox|Email Status Reports|email_notify_reports|checked|1|0",
		"checkbox|Email on List Changes|email_notify_changes||1|0",
		"checkbox|Email on Upload Successful|email_notify_uploads||1|0",
		"checkbox|Email on Item Approval|email_notify_approvals||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );
		

	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Edit = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD USER",
		"code|<hr style='border:0px; height:1px;'>",
		"select|User Type|type|Y||vendor=Vendor|admin=Administrator",
		"text|Contact ID|contact_id|Y|60|80",
		"text|Events List|events|Y|60|80",
		"checkbox|Email Status Reports|email_notify_reports|checked|1|0",
		"checkbox|Email on List Changes|email_notify_changes||1|0",
		"checkbox|Email on Upload Successful|email_notify_uploads||1|0",
		"checkbox|Email on Item Approval|email_notify_approvals||1|0",
		"text|Updated By|updated_by|Y|60|80",
        "text|Active|active|Y|1|1",
		"submit|Add Item|$this->Add_Submit_Name",
    );


	#TABLE CREATION QUERY
	#=======================================================================
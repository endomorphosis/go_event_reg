<?PHP

// FILE: class.DeliverablesGroups.php

class DeliverablesGroups extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';


	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';

	
	#INITIAL SEARCH PAGE - TABLE VARIABLES
	#===================================================================================================
	#GROUPS#
    $this->Default_Fields = 'id,group_name'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table  = 'deliverables_groups';
    $this->Field_Titles = array(
        'id' => 'Id',
        'group_name' => 'Group Name',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Add = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD GROUP",
		"code|<hr style='border:0px; height:1px;'>",
        "text|Group Name|group_name|Y|60|80",
        "textbox|Description|description|Y|60|5",
		"text|Item IDs|item_id|Y|60|80",
        "select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"submit|Add Item|$this->Add_Submit_Name",
    );


	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Edit = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD GROUP",
		"code|<hr style='border:0px; height:1px;'>",
        "text|Group Name|group_name|Y|60|80",
        "textbox|Description|description|Y|60|5",
		"text|Item IDs|item_id|Y|60|80",
        "select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"text|Updated By|updated_by|Y|60|80",
		"checkbox|Active|active||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );


	#TABLE CREATION QUERY
	#===================================================================================================
    $this->Table_Creation_Query = "		
        CREATE TABLE IF NOT EXISTS `deliverables_groups` (
        `id` int(11) NOT NULL auto_increment,
        `group_name` varchar(255) NOT NULL,
        `item_id` varchar(255) NOT NULL,
        `event` varchar(255) NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
        `updated` timestamp NOT NULL default CURRENT_
<?PHP

// FILE: class.DeliverablesItems.php

class DeliverablesItems extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';


	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';


	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
	#ITEMS#
    $this->Default_Fields = 'id,title'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table  = 'deliverables_items';
    $this->Field_Titles = array(
        'id' => 'Id',
        'title' => 'Title',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Add = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD ITEM",
		"code|<hr style='border:0px; height:1px;'>",
        "text|Title|title|Y|60|80",
        "textbox|Description|description|Y|60|5",
        "select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
        "dateYMD|Due Date|due_date|Y|40|40",
		"select|Type|type|Y||type1=Type 1|type2=Type 2|type3=Type 3",
		"checkbox|Send Alert Email|alert_email||0|0",
		"select|Alert 1|alert_email_day_1|N||1=1 Day|2=2 Days|7=1 Week",
		"select|Alert 2|alert_email_day_2|N||1=1 Day|2=2 Days|7=1 Week",
		"checkbox|Require Verification of Content|verification||0|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );


	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Edit = array(
	    "form|$this->Action_Link|post|name",
        "text|Title|title|Y|60|80",
        "textbox|Description|description|Y|60|5",
        "text|Event|event|N|60|80",
        "text|Due Date|due_date|Y|40|40",
		"select|Type|type|Y||type1=Type 1|type2=Type 2|type3=Type 3",
		"checkbox|Send Alert Email|alert_email||0|0",
		"select|Alert 1|alert_email_day_1|N||1=1 Day|2=2 Days|7=1 Week",
		"select|Alert 2|alert_email_day_2|N||1=1 Day|2=2 Days|7=1 Week",
		"checkbox|Require Verification of Content|verification||0|0",
        "text|Updated By|updated_by|Y|60|80",
		"checkbox|Active|active||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
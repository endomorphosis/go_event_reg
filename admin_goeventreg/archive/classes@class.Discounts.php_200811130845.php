<?php
class Discounts extends BaseClass
{
    public function  __construct() 
    {  
    
        parent::__construct(); 
    
        $this->ClassInfo = array(
            'Created By'  => 'Benjamin Barber',
            'Description' => 'Create and manage showcase discounts',
            'Created'     => '2008-11-12',
            'Updated'     => '2008-11-12',
        );

        $this->Add_Submit_Name  = 'DISCOUNTS_SUBMIT_EDIT';
        $this->Edit_Submit_Name = 'DISCOUNTS_SUBMIT_EDIT';
        $this->Table  = 'discounts'; 
        $this->Flash_Field = 'description';
        
        $this->Field_Titles = array(
            'id' => 'Id',
            'event' => 'Event',
            'discount_code' => 'Discount Code',
            'description' => 'Description',
            'discount_amount' => 'Discount Amount',
            'discount_type' => 'Discount Type',
            'expiration_date' => 'Expiration Date',
            'active' => 'Active',
            'updated' => 'Updated',
            'created' => 'Created'
        );

        $this->Form_Data_Array_Add = array(
            "form|$this->Action_Link|post|name",
            "select|Event|event|required||IST|TST|ICC",
            "text|Code|discount_code|Y|12|12",
            "textarea|Description|description|Y|30|6",
            "text|Discount|discount_amount|Y|12|12||$this->Mask_Integer",
            "radio|Type of Discount|discount_type|required||dollars|percent",
            "dateYMD|Expiration|expiration_date|Y-M-D|Y|NOW|12",
            "submit|Add Discount|$this->Add_Submit_Name",
            "endform");

        $this->Form_Data_Array_Edit = array (
            "form|$this->Action_Link|post|name",
            "select|Event|event|required||IST|TST|ICC",
            "text|Code|discount_code|Y|12|12|||",
            "textarea|Description|description|Y|30|6||",
            "text|Discount|discount_amount|Y|12|12||$this->Mask_Integer",
            "radio|Type of Discount|discount_type|required||dollars|percent",
            "dateYMD|Expiration|expiration_date|Y-M-D|Y|NOW|12",
            "checkbox|Active|active||1|0",
            "submit|Save Discount|$this->Edit_Submit_Name",
            "endform");

       
        
        $this->Table_Creation_Query = "
            CREATE TABLE IF NOT EXISTS `discounts` (
            `id` int(11) NOT NULL auto_increment,
            `event` varchar(64) NOT NULL,
            `discount_code` varchar(16) NOT NULL,
            `description` varchar(1024) NOT NULL,
            `discount_amount` varchar(8) NOT NULL,
            `discount_type` varchar(8) NOT NULL,
            `expiration_date` varchar(32) NOT NULL,
            `active` tinyint(1) NOT NULL default '1',
            `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
            `create
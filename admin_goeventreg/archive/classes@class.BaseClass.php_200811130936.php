<?php

class BaseClass
{
    public  $ClassInfo;

    public  $Add_Submit_Name;
    public  $Edit_Submit_Name;
    public  $Error  = '';
    public  $Table;
    public  $Action_Link;

    public  $Field_Titles;
    public  $Form_Data_Array_Add;
    public  $Form_Data_Array_Edit;
    public  $Unique_Fields;
    public  $Default_Fields;

    public  $Field_Values = array (
                'active' => array(1 => 'Yes', 2=> 'No')
            );
    public  $Flash_Field  = '';

    public  $Edit_Links;

    public  $Mask_Integer    = '^[0-9]+$';
    public  $Mask_Name       = '^[a-zA-Z0-9 \'\-]+$';
    public  $Mask_Username   = '^[a-zA-Z0-9]+$';
    public  $Mask_Password   = '^[a-zA-Z0-9:\-\!\@\#\$\%\^\&\*_]+$';
    public  $Mask_Email      = '^([0-9a-z]+)([0-9a-z\.-_]+)@([0-9a-z\.-_+)\.([0-9a-z]+)';
    public  $Mask_Real       = '^[0-9.\-]+$';
    public  $Mask_RealC      = '^[0-9\,.\-]+$';
    public  $Mask_Zip        = '^[0-9\-]+$';
    public  $Mask_4int       = '^[0-9]{4}$';
    public  $Mask_2int       = '^[0-9]{2}$';
    public  $Mask_Char       = '^[a-zA-Z]+$';
    public  $Mask_2chr       = '^[a-zA-Z]{2}$';
    public  $Mask_General_Line = '^[a-zA-Z0-9_ \!-\?[:punct:]]+$';
    public  $Mask_General; // in construct
    public  $Mask_Words      = '^[[:alnum:][:space:][:punct:]]+$';
    public  $Mask_Dir        = '^[a-zA-Z0-9\/_\.\-]+$';

    private $Table_Creation_Query;
    public  $Default_Table_Options = 'align="center" cellspacing="1" cellpadding="0"';
    public  $Default_Td_Options    = '';
    public  $Default_Th_Options    = '';


    public function  __construct()
    {
        $this->Action_Link   = $_SERVER['REQUEST_URI'];
        $this->Mask_General  = '^[a-zA-Z0-9_ ' . "\r\n\t" . '\!-\?[:punct:]]+$';
        $this->Edit_Links    = <<<LBL1
<div style="width:8.5em; text-align:center;">
<a class="dbeditbutton" href="#"
onclick="document.admin.action='$this->Action_Link?view_record=@@ID@@'; document.admin.submit(); return false;">View</a>&nbsp;<a
class="dbeditbutton" href="#"
onclick="document.admin.action='$this->Action_Link?edit_record=@@ID@@'; document.admin.submit(); return false;">Edit</a>&nbsp;<a
class="dbeditbutton" href="#"
onclick="if (confirm('Are you sure you want to delete this record (id=@@ID@@)?')){
  document.admin.action='$this->Action_Link?delete_record=@@ID@@';
  document.admin.submit();
}
 return false;">Delete</a>
</div>
LBL1;
    }


    public function CreateTable()
    {
        return mysql_query($this->Table_Creation_Query);
    }


    public function AddRecord()
    {
        $this->Error = '';

        if (havesubmit($this->Add_Submit_Name)) {

            $FormArray = ProcessForm($this->Form_Data_Array_Add,
              $this->Edit_Submit_Name, $this->Default_Table_Options,
              $this->Default_Th_Options, $this->Default_Td_Options, $this->Error);

            if (!$this->Error and $this->Unique_Fields) {
                // check for unique fields
                $fields = explode(',', $this->Unique_Fields);
                foreach ($fields as $field) {
                    if ( !db_IsUnique($this->Table,$field,$FormArray[$field],'')) {
                        $this->Error = $this->Field_Titles[$field] . ' already exits!';
                        break;
                    }
                }
            }
            if (!$this->Error) {
                 $fields = db_Keys($FormArray).',created';
                 $values = db_Values($FormArray).",NOW()";
                 if (db_AddRecord($this->Table,$fields,$values)) {
                   AddMessage("Record: [{$FormArray[$this->Flash_Field]}] Added");
                 } else {$this->Error .= 'DB Write Error';}
        	}

        }
        if (!havesubmit($this->Add_Submit_Name) or $this->Error) {
            WriteError($this->Error);
            echo OutputForm($this->Form_Data_Array_Add,Post($this->Add_Submit_Name));
        }

    }


    public function PrePopulateFormValues($id, $field='id')
    {
        global $FormPrefix;
        // ------- prepopulate fields -------
        $Record   = db_GetRecord($this->Table,'*',"`$field`='$id'");
        foreach ($Record as $key => $value) {
                $_POST[$FormPrefix.$key] = $value;
        }
    }

    public function EditRecord($id, $field='id')
    {
        $this->Error = '';

        if (havesubmit($this->Edit_Submit_Name)) {

            $FormArray = ProcessForm($this->Form_Data_Array_Edit,
                $this->Edit_Submit_Name, $this->Default_Table_Options,
                $this->Default_Th_Options, $this->Default_Td_Options, $this->Error);

            if (!$this->Error and $this->Unique_Fields) {
                $fields = explode(',', $this->Unique_Fields);
                foreach ($fields as $field) {
                    if ( !db_IsUnique($this->Table,$field,$FormArray[$field],"$field!=$id")) {
                        $this->Error = $this->Field_Titles[$field] . ' already exits!';
                        break;
                    }
                }
            }

            if (!$this->Error) {
                $key_values = db_KeyValues($FormArray);
                if(db_UpdateRecord($this->Table,$key_values,"`$field`='$id'")){
                    AddFlash("Template [{$FormArray[$this->Flash_Field]}] Updated");
                } else {$this->Error .= 'DB Write Error';}
        	}

        }


        if (!havesubmit($this->Edit_Submit_Name) or $this->Error) {
            if (!$this->Error) {
                // ------- prepopulate fields -------
            	$this->PrePopulateFormValues($id);
            }
            WriteError($this->Error);
            echo OutputForm($this->Form_Data_Array_Edit,Post($this->Edit_Submit_Name));
        }

    }

    public  $Search_Selection_Operators = array('All','=','Not =','&lt;','&gt;','&lt;=','&gt;=','includes');


    public function DisplaySearchSelectionOperators($var)
    {
        $RESULT = '
        <select class="table_search_display_operators" name="TABLE_SEARCH_OPERATOR_'.$var.'" id="TABLE_SEARCH_OPERATOR_'.$var.'">';
        foreach ($this->Search_Selection_Operators as $key) {
            $selected = (Post('TABLE_SEARCH_OPERATOR_'.$var) == $key)? ' selected="selected"' : '';
            $RESULT .= "
            <option value=\"$key\"$selected>$key</option>";
        }
        $RESULT .= '
        </select>';
        return $RESULT;
    }

    public function DisplaySearchTab()
    {
        $RESULT = '
            <form name="TABLE_SEARCH_SELECTION" id="TABLE_SEARCH_SELECTION" method="post" action="'.$this->Action_Link.'">
            <table class="table_search_tab" border="0" cellspacing="1" cellpadding="0">
            <tbody>
            <tr>
              <th>Field</th><th>Select</th><th>Value</th><th>Display</th>
            </tr>';
        
        foreach ($this->Field_Titles as $field => $title) {
            $select   = $this->DisplaySearchSelectionOperators($field);
            $value    = TransformContent(Post('TABLE_SEARCH_VALUE_'.$field),'TS');
            $input    = "<input type=\"text\" name=\"TABLE_SEARCH_VALUE_$field\" value=\"$value\" size=\"40\" />";
            $checked  = (Post('TABLE_DISPLAY_'.$field) == 1)? ' checked="checked"' : '';
            $checkbox = "<input type=\"checkbox\" name=\"TABLE_DISPLAY_$field\" value=\"1\" />";
            $RESULT .= "
            <tr>
                <td>$title</td>
                <td>$select</td>
                <td>$input</td>
                <td>$checkbox</td>
            </tr>";
        }
        
        $RESULT .= '
            </tbody>
            </table>
            <input type="button" value="Search" onclick="tableSearch(\''.$this->Table.'\');" />
            </form>';
        
        return $RESULT;
    }

    public function ListTable()
    {
        global $FieldValues;
        $FieldValues = $this->Field_Values;
        echo $this->DisplaySearchTab();
        $keys = '*';
        $conditions = '';
        $order = 'id';
        $startlist = 0;
        $listsize  = 0;
        $array = db_GetArray($this->Table, $keys, $conditions, $order, $startlist, $listsize, $num_rows);
        echo db_output_Table($array,$this->Field_Titles, $this->Default_Table_Options,'Update',$this->Edit_Links,'id');
    }

}
<?php
$TABLE = Get('table');

if (empty($TABLE)) {
    echo "<h1>Cannot find Table</h1>";
}

$id = Get('id');
$CLASS_NAME = str_replace(' ','',NameToTitle($TABLE));

/*
$ALIASES = array(
    'DeliverablesGroups' => 'Deliverables'
);
if (array_key_exists($CLASS_NAME, $ALIASES)) $CLASS_NAME = $ALIASES[$CLASS_NAME]; */

echo "<h1>$CLASS_NAME</h1>";

include 'classes/class.' . $CLASS_NAME . '.php';

echo "<h1>Edit Record - $CLASS_NAME ($id)</h1>";

$Obj = new $CLASS_NAME;


//$this->Table_Item;

$Obj->EditRecord($id);


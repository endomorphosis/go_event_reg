<?php
$TABLE = 'mail_queue';

$ProcessedCount = db_Count($TABLE, "sent!=0 OR error !=''");

$FailedCount = db_Count($TABLE, "error!=''");

$SentCount = $ProcessedCount - $FailedCount;


print <<<LBL1
<h1>Mail Status</h1>
<table id="statustable" cellpadding="0" cellspacing="1" border="0" width="100">
<tbody>
<tr><th>Sent</th><td>$SentCount</td></tr>
<tr><th>Failed</th><td>$FailedCount</td></tr>
<tr><th>Total</th><td>$ProcessedCount</td></tr>
<tr><td colspan="2"><a class="stdbutton" href="$THIS_PAGE?DIALOG=1;">Refresh</a></td></tr>
</tbody>
</table>
LBL1;


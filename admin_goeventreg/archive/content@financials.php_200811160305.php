<?php

include 'classes/class.Financials.php';

$Financials = new Financials;
$Value = '';
$Currency ='';

SetGet('ADD');
SetGet('edit');
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=1`>Create Invoice</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=2`>Create Estimate</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=3`>Send Invoice/a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=4`>Send Estimate</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=5`>Contact Vendor</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=6`>Recieve Po</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=7`>Adjustment</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=8`>Recieved Payment</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=9`>Deposit Payment</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=10`>Refund Payment</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1&edit=11`>Transfer Funds</a>");

if ($ADD == 1 && $edit == 1) {
	$Value = 'invoice';	
	$Financials->Form_Data_Array_Add = array(
		"form|$Financials->Action_Link|post|name",
		"text|Event Id|event_id|Y|8|8|$Mask_Integer",
        "text|Company Id|company_id|Y|8|8|$Mask_Integer",
        "hidden|transaction_type|$Value",
        "select|Currency|currency|$Currency",
		"dateYMD|Transaction Date|transaction_date|Y-M-D|Y|NOW|12",
        "time|Transaction Time|transaction_time|Y",
        "text|Transaction Amount|transaction_amount|Y|16|16",
        "text|Created By|created_by|Y|16|16",
		"dateYMD|Invoice Date|invoice_date|Y-M-D|Y|NOW|12",
		"textarea|Notes|notes|Y|60|6",
        "radioh|Success|success|Y||1=yes|0=no",
        "text|Po Id|po_id|Y|8|8|$Mask_Integer",
        "text|Invoice Id|invoice_id|Y|8|8|$Mask_Integer",
        "text|Estimate Id|estimate_id|Y|8|8|$Mask_Integer",
        "text|Payment Id|payment_id|Y|8|8|$Mask_Integer",
        "text|Deposit Id|deposit_id|Y|8|8|$Mask_Integer",
        "radioh|Contact Method|contact_method|Y||Email|Mail|Phone|Fax|",
	    "textarea|Contact Reason|contact_reason|N|60|6",
	    "textarea|Contact Resolution|contact_resolution|N|60|6",
		"dateYMD|Original Invoice Date|original_invoice_date|Y-M-D|Y|NOW|12",
        "text|Original Invoice Amount|original_invoice_amount|Y|16|16",
        "radioh|Payment Method|payment_method|Y||Credit|EFT",
        "text|Tracking Number|tracking_number|Y|32|32",
        "text|Source Account #|source_account|Y|32|32",
        "text|Destination Account #|destination_account|Y|32|32",
        "radioh|Cancel Type|cancel_type|Y||PO|Invoice|Estimate",
		"text|Created Method|created_method|Y|
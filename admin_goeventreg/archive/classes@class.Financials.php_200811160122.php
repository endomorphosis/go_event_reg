<?php

// FILE: class.Financials.php

class Financials extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage financials',
        'Created'     => '2008-11-15',
        'Updated'     => '2008-11-15'
    );

    $this->Add_Submit_Name  = 'FINANCIALS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'FINANCIALS_SUBMIT_EDIT';
    $this->Table  = 'financials';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'event_id' => 'Event Id',
        'company_id' => 'Company Id',
        'transaction_type' => 'Transaction Type',
        'currency' => 'Currency',
        'transaction_date' => 'Transaction Date',
        'transaction_time' => 'Transaction Time',
        'transaction_amount' => 'Transaction Amount',
        'created_by' => 'Created By',
        'invoice_date' => 'Invoice Date',
        'notes' => 'Notes',
        'success' => 'Success',
        'po_id' => 'Po Id',
        'invoice_id' => 'Invoice Id',
        'estimate_id' => 'Estimate Id',
        'payment_id' => 'Payment Id',
        'deposit_id' => 'Deposit Id',
        'contact_method' => 'Contact Method',
        'contact_reason' => 'Contact Reason',
        'contact_resolution' => 'Contact Resolution',
        'original_invoice_date' => 'Original Invoice Date',
        'original_invoice_amount' => 'Original Invoice Amount',
        'payment_method' => 'Payment Method',
        'tracking_number' => 'Tracking Number',
        'source_account' => 'Source Account',
        'destination_account' => 'Destination Account',
        'cancel_type' => 'Cancel Type',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

    $this->Form_Data_Array_Add = array(
        "text|Event Id|event_id|Y|8|8",
        "text|Company Id|company_id|Y|8|8",
        "text|Transaction Type|transaction_type|Y|16|16",
        "text|Currency|currency|Y|16|16",
        "text|Transaction Date|transaction_date|Y|16|16",
        "text|Transaction Time|transaction_time|Y|16|16",
        "text|Transaction Amount|transaction_amount|Y|16|16",
        "text|Created By|created_by|Y|16|16",
        "text|Invoice Date|invoice_date|Y|16|16",
        "text|Notes|notes|Y|60|1024",
        "text|Success|success|Y|1|1",
        "text|Po Id|po_id|Y|8|8",
        "text|Invoice Id|invoice_id|Y|8|8",
        "text|Estimate Id|estimate_id|Y|8|8",
        "text|Payment Id|payment_id|Y|8|8",
        "text|Deposit Id|deposit_id|Y|8|8",
        "text|Contact Method|contact_method|Y|16|16",
        "text|Contact Reason|contact_reason|Y|60|1024",
        "text|Contact Resolution|contact_resolution|Y|60|1024",
      
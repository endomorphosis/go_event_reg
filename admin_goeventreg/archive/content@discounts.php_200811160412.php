<?php

include 'classes/class.Discounts.php';

$Discount = new Discounts;

SetGet('ADD');

printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1`>Add Discount</a>");

if ($ADD == 1) {
    $Discount->AddRecord();
}

if (!$ADD) {
    $Discount->ListTable();
}
$TABLE     = 'email_templates';
$WHERE     = "active=1 AND name!=''";
$templates = db_GetAssocArray($TABLE,'id','name',$WHERE);

$dropdown  = HTML_AssocArrayToDropDown($templates, $FormPrefix.'templateId', 'dropdown', 1);
echo $dropdown;
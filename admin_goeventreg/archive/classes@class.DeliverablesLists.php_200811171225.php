<?PHP

// FILE: class.DeliverablesLists.php

class DeliverablesLists extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';


	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';


	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
	#LISTS#
    $this->Default_Fields = 'id,event'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table  = 'deliverables_lists';
    $this->Field_Titles = array(
        'id' => 'Id',
        'event' => 'Event',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Add = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD LIST",
		"code|<hr style='border:0px; height:1px;'>",
		"text|User ID|user_id|Y|60|80",
		"select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"text|Item ID|item_id|Y|60|80",
		"dateYMD|Due Date|due_date|Y|40|40",
		"dateYMD|Pending Date|pending_date|Y|40|40",
		"dateYMD|Verified Date|verified_date|Y|40|40",
		"text|Verified User ID|verified_user_id|Y|60|80",
		"text|Filename|filename|Y|60|80",
		"submit|Add Item|$this->Add_Submit_Name",
    );


	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Edit = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD LIST",
		"code|<hr style='border:0px; height:1px;'>",
		"text|User ID|user_id|Y|60|80",
		"select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"text|Item ID|item_id|Y|60|80",
		"dateYMD|Due Date|due_date|Y|40|40",
		"dateYMD|Pending Date|pending_date|Y|40|40",
		"dateYMD|Verified Date|verified_date|Y|40|40",
		"text|Verified User ID|verified_user_id|Y|60|80",
		"text|Filename|filename|Y|60|80",
		"text|Updated By|updated_by|Y|60|80",
        "text|Active|active|Y|1|1",
		"submit|Add Item|$this->Add_Submit_Name",
    );


	#TABLE CREATION QUERY
	#===================================================================================================
    $this->Table_Creation_Query = "
        CREATE TABLE IF NOT EXISTS `deliverables_l
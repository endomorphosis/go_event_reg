
body{
	margin:0;
	padding:0;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
}



#page{
	margin:0;
	padding:0;
	width:612px;
	text-align:left;
}

#container_div{
	width:538px;
	margin:37px auto 0 auto;
	background:#FFF;
}



hr {
	color: #000000;
	background-color: #000000;
	height: 1px;
}


#wrapper{
	width: 100%;
	margin: 0 auto;
}
.box{
	background: #fff;
	vertical-align: top;
}
.boxholder{
	clear: both;
	padding: 5px;
	padding-top: 10px;
	background: #8DC70A;
}

.tableSectionHeader {
	font-size: 14px;
	font-weight: bold;
	color: #163957;
	border-bottom: 1px dashed #163957;
}
.boxContents{
	padding:20px;
}

#dialogcontainer {
    background: #fff;
	border: 5px solid #8DC70A;
    margin :  0px;
    padding : 0px;
    table-layout:auto;
}

#dialogcontent {
	background: #fff;
	padding: 1em;
}


#dialogcontainer td {
    padding : 3px;
}

#dialogcontent textarea{
  width : 100%;
}

/* END OF CSS FOR SEARCH PAGE TABS */

#data {
	background-image: url(images/background_color.jpg);
}

.mainContentArea {
	background: #fff;
	border: 5px solid #8DC70A;
	padding: 5px;
}
/* ======================= APPLICTION FORMS (DIALOGS) =======================  */
.appformcontent{
    background: #fff;
	border: 5px solid #8DC70A;
    margin :  0px;
    padding : 5px;
    overflow : scroll;
}

.appform {
  position : absolute;
  background-color : #f2f2f2;
  border : 1px solid #fff;
  border-color : #ddd #666 #555 #ccc;
  padding : 5px;
  z-index : 100;
}
.appform h3 {
  color : #fff;
}
.appiframe{
  margin : 0px;
}

.appform_titlebar {
  background-color : #050;
  color : #fff;
}

.appform_titlebar h1 {
  font-size : 1em;
  padding : 3px 5px;
  border-bottom : 1px solid #fff;
}

.appform_controls {
  float : right;
  color : #fff;
  padding : 4px 0px;
}
.appform_controls a {
  color : #fff;
  font-weight : bold;
  text-decoration : none;
  border-left : 1px solid #ccc;
  background-color : #050;
  padding : 4px 5px 3px 5px;
  font-size: 1.1em;
}
.appform_controls a:hover {
 background-color : #900;
} 

#appform_taskbar {
  position : fixed;
  bottom : 0;
  background-color : #8DC70A;
  color : #fff;
  width : 100%;
  padding : 3px;
  clear : both;
  z-index : 10000;
}
#appform_taskbar  a.taskbar_minimized   {
  display : inline;
  background-color: #050;
  color : #fff;
  border : 1px solid #fff;
  border-color : #ddd #666 #555 #ccc;
  text-decoration : none;
  padding : 1px 3px;
  margin : 2px;
}
#appform_taskbar  a.taskbar_visible   {
  display : inline;
  background-color: #888;
  color : #fff;
  border : 1px solid #fff;
  border-color : #ddd #666 #555 #ccc;
  text-decoration : none;
  padding : 1px 3px;
  margin : 2px;
}

#appform_t
<name>Import Email Data</name>
<summary>Import Email Data</summary>
<title>Import Email Data</title>
<description>Import Email Data</description>
<keywords></keywords>

<style>
    #import_table {
        background-color : #888;
    }
    #import_table td, #import_table th {
        padding : 1px 3px;
    }
    #import_table td {
        background-color : #fff;
    }
    #import_table th {
        background-color : #8DC70A;
        color : #fff;
        font-weight : bold;
    }
    #upload_marker {
        background:url(images/upload.gif) no-repeat;
        width : 32px;
        height : 32px;
        display : none;
        position : relative;
        margin-left : 100px;
        margin-top : -32px;
    }
    .box { width : 900px!important;}
</style>

<scriptinclude></scriptinclude>

<script>
    function getId(id)
    {
        return document.getElementById(id);
    }

    function setColColors(col,backcolor,textcolor)
    {
        // check if already set

        //alert('BC=' +backcolor + ', TC=' + textcolor + ' ID='+'table_'+col+'_1');

        var telem = getId('table_'+col+'_1');
        if ((telem) && (telem.backgroundColor == backcolor)) return;

        var j = 1;
        while (getId('table_'+col+'_' + j)){
            var myelem = getId('table_'+col+'_'+j);
            myelem.style.backgroundColor = backcolor;
            myelem.style.color = textcolor;
            j++;
        }
    }

    function colorColumns()
    {
        // clear conflic status, create an element variable (conflictStatus)
        var i = 1;
        while (getId('columnselect'+i)){
            getId('columnselect'+i).conflictStatus = false;
            i++;
        }


        // find the conflicts
        var i = 1;
        while (getId('columnselect'+i)){
            k = i+1;
            ielement = getId('columnselect'+i);
            var idx1 = ielement.selectedIndex;
            if (idx1 !=0) {
                while (getId('columnselect'+k)){
                    var kelement = getId('columnselect'+k);
                    var idx2 = kelement.selectedIndex;
                    if (idx1 == idx2) {
                        ielement.conflictStatus = true;
                        kelement.conflictStatus = true;
                    }
                    k++;
                }
            }
            i++;
        }

        // set the colors
        var i = 1;
        while (getId('columnselect'+i)) {
            ielement = getId('columnselect'+i);
            idx1 = ielement.selectedIndex;
            if( idx1 > 0) {
                if (ielement.conflictStatus) setColColors(i,'#f00','#fff');
                else setColColors(i,'#ff7','#333');
            } else setColColors(i,'','');
            i++;
        }
    }

 
<?PHP
if (isset($_GET['AJAX'])) {
    include '../helper/ajax_helper.php';
    $event = Get('event');
    echo "<option>GOT AJAX: event=$event</option>";
    exit;
}
include 'classes/class.Deliverables.php';
include 'classes/class.DeliverablesConfig.php';
include 'classes/class.DeliverablesGroups.php';
include 'classes/class.DeliverablesItems.php';
include 'classes/class.DeliverablesLists.php';
include 'classes/class.DeliverablesNotes.php';
include 'classes/class.DeliverablesUsers.php';

writedbquery();
SetGet('ADD');

echo '<table><tr>
<td>';
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1`>Add Item</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=2`>Add Group</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=3`>Add User</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=4`>Add List</a>");
echo '</td>
<td>';
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=5`>List Item</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=6`>List Group</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=7`>List User</a>");
printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=8`>List List</a>");
echo '</td>
</tr></table>';

switch ($ADD)
{
	case 1:
		$Deliverables = new DeliverablesItems;
		$Deliverables->AddRecord();
	break;
	case 2:
		$Deliverables = new DeliverablesGroups;
		$Deliverables->AddRecord();
		echo '<br /><br />';
		$Deliverables->CreateCheckboxListItems();
	break;
	case 3:
		$Deliverables = new DeliverablesUsers;
		$Deliverables->AddRecord();
	break;
	case 4:
		$Deliverables = new DeliverablesLists;
		$Deliverables->AddRecord();
	break;
	case 5:
		$Deliverables = new DeliverablesItems;
	    $Deliverables->ListTable();	
	break;	
	case 6:
		$Deliverables = new DeliverablesGroups;
	    $Deliverables->ListTable();	
	break;	
	case 7:
		$Deliverables = new DeliverablesUsers;
	    $Deliverables->ListTable();	
	break;	
	case 8:
		$Deliverables = new DeliverablesLists;
	    $Deliverables->ListTable();	
	break;	
	default:
	break;
}

//$Deliverables->CreateTable();
//WriteDBQuery();

?>

<script type="text/javascript" language="JavaScript">
function updateCombinedField(source, target, id)
{
	var newSource = "FORM_" + source;
	var newTarget = "FORM_" + target;
	
	var theList = document.getElementById(newTarget).value;
	var checkedStatus = document.getElementById(newSource).checked;

	if (checkedStatus)
	{
		//ADDING ID TO LIST
		var newList = theList + id + "|";
	} else {
		//REMOVING FROM LIST
		var theListParts = theList.split(\'|\');

		for (i=0; i<theListParts.length; i++)
		{
			if (theListParts[i] == id)
			{
				theListParts.splice(i,1);

				//STOP LOOP FROM RUNNING ANY FURTHER
				i = theListParts
<?PHP
// DB Admin Authentication
// reqires tools.php, form_helper.php (for e-mail check)
return;
$Mask_Username    = '^([0-9a-z]+)([0-9a-z\.-_]+)@([0-9a-z\.-_+)\.([0-9a-z]+)';
$Mask_Password    = '^[a-zA-Z0-9:\-\!\@\#\$\%\^\&\*_]+$';

$LOGIN_TITLE            = 'Event Admin System';
$LOGIN_LOGFILE          = 'logs/login.dat';
$SITE_CSS               = 'css/site.css';

$ADMIN_TABLE            = 'admin_users';
// tables fields: id, email, first_name, last_name, super_user

$ADMIN_LOGIN_OK         = ''; //Check this variable to see if user has logged in
$ADMIN_LOGIN            = ''; //THIS IS ARRAY OF ACCESS TO PAGE LEVELS
$ADMIN_NAME             = ''; // the name of the person;
$ADMIN_USER_ID          = ''; // the id of the users
$ADMIN_SUPER_USER       = ''; // when this is set, the user is a super user

$ADMIN_ERROR            = ''; // initialize for error checking below

SetPost('USER PASS LOGIN');  // sets the posted variables

$HeadStart = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>'.$LOGIN_TITLE.'</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" type="text/css" href="'.$SITE_CSS .'" />
';



//===========================LOGOUT=======================
if (Get('LOGOUT')) {
    unset(
        $_SESSION[$SESSION_LOGIN_BASE.'LOGIN'],
        $_SESSION[$SESSION_LOGIN_BASE.'LOGIN_OK']
    );

    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time()-42000, '/');
    }

    print <<<LOUT
$HeadStart
</head>
<body class="admin" style="text-align:center;">
    <div id="login">
        <p id="loginheading">
            $LOGIN_TITLE
        </p>
            <h1>Good Bye!</h1>
        <p>
            <input type="button" value="Log In" onclick="window.location='index.php'" />
        </p>
    </div>
</body>
</html>
LOUT;
    exit;
}

//=======================AUTHENTICATION=========================




if ($LOGIN and $USER and $PASS) {
    $USER = strtolower($USER);
    if (!CheckEmail($USER)) $ADMIN_ERROR = 'Username must be a Valid Email Address';
    elseif ((ereg($Mask_Username,$USER)) and (ereg($Mask_Password,$PASS))) {
        $search_user   = db_QuoteValue($USER);
        $ThisUserArray = db_GetRecord($ADMIN_TABLE,'*',"email=$search_user");
        if (empty($ThisUserArray)) {
            $ADMIN_ERROR = 'User not found';
        } elseif (trim($ThisUserArray['password']) === $PASS) {
            session_regenerate_id();
            $_SESSION[$SESSION_LOGIN_BASE.'LOGIN']      = $ThisUserArray;
            $_SESSION[$SESSION_LOGIN_BASE.'LOGIN_OK']   = 'OK';
            $_SESSION[$SESSION_LOGIN_BASE.'N
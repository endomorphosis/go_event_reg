/*
 * jQuery UI Spinner
 *
 * Copyright (c) 2008 jQuery
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Spinner
 *
 * Depends:
 *  ui.core.js
 */
(function($) {

$.widget("ui.spinner", {
	init: function() {

		//Initialize needed constants
		var self = this;
		this.element.addClass("ui-spinner");
		this.element[0].value = this.options.start;
		
		var pickerHeight = this.element.innerHeight() / 2 - parseInt(this.element.css("borderTopWidth"),10) - 2;
		this.element
		.wrap("<div>")
		.parent()
			.css({
				position: this.element.css("position") == "static" ? "relative" : this.element.css("position"),
				left: this.element.css("left"),
				top: this.element.css("top"),
				width: this.element.outerWidth(),
				height: this.element.outerHeight()
			})
			.css("float", this.element.css("float"))
			.prepend('<div class="ui-spinner-up"></div>')
			.find("div.ui-spinner-up")
				.bind("mousedown", function() { if(!self.counter) self.counter = 1; self.mousedown(100, "up"); })
				.bind("mouseup", function(e) { self.counter = 0; if(self.timer) window.clearInterval(self.timer); self.element[0].focus(); self.propagate("change", e); })
				.css({ height: pickerHeight, top: parseInt(this.element.css("borderTopWidth"),10)+1, right: parseInt(this.element.css("borderRightWidth"),10)+1 })
			.end()
			.append('<div class="ui-spinner-down"></div>')
			.find("div.ui-spinner-down")
				.bind("mousedown", function() { if(!self.counter) self.counter = 1; self.mousedown(100, "down"); })
				.bind("mouseup", function(e) { self.counter = 0; if(self.timer) window.clearInterval(self.timer); self.element[0].focus(); self.propagate("change", e); })
				.css({ height: pickerHeight, bottom: parseInt(this.element.css("borderBottomWidth"),10)+1, right: parseInt(this.element.css("borderRightWidth"),10)+1 })
			.end()
		;
		
		this.element
		.bind("keydown.spinner", function(e) {
			if(!self.counter) self.counter = 1;
			self.keydown.call(self, e);
		})
		.bind("keyup.spinner", function(e) {
			self.counter = 0;
			self.cleanUp();
			self.propagate("change", e);
		})
		;
		if ($.fn.mousewheel) {
			this.element.mousewheel(function(e, delta) { self.mousewheel(e, delta); });
		}

	},
	plugins: {},
	constrain: function() {
		if(this.options.min != undefined && this.element[0].value < this.options.min) this.element[0].value = this.options.min;
		if(this.options.max != undefined && this.element[0].value > this.options.max) this.element[0].value = this.options.max;
	},
	cleanUp: function() {
		this.element[0].value = this.element[0].value.replace(/[^0-9\-]/g, '');
		this.constrain();
	},
	down: function(e) {
		if(isNaN(parseInt(this.element[0].value,10))) this.element[0].value = this.options.start;
		t
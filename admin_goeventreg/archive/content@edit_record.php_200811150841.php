<?php
$TABLE = Get('table');    

if (empty($TABLE)) {
    echo "<h1>Cannot find Table</h1>";
}

$id = Get('id');
$CLASS_NAME = str_replace(' ','',NameToTitle($TABLE));
include 'classes/class.' . $CLASS_NAME . '.php';
    
echo "<h1>Edit Record - $CLASS_NAME ($id)</h1>";

$Obj = new $CLASS_NAME;

$Obj->EditRecord($id);

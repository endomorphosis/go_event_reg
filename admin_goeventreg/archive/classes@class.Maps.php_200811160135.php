<?php

// FILE: class.Maps.php

class Maps extends BaseClass
{
	public $key = 'ABQIAAAAQM685LKUO229I3c66OAIbhRRA1T7WwlX6NlbsSu9ZiqMrU2t4RTPY7lY0l4LKxh1_WZdRiDReR4Czg';

    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Benjamin Barber',
        'Description' => 'Create and manage maps',
        'Created'     => '2008-11-15',
        'Updated'     => '2008-11-15'
    );

    $this->Add_Submit_Name  = 'MAPS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'MAPS_SUBMIT_EDIT';
    $this->Table  = 'maps';
    $this->Flash_Field = 'description';

    $this->Field_Titles = array(
        'id' => 'Id',
        'event_id' => 'Event Id',
        'address' => 'Address',
        'description' => 'Description',
        'latlng' => 'Latlng',
        'width' => 'Width',
        'height' => 'Height',
        'zoom' => 'Zoom',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

    $this->Form_Data_Array_Add = array(
        "text|Event Id|event_id|Y|60|64",
        "text|Address|address|Y|60|128",
        "text|Description|description|Y|60|500",
        "text|Latlng|latlng|Y|60|80",
        "text|Width|width|Y|11|11",
        "text|Height|height|Y|11|11",
        "text|Zoom|zoom|Y|11|11"
    );

    $this->Form_Data_Array_Edit = array(
        "text|Event Id|event_id|Y|60|64",
        "text|Address|address|Y|60|128",
        "text|Description|description|Y|60|500",
        "text|Latlng|latlng|Y|60|80",
        "text|Width|width|Y|11|11",
        "text|Height|height|Y|11|11",
        "text|Zoom|zoom|Y|11|11",
        "text|Active|active|Y|1|1"
    );

    $this->Default_Fields = 'id,event_id,address,description,latlng,width,height,zoom,active,updated,created';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "CREATE TABLE IF NOT EXISTS `financials` (
		`id` int(11) NOT NULL auto_increment,
		`event_id` int(8) NOT NULL,
		`company_id` int(8) NOT NULL,
		`transaction_type` varchar(16) NOT NULL,
		`transaction_id` int(8) NOT NULL,
		`transaction_date` varchar(16) NOT NULL,
		`transaction_time` varchar(16) NOT NULL,
		`transaction_amount` int(16) NOT NULL,
		`created_by` varchar(16) NOT NULL,
		`invoice_date` varchar(16) NOT NULL,
		`notes` varchar(1024) NOT NULL,
		`success` tinyint(1) NOT NULL,
		`po_id` int(8) NOT NULL,
		`invoice_id` int(8) NOT NULL,
		`estimate_id` int(8) NOT NULL,
		`payment_id` int(8) NOT NULL,
		`deposit_id` int(8) NOT NULL,
		`contact_method` varchar(16) NOT NULL,
		`contact_reason` varchar(1024) NOT NULL,
		`contact_resolution` varchar(1024) NOT NULL,
		`original_invoice_date` varchar(16) NOT NULL,
		`original_invoice_amount` int(16) NOT NULL,
		`payment_method` varchar(16) NOT NU
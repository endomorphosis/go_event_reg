var appformZindex = 1000;
var appformIdCounter = 0;
var appformProcessing = false;
var taskbarId = 'appform_taskbar';

function appformCreateWithContent(title, content, containerId, width, height)
{
    if (appformProcessing) return;
    appformProcessing = true;
    appformIdCounter++;
    appformZindex++;
    var id = 'appform' + appformIdCounter;
    var controls = '<div class="appform_controls">' +
     '<a href="#" title="Minimize" onclick="appformMinimize(\'' + id + '\',\''+ title + '\'); return false;">_</a>' +
     '<a href="#" title="Close" onclick="appformClose(\'' + id + '\'); return false;">X</a></div>';
    var titlebar = '<div id="titlebar_' + id + '" class="appform_titlebar"><h1>' + title + '</h1></div>';
    var content = '<div class="appform" id="' + id + '">' + controls + titlebar + '<div class="appformcontent">' + content +'</div></div>';
    $('#'+ containerId).prepend(content);
    //$('#' + id).draggable({handle : '#titlebar_'+id}).resizable({handles : 'all'});
    //$('#' + id).resizable({handles : 'all'});
    $('#' + id).ppdrag();
    $('#' + id).css('z-index',appformZindex);
    $('#' + id).width(width);
    $('#' + id).height(height);
    $('#' + id).mousedown(function () {appformActivate(this.id);});
    appformAddToTaskbar(id, title, true);
    appformProcessing = false;
}



function appformCreate(title, file, containerId) {
    if (appformProcessing) return;
    appformProcessing = true;
    appformIdCounter++;
    appformZindex++;
    var id = 'appform' + appformIdCounter;
    var iframeId = 'appformIframe' + appformIdCounter;
    
    file = (file.indexOf('___')>0)? file + '&' : file + '___';
    var dialogFile =  file + 'IFRAMEID=' +iframeId + '&DIALOGID=' + id;
    
    var controls = '<div class="appform_controls">' +
     '<a href="#" title="Minimize" onclick="appformMinimize(\'' + id + '\',\''+ title + '\'); return false;">_</a>' +
     '<a href="#" title="Maximize" onclick="appformMaximize(\'' + id + '\'); return false;">^</a>' +
     '<a href="#" title="Close" onclick="appformClose(\'' + id + '\'); return false;">X</a></div>';
    var titlebar = '<div id="titlebar_' + id + '" class="appform_titlebar"><h1>' + title + '</h1></div>';
    var content = '<div class="appform" id="' + id + '">' + controls + titlebar + '<iframe id="'+ iframeId +'" class="appiframe" src="' + dialogFile +'"></iframe></div>';
    //var content = '<div class="appform" id="' + id + '"><iframe id="'+ iframeId +'" class="appiframe" src="' + dialogFile +'"></iframe></div>';
    $('#'+ containerId).prepend(content);
    //$('#' + id).draggable({handle : '#titlebar_'+id}).resizable({handles : 'all'});
    //$('#' + id).resizable({handles : 'all'});
    $('#' + id).ppdrag();
    $('#' + id).css('z-index',appformZindex);
    $('#' + id).mousedown(functi
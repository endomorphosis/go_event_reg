<?PHP

// FILE: class.Deliverables.php

class Deliverables extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';

	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD
	#FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';




	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
	#ITEMS#
    $this->Default_Fields_Item = 'id,title'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table_Item  = 'deliverables_items';
    $this->Field_Titles_Item = array(
        'id' => 'Id',
        'title' => 'Title',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

	#GROUPS#
    $this->Default_Fields_Group = 'id,group_name'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table_Group  = 'deliverables_groups';
    $this->Field_Titles_Group = array(
        'id' => 'Id',
        'group_name' => 'Group Name',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

	#USERS#
    $this->Default_Fields_User = 'id,type'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table_User  = 'deliverables_users';
    $this->Field_Titles_User = array(
        'id' => 'Id',
        'type' => 'Type',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

	#LISTS#
    $this->Default_Fields_List = 'id,event'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table_List  = 'deliverables_lists';
    $this->Field_Titles_List = array(
        'id' => 'Id',
        'event' => 'Event',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );






    $this->Form_Data_Array_Add_Item = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD ITEM",
		"code|<hr style='border:0px; height:1px;'>",
        "text|Title|title|Y|60|80",
        "textbox|Description|description|Y|60|5",
        "select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
        "dateYMD|Due Date|due_date|Y|40|40",
		"select|Type|type|Y||type1=Type 1|type2=Type 2|type3=Type 3",
		"checkbox|Send Alert Email|alert_email||0|0",
		"select|Alert 1|alert_email_day_1|N||1=1 Day|2=2 Days|7=1 Week",
		"select|Alert 2|alert_email_day_2|N||1=1 Day|2=2 Days|7=1 Week",
		"checkbox|Require Verification of Content|verification||0|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );

    $this->Form_Data_Array_Edit_Item = array(
	    "form|$this->Action_Link|post|name",
        "text|Title|title|Y|60|80",
        "textbox|Description|description|Y|60|5",
        "text|Event|event|N|60|80",
        "text|Due Date|due_date|Y|40|40",
		"select|Type|type|Y||type1=Type 1|type2=Type 2|type3=Type 3",
		"checkbox|Send Alert Email|alert_email||0|0",
		"select|Alert 1|alert_email_day_1|N||1=1 Day|2=2 Days|7=1 Week",
		"select|Alert 2|alert_email_day_2|N||1=1 Day|2=2 Days|7=1 Week",
		"checkbox|Require Verification of Content|verification||0|0",
        "text|Updated By|updated_by|Y|60|80",
		"checkbox|Active|active||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );


    $this->Form_Data_Array_Add_Group = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD GROUP",
		"code|<hr style='border:0px; height:1px;'>",
        "text|Group Name|group_name|Y|60|80",
        "textbox|Description|description|Y|60|5",
		"text|Item IDs|item_id|Y|60|80",
        "select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"submit|Add Item|$this->Add_Submit_Name",
    );

    $this->Form_Data_Array_Add_List = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD LIST",
		"code|<hr style='border:0px; height:1px;'>",
		"text|User ID|user_id|Y|60|80",
		"select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"text|Item ID|item_id|Y|60|80",
		"dateYMD|Due Date|due_date|Y|40|40",
		"dateYMD|Pending Date|pending_date|Y|40|40",
		"dateYMD|Verified Date|verified_date|Y|40|40",
		"text|Verified User ID|verified_user_id|Y|60|80",
		"text|Filename|filename|Y|60|80",
		"submit|Add Item|$this->Add_Submit_Name",
    );

    $this->Form_Data_Array_Add_User = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD USER",
		"code|<hr style='border:0px; height:1px;'>",
		"select|User Type|type|Y||vendor=Vendor|admin=Administrator",
		"text|Contact ID|contact_id|Y|60|80",
		"text|Events List|events|Y|60|80",
		"checkbox|Email Status Reports|email_notify_reports|checked|1|0",
		"checkbox|Email on List Changes|email_notify_changes||1|0",
		"checkbox|Email on Upload Successful|email_notify_uploads||1|0",
		"checkbox|Email on Item Approval|email_notify_approvals||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );
		




    $this->Table_Creation_Query = "CREATE TABLE IF NOT EXISTS `deliverables_config` (
        `id` int(11) NOT NULL auto_increment,
        `name` varchar(255) NOT NULL,
        `value` text NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
		`updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
		`created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
		
		
        CREATE TABLE IF NOT EXISTS `deliverables_groups` (
        `id` int(11) NOT NULL auto_increment,
        `group_name` varchar(255) NOT NULL,
        `item_id` varchar(255) NOT NULL,
        `event` varchar(255) NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
        `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
        `created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
		
		
        CREATE TABLE IF NOT EXISTS `deliverables_items` (
        `id` int(11) NOT NULL auto_increment,
        `title` varchar(255) NOT NULL,
        `description` text NOT NULL,
        `event` varchar(255) NOT NULL,
        `due_date` varchar(255) NOT NULL,
        `type` varchar(255) NOT NULL,
        `alert_email` int(1) NOT NULL,
        `alert_email_day_1` int(5) NOT NULL,
        `alert_email_day_2` int(5) NOT NULL,
        `verification` int(1) NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
        `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
        `created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
		
		
        CREATE TABLE IF NOT EXISTS `deliverables_lists` (
        `id` int(11) NOT NULL auto_increment,
        `user_id` int(11) NOT NULL,
        `event` varchar(255) NOT NULL,
        `item_id` int(11) NOT NULL,
        `due_date` varchar(255) NOT NULL,
        `pending_date` varchar(255) NOT NULL,
        `verified_date` varchar(255) NOT NULL,
        `verified_user_id` int(11) NOT NULL,
        `filename` varchar(255) NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
		`updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
		`created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
		
		
        CREATE TABLE IF NOT EXISTS `deliverables_notes` (
        `id` int(11) NOT NULL auto_increment,
        `list_id` int(11) NOT NULL,
        `note` text NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
        `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
        `created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
		
		
        CREATE TABLE IF NOT EXISTS `deliverables_users` (
        `id` int(11) NOT NULL auto_increment,
		`contact_id` int(11) NOT NULL,
        `type` varchar(255) NOT NULL,
        `events` text NOT NULL,
        `email_notify_reports` int(1) NOT NULL,
		`email_notify_changes` int(1) NOT NULL,
		`email_notify_uploads` int(1) NOT NULL,
        `email_notify_approvals` int(1) NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
		`updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
		`created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
	
        ";	
    }
}
?>
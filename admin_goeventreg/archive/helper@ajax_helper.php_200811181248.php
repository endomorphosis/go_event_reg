<?php
include $_SERVER['DOCUMENT_ROOT'] . '/lib/tools.php';


// -------- authentication check --------
$SESSION_LOGIN_BASE     = 'ADMIN_GOEVENTREG_';
$ADMIN_LOGIN_OK         = Session($SESSION_LOGIN_BASE.'LOGIN_OK');
if (!$ADMIN_LOGIN_OK) exit;

include '../config/siteconfig.php';
include "$LIB/db_helper.php";
include "$LIB/form_helper.php";
include '../classes/class.BaseClass.php';

if(Post('data')) {
    $var_pairs = explode('&', $_POST['data']);

    $DATA = array();
    foreach ($var_pairs as $field) {
        list($key,$value) = explode('=', $field);
        $key              = urldecode($key);
        $value            = urldecode($value);
        $DATA[$key]       = $value;
    }
}

function EchoData()
{
    global $DATA;
    foreach ($DATA as $key => $value) {
        echo "$key|$value\n";    
    }
}
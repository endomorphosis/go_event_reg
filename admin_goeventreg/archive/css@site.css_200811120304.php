/*div {
  border : 1px dashed #f00;
}
*/

/* =================BODY================= */
body {
  text-align : center;
  background-color : #ccc;
  font-family : Verdana, Arial, Helvetica, sans-serif;
}

body.print {
  background-color : #fff;
  text-align : left;
}

#container {
  text-align : left;
  margin : 0px auto;
  width : 760px;
  border : 1px solid #888;
  background-color : #fff;
  background-image : url();
  background-repeat : repeat-y;
}

/* =================HEADER================= */
#header {
  display : block;
  width : 760px;
  height : 80px;
  background-image : url();
  background-color : #006;
}

#pheader {
  text-align : right;
  border-bottom : 4px solid #036;
}

#pheader h1 {
  color : #036;
  font-size : 2em;
  margin : 0px 20px;
}

#return {
  position : absolute;
}

/* =================SIDE BAR================= */
#sidebar {
  float : left;
  display : block;
  width : 170px;
  background-color : #066;
}

div.sidetext {
  font-size : 0.7em;
  color : #fff;
}

/* =================MENU================= */
#menu {
  margin : 10px auto;
  width : 135px;
}

/* ------ Vertical Menu with Pop-Out Submenus ------- */
ul.makeMenu, ul.makeMenu ul {
  font-size : 0.9em;
  font-weight : bold;
  width : 133px;
  border-top : 1px solid #000;
  border-left : 1px solid #000;
  border-right : 1px solid #000;
  padding-left : 0px;
  cursor : default;
  margin : 0px auto;
}

ul.makeMenu li {
  border-bottom : 1px solid #000;
  padding : 0.1em 12px 0.1em 0.5em;
  list-style-type : none;
  margin : 0px;
  position : relative;
  color : #fff;
  z-index : 100;
}

ul.makeMenu li > ul {
  display : none;
  position : absolute;
  top : 0px;
  left : 133px;
  width : 133px
}

ul.makeMenu li:hover, ul.makeMenu li.CSStoHighlight {
}

ul.makeMenu ul.CSStoShow {
  display : block;
}

ul.makeMenu li:hover > ul {
  display : block;
}

ul.makeMenu li a {
  display : block;
  width : 100%;
  text-decoration : none;
}

li.arrow {
  background : url(images/tri-blue.gif);
  background-repeat : no-repeat;
  background-position : right;
}

/* -- menu border color -- */
ul.makeMenu, ul.makeMenu ul, ul.makeMenu li {
  border-color : #039;
}

/* -- menu text color -- */
ul.makeMenu li a {
  color : #039;
}

/* -- menu background color -- */
ul.makeMenu, ul.makeMenu ul {
  background-color : #ccc;
}

/* -- menu text hover color -- */
ul.makeMenu li a:hover, ul.makeMenu li a.CSStoHighLink, ul.makeMenu li:hover > a  {
  color : #000;
}

/* -- menu background hover color -- */
ul.makeMenu li:hover, ul.makeMenu li.CSStoHighlight, ul.makeMenu li:hover > a  {
  background-color : #888;
}

#pageselected {
  background-color : #888;
}

#pageselected > a:first-child {
  color : #fff;
}
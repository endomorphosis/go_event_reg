<?php
ini_set('display_errors','1');

//-----------------------------------------------------------
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/page_helper.php';

$QUERY_STRING = str_replace('___',':',$QUERY_STRING);  // ':' not working with JavaScript

//-------------------------- custom -------------------------

include "$LIB/db_helper.php";
include "$LIB/html_helper.php";
include "$LIB/form_helper.php";
include "$LIB/custom_error.php";
include 'helper/i_menu.php';
include 'classes/class.BaseClass.php';


if (Get('QUERY') == 'ON') $_SESSION['WANT_DB_QUERIES'] = 1;
if (Get('QUERY') == 'OFF') unset($_SESSION['WANT_DB_QUERIES']);

$USER_NAME = 'Admin User'; //<<<<<<<<<<---------- FIX: SET FROM AUTHENTICATION ----------<<<<<<<<<<
define('SUPER_USER', true);//<<<<<<<<<<---------- FIX: SET FROM AUTHENTICATION ----------<<<<<<<<<<

//-----------------------------------------------------------

//============CREATE PAGE NAMES============
GetPageName();

//============WRITE LOG============
WriteTrackingLog();
//BlockedIPCheck();

//==========GET CONTENT FILE NAMES===========
GetPageFileNames();

//==========GET PAGE VARIABLES===========
GetTitleVariables();

if ($PAGE['pagename']!='index') $PAGE['template'] = 'dialog_template.html';
elseif(empty($PAGE['template'])){
  $PAGE['template'] = ($PAGE['print'])? 'ptemplate.html' : 'template.html';
}

//==========GET CONTENT===========
$PAGE_STREAM = file_get_contents("$SITE_ROOT/templates/{$PAGE['template']}");

ob_start(); include $PAGE['contentfilename']; $PAGE_CONTENT = ob_get_contents(); ob_end_clean();


SwapStdMarkUp();

//-------------------------- custom -------------------------
$querybutton = '';
$dbmessages  = '';
$querybutton = '';
$SCRIPTEND   = '';

if(SUPER_USER) {
    if(Session('WANT_DB_QUERIES')) {
        //$dbmessages = WriteDbQueryText() . '<p style="text-align:center; padding-top:10px;"><a class="stdbutton" style="display:inline;" target="_blank" href="index.php?QUERY=OFF">Turn Off Query Display</a></p>';
        $SCRIPTEND = '';  //"<script type=\"text/javascript\">typehumanMsg.displayMsg('asdf');</script>\n";
    } else {
        $querybutton = '<a style="position:absolute; right:10px; top:10px; font-size:0.8em; width:5em;" class="stdbutton" target="_blank" href="index.php?QUERY=ON">Query On</a>';
    }
}

$UserInfo = "<div>Welcome <b>$USER_NAME</b></div><a href=\"$PHP_SELF?LOGOUT=1\">Logout</a>";

$BASENAME = $HTTP_HOST . dirname($PHP_SELF).'/';
$BASENAME = empty($HTTPS)? 'http://'.$BASENAME : 'https://'. $BASENAME;

$SwapArray = array(
'@@PHPERROR@@'   => CustomErrorText(),
'@@QUERY@@'      => $querybutton,
'@@MENU@@'       => $MENU_ITEMS,
'@@USER@@'       => $UserInfo,
'@@DBMESSAGES@@' => $dbmessages, //.'<p style="text-align:center; padding-top:10px;"><a class="stdbut
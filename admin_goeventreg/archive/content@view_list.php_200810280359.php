<?PHP
if (Post('Submit'))
{
	$_SESSION['table'] = Post('eventName');
	$_SESSION['week']  = Post('eventWeek');
	$week = Post('eventWeek');
	switch ($week)
	{
		case "":
			$_SESSION['weekWhere'] = " AND week!=999";
			break;
		case "ALL":
			$_SESSION['weekWhere'] = " AND week!=999";
			break;
		default:
			$_SESSION['weekWhere'] = " AND week=$week";
			break;
	}
	header('Location: ' . $_SERVER['PHP_SELF']);
}


$_SESSION['EDIT_TABLE_INFO'] = array (
    'TITLE' => Session('table') . ' (Week ' . Session('week') . ')',
    'TABLE' => Session('table'),
    'WHERE' => 'active=1',
    'ORDER' => 'name',
    'ID'    => 'id',
    'COLS'  => 'id|name|cell|home|email|other',
//    'EDIT_ARRAY' => array (
//        "hidden|id|",
//        "text|Name|name|Y|30|40||$Mask_Name",
//        "phone|Cell #|cell|N",
//        "phone|Home #|home|N",
//        "email|Email|email|N|30|40|",
//        "textarea|Other|other|N|40|4||$Mask_General"
//    )
);

?>


<div class="tableSectionHeader">
COMPANY CONTACTS
</div>

<br />

<div id="contactForm">
      <input type='button' name='contact' value='Add New Contact' class='demo'/>
</div>


<br /><br />
<form name="frmSettings" action="<?PHP $REQUEST_URI; ?>" method="post">
      <p>EVENT NAME
        <select name="eventName" id="eventName">
          <option value=""></option>
          <option value="intel_server_affidavit_2008" <?PHP if (Session('table') == "intel_server_affidavit_2008") echo ' selected="selected"'; ?>>Intel Your Server Innovation 2008</option>
          <option value="intel_desktop_affidavit_2008"<?PHP if (Session('table') == "intel_desktop_affidavit_2008") echo ' selected="selected"'; ?>>Intel Your Desktop Innovation 2008 - Entry Prize</option>
          <option value="intel_desktop_grand_affidavit_2008"<?PHP if (Session('table') == "intel_desktop_grand_affidavit_2008") echo ' selected="selected"'; ?>>Intel Your Desktop Innovation 2008 - Grand Prize</option>
        </select>
      </p>
        <p>WEEK
          <select name="eventWeek" id="eventWeek">
            <option value=""></option>
            <option value="ALL" <?PHP if (Session('week') == "ALL") echo ' selected="selected"'; ?>>ALL</option>
            <option value="1" <?PHP if (Session('week') == 1) echo ' selected="selected"'; ?>>1</option>
            <option value="2" <?PHP if (Session('week') == 2) echo ' selected="selected"'; ?>>2</option>
            <option value="3" <?PHP if (Session('week') == 3) echo ' selected="selected"'; ?>>3</option>
            <option value="4" <?PHP if (Session('week') == 4) echo ' selected="selected"'; ?>>4</option>
            <option value="5" <?PHP if (Session('week') == 5) echo ' selected="selected"'; ?>>5</option>
            <option value="6" <?PHP if (Session('week') == 6) echo ' selecte
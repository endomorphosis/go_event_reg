/*
        Added styles for table
*/

  #tbl_list {
    background-color : #888;
    width : 100%;
    border-spacing : 0px;
    border: 1px solid #555;
  }
  #tbl_list th {
    background-color : #8DC70A;
    color : #333;
    border : none;
    padding: 2px 4px;
    text-align:left;   
	font-size:11px; 
  }

  #tbl_list tr:hover td{
    background-color : #ccf;
  }
  #tbl_list td:first-child, #tbl_list th:first-child {
    text-align:right;
    padding-right:5px;
  }
  
  #tbl_list td {
    background-color : #fff;
    border-top: 1px solid #888;
    padding: 2px 4px;
    vertical-align:top;
	font-size:11px;
  }
  
  #tbl_list td.yellow {
    background-color : yellow;
    border-top: 1px solid #888;
    padding: 2px 4px;
    vertical-align:top;
	font-size:11px;
  }

  #tbl_list a.row_edit, #tbl_list a.row_delete {
    background-image : url(../images/b_edit.gif);
    background-repeat : no-repeat;
    display : block;
    width : 17px;
    height : 15px;
  }
  #tbl_list a.row_edit {
    background-image : url(../images/b_edit.gif);
  }
  
  #tbl_list a.row_delete {
    background-image : url(../images/b_delete.gif);
  }
  
  #table_addcontact a {
    background-image : url(../images/b_add.gif);
    background-repeat : no-repeat;
    background-position: 0px 4px;
    color : #333;
    display : block;
    padding:4px 3px 3px 20px;
    white-space: nowrap;
  }
  #table_addcontact a:hover {
    background-color : #ccf;
  }
  
  #tabletitle {
    font-size:1.5em;
    text-align:left!important;
    padding:10px!important;
    color : #fff!important;
    font-weight:bold;
  }




/*
 * SimpleModal Contact Form
 * http://www.ericmmartin.com/projects/simplemodal/
 * http://code.google.com/p/simplemodal/
 *
 * Copyright (c) 2008 Eric Martin - http://ericmmartin.com
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Revision: $Id: contact.css 132 2008-05-23 16:05:17Z emartin24 $
 *
 */

body {padding:0; margin:0; height:100%; width:100%;}

/* Overlay */
#contact-overlay {background-color:#000; cursor:wait;}

/* Container */
#contact-container {width:420px; left:50%; top:15%; margin-left:-210px; font-family:'Trebuchet MS', Verdana, Arial; font-size:16px; text-align:left;}
#contact-container .contact-content {background-color:#333; color:#ddd; height:40px;}
#contact-container h1 {color:#d76300; margin:0; padding:0 0 6px 12px; font-size:1.2em; text-align:left;}
#contact-container .contact-loading {position:absolute; background:url(../images/contact/loading.gif) no-repeat; z-index:8000; height:55px; width:54px; margin:-14px 0 0 170px; padding:0;}
#contact-container .contact-message {text-align:center;}
#contact-container .contact-error {width:92%; font-size:.8em; bac
/*
 * ppDrag 0.2 - Extremely Fast Drag&Drop for jQuery
 * http://ppdrag.ppetrov.com/
 *
 * Copyright (c) 2008 Peter Petrov (ppetrov AT ppetrov.com)
 * Licensed under the LGPL (LGPL-LICENSE.txt) license.
 
Include jQuery:
<script src="jquery.js" type="text/javascript"></script>
Include ppDrag:
    <script src="jquery.ppdrag.js" type="text/javascript"></script>
    Activate ppDrag. Example:
    $(document).ready(function() {
        $("#element1").ppdrag();
    });
Note: at the moment ppDrag doesn't support elements with static positioning. Please use either relative, absolute, or fixed positioning.
(optional) Specify options. Example:
$(document).ready(function() {
    $("#element2").ppdrag({ zIndex: 1000 });
});
(optional) When no longer needed, you can deactivate ppDrag. Example:
$("#element2").ppdrag("destroy");
 
 */

(function($) {
	
	$.fn.ppdrag = function(options) {
		if (typeof options == 'string') {
			if (options == 'destroy') return this.each(function() {
				$.ppdrag.removeEvent(this, 'mousedown', $.ppdrag.start, false);
				$.data(this, 'pp-ppdrag', null);
			});
		}
		return this.each(function() {
			$.data(this, 'pp-ppdrag', { options: $.extend({}, options) });
			$.ppdrag.addEvent(this, 'mousedown', $.ppdrag.start, false);
		});
	};
	
	$.ppdrag = {
		start: function(event) {
			if (!$.ppdrag.current) {
				$.ppdrag.current = { 
					el: this,
					oleft: parseInt(this.style.left) || 0,
					otop: parseInt(this.style.top) || 0,
					ox: event.pageX || event.screenX,
					oy: event.pageY || event.screenY
				};
				var current = $.ppdrag.current;
				var data = $.data(current.el, 'pp-ppdrag');
				if (data.options.zIndex) {
					current.zIndex = current.el.style.zIndex;
					current.el.style.zIndex = data.options.zIndex;
				}
				$.ppdrag.addEvent(document, 'mouseup', $.ppdrag.stop, true);
				$.ppdrag.addEvent(document, 'mousemove', $.ppdrag.drag, true);
			}
			if (event.stopPropagation) event.stopPropagation();
			if (event.preventDefault) event.preventDefault();
			return false;
		},
		
		drag: function(event) {
			if (!event) var event = window.event;
			var current = $.ppdrag.current;
			current.el.style.left = (current.oleft + (event.pageX || event.screenX) - current.ox) + 'px';
			current.el.style.top = (current.otop + (event.pageY || event.screenY) - current.oy) + 'px';
			if (event.stopPropagation) event.stopPropagation();
			if (event.preventDefault) event.preventDefault();
			return false;
		},
		
		stop: function(event) {
			var current = $.ppdrag.current;
			var data = $.data(current.el, 'pp-ppdrag');
			$.ppdrag.removeEvent(document, 'mousemove', $.ppdrag.drag, true);
			$.ppdrag.removeEvent(document, 'mouseup', $.ppdrag.stop, true);
			if (data.options.zIndex) {
				current.el.style.zIndex = current.z
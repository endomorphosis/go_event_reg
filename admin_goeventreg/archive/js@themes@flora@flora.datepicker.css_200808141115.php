/* Main Flora Style Sheet for jQuery UI ui-datepicker */
#ui-datepicker-div, .ui-datepicker-inline {
	font-family: Arial,Helvetica,sans-serif;
	font-size: 14px;
	padding: 0;
	margin: 0;
	background: #E0F4D7;
	width: 185px;
}
#ui-datepicker-div {
	display: none;
	border: 1px solid #FF9900;
	z-index: 10;
}
.ui-datepicker-inline {
	float: left;
	display: block;
	border: 0;
}
.ui-datepicker-rtl {
	direction: rtl;
}
.ui-datepicker-dialog {
	padding: 5px !important;
	border: 4px ridge #83C948 !important;
}
.ui-datepicker-disabled {
	position: absolute;
	z-index: 10;
	background-color: white;
	opacity: 0.5;
}
button.ui-datepicker-trigger {
	width: 25px;
}
img.ui-datepicker-trigger {
	margin: 2px;
	vertical-align: middle;
}
.ui-datepicker-prompt {
	float: left;
	padding: 2px;
	background: #E0F4D7;
	color: #000;
}
*html .ui-datepicker-prompt {
	width: 185px;
}
.ui-datepicker-control, .ui-datepicker-links, .ui-datepicker-header, .ui-datepicker {
	clear: both;
	float: left;
	width: 100%;
	color: #FFF;
}
.ui-datepicker-control {
	background: #FF9900;
	padding: 2px 0px;
}
.ui-datepicker-links {
	background: #E0F4D7;
	padding: 2px 0px;
}
.ui-datepicker-control, .ui-datepicker-links {
	font-weight: bold;
	font-size: 80%;
	letter-spacing: 1px;
}
.ui-datepicker-links label {
	padding: 2px 5px;
	color: #888;
}
.ui-datepicker-clear, .ui-datepicker-prev {
	float: left;
	width: 34%;
}
.ui-datepicker-rtl .ui-datepicker-clear, .ui-datepicker-rtl .ui-datepicker-prev {
	float: right;
	text-align: right;
}
.ui-datepicker-current {
	float: left;
	width: 30%;
	text-align: center;
}
.ui-datepicker-close, .ui-datepicker-next {
	float: right;
	width: 34%;
	text-align: right;
}
.ui-datepicker-rtl .ui-datepicker-close, .ui-datepicker-rtl .ui-datepicker-next {
	float: left;
	text-align: left;
}
.ui-datepicker-header {
	padding: 1px 0 3px;
	background: #83C948;
	text-align: center;
	font-weight: bold;
	height: 1.3em;
}
.ui-datepicker-header select {
	background: #83C948;
	color: #000;
	border: 0px;
	font-weight: bold;
}
.ui-datepicker {
	background: #CCC;
	text-align: center;
	font-size: 100%;
}
.ui-datepicker a {
	display: block;
	width: 100%;
}
.ui-datepicker-title-row {
	background: #B1DB87;
	color: #000;
}
.ui-datepicker-title-row .ui-datepicker-week-end-cell {
	background: #B1DB87;
}
.ui-datepicker-days-row {
	background: #FFF;
	color: #666;
}
.ui-datepicker-week-col {
	background: #B1DB87;
	color: #000;
}
.ui-datepicker-days-cell {
	color: #000;
	border: 1px solid #DDD;
}
.ui-datepicker-days-cell a {
	display: block;
}
.ui-datepicker-week-end-cell {
	background: #E0F4D7;
}
.ui-datepicker-unselectable {
	color: #888;
}
.ui-datepicker-week-over, .ui-datepicker-week-over .ui-datepicker-week-end-cell 
/*
 * jQuery UI Magnifier
 *
 * Copyright (c) 2008 jQuery
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Magnifier
 *
 * Depends:
 *  ui.core.js
 */
(function($) {

var counter = 0;

$.widget("ui.magnifier", {
	init: function() {
		var self = this,
			o = this.options;
		
		this.element
			.addClass("ui-magnifier")
			.bind('click.magnifier', function(e) {
				(!self.disabled && o.click && o.click.apply(this, [e, {
					options: self.options,
					current: self.current[0],
					currentOffset: self.current[1]
				}]));
			});
		
		// the element must have relative or absolute positioning
		if (!(/^(r|a)/).test(this.element.css("position"))) {
			this.element.css("position", "relative");
		}
		
		this.items = [];
		this.element.find(o.items).each(function() {
			var $this = $(this);
			// TODO: use a hash so references to this data is readable
			self.items.push([
				this,
				$this.offset(),
				[$this.width(),$this.height()],
				(o.overlap ? $this.position() : null)
			]);
			
			(o.opacity && $this.css('opacity', o.opacity.min));
		});
		
		// absolutize
		(o.overlap && $.each(this.items, function() {
			$(this[0]).css({
				position: "absolute",
				top: this[3].top,
				left: this[3].left
			});
		}));
		
		this.identifier = ++counter;
		$(document).bind("mousemove.magnifier"+this.identifier, function(e) {
			(self.disabled || self.magnify.apply(self, [e]));
		});
		
		this.pp = this.element.offset();
	},
	
	destroy: function() {
		this.reset();
		this.element
			.removeClass("ui-magnifier ui-magnifier-disabled")
			.unbind(".magnifier");
		$(document).unbind("mousemove.magnifier"+this.identifier);
	},
	
	disable: function() {
		this.reset();
		$.widget.prototype.disable.apply(this, arguments);
	},
	
	reset: function(e) {
		var o = this.options;
		
		$.each(this.items, function() {
			var item = this;
			$(item[0]).css({
				width: item[2][0],
				height: item[2][1],
				top: (item[3] ? item[3].top : 0),
				left: (item[3] ? item[3].left : 0)
			});
			
			(o.opacity && $(item[0]).css('opacity', o.opacity.min));
			(o.zIndex && $(item[0]).css("z-index", ""));
		});
	},
	
	magnify: function(e) {
		var p = [e.pageX,e.pageY], o = this.options, c, distance = 1;
		this.current = this.items[0];
		
		// Compute the parent's distance
		// we don't need to fire anything if we are not near the parent
		var overlap = ((p[0] > this.pp.left-o.distance) &&
			(p[0] < this.pp.left + this.element[0].offsetWidth + o.distance) &&
			(p[1] > this.pp.top-o.distance) &&
			(p[1] < this.pp.top + this.element[0].offsetHeight + o.distance));
		if (!overlap) { return false; }
		
		for (var i=0; i<this.items.length; i++) {
			c = this.items[i];
			
<?php
ini_set('display_errors','1');

//-----------------------------------------------------------
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/page_helper.php';

$QUERY_STRING = str_replace('___',':',$QUERY_STRING);  // ':' not working with JavaScript

//-------------------------- custom -------------------------

include "$LIB/db_helper.php";
include "$LIB/html_helper.php";
include "$LIB/form_helper.php";
include "$LIB/custom_error.php";
include "helper/auth_helper.php";
include 'helper/i_menu.php';
include 'classes/class.BaseClass.php';


$USER_NAME = 'Admin User'; //<<<<<<<<<<---------- FIX: SET FROM AUTHENTICATION ----------<<<<<<<<<<
define('SUPER_USER', true);//<<<<<<<<<<---------- FIX: SET FROM AUTHENTICATION ----------<<<<<<<<<<

//-----------------------------------------------------------

//============CREATE PAGE NAMES============
GetPageName();

//============WRITE LOG============
WriteTrackingLog();
//BlockedIPCheck();

//==========GET CONTENT FILE NAMES===========
GetPageFileNames();

//==========GET PAGE VARIABLES===========
GetTitleVariables();

if ($PAGE['pagename']!='index') $PAGE['template'] = 'dialog_template.html';
elseif(empty($PAGE['template'])){
  $PAGE['template'] = ($PAGE['print'])? 'ptemplate.html' : 'template.html';
}

//==========GET CONTENT===========
$PAGE_STREAM = file_get_contents("$SITE_ROOT/templates/{$PAGE['template']}");

ob_start(); include $PAGE['contentfilename']; $PAGE_CONTENT = ob_get_contents(); ob_end_clean();



//-------------------------- custom -------------------------
$querybutton = '';
$dbmessages  = '';
$querybutton = '';
$SCRIPTEND   = '';

if(SUPER_USER) {
    $QUERY = Get('QUERY');
    if ($QUERY == 'ON') $_SESSION['WANT_DB_QUERIES'] = 1;
    elseif ($QUERY == 'OFF') unset($_SESSION['WANT_DB_QUERIES']);
    if ($QUERY) exit;  // exit, because using AJAX to set the Session
    
    $setvalue = Session('WANT_DB_QUERIES')? 'OFF' : 'ON';
    
    $script = <<<SULBL1
    var dbQueryValue = '$setvalue';
    function setDbQuery() {
        var lastValue = dbQueryValue;
        dbQueryValue = (dbQueryValue=='ON')? 'OFF' : 'ON';
        $.get('index___QUERY=' + dbQueryValue, '', function(){
              $('#DB_QUERY_BUTTON').empty().append('Query ' + lastValue);
        } );

    }
SULBL1;

    $PAGE['script'] .= JavaScriptString($script);


    if(Session('WANT_DB_QUERIES')) {
        $dbmessages  = WriteDbQueryText();
    }
    $querybutton = '
        <a id="DB_QUERY_BUTTON" style="font-size:0.8em; width:5em; float:right;" 
              class="stdbutton" href="#" 
              onclick="setDbQuery(); return false;">Query '.$setvalue.'</a>';
    
}
SwapStdMarkUp();

$UserInfo = "<div>Welcome <b>$USER_NAME</b></div><a href=\"$PHP_SELF?LOGOUT=1\">Logout</a>";

$BASENAME
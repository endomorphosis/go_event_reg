			<?php

// FILE: class.Events.php

class Events extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage events',
        'Created'     => '2008-11-15',
        'Updated'     => '2008-11-15'
    );

    $this->Add_Submit_Name  = 'EVENTS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'EVENTS_SUBMIT_EDIT';
    $this->Table  = 'events';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'name' => 'Name',
        'description' => 'Description',
        'start_date' => 'Start Date',
        'start_time' => 'Start Time',
        'end_date' => 'End Date',
        'end_time' => 'End Time',
        'timezone_gmt' => 'Timezone Gmt',
        'location_city' => 'Location City',
        'location_state' => 'Location State',
        'location_country' => 'Location Country',
        'loc_address_1' => 'Loc Address 1',
        'loc_address_2' => 'Loc Address 2',
        'loc_postalcode' => 'Loc Postalcode',
        'location_description' => 'Location Description',
        'languages' => 'Languages',
        'currency' => 'Currency',
        'showcase_vendors' => 'Showcase Vendors',
        'attendee_cost' => 'Attendee Cost',
        'event_owner_name' => 'Event Owner Name',
        'event_owner_phone' => 'Event Owner Phone',
        'event_type' => 'Event Type',
        'event_requestor_name' => 'Event Requestor Name',
        'event_requestor_phone' => 'Event Requestor Phone',
        'membership_status_registered' => 'Membership Status Registered',
        'membership_status_member' => 'Membership Status Member',
        'membership_status_associate' => 'Membership Status Associate',
        'membership_status_premiere' => 'Membership Status Premiere',
        'membership_status_all' => 'Membership Status All',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

	
	/*	"text|Event Id|event_id|Y|8|8|$Mask_Integer",
        "text|Company Id|company_id|Y|8|8|$Mask_Integer",
        "hidden|transaction_type|$Value",
        "select|Currency|currency|$Currency",
		"dateYMD|Transaction Date|transaction_date|Y-M-D|Y|NOW|12",
        
        "text|Transaction Amount|transaction_amount|Y|16|16",
        "text|Created By|created_by|Y|16|16",
		
		
        "radioh|Success|success|Y||1=yes|0=no",
        "text|Po Id|po_id|Y|8|8|$Mask_Integer",
        "text|Invoice Id|invoice_id|Y|8|8|$Mask_Integer",
        "text|Estimate Id|estimate_id|Y|8|8|$Mask_Integer",
        "text|Payment Id|payment_id|Y|8|8|$Mask_Integer",
        "text|Deposit Id|deposit_id|Y|8|8|$Mask_Integer",
        "radioh|Contact Method|contact_method|Y||Email|Mail|Phone
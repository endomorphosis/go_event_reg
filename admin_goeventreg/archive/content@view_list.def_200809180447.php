<name>View List</name>
<summary>View List</summary>
<title>View List</title>
<description>View List</description>
<keywords></keywords>
<style></style>
<scriptinclude>ajax/i_ajax_functions.js,jquery/jquery.simplemodal.js,ajax_db_edit_modal_form.js</scriptinclude>
<script>
function deactivateCode(row, table)
{
	if(confirm('Are you sure you want to deactivate the code?'))
	{
		//document.getElementById('deleteRow').value = row;
		createRequest();
		var url = 'ajax/a_code_deactivate.php?row=' + row + '&table=' + table;
		request.open("GET", url, true);
		request.onreadystatechange = ajaxActionDeactivateConfirm;
		request.send(null);
	}
	return false;
}
function createCode(row, table)
{
		document.body.style.cursor = 'wait';
		//document.getElementById('deleteRow').value = row;
		createRequest();
		var url = 'ajax/a_code_create.php?row=' + row + '&table=' + table;
		request.open("GET", url, true);
		request.onreadystatechange = ajaxActionCreateConfirm;
		request.send(null);
}
function createCopyMark(row, table)
{
	var fromId = prompt("Enter copy FROM row ID");
	if (fromId!=null && fromId!="")
	{
		document.body.style.cursor = 'wait';
		//document.getElementById('deleteRow').value = row;
		createRequest();
		var url = 'ajax/a_copymark_create.php?row=' + row + '&table=' + table + '&fromRow=' + fromId;
		request.open("GET", url, true);
		request.onreadystatechange = ajaxActionCreateCopyMarkConfirm;
		request.send(null);
	}
}
function createMessageMark(row, table)
{
	var note = prompt("Enter note");
	if (note!=null && note!="")
	{
		document.body.style.cursor = 'wait';
		//document.getElementById('deleteRow').value = row;
		createRequest();
		var url = 'ajax/a_messagemark_create.php?row=' + row + '&table=' + table + '&note=' + note;
		request.open("GET", url, true);
		request.onreadystatechange = ajaxActionCreateMessageMarkConfirm;
		request.send(null);
	}
}
function copyRecord(row, table)
{
	var fromId = prompt("Enter copy FROM row ID");
	if (fromId!=null && fromId!="")
	{
		document.body.style.cursor = 'wait';
		//document.getElementById('deleteRow').value = row;
		createRequest();
		var url = 'ajax/a_copy_record.php?row=' + row + '&table=' + table + '&fromRow=' + fromId;
		request.open("GET", url, true);
		request.onreadystatechange = ajaxActionCopyRecordConfirm;
		request.send(null);
	}
}
function ajaxActionDeactivateConfirm()
{
	var message 	= "Unable to deactivate code at the current time!";
	var checkVal 	= "0";
	if (request.readyState == 4) {		if (request.status == 200) {			if (request.responseText == checkVal)			{				window.location.reload();			} else {				alert(message);			}		}	}
}

function ajaxActionCreateConfirm()
{
	var message 	= "Unable to create code at the current time!";
	var checkVal 	= "1";
	if (request.readyState
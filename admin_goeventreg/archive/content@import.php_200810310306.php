<?php
//========================== AJAX PROCESSING ==========================

$AJAX = isset($_GET['AJAX'])? $_GET['AJAX'] : '';

if ($AJAX) {
    include '../helper/ajax_helper.php';  // ---------- load AJAX support routines -------------

    $data = Post('data');
    $fields = explode("&",$data);
    $vars = array();
    $varlist = '';
    $columns = array();
    $havedata = false;

    foreach ($fields as $field) {
        list($key,$value) = explode('=', $field);

        if ($value) $vars[urldecode($key)] = urldecode($value);

        $column = !empty($value)? strFrom($key,'columnselect') : 0;

        if(strFrom($key,'columnselect') != '') {
            if ($column) {
               $varlist .= "$value,";
               $columns[] = $column;
               $havedata = true;
            } else {
                $varlist .= '@dummy,';
            }
        }

    }
    $DB_TABLE = $vars['DB_TABLE'];
    
    if (!empty($vars['group_name']) and (strpos($varlist, 'group_name') === false)) {
        $SET = " SET group_name='{$vars['group_name']}'";
    } else $SET = '';

    $varlist = substr($varlist,0,-1);
    // ===================== READY TO UPLOAD=====================

    $FILE = str_replace('\\','/',$vars['TempFile']);
    readln($FILE, 1);
    $headings = readln();
    $headingCount = count($headings);
    if (!$DB_TABLE) {echo "<h1>NO TABLE DEFINED!</h1>";
        exit;
    }

    $QUERY = "LOAD DATA INFILE '$FILE' INTO TABLE `$DB_TABLE`
    FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    ($varlist)$SET;";

    if($havedata) {
        $RESULT = mysql_query($QUERY);
        if($RESULT) echo 'FILE UPLOADED!';
        else echo 'UPLOAD FAILED!';
    } else {
        echo 'No Data Defined to Upload!';
    }

    exit; //---------------------- exit AJAX program ------------------
}
//=========================================================================



SetPost('FILENAME DOCUMENTUPLOAD TempFile NewDocFile NewDocSize DB_TABLE');
$NewFileName = dirname(dirname(__FILE__)) .'/tmp/uploadfile' . session_id() . '.csv';

$tables = db_GetTables();
$AssocTables = array();
foreach ($tables as $table) {
    $AssocTables[$table] = NameToTitle($table);
}
$TableDropDown = HTML_AssocArrayToDropDown($AssocTables, 'DB_TABLE', 'dropdown', Post('DB_TABLE'), 1, 
"onchange=\"if(this.value=='') {
        $('#submit').hide();
        } else {
            $('#submit').show();
        }\"");

print <<<LBL1
<form method="post" action="$REQUEST_URI" enctype="multipart/form-data">
<input type="file" name="FILENAME" size="80" value="$FILENAME"
    onchange="if(this.value=='') {
        $('#upload').hide();
        } else {
            $('#upload').show();
            $('#uploadcontent').hi
<?php

// FILE: class.VendorEventRegistrations.php

class VendorEventRegistrations extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage vendor_event_registrations',
        'Created'     => '2008-11-18',
        'Updated'     => '2008-11-18'
    );

    $this->Add_Submit_Name  = 'VENDOR_EVENT_REGISTRATIONS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'VENDOR_EVENT_REGISTRATIONS_SUBMIT_EDIT';
    $this->Table  = 'vendor_event_registrations';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'company_id' => 'Company Id',
        'event_id' => 'Event Id',
        'primary_contact_id' => 'Primary Contact Id',
        'secondary_contact_id' => 'Secondary Contact Id',
        'sponsorship_level' => 'Sponsorship Level',
        'number_of_badges' => 'Number Of Badges',
        'hotel_rooms' => 'Hotel Rooms',
        'charge_for_hotel' => 'Charge For Hotel',
        'promotional_codes' => 'Promotional Codes',
        'approval_status' => 'Approval Status',
        'other_items' => 'Other Items',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

    $this->Form_Data_Array_Add = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|Company Id|company_id|Y|11|11",
        "text|Event Id|event_id|Y|11|11",
        "text|Primary Contact Id|primary_contact_id|Y|11|11",
        "text|Secondary Contact Id|secondary_contact_id|Y|11|11",
        "text|Sponsorship Level|sponsorship_level|Y|60|80",
        "text|Number Of Badges|number_of_badges|Y|11|11",
        "text|Hotel Rooms|hotel_rooms|Y|11|11",
        "checkbox|Charge For Hotel|charge_for_hotel|Y||1|0",
        "text|Promotional Codes|promotional_codes|Y|60|255",
        "text|Approval Status|approval_status|Y|40|40",
        "textarea|Other Items|other_items|Y|80|10",
        "submit|Add Record|$this->Add_Submit_Name",
        "endform"
    );

    $this->Form_Data_Array_Edit = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|Company Id|company_id|Y|11|11",
        "text|Event Id|event_id|Y|11|11",
        "text|Primary Contact Id|primary_contact_id|Y|11|11",
        "text|Secondary Contact Id|secondary_contact_id|Y|11|11",
        "text|Sponsorship Level|sponsorship_level|Y|60|80",
        "text|Number Of Badges|number_of_badges|Y|11|11",
        "text|Hotel Rooms|hotel_rooms|Y|11|11",
        "checkbox|Charge For Hotel|charge_for_hotel|Y||1|0",
        "text|Promotional Codes|promotional_codes|Y|60|255",
        "text|Approval Status|approval_status|Y|40|40",
        "textarea|Other Items|other_items|Y|80|10",
        "text|Active|active|Y|1|1",
        "submit|Update Record|$this->Edit_Submit_Name",
        "endform"
    );

    $this->Default_Fields = 'company_id,event_id,primary_contact_id,secondary_contact_id,sponsorship_level';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
    }
}
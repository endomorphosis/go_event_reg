<?php

// FILE: class.Event_contacts.php

class Event_contacts extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage event_contacts',
        'Created'     => '2008-11-13',
        'Updated'     => '2008-11-13'
    );

    $this->Add_Submit_Name  = 'EVENT_CONTACTS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'EVENT_CONTACTS_SUBMIT_EDIT';
    $this->Table  = 'event_contacts';
    $this->Flash_Field = 'contact_id';

    $this->Field_Titles = array(
        'id' => 'Id',
        'event_id' => 'Event Id',
        'contact_id' => 'Contact Id',
        'vendor' => 'Vendor',
        'primary' => 'Primary',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created',
    );

    $this->Form_Data_Array_Add = array(
        "text|Event Id|event_id|Y|11|11"
        "text|Contact Id|contact_id|Y|11|11"
        "text|Vendor|vendor|Y|1|1"
        "text|Primary|primary|Y|1|1"
    );

    $this->Form_Data_Array_Edit = array(
        "text|Event Id|event_id|Y|11|11"
        "text|Contact Id|contact_id|Y|11|11"
        "text|Vendor|vendor|Y|1|1"
        "text|Primary|primary|Y|1|1"
        "text|Active|active|Y|1|1"
    );

    $this->Default_Fields = 'id,event_id,contact_id,vendor,primary,active,updated,created';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
    }
}
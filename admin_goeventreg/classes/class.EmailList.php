<?php

// FILE: class.EmailList.php

class EmailList extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage email_list',
        'Created'     => '2008-11-15',
        'Updated'     => '2008-11-15'
    );

    $this->Add_Submit_Name  = 'EMAIL_LIST_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'EMAIL_LIST_SUBMIT_EDIT';
    $this->Table  = 'email_list';
    $this->Flash_Field = 'email';

    $this->Field_Titles = array(
        'id' => 'Id',
        'group_name' => 'Group Name',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'name' => 'Name',
        'company_name' => 'Company Name',
        'email' => 'Email',
        'country' => 'Country',
        'updated' => 'Updated',
    );

    $this->Form_Data_Array_Add = array(
        "form|$this->Action_Link|post|name",
        "text|Group Name|group_name|Y|40|40",
        "text|First Name|first_name|Y|40|40",
        "text|Last Name|last_name|Y|40|40",
        "text|Name|name|Y|60|80",
        "text|Company Name|company_name|Y|40|40",
        "email|Email|email|Y|60|80",
        "country|Country|country|Y|40|40",
        "submit|Add Discount|$this->Add_Submit_Name",
        "endform"
    );

    $this->Form_Data_Array_Edit = array(
        "form|$this->Action_Link|post|name",
        "text|Group Name|group_name|Y|40|40",
        "text|First Name|first_name|Y|40|40",
        "text|Last Name|last_name|Y|40|40",
        "text|Name|name|Y|60|80",
        "text|Company Name|company_name|Y|40|40",
        "email|Email|email|Y|60|80",
        "country|Country|country|Y|40|40",
        "submit|Save Discount|$this->Edit_Submit_Name",
        "endform"
    );

    $this->Default_Fields = 'group_name,first_name,last_name,email';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
    }
}
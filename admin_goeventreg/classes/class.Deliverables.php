<?PHP

// FILE: class.Deliverables.php

class Deliverables extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';

	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';


	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
    $this->Default_Fields = ''; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table  = '';
    $this->Field_Titles = array();


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Edit_Item = array();


	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Add = array();


	#TABLE CREATION QUERY
	#===================================================================================================
    $this->Table_Creation_Query = "";	
	
    }
}
?>
<?PHP

// FILE: class.DeliverablesConfig.php

class DeliverablesConfig extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';

	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';




	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
	#ITEMS#
    $this->Default_Fields_Item = 'id,title'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table_Item  = 'deliverables_items';
    $this->Field_Titles_Item = array(
        'id' => 'Id',
        'title' => 'Title',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================


	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================



	#TABLE CREATION QUERY
	#===================================================================================================
    $this->Table_Creation_Query = "
	    CREATE TABLE IF NOT EXISTS `deliverables_config` (
        `id` int(11) NOT NULL auto_increment,
        `name` varchar(255) NOT NULL,
        `value` text NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
		`updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
		`created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
        ";	
    }
}
?>
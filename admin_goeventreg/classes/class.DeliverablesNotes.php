<?PHP

// FILE: class.DeliverablesNotes.php

class DeliverablesNotes extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';


	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';


	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
	#NEED NOTES CODE HERE
	#NEED NOTES CODE HERE
	#NEED NOTES CODE HERE


	#TABLE CREATION QUERY
	#===================================================================================================
    $this->Table_Creation_Query = "
        CREATE TABLE IF NOT EXISTS `deliverables_notes` (
        `id` int(11) NOT NULL auto_increment,
        `list_id` int(11) NOT NULL,
        `note` text NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
        `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
        `created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
        ";	
    }
}
?>
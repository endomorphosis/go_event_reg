			<?php

// FILE: class.Events.php

class Events extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage events',
        'Created'     => '2008-11-15',
        'Updated'     => '2008-11-15'
    );

    $this->Add_Submit_Name  = 'EVENTS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'EVENTS_SUBMIT_EDIT';
    $this->Table  = 'events';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'name' => 'Name',
        'description' => 'Description',
        'start_date' => 'Start Date',
        'start_time' => 'Start Time',
        'end_date' => 'End Date',
        'end_time' => 'End Time',
        'timezone_gmt' => 'Timezone Gmt',
        'location_city' => 'Location City',
        'location_state' => 'Location State',
        'location_country' => 'Location Country',
        'loc_address_1' => 'Loc Address 1',
        'loc_address_2' => 'Loc Address 2',
        'loc_postalcode' => 'Loc Postalcode',
        'location_description' => 'Location Description',
        'languages' => 'Languages',
        'currency' => 'Currency',
        'showcase_vendors' => 'Showcase Vendors',
        'attendee_cost' => 'Attendee Cost',
        'event_owner_name' => 'Event Owner Name',
        'event_owner_phone' => 'Event Owner Phone',
        'event_type' => 'Event Type',
        'event_requestor_name' => 'Event Requestor Name',
        'event_requestor_phone' => 'Event Requestor Phone',
        'membership_status_registered' => 'Membership Status Registered',
        'membership_status_member' => 'Membership Status Member',
        'membership_status_associate' => 'Membership Status Associate',
        'membership_status_premiere' => 'Membership Status Premiere',
        'membership_status_all' => 'Membership Status All',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

	
	/*	"text|Event Id|event_id|Y|8|8|$Mask_Integer",
        "text|Company Id|company_id|Y|8|8|$Mask_Integer",
        "hidden|transaction_type|$Value",
        "select|Currency|currency|$Currency",
		"dateYMD|Transaction Date|transaction_date|Y-M-D|Y|NOW|12",
        
        "text|Transaction Amount|transaction_amount|Y|16|16",
        "text|Created By|created_by|Y|16|16",
		
		
        "radioh|Success|success|Y||1=yes|0=no",
        "text|Po Id|po_id|Y|8|8|$Mask_Integer",
        "text|Invoice Id|invoice_id|Y|8|8|$Mask_Integer",
        "text|Estimate Id|estimate_id|Y|8|8|$Mask_Integer",
        "text|Payment Id|payment_id|Y|8|8|$Mask_Integer",
        "text|Deposit Id|deposit_id|Y|8|8|$Mask_Integer",
        "radioh|Contact Method|contact_method|Y||Email|Mail|Phone|Fax|",
	    "textarea|Contact Reason|contact_reason|N|60|6",
	    "textarea|Contact Resolution|contact_resolution|N|60|6",
		"dateYMD|Original Invoice Date|original_invoice_date|Y-M-D|Y|NOW|12",
        "text|Original Invoice Amount|original_invoice_amount|Y|16|16",
        "radioh|Payment Method|payment_method|Y||Credit|EFT",
        "text|Tracking Number|tracking_number|Y|32|32",
        "text|Source Account #|source_account|Y|32|32",
        "text|Destination Account #|destination_account|Y|32|32",
        "radioh|Cancel Type|cancel_type|Y||PO|Invoice|Estimate",
        "text|Active|active|Y|1|1" */
    $this->Form_Data_Array_Add = array(
        "form|$this->Action_Link|post|name",
		"text|Name|name|Y|60|64",
        "textarea|Description|description|Y|60|6",
        "dateYMD|Start Date|start_date|Y-M-D|Y|NOW|12",
        "time|Start Time|start_time|Y",
        "dateYMD|End Date|end_date|Y-M-D|Y|NOW|12",
        "time|End Time|end_time|Y",
		"select|Timezone Gmt|timezone_gmt|Y||-12|-11|-10|-9|-8|-7|-6|-5|-4|-3|-2|-1|0|1|2|3|4|5|6|7|8|9|10|11|12",
        "text|Loc Address 1|loc_address_1|Y|60|64",
        "text|Loc Address 2|loc_address_2|N|60|64",
        "text|Location City|location_city|Y|32|32",
        "country|Location Country|location_country|Y",
        "intstate|Location State|location_state|Y",
        "text|Loc Postalcode|loc_postalcode|Y|16|16|$this->Mask_Integer",
		"textarea|Location Description|location_description|Y|60|6",
        "text|Languages|languages|Y|60|1024",
        "text|Currency|currency|Y|60|1024",
		"textarea|Showcase Vendors|showcase_vendors|Y|60|6",
        "text|Attendee Cost|attendee_cost|Y|16|16|$this->Mask_Integer",
        "text|Event Owner Name|event_owner_name|Y|60|1024",
        "text|Event Owner Phone|event_owner_phone|Y|32|32",
		"radioh|Event Type|event_type|Y||ISS|ICC|TST|Other",
        "text|Event Requestor Name|event_requestor_name|Y|32|32",
        "text|Event Requestor Phone|event_requestor_phone|Y|32|32",
        "checkbox|Membership Status Registered|membership_status_registered||0|1",
        "checkbox|Membership Status Member|membership_status_member||0|1",
        "checkbox|Membership Status Associate|membership_status_associate||0|1",
        "checkbox|Membership Status Premiere|membership_status_premiere||0|1",
        "checkbox|Membership Status All|membership_status_all||0|1",
		"text|Created By|created_by|Y|16|16",
		"text|Created Method|created_method|Y|16|16",
		"submit|Add Event|$this->Add_Submit_Name",
		);

    $this->Form_Data_Array_Edit = array(
        "form|$this->Action_Link|post|name",
		"text|Name|name|Y|60|64",
        "text|Description|description|Y|60|4096",
        "text|Start Date|start_date|Y|16|16",
        "text|Start Time|start_time|Y|16|16",
        "text|End Date|end_date|Y|16|16",
        "text|End Time|end_time|Y|16|16",
		"select|Timezone Gmt|timezone_gmt|Y||-12|-11|-10|-9|-8|-7|-6|-5|-4|-3|-2|-1|0|1|2|3|4|5|6|7|8|9|10|11|12",        "text|Location City|location_city|Y|16|16",
        "country|Location Country|location_country|Y",
        "intstate|Location State|location_state|Y",
        "text|Loc Address 1|loc_address_1|Y|60|64",
        "text|Loc Address 2|loc_address_2|Y|60|64",
        "text|Loc Postalcode|loc_postalcode|Y|16|16",
        "text|Location Description|location_description|Y|60|1024",
        "text|Languages|languages|Y|60|1024",
        "text|Currency|currency|Y|60|1024",
        "text|Showcase Vendors|showcase_vendors|Y|60|2048",
        "text|Attendee Cost|attendee_cost|Y|16|16",
        "text|Event Owner Name|event_owner_name|Y|60|1024",
        "phone|Event Owner Phone|event_owner_phone|Y|32|32",
        "text|Event Type|event_type|Y|32|32",
        "text|Event Requestor Name|event_requestor_name|Y|32|32",
        "phone|Event Requestor Phone|event_requestor_phone|Y|32|32",
        "checkbox|Membership Status Registered|membership_status_registered||0|1",
        "checkbox|Membership Status Member|membership_status_member||0|1",
        "checkbox|Membership Status Associate|membership_status_associate||0|1",
        "checkbox|Membership Status Premiere|membership_status_premiere||0|1",
        "checkbox|Membership Status All|membership_status_all||0|1",
		"checkbox|Active|active||0|1",
		"submit|Save Discount|$this->Edit_Submit_Name",
    );

    $this->Default_Fields = 'id,name,start_date,end_date,location_city,location_state,location_country,event_owner_name,event_type,event_requestor_name,active';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
	writedbquery();
    }
}
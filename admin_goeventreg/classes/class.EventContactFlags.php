<?php

// FILE: class.EventContactFlags.php

class EventContactFlags extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage event_contact_flags',
        'Created'     => '2008-11-13',
        'Updated'     => '2008-11-13'
    );

    $this->Add_Submit_Name  = 'EVENT_CONTACT_FLAGS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'EVENT_CONTACT_FLAGS_SUBMIT_EDIT';
    $this->Table  = 'event_contact_flags';
    $this->Flash_Field = 'id';

    $this->Field_Titles = array(
        'id' => 'Id',
        'event_id' => 'Event Id',
        'contact_id' => 'Contact Id',
        'flag' => 'Flag',
        'value' => 'Value',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created',
    );

    $this->Form_Data_Array_Add = array(
        "text|Event Id|event_id|Y|11|11"
        "text|Contact Id|contact_id|Y|11|11"
        "text|Flag|flag|Y|60|127"
        "text|Value|value|Y|60|127"
    );

    $this->Form_Data_Array_Edit = array(
        "text|Event Id|event_id|Y|11|11"
        "text|Contact Id|contact_id|Y|11|11"
        "text|Flag|flag|Y|60|127"
        "text|Value|value|Y|60|127"
        "text|Active|active|Y|1|1"
    );

    $this->Default_Fields = 'id,event_id,contact_id,flag,value,active,updated,created';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
    }
}
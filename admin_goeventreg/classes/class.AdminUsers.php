<?php

// FILE: class.AdminUsers.php

class AdminUsers extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();
global $SESSION_LOGIN_BASE;
global $ADMIN_USER_ID; //= Session($SESSION_LOGIN_BASE.'USER_ID');
global $ADMIN_NAME    ;  // = Session($SESSION_LOGIN_BASE.'NAME');
    $this->ClassInfo = array(
        'Created By'  => 'Benjamin Barber',
        'Description' => 'Create and manage admin_users',
        'Created'     => '2008-11-18',
        'Updated'     => '2008-11-18'
    );

    $this->Add_Submit_Name  = 'ADMIN_USERS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'ADMIN_USERS_SUBMIT_EDIT';
    $this->Table  = 'admin_users';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'email' => 'Email',
        'password' => 'Password',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'super_user' => 'Super User',
        'roles' => 'Roles',
        'created_by' => 'Created By',
        'company_name' => 'Company Name'
    );

    $this->Form_Data_Array_Add = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|Email|email|Y|60|64",
        "text|Password|password|Y|32|32",
        "text|First name|first_name|Y|32|32",
        "text|Last name|last_name|Y|32|32",
        "checkbox|Super User|super_user||1|0",
        "text|Roles|roles|N|60|128",
        "hidden|Created_By|$ADMIN_NAME",
        "text|Company Name|company_name|N|32|32",
        "submit|Add Record|$this->Add_Submit_Name",
        "endform"
    );

    $this->Form_Data_Array_Edit = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|Email|email|Y|60|64",
        "text|Password|password|Y|32|32",
        "text|First name|first_name|Y|32|32",
        "text|Last name|last_name|Y|32|32",
        "checkbox|Super User|super_user||1|0",
        "text|Roles|roles|N|60|128",
        "hidden|Created_By|$ADMIN_NAME",
        "text|Company Name|company_name|N|32|32",
        "submit|Update Record|$this->Edit_Submit_Name",
        "endform"
    );

    $this->Default_Fields = 'id,email,password,first_name,last_name,super_user,roles,created_by,company_name';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
    }
}
<?php

// FILE: class.EventSeries.php

class EventSeries extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage event_series',
        'Created'     => '2008-11-17',
        'Updated'     => '2008-11-17'
    );

    $this->Add_Submit_Name  = 'EVENT_SERIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'EVENT_SERIES_SUBMIT_EDIT';
    $this->Table  = 'event_series';
    $this->Flash_Field = 'series_name';

    $this->Field_Titles = array(
        'id' => 'Id',
        'event_type' => 'Event Type',
        'geo' => 'Geo',
        'series_name' => 'Series Name',
        'notes' => 'Notes',
        'created_by' => 'Created By',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

    $this->Form_Data_Array_Add = array(
        "form|$this->Action_Link|post|db_edit_form",
        'select|Event Type|event_type|Y||iss=ISS|tst=TST|icc=ICC',
        'select|Geo|geo|Y||APAC|EMEA|LAR|NAMO|PRC',
        "text|Series Name|series_name|Y|60|80",
        "textarea|Notes|notes|N|80|10",
        "text|Created By|created_by|Y|60|80",
        "submit|Add Record|$this->Add_Submit_Name",
        "endform"
    );

    $this->Form_Data_Array_Edit = array(
        "form|$this->Action_Link|post|db_edit_form",
        'select|Event Type|event_type|Y||iss=ISS|tst=TST|icc=ICC',
        'select|Geo|geo|Y||APAC|EMEA|LAR|NAMO|PRC',
        "text|Series Name|series_name|Y|60|80",
        "textarea|Notes|notes|N|80|10",
        "text|Created By|created_by|Y|60|80",
        "text|Active|active|Y|1|1",
        "submit|Update Record|$this->Edit_Submit_Name",
        "endform"
    );

    $this->Default_Fields = 'event_type,geo,series_name';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
    }
}
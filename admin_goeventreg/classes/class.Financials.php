<?php

// FILE: class.Financials.php

class Financials extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage financials',
        'Created'     => '2008-11-15',
        'Updated'     => '2008-11-15'
    );

    $this->Add_Submit_Name  = 'FINANCIALS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'FINANCIALS_SUBMIT_EDIT';
    $this->Table  = 'financials';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'event_id' => 'Event Id',
        'company_id' => 'Company Id',
        'transaction_type' => 'Transaction Type',
        'currency' => 'Currency',
        'transaction_date' => 'Transaction Date',
        'transaction_time' => 'Transaction Time',
        'transaction_amount' => 'Transaction Amount',
        'created_by' => 'Created By',
        'invoice_date' => 'Invoice Date',
        'notes' => 'Notes',
        'success' => 'Success',
        'po_id' => 'Po Id',
        'invoice_id' => 'Invoice Id',
        'estimate_id' => 'Estimate Id',
        'payment_id' => 'Payment Id',
        'deposit_id' => 'Deposit Id',
        'contact_method' => 'Contact Method',
        'contact_reason' => 'Contact Reason',
        'contact_resolution' => 'Contact Resolution',
        'original_invoice_date' => 'Original Invoice Date',
        'original_invoice_amount' => 'Original Invoice Amount',
        'payment_method' => 'Payment Method',
        'tracking_number' => 'Tracking Number',
        'source_account' => 'Source Account',
        'destination_account' => 'Destination Account',
        'cancel_type' => 'Cancel Type',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

    $this->Form_Data_Array_Add = array(
        "text|Event Id|event_id|Y|8|8",
        "text|Company Id|company_id|Y|8|8",
        "text|Transaction Type|transaction_type|Y|16|16",
        "text|Currency|currency|Y|16|16",
        "text|Transaction Date|transaction_date|Y|16|16",
        "text|Transaction Time|transaction_time|Y|16|16",
        "text|Transaction Amount|transaction_amount|Y|16|16",
        "text|Created By|created_by|Y|16|16",
        "text|Invoice Date|invoice_date|Y|16|16",
        "text|Notes|notes|Y|60|1024",
        "text|Success|success|Y|1|1",
        "text|Po Id|po_id|Y|8|8",
        "text|Invoice Id|invoice_id|Y|8|8",
        "text|Estimate Id|estimate_id|Y|8|8",
        "text|Payment Id|payment_id|Y|8|8",
        "text|Deposit Id|deposit_id|Y|8|8",
        "text|Contact Method|contact_method|Y|16|16",
        "text|Contact Reason|contact_reason|Y|60|1024",
        "text|Contact Resolution|contact_resolution|Y|60|1024",
        "text|Original Invoice Date|original_invoice_date|Y|16|16",
        "text|Original Invoice Amount|original_invoice_amount|Y|16|16",
        "text|Payment Method|payment_method|Y|16|16",
        "text|Tracking Number|tracking_number|Y|32|32",
        "text|Source Account|source_account|Y|32|32",
        "text|Destination Account|destination_account|Y|32|32",
        "text|Cancel Type|cancel_type|Y|16|16"
    );

    $this->Form_Data_Array_Edit = array(
        "text|Event Id|event_id|Y|8|8",
        "text|Company Id|company_id|Y|8|8",
        "text|Transaction Type|transaction_type|Y|16|16",
        "text|Currency|currency|Y|16|16",
        "text|Transaction Date|transaction_date|Y|16|16",
        "text|Transaction Time|transaction_time|Y|16|16",
        "text|Transaction Amount|transaction_amount|Y|16|16",
        "text|Created By|created_by|Y|16|16",
		"text|Created Method|created_method|Y|16|16",
        "text|Invoice Date|invoice_date|Y|16|16",
        "text|Notes|notes|Y|60|1024",
        "text|Success|success|Y|1|1",
        "text|Po Id|po_id|Y|8|8",
        "text|Invoice Id|invoice_id|Y|8|8",
        "text|Estimate Id|estimate_id|Y|8|8",
        "text|Payment Id|payment_id|Y|8|8",
        "text|Deposit Id|deposit_id|Y|8|8",
        "text|Contact Method|contact_method|Y|16|16",
        "text|Contact Reason|contact_reason|Y|60|1024",
        "text|Contact Resolution|contact_resolution|Y|60|1024",
        "text|Original Invoice Date|original_invoice_date|Y|16|16",
        "text|Original Invoice Amount|original_invoice_amount|Y|16|16",
        "text|Payment Method|payment_method|Y|16|16",
        "text|Tracking Number|tracking_number|Y|32|32",
        "text|Source Account|source_account|Y|32|32",
        "text|Destination Account|destination_account|Y|32|32",
        "text|Cancel Type|cancel_type|Y|16|16",
        "text|Active|active|Y|1|1"
    );

    $this->Default_Fields = 'id,event_id,company_id,transaction_type,currency,transaction_date,transaction_time,transaction_amount,created_by,invoice_date,notes,success,po_id,invoice_id,estimate_id,payment_id,deposit_id,contact_method,contact_reason,contact_resolution,original_invoice_date,original_invoice_amount,payment_method,tracking_number,source_account,destination_account,cancel_type,active,updated,created,event_type';

    $this->Unique_Fields = '';

        $this->Table_Creation_Query = "CREATE TABLE IF NOT EXISTS `financials` (
	`id` int(11) NOT NULL auto_increment,
	`event_id` int(8) NOT NULL,
	`event_type` varchar(8) NOT NULL,
	`company_id` int(8) NOT NULL,
	`transaction_type` varchar(16) NOT NULL,
	`currency` varchar(16) NOT NULL,
	`transaction_date` varchar(16) NOT NULL,
	`transaction_time` varchar(16) NOT NULL,
	`transaction_amount` int(16) NOT NULL,
	`created_by` varchar(16) NOT NULL,
	`invoice_date` varchar(16) NOT NULL,
	`notes` varchar(1024) NOT NULL,
	`success` tinyint(1) NOT NULL,
	`po_id` int(8) NOT NULL,
	`invoice_id` int(8) NOT NULL,
	`estimate_id` int(8) NOT NULL,
	`payment_id` int(8) NOT NULL,
	`deposit_id` int(8) NOT NULL,
	`contact_method` varchar(16) NOT NULL,
	`contact_reason` varchar(1024) NOT NULL,
	`contact_resolution` varchar(1024) NOT NULL,
	`original_invoice_date` varchar(16) NOT NULL,
	`original_invoice_amount` int(16) NOT NULL,
	`payment_method` varchar(16) NOT NULL,
	`tracking_number` varchar(32) NOT NULL,
	`source_account` varchar(32) NOT NULL,
	`destination_account` varchar(32) NOT NULL,
	`cancel_type` varchar(16) NOT NULL,
	`active` tinyint(1) NOT NULL default '1',
	`updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
	`created` datetime NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
	";
    }
}
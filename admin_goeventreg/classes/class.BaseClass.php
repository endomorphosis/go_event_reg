<?php

class BaseClass
{
    public  $ClassInfo;

    public  $Add_Submit_Name;
    public  $Edit_Submit_Name;
    public  $Error  = '';
    public  $Table;
    public  $Action_Link;

    public  $Field_Titles;
    public  $Form_Data_Array_Add;
    public  $Form_Data_Array_Edit;
    public  $Unique_Fields;
    public  $Default_Fields;

    public  $Field_Values = array (
                'active' => array(1 => 'Yes', 2=> 'No')
            );
    public  $Flash_Field  = '';

    public  $Mask_Integer    = '^[0-9]+$';
    public  $Mask_Name       = '^[a-zA-Z0-9 \'\-]+$';
    public  $Mask_Username   = '^[a-zA-Z0-9]+$';
    public  $Mask_Password   = '^[a-zA-Z0-9:\-\!\@\#\$\%\^\&\*_]+$';
    public  $Mask_Email      = '^([0-9a-z]+)([0-9a-z\.-_]+)@([0-9a-z\.-_+)\.([0-9a-z]+)';
    public  $Mask_Real       = '^[0-9.\-]+$';
    public  $Mask_RealC      = '^[0-9\,.\-]+$';
    public  $Mask_Zip        = '^[0-9\-]+$';
    public  $Mask_4int       = '^[0-9]{4}$';
    public  $Mask_2int       = '^[0-9]{2}$';
    public  $Mask_Char       = '^[a-zA-Z]+$';
    public  $Mask_2chr       = '^[a-zA-Z]{2}$';
    public  $Mask_General_Line = '^[a-zA-Z0-9_ \!-\?[:punct:]]+$';
    public  $Mask_General; // in construct
    public  $Mask_Words      = '^[[:alnum:][:space:][:punct:]]+$';
    public  $Mask_Dir        = '^[a-zA-Z0-9\/_\.\-]+$';

    private $Table_Creation_Query;
    public  $Default_Table_Options = 'cellspacing="1" cellpadding="0"';
    public  $Default_Td_Options    = '';
    public  $Default_Th_Options    = '';
    public  $Edit_Links  = '
        <td align="center"><a href="#" class="row_view" title="View" onclick="tableEditClick(\'VALUE\',\'view\'); return false;"></a></td>
        <td align="center"><a href="#" class="row_edit" title="Edit" onclick="tableEditClick(\'VALUE\',\'edit\'); return false;"></a></td>
        <td align="center"><a href="#" class="row_delete" title="Delete" onclick="tableDeleteClick(\'VALUE\'); return false; "></a></td>';

    public function  __construct()
    {
        $this->Action_Link   = $_SERVER['REQUEST_URI'];
        $this->Mask_General  = '^[a-zA-Z0-9_ ' . "\r\n\t" . '\!-\?[:punct:]]+$';        
    }


    public function CreateTable()
    {
        return mysql_query($this->Table_Creation_Query);
    }


    public function AddRecord()
    {
        $this->Error = '';

        if (havesubmit($this->Add_Submit_Name)) {

            $FormArray = ProcessForm($this->Form_Data_Array_Add,
              $this->Edit_Submit_Name, $this->Default_Table_Options,
              $this->Default_Th_Options, $this->Default_Td_Options, $this->Error);

            if (!$this->Error and $this->Unique_Fields) {
                // check for unique fields
                $fields = explode(',', $this->Unique_Fields);
                foreach ($fields as $field) {
                    if ( !db_IsUnique($this->Table,$field,$FormArray[$field],'')) {
                        $this->Error = $this->Field_Titles[$field] . ' already exits!';
                        break;
                    }
                }
            }
            if (!$this->Error) {
                $fields = db_Keys($FormArray).',created';
                $values = db_Values($FormArray).",NOW()";
                if (db_AddRecord($this->Table,$fields,$values)) {
                   $flash = (!empty($FormArray[$this->Flash_Field]))? $FormArray[$this->Flash_Field] : mysql_insert_id();
                   AddMessage("Record: [$flash] Added");
                } else {$this->Error .= 'DB Write Error';}
        	}

        }
        if (!havesubmit($this->Add_Submit_Name) or $this->Error) {
            WriteError($this->Error);
            echo OutputForm($this->Form_Data_Array_Add,Post($this->Add_Submit_Name));
        }

    }


    public function PrePopulateFormValues($id, $field='id')
    {
        global $FormPrefix;
        // ------- prepopulate fields -------
        $Record   = db_GetRecord($this->Table,'*',"`$field`='$id'");
        foreach ($Record as $key => $value) {
                $_POST[$FormPrefix.$key] = $value;
        }
    }

    public function EditRecord($id, $id_field='id')
    {
        $this->Error = '';

        if (havesubmit($this->Edit_Submit_Name)) {

            $FormArray = ProcessForm($this->Form_Data_Array_Edit,
                $this->Edit_Submit_Name, $this->Default_Table_Options,
                $this->Default_Th_Options, $this->Default_Td_Options, $this->Error);

            if (!$this->Error and $this->Unique_Fields) {
                $fields = explode(',', $this->Unique_Fields);
                foreach ($fields as $field) {
                    if ( !db_IsUnique($this->Table,$field,$FormArray[$field],"$id_field!=$id")) {
                        $this->Error = $this->Field_Titles[$field] . ' already exits!';
                        break;
                    }
                }
            }

            if (!$this->Error) {
                $key_values = db_KeyValues($FormArray);
                if(db_UpdateRecord($this->Table,$key_values,"`$id_field`='$id'")){
                    $flash = (!empty($FormArray[$this->Flash_Field]))? $FormArray[$this->Flash_Field] : $id;
                    AddFlash("Record [$flash] Updated");
                } else {$this->Error .= 'DB Write Error';}
        	}

        }


        if (!havesubmit($this->Edit_Submit_Name) or $this->Error) {
            if (!$this->Error) {
                // ------- prepopulate fields -------
            	$this->PrePopulateFormValues($id);
            }
            WriteError($this->Error);
            echo OutputForm($this->Form_Data_Array_Edit,Post($this->Edit_Submit_Name));
        }

    }

    public  $Search_Selection_Operators = array('All','=','Not =','&lt;','&gt;','&lt;=','&gt;=','includes');


    public function DisplaySearchSelectionOperators($var)
    {
        $RESULT = '
        <select class="table_search_display_operators" name="TABLE_SEARCH_OPERATOR_'.$var.'" id="TABLE_SEARCH_OPERATOR_'.$var.'">';
        foreach ($this->Search_Selection_Operators as $key) {
            $selected = (Post('TABLE_SEARCH_OPERATOR_'.$var) == $key)? ' selected="selected"' : '';
            $RESULT .= "
            <option value=\"$key\"$selected>$key</option>";
        }
        $RESULT .= '
        </select>';
        return $RESULT;
    }

    public function DisplaySearchTab()
    {
        $default_fields = explode(',',$this->Default_Fields);
        foreach ($default_fields as $field) $_POST['TABLE_DISPLAY_'.$field] = 1;
        $RESULT = '
            <form name="TABLE_SEARCH_SELECTION" id="TABLE_SEARCH_SELECTION" method="post" action="'.$this->Action_Link.'">
            <table id="TABLE_SEARCH_TAB_WRAPPER" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr><td>
            <div id="TABLE_SEARCH_SELECTION_SHOW">            
                <a href="#" onclick="tableSearchDisplayToggle(); return false;">Search Selection</a>
            </div>
            <input type="hidden" id="TABLE_NAME" name="TABLE_NAME" value="'. $this->Table . '" />
            <table id="TABLE_SEARCH_TAB" border="0" cellspacing="1" cellpadding="0">
            <tbody>            
            <tr>
              <th>Field</th><th>Select</th><th>Value</th><th>Display</th><th>Order (A/D)</th>
            </tr>';

        foreach ($this->Field_Titles as $field => $title) {
            $select   = $this->DisplaySearchSelectionOperators($field);
            $value    = TransformContent(Post('TABLE_SEARCH_VALUE_'.$field),'TS');
            $input    = "<input type=\"text\" name=\"TABLE_SEARCH_VALUE_$field\" value=\"$value\" size=\"40\" />";
            $checked  = (Post('TABLE_DISPLAY_'.$field) == 1)? ' checked="checked"' : '';
            $checkbox = "<input type=\"checkbox\" name=\"TABLE_DISPLAY_$field\" value=\"1\"$checked />";

            $checked  = ( ((Post('TABLE_ORDER') == '') and ($field == 'id')) or
                          (Post('TABLE_ORDER') == $field) )? ' checked="checked"' : '';
            $radio    = "<input type=\"radio\" name=\"TABLE_ORDER\" value=\"$field\"$checked /><br />";
            
            $checked  = ( Post('TABLE_ORDER') == "$field DESC")? ' checked="checked"' : '';
            
            $radio   .= "<input type=\"radio\" name=\"TABLE_ORDER\" value=\"$field DESC\"$checked />";
            

            $RESULT .= "
            <tr>
                <td align=\"right\">$title</td>
                <td>$select</td>
                <td>$input</td>
                <td align=\"center\">$checkbox</td>
                <td align=\"center\">$radio</td>
            </tr>";
        }

        $RESULT .= '
            <tr id="UPDATE_BUTTON">
                <td colspan="5">
                    <a href="#" onclick="tableSearch(\'SHOW\'); return false;" />Update</a>
                </td>
            </tr>
            </tbody>
            </table>
            </td></tr></tbody></table>
            </form>';

        return $RESULT;
    }

    public function DisplayEditViewTab()
    {
        return '<div id="DISPLAY_VIEW_TAB"></div>';
    }

    public function ListTable()
    {
        $this->Default_Table_Options .= ' id="TABLE_DISPLAY"';

        echo $this->DisplaySearchTab();
        $keys = '*';
        $conditions = '';
        $order = 'id';
        $start_list = 1;
        $list_size  = 25;
        $array = db_GetArray($this->Table, $this->Default_Fields . ',id AS TID', $conditions, $order, $start_list-1, $list_size, $num_rows);
        echo $this->OutputTable($array,$num_rows,$start_list,$list_size);

        echo $this->DisplayEditViewTab();

    }

    public function RowSelect($num_rows, $start_row, $row_count)
    {
        $RESULT = '<div class=rowselect">
            <input type="hidden" id="NUMBER_ROWS" name="NUMBER_ROWS" value="' . $num_rows . '" />';
        if ($start_row > $num_rows) $start_row = 1;
        if ($num_rows > $row_count) {
            $RESULT .= '
            <p>Found: "' . number_format($num_rows) . '" records</p>
            <p>Start Row: <input type="text" size="6" maxlength="10" id="TABLE_STARTROW" name="TABLE_STARTROW" value="' .$start_row . '" />&nbsp;&nbsp;
            Rows per Page: <input type="text" size="6" maxlength="10" id="TABLE_ROWS" name="TABLE_ROWS" value="' . $row_count . '" />&nbsp;&nbsp;
            <input class="SEARCH_BUTTON" type="button" value="Show" onclick="tableSearch(\'SHOW\');" />&nbsp;';
            if ($start_row > 1) {
                $RESULT .= '<input class="SEARCH_BUTTON" type="button" value="&lt;&lt;" onclick="tableSearch(\'HOME\');" />&nbsp;';
            }
            if ($start_row > 1) {
                $RESULT .= '<input class="SEARCH_BUTTON" type="button" value="&nbsp;<&nbsp;" onclick="tableSearch(\'PREVIOUSPAGE\');" />&nbsp;';
            }
            if ($num_rows >= $start_row+$row_count) {
                $RESULT .= '<input class="SEARCH_BUTTON" type="button" value="&nbsp;&gt;&nbsp;" onclick="tableSearch(\'NEXTPAGE\');" />&nbsp;';
            }
            if ($num_rows >= $start_row+$row_count) {
                $RESULT .= '<input class="SEARCH_BUTTON" type="button" value="&gt;&gt;" onclick="tableSearch(\'END\');" />';
            }
            $RESULT .= '</p></div>';
        } else {
            $RESULT .= '
              <input type="hidden" id="TABLE_STARTROW" name="TABLE_STARTROW" value="' . $start_row . '" />
              <input type="hidden" id="TABLE_ROWS" name="TABLE_ROWS" value="' . $row_count . '" />
              </div>';
        }
        return $RESULT;
    }
    

    public function OutputTable($search_array, $num_rows, $start_row, $row_count, $field_titles='')
    {
        if (count($search_array) == 0) return '';

        $field_values = $this->Field_Values;
        if (empty($field_titles)) $field_titles = $this->Field_Titles;

        $RESULT  = "\n\n<table $this->Default_Table_Options>\n";
        $RESULT .= "<tbody>\n";
        
        $colcount = count($search_array[0]) + 4;
        
        $RESULT .= '
            <tr id="table_title">
                <td colspan="'. $colcount. '">
                    Search Results : '. NameToTitle($this->Table). '
                </td>
            </tr>';
        
        $RESULT .= "<tr><td class=\"row_select\" colspan=\"$colcount\">" . $this->RowSelect($num_rows, $start_row, $row_count) ."</td></tr>\n";
        
        $RESULT .= "<tr><th>No.</th>";


        foreach ($search_array[0] as $key => $value) {
            if ($key <> 'TID') $RESULT .= "<th>{$field_titles[$key]}</th>";
        }
        $RESULT .= '<th colspan="3"></th>';  // add column for edit links
        $RESULT .= "</tr>\n";
        $evenodd = 2;
        $count = $start_row-1;
        foreach ($search_array as $row) {
            $count++;
            $evenodd = 3 - $evenodd;
            $class = ($evenodd == 1) ? 'odd' : 'even';
            $RESULT .= "<tr id=\"TABLE_ROW_ID_{$row['TID']}\" class=\"$class\"><td align=\"right\">$count.</td>";
            //if ($wantedit) $RESULT .= '<td>'. str_replace('@@ID@@',$row[$id],$EditLinks) .'</td>';
            foreach ($field_titles as $key => $value) {
                if (isset($row[$key])) {
                    $field = $row[$key];
                    $outvalue = (empty($field_values[$key][$field]))?
                        $field :
                        $field_values[$key][$field];
                    $RESULT .= "<td>$outvalue</td>";
                }
            }
            $RESULT .= str_replace('VALUE', $row['TID'], $this->Edit_Links);
            $RESULT .= "</tr>\n";
        }
        $RESULT .= "</tbody></table>\n";
        
        return $RESULT;
    }

    public function ViewRecord($id, $field='id')
    {
        $id = db_QuoteValue($id);
        $field = TransformContent($field, 'TQS');
        echo db_ViewRecord($this->Table,$this->Field_Titles,$this->Default_Table_Options,'*',"`$field`=$id");
    }
}
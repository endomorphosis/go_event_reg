<?php

// FILE: class.Sponsorship.php

class Sponsorship extends BaseClass
{

    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage sponsorship',
        'Created'     => '2008-11-25',
        'Updated'     => '2008-11-25'
    );

    $this->Add_Submit_Name  = 'SPONSORSHIP_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'SPONSORSHIP_SUBMIT_EDIT';
    $this->Table  = 'sponsorship';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'event_id' => 'Event Id',
        'sponsor_type' => 'Sponsor Type',
        'opportunity' => 'Opportunity',
        'vis' => 'Vis',
        'company_id' => 'Company Id',
        'created_by' => 'Created By',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created',
        'status' => 'Status'
    );

    $this->Form_Data_Array_Add = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|Event name|event_name|Y|8|8",
		"text|Event Id|event_id|Y|8|8",
        "text|Sponsor Type|sponsor_type|Y|32|32",
        "text|Opportunity|opportunity|Y|32|32",
		"radioh|Visable|vis|Y||1=yes|0=no",
        "text|Company Id|company_id|Y|8|8",
        "hidden|created_by|$ADMIN_NAME",
        "text|Status|status|Y|8|8",
        "submit|Add Record|$this->Add_Submit_Name",
        "endform"
    );

    $this->Form_Data_Array_Edit = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|Event Id|event_id|Y|8|8",
        "text|Sponsor Type|sponsor_type|Y|32|32",
        "text|Opportunity|opportunity|Y|32|32",
        "text|Visable|vis|Y|1|1",
        "text|Company Id|company_id|Y|8|8",
        "hidden|created_by|$ADMIN_NAME",
        "text|Active|active|Y|1|1",
        "text|Status|status|Y|8|8",
        "submit|Update Record|$this->Edit_Submit_Name",
        "endform"
    );

    $this->Default_Fields = 'id,event_id,sponsor_type,opportunity,vis,company_id,created_by,active,updated,created,status';

    $this->Unique_Fields = '';

    $this->Table_Creation_Query = "";
    }
	public function list_sponsorships ($company_id, $sponsor_type, $event_id)
	{
	
	$myformdata = array(
		"form|$this->Action_Link|post",
		'code|<div id="hidden_vars">',
		"hidden|table|null",
		"hidden|search_string|null",
		"hidden|search|null",
		"hidden|param|null",
		"hidden|type|null",
		'code|</div>',
 "select|opportunity|opportunity|Y|"." onclick=\" AjaxDynamicSelect1('opportunity', 'sponsorship', 'opportunity', '$sponsor_type', '$event_id' );  \" "."|",
 "hidden|company_id|$company_id",
 "hidden|event_id|$event_id",
 "hidden|status|pending",
 "hidden|vis|0",
 "submit|Submit|$this->Add_Submit_Name|",
	);
	echo OutputForm($myformdata);
echo assocarraytostr($myformdata);
	echo 'test';
	
	}
}
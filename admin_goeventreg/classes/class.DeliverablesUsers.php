<?PHP

// FILE: class.DeliverablesUsers.php

class DeliverablesUsers extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';

	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';


	#INITIAL SEARCH TABLE VARIABLES
	#===================================================================================================
	#USERS#
    $this->Default_Fields = 'id,type'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table  = 'deliverables_users';
    $this->Field_Titles = array(
        'id' => 'Id',
        'type' => 'Type',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Add = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD USER",
		"code|<hr style='border:0px; height:1px;'>",
		"select|User Type|type|Y||vendor=Vendor|admin=Administrator",
		"text|Contact ID|contact_id|Y|60|80",
		"text|Events List|events|Y|60|80",
		"checkbox|Email Status Reports|email_notify_reports|checked|1|0",
		"checkbox|Email on List Changes|email_notify_changes||1|0",
		"checkbox|Email on Upload Successful|email_notify_uploads||1|0",
		"checkbox|Email on Item Approval|email_notify_approvals||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );
		

	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================
    $this->Form_Data_Array_Edit = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD USER",
		"code|<hr style='border:0px; height:1px;'>",
		"select|User Type|type|Y||vendor=Vendor|admin=Administrator",
		"text|Contact ID|contact_id|Y|60|80",
		"text|Events List|events|Y|60|80",
		"checkbox|Email Status Reports|email_notify_reports|checked|1|0",
		"checkbox|Email on List Changes|email_notify_changes||1|0",
		"checkbox|Email on Upload Successful|email_notify_uploads||1|0",
		"checkbox|Email on Item Approval|email_notify_approvals||1|0",
		"text|Updated By|updated_by|Y|60|80",
        "text|Active|active|Y|1|1",
		"submit|Add Item|$this->Add_Submit_Name",
    );


	#TABLE CREATION QUERY
	#===================================================================================================
    $this->Table_Creation_Query = "
        CREATE TABLE IF NOT EXISTS `deliverables_users` (
        `id` int(11) NOT NULL auto_increment,
		`contact_id` int(11) NOT NULL,
        `type` varchar(255) NOT NULL,
        `events` text NOT NULL,
        `email_notify_reports` int(1) NOT NULL,
		`email_notify_changes` int(1) NOT NULL,
		`email_notify_uploads` int(1) NOT NULL,
        `email_notify_approvals` int(1) NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
		`updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
		`created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
        ";	
    }


    #====================================================
    #CUSTOM FUNCTIONS
    #====================================================
	public function CreateCheckboxList()
	{
		#GO TO DATABASE AND GET EVENT LIST
		$EVENT_LSIT = 'ISS|TST|ICC|';
		$EVENT_LIST = explode('|', $EVENT_LSIT);
		
		$FormDataArrayTemp = array ();
		for ($a=1; $a < count($EVENT_LIST)+1; $a++)
		{
			$DISPLAY = "{$EVENT_LIST[$a]['title']} ({$EVENT_LIST[$a]['type']}) (-) ({$EVENT_LIST[$a]['due_date']})";
			array_push($FormDataArrayTemp, "checkboxGroup||varname_$a|c_hecked|1|0|$DISPLAY| onchange=\"updateCombinedField('varname_$a', 'item_id', '{$EVENT_LIST[$a]['id']}')\"");
		}
		$DISPLAY = '';

	}

}
?>
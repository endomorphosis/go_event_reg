<?php

// FILE: class.Companies.php

class Companies extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage companies',
        'Created'     => '2008-11-15',
        'Updated'     => '2008-11-15'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Table  = 'companies';
    $this->Flash_Field = 'company_name';

    $this->Field_Titles = array(
        'id' => 'Id',
        'company_name' => 'Company Name',
        'address1' => 'Address 1',
        'address2' => 'Address 2',
        'city' => 'City',
        'state' => 'State',
        'postal_code' => 'Postal Code',
        'country' => 'Country',
        'phone_number' => 'Phone Number',
        'fax_number' => 'Fax Number',
        'website' => 'Website',
        'business_id' => 'Business Id',
        'created_by' => 'Created By',
        'updated_by' => 'Updated By',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

    $this->Form_Data_Array_Add = array(
	    "form|$this->Action_Link|post|name",
        "text|Company Name|company_name|Y|60|80",
        "text|Address 1|address1|Y|60|80",
        "text|Address 2|address2|N|60|80",
        "text|City|city|Y|40|40",
        "intstate|State|state|Y|options|country",
        "text|Postal Code|postal_code|Y|20|20|$this->Mask_General",
        "country|Country|country|Y|",
        "phone|Phone Number|phone_number|Y|20|20",
        "phone|Fax Number|fax_number|N|20|20",
        "text|Website|website|N|60|80",
        "text|Business Id|business_id|Y|40|40|$this->Mask_Integer",
        "text|Created By|created_by|Y|60|80",
		"submit|Add Company|$this->Add_Submit_Name",
    );

    $this->Form_Data_Array_Edit = array(
	    "form|$this->Action_Link|post|name",
        "text|Company Name|company_name|Y|60|80",
        "text|Address 1|address1|Y|60|80",
        "text|Address 2|address2|N|60|80",
        "text|City|city|Y|40|40",
        "intstate|State|state|Y|options|country",
        "text|Postal Code|postal_code|Y|20|20|$this->Mask_General",
        "country|Country|country|Y|",
        "phone|Phone Number|phone_number|Y|20|20",
        "phone|Fax Number|fax_number|N|20|20",
        "text|Website|website|N|60|80",
        "text|Business Id|business_id|Y|40|40|$this->Mask_Integer",
        "text|Updated By|updated_by|Y|60|80",
        "text|Active|active|Y|1|1",
        "submit|Update Company|$this->Edit_Submit_Name"
    );

    $this->Default_Fields = 'company_name,city,state,country,business_id';

    $this->Unique_Fields = 'business_id';

    $this->Table_Creation_Query = "CREATE TABLE IF NOT EXISTS `companies` (
		`id` int(11) NOT NULL auto_increment,
		`name` varchar(64) NOT NULL,
		`address_1` varchar(64) NOT NULL,
		`address_2` varchar(64) NOT NULL,
		`city` varchar(32) NOT NULL,
		`state` varchar(32) NOT NULL,
		`zipcode` int(16) NOT NULL,
		`url` varchar(256) NOT NULL,
		`access_code` varchar(32) NOT NULL,
		`username` varchar(32) NOT NULL,
		`password` varchar(512) NOT NULL,
		`type` varchar(32) NOT NULL,
		`fse` varchar(64) NOT NULL,
		`district_manager` int(32) NOT NULL,
		`created_by` varchar(32) NOT NULL,
		`created_method` varchar(32) NOT NULL,
		`active` tinyint(1) NOT NULL default '1',
		`updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
		`created` datetime NOT NULL,
		PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
		";
    }
}
<?PHP

// FILE: class.DeliverablesGroups.php

class DeliverablesGroups extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => 'Richard Witherspoon',
        'Description' => 'Create and manage vendor deliverables',
        'Created'     => '2008-11-16',
        'Updated'     => '2008-11-16'
    );

    $this->Add_Submit_Name  = 'COMPANIES_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'COMPANIES_SUBMIT_EDIT';
    $this->Flash_Field = 'TID';


	#SET TO FIELDS THAT MUST BE UNIQUE (other than id) WHICH WILL STOP A RECORD FROM BEING ADDED IF ITS NOT UNIQUE
    $this->Unique_Fields = '';

	
	#INITIAL SEARCH PAGE - TABLE VARIABLES
	#===================================================================================================
	#GROUPS#
    $this->Default_Fields = 'id,group_name'; #COLUMNS SELECTED IN SEARCH RESULTS BY DEFAULT
    $this->Table  = 'deliverables_groups';
    $this->Field_Titles = array(
        'id' => 'Id',
        'group_name' => 'Group Name',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );


	#FORM ARRAY FOR ADDING A RECORD
	#===================================================================================================
    $FormDataArray1 = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD GROUP",
		"code|<hr style='border:0px; height:1px;'>",
        "text|Group Name|group_name|Y|60|80",
        "textbox|Description|description|Y|60|5",
	);
	$FormDataArray2 = $this->CreateCheckboxListItems();
	$FormDataArray3 = array(	
		"text|Item ID's|item_id|Y|60|80",
        "select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"text|Updated By|updated_by|Y|60|80",
		"checkbox|Active|active||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );
	
	 $this->Form_Data_Array_Edit = array_merge($FormDataArray1, $FormDataArray2, $FormDataArray3);


	#FORM ARRAY FOR EDITING A RECORD
	#===================================================================================================
    $FormDataArray1 = array(
	    "form|$this->Action_Link|post|name",
		"h1|ADD GROUP",
		"code|<hr style='border:0px; height:1px;'>",
        "text|Group Name|group_name|Y|60|80",
        "textbox|Description|description|Y|60|5",
	);
	$FormDataArray2 = $this->CreateCheckboxListItems();
	$FormDataArray3 = array(	
		"text|Item ID's|item_id|Y|60|80",
        "select|Event|event|Y||iss=ISS|tst=TST|icc=ICC",
		"text|Updated By|updated_by|Y|60|80",
		"checkbox|Active|active||1|0",
		"submit|Add Item|$this->Add_Submit_Name",
    );
echo AssocArrayToStr($this->Form_Data_Array_Edit);
	 $this->Form_Data_Array_Edit = array_merge($FormDataArray1, $FormDataArray2, $FormDataArray3);


	#TABLE CREATION QUERY
	#===================================================================================================
    $this->Table_Creation_Query = "		
        CREATE TABLE IF NOT EXISTS `deliverables_groups` (
        `id` int(11) NOT NULL auto_increment,
        `group_name` varchar(255) NOT NULL,
        `item_id` varchar(255) NOT NULL,
        `event` varchar(255) NOT NULL,
        `active` tinyint(1) NOT NULL default '1',
        `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
        `created` datetime NOT NULL,
        PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
        ";	
    }
	

	
	#====================================================
	#====================================================
    #CUSTOM FUNCTIONS
    #====================================================
	#====================================================
	public function CreateCheckboxListItems()
	{
		#GO TO DATABASE AND GET ITEM LIST (SHOULD BE BASED ON THE EVENT THE GROUP IS ASSIGNED TO
		$EVENT_ID = '';
	
		$db_table		='deliverables_items';
		$referencekey	='id';
		$keys			='*';
		$conditions		='active=1';
		$order			='ID ASC';
		$ITEM_LIST 		= db_GetArrayAssoc($db_table, $referencekey, $keys='*', $conditions='', $order='');

		$FormDataArrayTemp = array ();
		for ($a=1; $a < count($ITEM_LIST)+1; $a++)
		{
			$DISPLAY = "{$ITEM_LIST[$a]['title']} ({$ITEM_LIST[$a]['type']}) (-) ({$ITEM_LIST[$a]['due_date']})";
			array_push($FormDataArrayTemp, "checkboxGroup||varname_$a|c_hecked|1|0|$DISPLAY| onchange=\"updateCombinedField('varname_$a', 'item_id', '{$ITEM_LIST[$a]['id']}')\"");
			//$OUTPUT = "checkboxGroup||varname_$a|c_hecked|1|0|$DISPLAY| onchange=\"updateCombinedField('varname_$a', 'item_id', '{$ITEM_LIST[$a]['id']}')\",";
		}
		
		return $FormDataArrayTemp;

	}
}
?>
<?php

// FILE: class.Contacts.php

class Contacts extends BaseClass
{
    public function  __construct()
    {

    parent::__construct();


    $this->ClassInfo = array(
        'Created By'  => '',
        'Description' => 'Create and manage contacts',
        'Created'     => '2008-11-18',
        'Updated'     => '2008-11-18'
    );

    $this->Add_Submit_Name  = 'CONTACTS_SUBMIT_EDIT';
    $this->Edit_Submit_Name = 'CONTACTS_SUBMIT_EDIT';
    $this->Table  = 'contacts';
    $this->Flash_Field = '';

    $this->Field_Titles = array(
        'id' => 'Id',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'job_title' => 'Job Title',
        'address1' => 'Address 1',
        'address2' => 'Address 2',
        'city' => 'City',
        'state' => 'State',
        'country' => 'Country',
        'postal_code' => 'Postal Code',
        'company_id' => 'Company Id',
        'phone_number' => 'Phone Number',
        'fax_number' => 'Fax Number',
        'cell_number' => 'Cell Number',
        'password' => 'Password',
        'login_id' => 'Login Id',
        'email_address' => 'Email Address',
        'email_format' => 'Email Format',
        'created_by' => 'Created By',
        'updated_by' => 'Updated By',
        'active' => 'Active',
        'updated' => 'Updated',
        'created' => 'Created'
    );

    $this->Form_Data_Array_Add = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|First Name|first_name|Y|60|80",
        "text|Last Name|last_name|Y|60|80",
        "text|Job Title|job_title|Y|60|80",
        "text|Address 1|address1|Y|40|40",
        "text|Address 2|address2|Y|40|40",
        "text|City|city|Y|20|20",
        "intstate|State|state|Y||country",
        "country|Country|country|Y",
        "text|Postal Code|postal_code|Y|20|20",
        "text|Company Id|company_id|Y|11|11",
        "phone|Phone Number|phone_number|Y",
        "phone|Fax Number|fax_number|Y",
        "phone|Cell Number|cell_number|Y",
        "password|Password|password|Y|60|80",
        "text|Login Id|login_id|Y|40|40",
        "email|Email Address|email_address|Y|60|80",
        "text|Email Format|email_format|Y|4|4",
        "text|Created By|created_by|Y|60|80",
        "text|Updated By|updated_by|Y|60|80",
        "submit|Add Record|$this->Add_Submit_Name",
        "endform"
    );

    $this->Form_Data_Array_Edit = array(
        "form|$this->Action_Link|post|db_edit_form",
        "text|First Name|first_name|Y|60|80",
        "text|Last Name|last_name|Y|60|80",
        "text|Job Title|job_title|Y|60|80",
        "text|Address 1|address1|Y|40|40",
        "text|Address 2|address2|Y|40|40",
        "text|City|city|Y|20|20",
        "intstate|State|state|Y||country",
        "country|Country|country|Y",
        "text|Postal Code|postal_code|Y|20|20",
        "text|Company Id|company_id|Y|11|11",
        "phone|Phone Number|phone_number|Y",
        "phone|Fax Number|fax_number|Y",
        "phone|Cell Number|cell_number|Y",
        "password|Password|password|Y|60|80",
        "text|Login Id|login_id|Y|40|40",
        "email|Email Address|email_address|Y|60|80",
        "text|Email Format|email_format|Y|4|4",
        "text|Updated By|updated_by|Y|60|80",
        "text|Active|active|Y|1|1",
        "submit|Update Record|$this->Edit_Submit_Name",
        "endform"
    );

    $this->Default_Fields = 'first_name,last_name,job_title,company_id';

    $this->Unique_Fields = 'login_id';

    $this->Table_Creation_Query = "";
    }
}
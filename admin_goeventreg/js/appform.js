var appformZindex = 1000;
var appformIdCounter = 0;
var appformProcessing = false;
var taskbarId = 'appform_taskbar';

function appformCreateWithContent(title, content, containerId, width, height)
{
    if (appformProcessing) return;
    appformProcessing = true;
    appformIdCounter++;
    appformZindex++;
    var id = 'appform' + appformIdCounter;
    var controls = '<div class="appform_controls">' +
     '<a href="#" title="Minimize" onclick="appformMinimize(\'' + id + '\',\''+ title + '\'); return false;">_</a>' +
     '<a href="#" title="Close" onclick="appformClose(\'' + id + '\'); return false;">X</a></div>';
    var titlebar = '<div id="titlebar_' + id + '" class="appform_titlebar"><h1>' + title + '</h1></div>';
    var content = '<div class="appform" id="' + id + '">' + controls + titlebar + '<div class="appformcontent">' + content +'</div></div>';
    $('#'+ containerId).prepend(content);
    //$('#' + id).draggable({handle : '#titlebar_'+id}).resizable({handles : 'all'});
    //$('#' + id).resizable({handles : 'all'});
    $('#' + id).ppdrag();
    $('#' + id).css('z-index',appformZindex);
    $('#' + id).width(width);
    $('#' + id).height(height);
    $('#' + id).mousedown(function () {appformActivate(this.id);});
    appformAddToTaskbar(id, title, true);
    appformProcessing = false;
}



function appformCreate(title, file, containerId) {
    if (appformProcessing) return;
    appformProcessing = true;
    appformIdCounter++;
    appformZindex++;
    var id = 'appform' + appformIdCounter;
    var iframeId = 'appformIframe' + appformIdCounter;
    
    var dialogFile =  file + ';IFRAMEID=' +iframeId + ';DIALOGID=' + id;
    
    var controls = '<div class="appform_controls">' +
     '<a href="#" title="Minimize" onclick="appformMinimize(\'' + id + '\',\''+ title + '\'); return false;">_</a>' +
     '<a href="#" title="Maximize" onclick="appformMaximize(\'' + id + '\'); return false;">^</a>' +
     '<a href="#" title="Close" onclick="appformClose(\'' + id + '\'); return false;">X</a></div>';
    var titlebar = '<div id="titlebar_' + id + '" class="appform_titlebar"><h1>' + title + '</h1></div>';
    var content = '<div class="appform" id="' + id + '">' + controls + titlebar + '<iframe id="'+ iframeId +'" class="appiframe" src="' + dialogFile +'"></iframe></div>';
    //var content = '<div class="appform" id="' + id + '"><iframe id="'+ iframeId +'" class="appiframe" src="' + dialogFile +'"></iframe></div>';
    $('#'+ containerId).prepend(content);
    //$('#' + id).draggable({handle : '#titlebar_'+id}).resizable({handles : 'all'});
    //$('#' + id).resizable({handles : 'all'});
    $('#' + id).ppdrag();
    $('#' + id).css('z-index',appformZindex);
    $('#' + id).mousedown(function () {appformActivate(this.id);});
    appformAddToTaskbar(id, title, true);
    appformProcessing = false;
    window.scrollTo(0,0);
}

function appformActivate(id)
{
    if ( !$('#' + id) ) {
       alert ('element ' + id + ' not found');
       return;
    }
    if ($('#' + id).css('z-index') < appformZindex) {
        appformZindex++;
        $('#' + id).css('z-index',appformZindex);
    }
}

function appformClose(id)
{
    //var elem = document.getElementById(id);
    //var taskbarElem = document.getElementById('taskbar_'+ id);
    //elem.style.display = 'none';
    //$('#'+id).ppdrag('destroy');
    //elem.parentNode.removeChild(elem);
    //elem.innerHTML = '';  // appears to be fastest method to remove element
    //taskbarElem.parentNode.removeChild(taskbarElem);
    
    $('#'+id).fadeOut('normal', function () {
        $(this).ppdrag('destroy');
        setTimeout("$('#" + id + "').empty()",100);  // delay prevents hanging in Firefox
        //this.parentNode.removeChild(this);
        //this.innerHTML = '';  // appears to be fastest method to remove element
        $('#taskbar_'+ id).remove();
    });
}

function appformAddToTaskbar(id, title, visible)
{
    if (visible) {
        var content = '<a class="taskbar_visible" id="taskbar_'+ id +'" href="#" title="'+ title +'" onclick="appformActivate(\''+id+'\'); return false;">'+ title +'</a>';
        $('#taskbar_' + id).remove();
        $('#' + taskbarId).append(content).fadeIn();
    } else {
        var content = '<a class="taskbar_minimized" id="taskbar_'+ id +'" href="#" title="Restore '+ title +'" onclick="appformRestore(\'' + id + '\',\'' + title + '\'); return false;">'+ title +'</a>';
        $('#taskbar_' + id).remove();
        $('#' + taskbarId).append(content).fadeIn();
    }
}


function appformMinimize(id, title)
{
    $('#'+id).slideUp('normal', function () {
        appformAddToTaskbar(id, title, false);
      });
}

function appformMaximize(id)
{

    var screenWidth  = $(window).width()-5;
    var screenHeight = $(window).height();
    var appformIframeId = 'appformIframe'+ id.substr(7,99);
    $('#'+id).css({left:'-120px', top:'0px'}).width(screenWidth).height(screenHeight);
    $('#' + appformIframeId).css('width', screenWidth);
    $('#' + appformIframeId).css('height', screenHeight-40);
}

function appformRestore(id, title)  {
    appformActivate(id);
    $('#'+id).slideDown('normal', function () {
        appformAddToTaskbar(id, title, true);
      });
}


function previewPopUp(sourceField)
{
    var content = document.getElementById(sourceField).value;
    if (content == '') {
        alert('No Content to View');
        return;
    }
    if (sourceField == 'FORM_email_text') {
        content = content.replace(/\n/g,'<br />');
        content = '<div class="textfield">' + content + '</div>';
    }
    var title   = sourceField.replace('_',' ');
    var containerId = 'apps';
    var width  = '';
    var height = '';
    top.parent.appformCreateWithContent(title, content, containerId, width, height);
    //var popWindow 	= window.open('','Preview','toolbar=0,scrollbars=1,location=0,width=900,height=600,resizable=1')
    //popWindow.document.write(content);
}


/* =========================  TABLE SEARCH FUNCTIONS ============================= */
function tableDeleteClick(value)
{
    var nValue = value.replace(/TABLE_ROW_ID_/,'');
    var table = $('#TABLE_NAME').val();

    $('#TABLE_ROW_ID_' + value +' td').css('background-color','#ff7');

    if (confirm('Are you sure you want to delete row (' + nValue + ')?')) {
        tableEditClick(value,'delete');
        $.get('helper/table_ajax_helper.php?delete=' + nValue + '&table=' + table, '', function(data){
            if (data == 'ok') {
                $('#' + value).fadeOut();
            } else {
                alert('Error: Could not delete record!');
            }
        });
    }
    $('#TABLE_ROW_ID_' + value +' td').css('background-color','');
}

function tableEditClick(value,action)
{
    var nValue = value.replace(/TABLE_ROW_ID_/,'');
    var table = $('#TABLE_NAME').val();

    if (action == 'view') {
        top.parent.appformCreate('View Record '+table, 'view_record___table=' + table +'&id='+nValue, 'apps');
        //$('#DISPLAY_VIEW_TAB').load('helper/table_ajax_helper.php?view=' + nValue + '&table=' + table);
    }

    if (action == 'edit') {
        top.parent.appformCreate('Edit Record '+table, 'edit_record___table=' + table +'&id='+nValue, 'apps');
        //$('#DISPLAY_VIEW_TAB').load('helper/table_ajax_helper.php?edit=' + nValue + '&table=' + table);
    }
}

function tableSearchDisplayToggle()
{
    $('#TABLE_SEARCH_TAB').slideToggle();
}

function tableSearch(action)
{
    $.post('helper/table_ajax_helper.php?table_search=1&action=' + action,
          {data : $('#TABLE_SEARCH_SELECTION, #TABLE_STARTROW, #TABLE_ROWS, #NUMBER_ROWS').serialize()},
        function(data) {

            $('#TABLE_DISPLAY tbody').empty().append(data);
            ResizeIframe();

        });
}

/* =========================  end table search functions ============================= */







$(function () {

});



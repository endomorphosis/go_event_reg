<h1>Contact Us</h1>

<?php

/* -------------------------------------
REQUIRED VARIABLES
$SITECONFIG['emaillist'] : determines the recipients
$SITECONFIG['emailtopics'] : is an array of drop-down topics.
$SITECONFIG['companyaddress'] : the company address to output
$SITECONFIG['emailsubjectprefix'] = Subject prefix in email sent
$SITECONFIG['emailplaintext']    = 0 for HTML, 1 for Plaintext;
$SITECONFIG['emailsendswift']   = 0;
$SITECONFIG['contactaddress']   = 0 for no address, 1 for address box;

OPTION VARIABLES
$QUERYVAR['mail'] is 'get' variable for recipient drop-down list :  /contact:mail=My_Name  (underscores for spaces)
$QUERYVAR['subject'] is 'get' variable for the subject  :  /contact:subject=mysubject
$QUERYVAR['message'] is 'get' variable for a default message  :  /contact:message=mymessage
$SetTimeShift: is used to change posted time from server time.
$ContactUsText: is the introductory text before the form

TABLE STYLES: 
$SetPostedCell: is the style used for the posted table cell at the bottom of the message.  Changes form_helper default.
$TableOption: is the options for the HTML table head.
$ThOption: is the <th> heading options.
$TdOptions: is the <td> cell options.
---------------------------------------- */


include "$LIB/contact.php";
?>

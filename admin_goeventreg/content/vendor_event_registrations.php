<h1>Event Registrations</h1>

<?php
include 'classes/class.VendorEventRegistrations.php';

$Obj = new VendorEventRegistrations;

SetGet('ADD');

printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1`>Add Record</a>");

if ($ADD == 1) {
    $Obj->AddRecord();
}

if (!$ADD) {
    $Obj->ListTable();
}
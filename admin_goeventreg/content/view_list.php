<?PHP
if (Post('Submit'))
{
	$_SESSION['table'] = Post('eventName');
	$_SESSION['week']  = Post('eventWeek');
	$week = Post('eventWeek');
	switch ($week)
	{
		case "":
			$_SESSION['weekWhere'] = " AND week!=999";
			break;
		case "ALL":
			$_SESSION['weekWhere'] = " AND week!=999";
			break;
		default:
			$_SESSION['weekWhere'] = " AND week=$week";
			break;
	}
	header('Location: ' . $_SERVER['PHP_SELF']);
}


$_SESSION['EDIT_TABLE_INFO'] = array (
    'TITLE' => Session('table') . ' (Week ' . Session('week') . ')',
    'TABLE' => Session('table'),
    'WHERE' => 'active=1',
    'ORDER' => 'name',
    'ID'    => 'id',
    'COLS'  => 'id|name|cell|home|email|other',
//    'EDIT_ARRAY' => array (
//        "hidden|id|",
//        "text|Name|name|Y|30|40||$Mask_Name",
//        "phone|Cell #|cell|N",
//        "phone|Home #|home|N",
//        "email|Email|email|N|30|40|",
//        "textarea|Other|other|N|40|4||$Mask_General"
//    )
);

?>


<div class="tableSectionHeader">
COMPANY CONTACTS
</div>

<br />

<div id="contactForm">
      <input type='button' name='contact' value='Add New Contact' class='demo'/>
</div>


<br /><br />
<form name="frmSettings" action="<?PHP $REQUEST_URI; ?>" method="post">
      <p>EVENT NAME
        <select name="eventName" id="eventName">
          <option value=""></option>
          <option value="intel_server_affidavit_2008" <?PHP if (Session('table') == "intel_server_affidavit_2008") echo ' selected="selected"'; ?>>Intel Your Server Innovation 2008</option>
          <option value="intel_desktop_affidavit_2008"<?PHP if (Session('table') == "intel_desktop_affidavit_2008") echo ' selected="selected"'; ?>>Intel Your Desktop Innovation 2008 - Entry Prize</option>
          <option value="intel_desktop_grand_affidavit_2008"<?PHP if (Session('table') == "intel_desktop_grand_affidavit_2008") echo ' selected="selected"'; ?>>Intel Your Desktop Innovation 2008 - Grand Prize</option>
        </select>
      </p>
        <p>WEEK
          <select name="eventWeek" id="eventWeek">
            <option value=""></option>
            <option value="ALL" <?PHP if (Session('week') == "ALL") echo ' selected="selected"'; ?>>ALL</option>
            <option value="1" <?PHP if (Session('week') == 1) echo ' selected="selected"'; ?>>1</option>
            <option value="2" <?PHP if (Session('week') == 2) echo ' selected="selected"'; ?>>2</option>
            <option value="3" <?PHP if (Session('week') == 3) echo ' selected="selected"'; ?>>3</option>
            <option value="4" <?PHP if (Session('week') == 4) echo ' selected="selected"'; ?>>4</option>
            <option value="5" <?PHP if (Session('week') == 5) echo ' selected="selected"'; ?>>5</option>
            <option value="6" <?PHP if (Session('week') == 6) echo ' selected="selected"'; ?>>6</option>
            <option value="7" <?PHP if (Session('week') == 7) echo ' selected="selected"'; ?>>7</option>
            <option value="8" <?PHP if (Session('week') == 8) echo ' selected="selected"'; ?>>8</option>
            <option value="9" <?PHP if (Session('week') == 9) echo ' selected="selected"'; ?>>9</option>
            <option value="10" <?PHP if (Session('week') == 10) echo ' selected="selected"'; ?>>10</option>
            <option value="11" <?PHP if (Session('week') == 11) echo ' selected="selected"'; ?>>11</option>
          </select>
        </p>
        <p>
          <input type="submit" name="Submit" value="Go" />
        </p>
</form>
<br /><br />

<?PHP
	$TABLE = Session('table');
    $colCount     = 0;
    $cleanedArray = array();
    
	if (!empty($TABLE)) {
        $fieldArray 	= db_TableFieldTitleNames($TABLE);
    	$removeArray 	= array('Signature', 'Address', 'City', 'State', 'Zip', 'Math Result', 'Math Name', 'Math Date', 'Math Signature', 'Email Sent', 'Email Error', 'Datetime', 'GUID', 'Ship Status', 'Active', 'Ship Type', 'Type');
    	$cleanedArray 	= array_diff($fieldArray, $removeArray);
    	$colCount = count($cleanedArray);
    }
?>
<table id="tbl_list" border="0" cellspacing="0" cellpadding="0">
<thead>
<tr>
    <th colspan="<?PHP echo $colCount+1; ?>" id="tabletitle"><?PHP echo $_SESSION['EDIT_TABLE_INFO']['TITLE']; ?></th>
    <th colspan="2" id="table_addcontact"><a href="#">Add Contact</a></th>
</tr>
  <tr>
   <th style="display:none">&nbsp;</th>
<?PHP
$ccount = 4;
	foreach ($cleanedArray as $fieldName)
	{
		echo "<th>$fieldName</th>";
        $ccount++;
	}
	echo "<th>&nbsp;</th>";
	echo "<th>CODE</th>";
	echo "<th>EXPIRE</th>";
?>
	</tr>
 </thead>
 <tbody>

<?PHP
$STABLE = Session('table');
$missingCount = 0;
$totalCount   = 0;

if ( empty($STABLE) ) {
    echo "<tr><td colspan=\"$ccount\" style=\"text-align:center;\">No Table Identified!</td></tr>";
} else {
	//GET ALL SHORT URLS
	$TABLE			= 'short_url';
	$FIELDS 		= 'user_id, code, end_date';
	$WHERE			= "active=1 AND database_table='".Session('table')."'";
	$ORDER			= 'user_id';
	$result 		= selectAllTableSomeFields($TABLE, $FIELDS, $WHERE, $ORDER);
	while($row = mysql_fetch_assoc($result))
	{
		$i = $row['user_id'];
		$URL_ARRAY[$i]['code'] = $row['code'];
		$URL_ARRAY[$i]['end_date'] = $row['end_date'];
	}

	$TABLE 			= Session('table');
	$cleanedArray	= str_replace(' ','_',$cleanedArray);
	$FIELDS 		= '`' . implode("`, `",$cleanedArray) . '`';
	$WHERE 			= "active!='x1' AND datetime!='x1'" . $_SESSION['weekWhere'];
	$ORDER			= 'name ASC';
	$result 		= selectAllTableSomeFields($TABLE, $FIELDS, $WHERE, $ORDER);
	$number 		= 1;
	$missingCount 	= 0;
	$totalCount 	= 0;
	while($row = mysql_fetch_assoc($result))
	{
		$totalCount++;
		$color = ($number%2) ? '#FFFFFF' : '#CCCCCC';
		$number++;
		echo "<tr bgcolor='$color'>";
		$count = 1;
		foreach ($row as $fieldValue)
		{
			$altcolor = '';
			if ($count > 4 && $count < 11 && $count != 6) {
			$altcolor = ($fieldValue == NULL) ? " class='yellow'" : '';
			if ($count == 5 && $altcolor!=NULL) $missingCount++;
			}

			if ($count==1) $id = $fieldValue;
			echo "<td $altcolor>$fieldValue</td>";
			$count++;
		}



		$TABLE 			= Session('table');
		if (isset($URL_ARRAY[$id]['code'])) {
			echo "<td>
			<img src='images/b_delete.gif' title='Delete Short URL' alt='Delete Short URL' border='0' onclick='deactivateCode($id, \"$TABLE\")' style='cursor: pointer;' />
			<img src='images/b_copy.png' title='Copy Record' alt='Copy Record' border='0' onclick='copyRecord($id, \"$TABLE\")' style='cursor: pointer;' />
			</td>";

			echo '<td>'.$URL_ARRAY[$id]['code'].'</td>';
			echo '<td>'. date('m-d-y', $URL_ARRAY[$id]['end_date']) .'</td>';
		} else {
			echo "<td>
			<img src='images/b_add.gif' title='Add Short URL' alt='Add Short URL' border='0' onclick='createCode($id, \"$TABLE\"); return false;' style='cursor: pointer;' />
			<img src='images/b_edit.gif' title='Create copy mark' alt='Create copy mark' border='0' onclick='createCopyMark($id, \"$TABLE\")' style='cursor: pointer;' />
			<img src='images/b_mail.gif' title='Left Message' alt='Left Message' border='0' onclick='createMessageMark($id, \"$TABLE\")' style='cursor: pointer;' />
			<img src='images/b_copy.png' title='Copy Record' alt='Copy Record' border='0' onclick='copyRecord($id, \"$TABLE\")' style='cursor: pointer;' />
			</td>";
		}
		echo '</tr>';
	}
}    
?>
 </tbody>

</table>


<br /><br /><font color="#FF0000">
TOTAL MISSING RECORDS = <?PHP echo $missingCount; ?>
</font>
<br /><font color="#FF0000">
TOTAL RECORDS = <?PHP echo $totalCount; ?>
</font>

<h1>Edit Discount</h1>
<?php

if ($_GET['action'] == 'edit'){
echo Edit_Discount();

}

else {
echo Discount_DisplaywellList();

}

function Discount_DisplaywellList()
{
	$OUTPUT = '<br /><br />';
	global $FieldValues;
	$FieldValues = array(
		'verification' => array('1'=>'Yes', '0'=>'No')
	);

	$db_table 		= 'discounts';
	$referencekey 	= '*';
	$keys			= 'id, event, discount_code, description, discount_amount, discount_type, expiration_date, active';
	$conditions		= 'id!=0';
	$order			= 'id ASC';
	$result 		= db_GetArrayAll($db_table, $keys, $conditions, $order);
	$SearchArray 	= $result;
	$UserInfo		= '';
	$TableSetup		= 'class="deliverables_output_table" cellspacing="0" cellpadding="0"';
	$EditTitle		= 'ACTIONS';
	$EditLinks		= '<a href="#" onclick="parent.appformCreate(\'ITEM EDIT\', \'edit_discount.php\',\'apps\', \'action=edit&id=@@ID@@\'); return false;">EDIT</a>';
	$id				= 'id';
	$bypass			= 'id|';
		
	$OUTPUT .= db_output_table($SearchArray,$UserInfo,$TableSetup,$EditTitle,$EditLinks,$id,$bypass);

	return $OUTPUT;
}


function Edit_Discount()
{
$error = '';
$TID   = GetPostItem('templateId');

$FormDataArray = array (
  "form|$THIS_PAGE|post|name",
  "hidden|templateId|$TID",
  "select|Event|event|required||IST|TST|ICC",
  "text|Code|discount_code|Y|12|12|||",
  "textarea|Description|description|Y|30|6||",
  "text|Discount|discount_amount|Y|12|12||$Mask_Integer",
  "radio|Type of Discount|discount_type|required||dollars|percent",
  "dateYMD|Expiration|expiration_date|Y-M-D|Y|NOW|12",
  "checkbox|Active|active||1|0",
  "submit|Submit My Form|SUBMIT",
  "endform"
);


if (Post('Submit_Template')) {
    // ------------preload values-----------
	$TABLE = 'discounts';
	$WHERE    = 'active=1 AND id='.$_GET['id'];
    $Record   = db_GetRecord($TABLE,'*',$WHERE);
    foreach ($Record as $key => $value) $_POST[$FormPrefix.$key] = $value;
}


if (havesubmit('submit')) {
    // ------------save values-----------
    $FormArray = ProcessForm($FormDataArray,$table,'','','',$error); 
    
    if(!$error){
        unset($FormArray['templateId']);
        $key_values = db_KeyValues($FormArray);
        if(db_UpdateRecord($TABLE,$key_values,"id=$TID")){
            AddFlash("Template [{$FormArray[$flash_field ]}] Updated");
        } else {$error .= 'DB Write Error';}
    }
}
/*$dropdown = '';
$TABLE = 'discounts';
$WHERE     = "active=1";
$WHERE_EVENT = 'active=1 AND event="'.$select.'"';
$discount_template = db_GetAssocArray($TABLE,'id','discount_code',$WHERE_EVENT);
$event_template = db_GetAssocArray($TABLE,'id','event',$WHERE);

$dropdown  = HTML_AssocArrayToDropDown($event_template, $FormPrefix.'templateId', 'bluh', $TID);
$dropdown2 = HTML_AssocArrayToDropDown($discount_template, $FormPrefix.'templateId', 'dropdown2', $TID);
print <<<LBL1
<form name="frmTemplateSelect" method="post" action="$THIS_PAGE_QUERY" class="cmxform">
    <div class="formtitle">Event:</div>
    <div class="forminfo">
         <input type="submit" name="Submit_Template" value="LOAD" />
    </div>
</form>
<div class="tableSectionHeader"></div>
LBL1;
*/




WriteError($error);
echo OutputForm($FormDataArray,Post('submit'));
}



?>
<?PHP
$TABLE = 'messages';
$flash_field = 'name';
$error = '';
$MID   = GetPostItem('messageId');

$FormDataArray = array (
    "form|$THIS_PAGE_QUERY|post|cmxform",
    "hidden|messageId|$MID",
    "text|Name|name|Y|60|80",
    "text|Description|description|Y|60|255",
    "text|Creator|creator|Y|60|80",
    'code|<br />',
    "text|From Name|from_name|Y|60|80",
    "email|From Email|from_email|Y|60|80",
    "text|Subject|subject|Y|60|80",
    "html|Email Html|email_html|Y|80|10",
    "button|Preview Html|previewPopUp('{$FormPrefix}email_html');",
    "textarea|Email Text|email_text|Y|80|10",
    "button|Preview Text|previewPopUp('{$FormPrefix}email_text');",
    "dateYMD|Send Date|send_date|Y-M-D|Y|NOW|3",
    "time|Send Time|send_time|HH:MM|Y",
    "text|Table|table|Y|40|80",
    "text|Where|where|Y|60||",
    "checkbox|Active|active||1|0",
    "submit|Submit|submit",
    "endform"
);



if (Post('Submit_Message')) {
    // ------------preload values-----------
	$WHERE    = "active=1 AND id=$MID";
    $Record   = db_GetRecord($TABLE,'*',$WHERE);
    foreach ($Record as $key => $value) {
        if ($key == 'send_date_time') {
            $_POST[$FormPrefix.'send_date'] = strTo($value,' ');
            $_POST[$FormPrefix.'send_time'] = strFrom($value,' ');
        } else {
            $_POST[$FormPrefix.$key] = $value;
        }
    }
}


if (havesubmit('submit')) {
    // ------------save values-----------
    $FormArray = ProcessForm($FormDataArray,$table,'','','',$error);

    if(!$error){
        $FormArray['send_date_time']  = $FormArray['send_date'].' '.$FormArray['send_time'];
        //remove unneeded variables
        unset($FormArray['messageId'],$FormArray['send_date'],$FormArray['send_time']);

        $key_values = db_KeyValues($FormArray);
        if(db_UpdateRecord($TABLE,$key_values,"id=$MID")){
            AddFlash("Template [{$FormArray[$flash_field ]}] Updated");
        } else {$error .= 'DB Write Error';}
    }
}

// ------------ Message Selection --------------

$WHERE     = "active=1 AND name!=''";
$messages = db_GetAssocArray($TABLE,'id','name',$WHERE);

$dropdown  = HTML_AssocArrayToDropDown($messages, $FormPrefix.'messageId', 'dropdown', $MID, true, 'onchange="$(\'#message_values\').hide();"');

print <<<LBL1
<form name="frmMessageSelect" method="post" action="$THIS_PAGE_QUERY" class="cmxform">
    <div class="formtitle">Message:</div>
    <div class="forminfo">
        $dropdown&nbsp;<input type="submit" name="Submit_Message" value="LOAD" />
    </div>
</form>
<div class="tableSectionHeader"></div>
<div id="message_values">
LBL1;

WriteError($error);
echo OutputForm($FormDataArray,Post('submit'));
?>
</div>
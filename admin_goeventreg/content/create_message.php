<?PHP
//========================== AJAX PROCESSING ==========================

$AJAX = isset($_GET['AJAX'])? $_GET['AJAX'] : '';

function GetGroups($TABLE) {    
    $groupsAndFreq = db_GetFreq($TABLE,'group_name', 'group_name', '');
    $groups = array();
    foreach ($groupsAndFreq as $key=>$value) {
        $groups[$key] = "$key ($value)";
    }
    if ($groups) {
        $RESULT = HTML_AssocArrayToDropDown($groups, 'group_name', 'dropdown', Post('group_name'));
    } else {
        $RESULT = 'No Groups Found!';
    }
    return $RESULT;
}

if ($AJAX) {
    include '../helper/ajax_helper.php';  // ---------- load AJAX support routines -------------
    include "$LIB/html_helper.php";

    if ($AJAX == 'groups') {
        echo GetGroups(Get('table'));
    }
    exit;
}
//=========================================================================

$TABLE = 'email_templates';
$flash_field = 'name';
$error      = '';
$TID        = GetPostItem('templateId');
$DB_TABLE   = Post('dbtable');
$GROUP_NAME = Post('group_name');

$FormDataArray = array (
    //"form|$THIS_PAGE_QUERY|post|cmxform",
    "hidden|templateId|$TID",
    "text|Message Name|name|Y|60|80",
    "text|Description|description|Y|60|255",
    "text|Creator|creator|Y|60|80",
    'code|<br />',    
    "text|From Name|from_name|Y|60|80",
    "email|From Email|from_email|Y|60|80",
    "text|Subject|subject|Y|60|80",
    "html|Email Html|email_html|Y|80|10",
    "button|Preview Html|previewPopUp('{$FormPrefix}email_html');",
    "textarea|Email Text|email_text|Y|80|10",
    "button|Preview Text|previewPopUp('{$FormPrefix}email_text');",
    "dateYMD|Send Date|send_date|Y-M-D|Y|NOW|3",
    "time|Send Time|send_time|HH:MM|Y",
    "submit|Submit|submit"
);


if (Post('Submit_Template')) {
    // ------------preload values-----------
	$TABLE    = 'email_templates';
	$WHERE    = "active=1 AND id=$TID";
    $Record   = db_GetRecord($TABLE,'*',$WHERE);
    foreach ($Record as $key => $value) {
        //if ($key == 'email_text') $value = htmlentities($value);
        $_POST[$FormPrefix.$key] = $value;
    }
}


if (havesubmit('submit')) {

    $FormArray = ProcessForm($FormDataArray,$table,'','','',$error); 
    if (!$GROUP_NAME) $error .= 'Group Name is not Defined<br />';
    //create a time variable by combining two fields
    if (!$error) {
        $FormArray['send_date_time']  = $FormArray['send_date'].' '.$FormArray['send_time'];
        $FormArray['table'] = $DB_TABLE;
        //remove unneeded variables
        unset($FormArray['templateId'],$FormArray['send_date'],$FormArray['send_time']);
        
        $FormArray['where'] = "group_name='$GROUP_NAME'"; //<<<<<<<<<<---------- UPDATE THIS TO ADD MORE FLEXIBILITY TO SELECTION ----------<<<<<<<<<<

        $TABLE  = 'messages';
        $fields = db_Keys($FormArray).',created';
        $values = db_Values($FormArray).",NOW()";
        
        if(db_AddRecord($TABLE,$fields,$values)){
               AddMessage("Record: [{$FormArray[$flash_field ]}] Added");
        } else {
            $error .= 'DB Write Error';
        }    
        if (!$error) return;
    }
}


// ------------ Template Selection --------------
$TABLE     = 'email_templates';
$WHERE     = "active=1 AND name!=''";
$templates = db_GetAssocArray($TABLE,'id','name',$WHERE);

$TemplateDropdown  = HTML_AssocArrayToDropDown($templates, $FormPrefix.'templateId', 'dropdown', $TID);


$tables = db_GetTables();
$AssocTables = array();
foreach ($tables as $table) {
    $AssocTables[$table] = NameToTitle($table);
}
$TableDropdown = HTML_AssocArrayToDropDown($AssocTables, 'dbtable', 'dropdown', Post('dbtable'), 1, 'onchange="populateGroups();"');

$GroupDropDown = ($GROUP_NAME)? GetGroups($DB_TABLE) : 'None';

$THIS_PAGE_QUERY = htmlspecialchars($THIS_PAGE_QUERY);

print <<<LBL1

<div style="width:850px">
<form name="frmTemplateSelect" method="post" action="$THIS_PAGE_QUERY" class="cmxform">
    <div class="formtitle">Template:</div>
    <div class="forminfo">
        $TemplateDropdown&nbsp;<input type="submit" name="Submit_Template" value="LOAD" />
    </div>
</form>
<div class="tableSectionHeader"></div>
</div>


<form name="createmessage" method="post" action="$THIS_PAGE_QUERY" class="cmxform">
<table cellspacing="0" cellpadding="0" border="0">
<tbody><tr valign="top">
<td>
<div class="formtitle">Database Table:</div>
    <div class="forminfo2">
        $TableDropdown
    </div>

<div class="formtitle">Group:</div>
<div class="forminfo2"><span id="grouplist">$GroupDropDown</span></div>

</td>
<td>
LBL1;


WriteError($error);
echo OutputForm($FormDataArray,Post('submit'));

?>
</td>
</tr>
</tbody>
</table>
</form>
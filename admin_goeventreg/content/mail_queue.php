<?php

/* -- varlist in mail_queue --
name
email
uid
varlist
template_id
send_date_time
updated
created
*/
$TABLE = 'mail_queue';


$ProcessedCount   = db_Count($TABLE, "sent!=0 OR error !=''");
$FailedCount      = db_Count($TABLE, "error!=''");
$TotalCount       = db_count($TABLE,1);
$SentCount        = number_format($ProcessedCount - $FailedCount);
$UnprocessedCount = number_format($TotalCount - $ProcessedCount);

$ProcessedCount   = number_format($ProcessedCount);
$FailedCount      = number_format($FailedCount);
$TotalCount       = number_format($TotalCount);

print <<<LBL1
<table id="statustable" cellpadding="0" cellspacing="1" border="0">
<tbody>
<tr><th>Sent</th><td>$SentCount</td></tr>
<tr><th>Failed</th><td>$FailedCount</td></tr>
<tr><th>Not Sent</th><td>$UnprocessedCount</td></tr>
<tr><th>Total Processed</th><td>$ProcessedCount</td></tr>
<tr><th>Total</th><td>$TotalCount</td></tr>
<tr><td colspan="2" align="center"><a style="width:90%" class="stdbutton" href="$THIS_PAGE?DIALOG=1;">Refresh</a></td></tr>
</tbody>
</table>
LBL1;


?>
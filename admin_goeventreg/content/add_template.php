<?PHP
$TABLE = 'email_templates';
$flash_field = 'name';
$error = '';

$FormDataArray = array (
    "form|$THIS_PAGE_QUERY|post|cmxform",
    "text|Name|name|Y|60|80",
    "text|Description|description|Y|60|255",
    "text|Creator|creator|Y|60|80",
    'code|<br />',    
    "text|From Name|from_name|Y|60|80",
    "email|From Email|from_email|Y|60|80",
    "text|Subject|subject|Y|60|80",
    "html|Email Html|email_html|Y|80|10",
    "button|Preview Html|previewPopUp('{$FormPrefix}email_html');",
    "textarea|Email Text|email_text|Y|80|10",
    "button|Preview Text|previewPopUp('{$FormPrefix}email_text');",
    "submit|Submit|submit",
    "endform"
);


if (Post('submit')) {

    $FormArray = ProcessForm($FormDataArray,$table,'','','',$error); 
    
    if(!$error){
         $fields = db_Keys($FormArray).',created';
         $values = db_Values($FormArray).",NOW()";
         if(db_AddRecord($TABLE,$fields,$values)){
           AddMessage("Record: [{$FormArray[$flash_field ]}] Added");
         } else {$error .= 'DB Write Error';}
    }
}


if (!Post('submit') or $error) {
    WriteError($error);
    echo OutputForm($FormDataArray,Post('submit'));
}
?>
<?PHP

$TABLE = 'email_templates';
$flash_field = 'name';
$error = '';
$TID   = GetPostItem('templateId');

$FormDataArray = array (
    "form|$THIS_PAGE_QUERY|post|cmxform",
    "hidden|templateId|$TID",
    "text|Name|name|Y|60|80",
    "text|Description|description|Y|60|255",
    "text|Creator|creator|Y|60|80",
    'code|<br />',    
    "text|From Name|from_name|Y|60|80",
    "email|From Email|from_email|Y|60|80",
    "text|Subject|subject|Y|60|80",
    "html|Email Html|email_html|Y|80|10",
    "button|Preview Html|previewPopUp('{$FormPrefix}email_html');",
    "textarea|Email Text|email_text|Y|80|10",
    "button|Preview Text|previewPopUp('{$FormPrefix}email_text');",
    "checkbox|Active|active||1|0",
    "submit|Submit|submit",
    "endform"
);



if (Post('Submit_Template')) {
    // ------------preload values-----------
	$TABLE    = 'email_templates';
	$WHERE    = "active=1 AND id=$TID";
    $Record   = db_GetRecord($TABLE,'*',$WHERE);
    foreach ($Record as $key => $value) $_POST[$FormPrefix.$key] = $value;
}


if (havesubmit('submit')) {
    // ------------save values-----------
    $FormArray = ProcessForm($FormDataArray,$table,'','','',$error); 
    
    if(!$error){
        unset($FormArray['templateId']);
        $key_values = db_KeyValues($FormArray);
        if(db_UpdateRecord($TABLE,$key_values,"id=$TID")){
            AddFlash("Template [{$FormArray[$flash_field ]}] Updated");
        } else {$error .= 'DB Write Error';}
    }
}

// ------------ Template Selection --------------
$TABLE     = 'email_templates';
$WHERE     = "active=1 AND name!=''";
$templates = db_GetAssocArray($TABLE,'id','name',$WHERE);

$dropdown  = HTML_AssocArrayToDropDown($templates, $FormPrefix.'templateId', 'dropdown', $TID);

print <<<LBL1
<form name="frmTemplateSelect" method="post" action="$THIS_PAGE_QUERY" class="cmxform">
    <div class="formtitle">Template:</div>
    <div class="forminfo">
        $dropdown&nbsp;<input type="submit" name="Submit_Template" value="LOAD" />
    </div>
</form>
<div class="tableSectionHeader"></div>
LBL1;

WriteError($error);
echo OutputForm($FormDataArray,Post('submit'));
?>
<?PHP
if (Post('submit')) {
	$event			= $_POST['eventName'];
	$week 			= $_POST['week'];
	$type		 	= $_POST['type'];
	$seed 			= $_POST['seed'];		//GET THE SEED VALUE - WHICH DETERMINES WHICH LOOP OF RNDOM TO ACCEPT
	$min 			= $_POST['min'];		//GET THE SMALLEST NUMBER TO BE RETURNED
	$max 			= $_POST['max'];		//GET LARGEST NUMBER TO BE RETURNED	
	$loops			= $_POST['numberResults'];
	$random_array 	= array();

	$totalLoops		= 0;
	
	$totalCount = $max-($min-1);
	if ($totalCount>$loops) $totalCount=$loops;
	
	for ($y=0; $y<$loops; $y++)
	{	
		for ($i=0; $i<=$seed; $i++)
		{
			$n = rand($min, $max);				//GET RANDOM NUMBER
		}
		
		if (in_array($n, $random_array)) 
		{
			$y--;	//WE'LL NEED TO RUN ANOTHER ITERATION
		} else {
			$random_array[$y] = $n;					//STORE THE RANDOM NUMBER IN THE ARRAY
		}
		$totalLoops++;
	}
	
	sort($random_array); //SORT THE ARRAY
    
                                            //DISPLAY THE OUTPUT VALUES
    echo "<br /><br />";
    echo '<table width="500" cellspacing="2" cellpadding="5" border="0" align="center" style="border: 1px solid #000000; font-family:verdana; font-size:12px;">';
    echo '<tr class="odd"><td>CURRENT DATE &amp; TIME:    </td><td>' . date('c') . "</td></tr>";
    echo '<tr>            <td>CURRENT UNIX TIMESTAMP: </td><td>' . time() . "</td></tr>";
    echo '<tr class="odd"><td>EVENT NAME:             </td><td>' . $event . "</td></tr>";
    echo '<tr>            <td>QTY NUMBERS REQUESTED   </td><td>' . $loops . "</td></tr>";
    echo '<tr class="odd"><td>CURRENT WEEK:           </td><td>' . $week . "</td></tr>";
	echo '<tr>            <td>GROUP TYPE:             </td><td>' . $type . "</td></tr>";
	echo '<tr class="odd"><td>SEED USED:              </td><td>' . $seed . "</td></tr>";
	echo '<tr>            <td>MIN VALUE USED: 		  </td><td>' . $min . "</td></tr>";
	echo '<tr class="odd"><td>MAX VALUE USED: 		  </td><td>' . $max . "</td></tr>";
	echo '<tr>            <td>TOTAL LOOPS:	 		  </td><td>' . $totalLoops . "</td></tr>";		
	echo '<tr class="yellow"><td>RANDOM NUMBERS GERATED: </td><td>';
	
	for ($y=0; $y<$loops; $y++)
	{	
		$g = $y+1;
		$extra = "";
		if ($g >0 && $g <10) $extra = "&nbsp;";
		echo "<br />$extra $g| " . $random_array[$y];
		//echo "<br />" . $random_array[$y];
	}
	
	echo '</td></tr>';
	echo '<tr><td colspan="2"><input type="button" name="back" value="BACK" onclick="javascript:history.go(-1)" /></td></tr>';
	echo '</table>';

    return; //STOP PAGE FROM PROCESSING FURTHER
}
?>

<p>&nbsp;</p><form action="<?PHP echo $_SERVER['PHP_SELF']; ?>" method="post" name="randomForm" id="randomForm" class="cmxform">
  <p><label>EVENT NAME</label>
    <select name="eventName" id="eventName">
      <option value=""></option>
      <option value="Intel Your Server Innovation 2008">Intel Your Server Innovation 2008</option>
      <option value="Intel Your Desktop Innovation 2008">Intel Your Desktop Innovation 2008</option>
    </select>
  </p>
  <p><label>WEEK</label>
    <select name="week" id="week">
      <option value=""></option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
    </select>
  </p>
  <p><label>TYPE </label>
    <select name="type" id="type">
	<option value=""></option>
	<option value="DISTI">DISTI</option>
	<option value="RESELLER">RESELLER</option>
	</select>
  </p>
  <p><label>SEED </label>
    <input name="seed" type="text" id="seed" />
  </p>
  <p><label>MINIMUM VALUE</label>
    <input name="min" type="text" id="min" value="2" />
  </p>
  <p><label>MAXIMUM VALUE </label>
    <input name="max" type="text" id="max" />
  </p>
  <p><label># OF NUMBERS</label>
    <input name="numberResults" type="text" id="numberResults" />
  </p>
  <p>
    <input type="submit" name="submit" value="GENERATE" />
  </p>
</form>

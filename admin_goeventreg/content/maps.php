<?php
include 'classes/class.Maps.php';
?>

<h1>Search Map, Drag Marker to Update, Enter to Use it </h1>
<form style="position: relative; left: 10px;" action="#" onsubmit="showAddress(this.address.value); return false">
  <p>
    Enter The Address: <input type="text" size="74" name="address" value="1600 Amphitheatre Pky, Mountain View, CA" />
    <input type="submit" value="search" /><button>Enter</button>
  </p>
  <div id="map_canvas" style="width: 510px; height: 300px"></div>
</form>


<?php

$Maps = new Maps;

SetGet('ADD');

printqn("<a class=`stdbutton` href=`$THIS_PAGE_QUERY&amp;ADD=1`>Add Map</a>");

if ($ADD == 1) {
    $Maps->AddRecord();
}

if (!$ADD) {
    $Maps->ListTable();
}
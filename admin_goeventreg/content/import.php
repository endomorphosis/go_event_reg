<?php
//========================== AJAX PROCESSING ==========================

$AJAX = isset($_GET['AJAX'])? $_GET['AJAX'] : '';

if ($AJAX) {
    include '../helper/ajax_helper.php';  // ---------- load AJAX support routines -------------

    $data = Post('data');
    $fields = explode("&",$data);
    $vars = array();
    $varlist = '';
    $columns = array();
    $havedata = false;

    foreach ($fields as $field) {
        list($key,$value) = explode('=', $field);

        if ($value) $vars[urldecode($key)] = urldecode($value);

        $column = !empty($value)? strFrom($key,'columnselect') : 0;

        if(strFrom($key,'columnselect') != '') {
            if ($column) {
               $varlist .= "$value,";
               $columns[] = $column;
               $havedata = true;
            } else {
                $varlist .= '@dummy,';
            }
        }

    }
    $DB_TABLE = $vars['DB_TABLE'];
    
    if (!empty($vars['group_name']) and (strpos($varlist, 'group_name') === false)) {
        $SET = " SET group_name='{$vars['group_name']}'";
    } else $SET = '';

    $varlist = substr($varlist,0,-1);
    // ===================== READY TO UPLOAD=====================

    $FILE = str_replace('\\','/',$vars['TempFile']);
    readln($FILE, 1);
    $headings = readln();
    $headingCount = count($headings);
    if (!$DB_TABLE) {echo "<h1>NO TABLE DEFINED!</h1>";
        exit;
    }

    $QUERY = "LOAD DATA INFILE '$FILE' INTO TABLE `$DB_TABLE`
    FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    ($varlist)$SET;";

    if($havedata) {
        $RESULT = mysql_query($QUERY);
        if($RESULT) echo 'FILE UPLOADED!';
        else echo 'UPLOAD FAILED!';
    } else {
        echo 'No Data Defined to Upload!';
    }

    exit; //---------------------- exit AJAX program ------------------
}
//=========================================================================



SetPost('FILENAME DOCUMENTUPLOAD TempFile NewDocFile NewDocSize DB_TABLE');
$NewFileName = dirname(dirname(__FILE__)) .'/tmp/uploadfile' . session_id() . '.csv';

$tables = db_GetTables();
$AssocTables = array();
foreach ($tables as $table) {
    $AssocTables[$table] = NameToTitle($table);
}
$TableDropDown = HTML_AssocArrayToDropDown($AssocTables, 'DB_TABLE', 'dropdown', Post('DB_TABLE'), 1, 
"onchange=\"if(this.value=='') {
        $('#submit').hide();
        } else {
            $('#submit').show();
        }\"");

print <<<LBL1
<form method="post" action="$REQUEST_URI" enctype="multipart/form-data">
<input type="file" name="FILENAME" size="80" value="$FILENAME"
    onchange="if(this.value=='') {
        $('#upload').hide();
        } else {
            $('#upload').show();
            $('#uploadcontent').hide();
            if( $('#DB_TABLE').val() != '' ) $('#submit').show();
        }
        " />
<span id="upload" style="display:none;">
<br />$TableDropDown
&nbsp;<input id="submit" style="display:none;" type="submit" name="DOCUMENTUPLOAD" value="Upload" onclick="this.value='$HTML_SubmitClickText';" />

</span>
</form>
<div id="uploadcontent">
LBL1;

if ($DOCUMENTUPLOAD) {

echo "<h3>Table: $DB_TABLE</h3>";

    //================== UPLOAD DOCUMENT ===================

    if ($DOCUMENTUPLOAD) {
        $NewDocFile = $_FILES['FILENAME']['name'];
        $NewDocSize = number_format($_FILES['FILENAME']['size']);
        $TempFile   = $_FILES['FILENAME']['tmp_name'];
        $move_result = move_uploaded_file($TempFile, $NewFileName);
    }

    print "<p>File Name: <span style=\"color:#036\">$NewDocFile</span><br />\n";
    print "File Size: <span style=\"color:#036\">$NewDocSize</span> bytes</p>\n";
    if ($TempFile) {

        echo HTML_Form('post',$PHP_SELF);
        echo HTML_input_hidden('TempFile',$NewFileName);
        echo HTML_input_hidden('DB_TABLE',$DB_TABLE);
        echo HTML_input_hidden('NewDocFile',$NewDocFile);
        echo HTML_input_hidden('NewDocSize',$NewDocSize);

        $column_names = db_TableFieldTitleNames($DB_TABLE);
        unset($column_names['id'], $column_names['updated'], $column_names['created']);

        $linecount = number_format(FileLineCount($NewFileName)); //get line count

        echo "<p>Line Count: <span style=\"color:#036\">$linecount</span></p>\n";

        echo '<p>Group Name: ' . HTML_input_text('group_name',Post('group_name'),'input') . "</p>\n";

        readln($NewFileName,1);  // SETUP HANDLE & CSV
        echo '<table id="import_table" align="center" cellspacing="1" cellpadding="0" border="0"><tbody>';

        $heading = readln();
        $headingCount = count($heading);

        if ($headingCount) {
            echo HTML_TableRow($heading,'th');
            echo '<tr>';
            for ($i=1; $i<$headingCount+1; $i++) {
                echo '<td>' . HTML_AssocArrayToDropDown(
                                $column_names,
                                "columnselect$i",
                                'column_dropdown',
                                Post("columnselect$i"),
                                1,
                                'onchange="colorColumns();"'
                                ) . "</td>\n";
            }
            echo '</tr>';
        }

        $count = 1;
        while ( ($line = readln()) and ($count<11) ) {
            echo HTML_TableRowId($line, $count, 'table_');
            $count++;
        }
        echo '</tbody></table>';

        echo HTML_Button('ADD','Add To Database','submit',"AddToDB();");
        echo HTML_EndForm();

        echo '<div id="upload_marker"></div>';
        echo '<div id="upload_div"></div>';


    } else {
        print '<span style="background-color:#f00;">Your file could not be uploaded!</span></h2>';
    }
}


echo '</div>';

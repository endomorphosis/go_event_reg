<?PHP
// DB Admin Authentication
// reqires tools.php, form_helper.php (for e-mail check)
			//return;
$Mask_Username    = '^([0-9a-z]+)([0-9a-z\.-_]+)@([0-9a-z\.-_+)\.([0-9a-z]+)';
$Mask_Password    = '^[a-zA-Z0-9:\-\!\@\#\$\%\^\&\*_]+$';

$LOGIN_TITLE            = 'Event Admin System';
$SESSION_LOGIN_BASE     = 'ADMIN_LOGIN_';
$LOGIN_LOGFILE          = 'logs/login.dat';
$SITE_CSS               = 'css/site.css';

$ADMIN_TABLE            = 'admin_users';
// tables fields: id, email, first_name, last_name, super_user

$ADMIN_LOGIN_OK         = ''; //Check this variable to see if user has logged in
$ADMIN_LOGIN            = ''; //THIS IS ARRAY OF ACCESS TO PAGE LEVELS
$ADMIN_NAME             = ''; // the name of the person;
$ADMIN_USER_ID          = ''; // the id of the users
$ADMIN_SUPER_USER       = ''; // when this is set, the user is a super user
$ADMIN_ROLES	        = ''; // this is what roles are owned by a  person.
$ADMIN_ERROR            = ''; // initialize for error checking below

SetPost('USER PASS LOGIN');  // sets the posted variables

$HeadStart = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>'.$LOGIN_TITLE.'</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" type="text/css" href="'.$SITE_CSS .'" />
';



//===========================LOGOUT=======================
if (Get('LOGOUT')) {
    unset(
        $_SESSION[$SESSION_LOGIN_BASE.'LOGIN'],
        $_SESSION[$SESSION_LOGIN_BASE.'LOGIN_OK']
    );

    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time()-42000, '/');
    }

    print <<<LOUT
$HeadStart
</head>
<body class="admin" style="text-align:center;">
    <div id="login">
        <p id="loginheading">
            $LOGIN_TITLE
        </p>
            <h1>Good Bye!</h1>
        <p>
            <input type="button" value="Log In" onclick="window.location='index.php'" />
        </p>
    </div>
</body>
</html>
LOUT;
    exit;
}

//=======================AUTHENTICATION=========================




if ($LOGIN and $USER and $PASS) {
    $USER = strtolower($USER);
    if (!CheckEmail($USER)) $ADMIN_ERROR = 'Username must be a Valid Email Address';
    elseif ((ereg($Mask_Username,$USER)) and (ereg($Mask_Password,$PASS))) {
        $search_user   = db_QuoteValue($USER);
        $ThisUserArray = db_GetRecord($ADMIN_TABLE,'*',"email=$search_user");
        if (empty($ThisUserArray)) {
            $ADMIN_ERROR = 'User not found';
        } elseif (trim($ThisUserArray['password']) === $PASS) {
            session_regenerate_id();
            $_SESSION[$SESSION_LOGIN_BASE.'LOGIN']      = $ThisUserArray;
            $_SESSION[$SESSION_LOGIN_BASE.'LOGIN_OK']   = 'OK';
            $_SESSION[$SESSION_LOGIN_BASE.'NAME']       = trim($ThisUserArray['first_name'].' '.$ThisUserArray['last_name']);
            $_SESSION[$SESSION_LOGIN_BASE.'USER_ID']    = $ThisUserArray['id'];
            $_SESSION[$SESSION_LOGIN_BASE.'SUPER_USER'] = $ThisUserArray['super_user'];

			
            /*   $stringAccess = $ThisUserArray['pageTabAccess'];
			$stringAccess = substr($stringAccess,0,-1); //STRIP OFF A TRAILING PIPE CHARACTER
			$pieces = explode("|", $stringAccess);
			for ($i=0; $i<count($pieces); $i++)
			{
				$subpieces = explode("=", $pieces[$i]);
				$t = $subpieces[0];
				$accessArray[$t][0] = $subpieces[1];
			}
               */
			//$_SESSION[$SESSION_LOGIN_BASE.'ACCESS'] = $accessArray;

			//-------------------- update logfile
            $line=date('Y-m-d H:i:s').'|'.$_SESSION[$SESSION_LOGIN_BASE.'NAME']."\n";
            append_file($LOGIN_LOGFILE,$line);
			return;
        } else {
            $ADMIN_ERROR = 'Password not Correct';
        }
    } else {
        $ADMIN_ERROR = 'Illegal Characters in User Name or Password';
    }
}

// set the variables from session vars

$ADMIN_LOGIN      = Session($SESSION_LOGIN_BASE.'LOGIN'); //THIS IS ARRAY OF ACCESS TO PAGE LEVELS
$ADMIN_LOGIN_OK   = Session($SESSION_LOGIN_BASE.'LOGIN_OK');
$ADMIN_NAME       = Session($SESSION_LOGIN_BASE.'NAME');
$ADMIN_USER_ID    = Session($SESSION_LOGIN_BASE.'USER_ID');
$ADMIN_SUPER_USER = Session($SESSION_LOGIN_BASE.'SUPER_USER');
$ADMIN_ROLES 	  = Session($SESSION_LOGIN_BASE. 'ROLES');


if (empty($ADMIN_LOGIN_OK)) {

    if (!empty($ADMIN_ERROR)) $ADMIN_ERROR = '<div id="ERROR">'.$ADMIN_ERROR.'</div>';

    print <<<AUTH
$HeadStart
    <script type="text/javascript">
/* <![CDATA[ */
        function getId(id) {return document.getElementById(id);}
        var MyPassword = '<input type="@@TYPE@@" id="PASS" name="PASS" size="15" value="@@VALUE@@" />';
        function changePassText() {
            myElem  = getId('span_password');
            myValue = getId('PASS').value;
            myCheck = getId('pvcheck');
            if (myCheck.checked == true) {
              var myInput = MyPassword.replace('@@TYPE@@','text');
            } else {
              var myInput = MyPassword.replace('@@TYPE@@','password');
            }
            myElem.innerHTML= myInput.replace('@@VALUE@@',myValue);
        }
/* ]]> */
    </script>
</head>
<body class="admin" style="text-align:center;">
<form method="post" action="$REQUEST_URI">
<div id="login">
$ADMIN_ERROR
    <p id="loginheading">$LOGIN_TITLE</p>
    <table align="center">
        <tr>
            <th>
                User Name:
            </th>
            <td style="text-align:left;">
                <input type="text" name="USER" value="$USER" size="30" />
            </td>
        </tr>
        <tr>
            <th>Password:</th>
            <td style="text-align:left;">
                <span id="span_password"><input name="PASS" id="PASS" size="15" value="$PASS" type="password" /></span>
                <input id="pvcheck" type="checkbox" onclick="changePassText();"/>&nbsp;<span style="color:#000; font-size:0.7em;">Show&nbsp;Password</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" id="submit" align="center">
                <input type="submit" name="LOGIN" value="Log In" />
            </td>
        </tr>
    </table>
</div>
</form>
</body>
</html>
AUTH;
    exit;
}
?>

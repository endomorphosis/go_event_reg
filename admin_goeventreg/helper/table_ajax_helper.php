<?php
include 'ajax_helper.php';

SetGet('view edit delete table_search action','SQT');

if ($view or $edit or $delete) {
    $TABLE = Get('table');
    
    if (empty($TABLE)) {
        echo 'Cannot find Table';
        exit;
    }
    
    $CLASS_NAME = str_replace(' ','',NameToTitle($TABLE));
    include '../classes/class.' . $CLASS_NAME . '.php';
    $TableObj = new $CLASS_NAME;
}

if ($view) {
    
    $id = $view;
    if ($id) {
        $TableObj->ViewRecord($id);
    }
}

if ($delete) {
    
    $id = $delete;
    if ($id) {
        $id = db_QuoteValue($id);
        if (db_DeleteRecord($TABLE,"id=$id")) {
            echo 'ok';
        }
    }
}

if (Get('edit')) {
    
    $id = Get('edit');
    if ($id) {
        $TableObj->EditRecord($id);
    }
}


if ($table_search) {
    // need to return a table based upon the search result

    $operators = array('All','=','Not =','<','>','<=','>=','includes');

    $TABLE = $DATA['TABLE_NAME'];
    $CLASS_NAME = str_replace(' ','',NameToTitle($TABLE));
    include '../classes/class.' . $CLASS_NAME . '.php';
    
    $TableObj = new $CLASS_NAME;
    
    $WHERE = '';

    //search operator    : TABLE_SEARCH_OPERATOR_$field
    //search value input : TABLE_SEARCH_VALUE_$field
    //display checkbox   : TABLE_DISPLAY_$field


    //-------- display fields --------
    $DisplayFields = '';
    foreach ($DATA as $key=>$value) {
        $display = strFrom($key, 'TABLE_DISPLAY_');
        if ($display and $value) {
            $display = TransformContent($display,'TSQ');
            $DisplayFields .= "`$display`,";
        }
    }
    $DisplayFields = substr($DisplayFields,0,-1);


    //-------- create Where clause --------

    foreach ($DATA as $key=>$operator) {
        $field = strFrom($key, 'TABLE_SEARCH_OPERATOR_');
        if ($field and ($operator <> 'All')) {
            $filter = $DATA['TABLE_SEARCH_VALUE_' . $field];
            $field = TransformContent($field,'TSQ');
            $field = "`$field`";

            if ($operator == 'Not =') {
                $operator= '!=';
            }

            if ($WHERE) {
                $WHERE .= ' AND ';
            }

            if ($operator == 'includes') {
                $filter = db_QuoteValue("%$filter%");
                $WHERE .= " $field LIKE $filter";
            } else {
                $filter = db_QuoteValue($filter);
                $WHERE .= " $field $operator $filter";
            }
        }
    }


    $order = $DATA['TABLE_ORDER'];
    
    $start_row = max(TransformContent($DATA['TABLE_STARTROW'],'QST'),1);
    $row_count = max(TransformContent($DATA['TABLE_ROWS'],'QST'),1);
    
    $num_rows  = db_Count($TABLE, $WHERE);  // must recalculate rows because number may change based upon new query
    
    if ($start_row > $num_rows) $start_row = 1;
    
    if ($action == 'HOME')             $start_row = 1;
    elseif ($action == 'PREVIOUSPAGE') $start_row = max($start_row - $row_count,1);
    elseif ($action == 'NEXTPAGE')     $start_row = min($start_row + $row_count, $num_rows - $row_count);
    elseif ($action == 'END')          $start_row = max($num_rows - $row_count+1,1);
    
    $TableArray = db_GetArray($TABLE, $DisplayFields. ',id AS TID', $WHERE, $order, $start_row-1, $row_count, $num_rows);
    if ($TableArray) {
        $OutputTable = $TableObj->OutputTable($TableArray, $num_rows, $start_row, $row_count);
        echo TextBetween('<tbody>','</tbody>',$OutputTable);
    } else {
        echo '<tr><td><h3 class="center">No Items Found!</h3></td></tr>';
    }
    
    //echo '<tr><td>'.$GLOBALS['DbLastQuery'].'</td></tr>';//<<<<<<<<<<---------- REMOVE ----------<<<<<<<<<<
    

}

?>
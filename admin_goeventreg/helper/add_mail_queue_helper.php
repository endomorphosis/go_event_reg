<?php
$path = dirname(dirname(__FILE__));

include_once $path.'/includes/i_global.php';
include_once $path.'/lib/tools.php';
include_once $path.'/lib/db_helper.php';

SetGet('AJAX table where templateId sendtime');

$sendtime = db_QuoteValue($sendtime);

/* -- varlist in mail_queue --
name
email
uid
varlist
template_id
send_date_time
updated
created
*/
$ERROR = '';
$count = 0;
if (!$table)      $ERROR .= "Table Not Provided\n";
if (!$where)      $ERROR .= "Conditions Not Provided\n";
if (!$templateId) $ERROR .= "Template Not Provided\n";
if (!$sendtime)   $ERROR .= "Time Not Provided\n";

if (empty($ERROR)) {
    if (!db_TableExists($table)) {
        $ERROR .= "Table ($table) Not Found\n";
    } else {
        $QUERY = "SELECT * FROM `$table` WHERE $where";
        $db_query = mysql_query($QUERY);
        SetDbQuery('Custom',$QUERY);
        if ($db_query) {
            while ($row = mysql_fetch_assoc($db_query)) {
                $fullname = trim($row['first_name'].' '.$row['last_name']);
                $name = $row['name'];
                $name = ($fullname)? $fullname : $name;
                $name = db_QuoteValue($name);
                $email = db_QuoteValue($row['email']);
                $varlist = '';
                foreach ($row as $key=>$value) {
                    $value = str_replace("\t",' ',$value);
                    $varlist .= "<KK>$key</KK><VV>$value</VV>\n";
                }
                $varlist = db_QuoteValue($varlist);
                $keys = 'name,email,uid,varlist,template_id,send_date_time,created';
                $values = "$name,$email,'',$varlist,$templateId,$sendtime,NOW()";
                if (db_AddRecord('mail_queue',$keys,$values)) $count++;
            }
        } else $ERROR .= "Mail Queue Read Table Error\n";
    }
}
if($AJAX) echo "$ERROR|$count";
?>